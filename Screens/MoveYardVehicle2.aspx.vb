Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess
Imports Made4Net.Shared

Partial Public Class MoveYardVehicle2
    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub doBack()
        Session.Remove("YardMovementEquipment")
        Response.Redirect(MapVirtualPath("Screens/MoveYardVehicle.aspx"))
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddSpacer()
        DO1.AddLabelLine("VehicleId")
        DO1.AddLabelLine("Trailer")
        DO1.AddLabelLine("YardLocation")
        DO1.AddSpacer()
        DO1.AddTextboxLine("DestYardLocation", "Move To")
        DO1.FocusField = "DestYardLocation"
        DO1.DefaultButton = "Next"
        Dim oYardEquipment As WMS.Logic.YardEquipment = Session("YardMovementEquipment")
        DO1.Value("VehicleId") = oYardEquipment.VEHICLE
        DO1.Value("Trailer") = oYardEquipment.TRAILER
        DO1.Value("YardLocation") = oYardEquipment.YARDLOCATION
    End Sub

    Private Sub doNext(ByVal pShouldDetachTrailer As Boolean)
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim sUser As String = WMS.Logic.Common.GetCurrentUser
            If Not WMS.Logic.YardLocation.Exists(DO1.Value("DestYardLocation")) Then
                Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("Yard location does not exist"))
                DO1.Value("DestYardLocation") = ""
                Return
            End If
            Dim oYardEquipment As WMS.Logic.YardEquipment = Session("YardMovementEquipment")
            If pShouldDetachTrailer AndAlso oYardEquipment.TRAILER <> "" Then
                oYardEquipment.DetachtrailerFromVehicle(sUser)
            End If
            oYardEquipment.MoveToLocation(DO1.Value("DestYardLocation"), False, sUser)

            'Used to create tasks for manual moves as well - no need to do that....
            'Dim oYardMovement As New WMS.Logic.YardMovement()
            'oYardMovement.Create("", oYardEquipment.EQUIPMENTID, DO1.Value("DestYardLocation"), WMS.Lib.TASKTYPE.YARDVEHICLEMOVE, sUser)
            'oYardMovement.Move(sUser, oYardEquipment)
        Catch m4nEx As Made4Net.Shared.M4NException
            MessageQue.Enqueue(m4nEx.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            DO1.Value("DestYardLocation") = ""
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            DO1.Value("DestYardLocation") = ""
            Return
        End Try
        doBack()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "move"
                doNext(False)
            Case "move vehicle only"
                doNext(True)
            Case "back"
                doBack()
        End Select
    End Sub

End Class
