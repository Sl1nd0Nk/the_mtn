Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports WMS.MobileWebApp.apexInbound
Imports WMS.MobileWebApp.apexInventory
Imports WMS.MobileWebApp.apexSerial
Imports System.Collections.Generic

<CLSCompliant(False)> Public Class ReturnsPutAway
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject
    Protected WithEvents Timeout As Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If WMS.Logic.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If

        If Not IsPostBack Then
            If Not UsingReturnPutawayMH(Session("MHTYPE")) Then
                Session.Add("Elogout", True)
            End If

            If Not APXINV_CheckIfTableExist("apx_returns_putaway") Then
                APXINV_CreateTable("apx_returns_putaway", New List(Of String)() From {
                                                        "Mode NVARCHAR (20) NULL",
                                                        "UserID NVARCHAR (50) NULL",
                                                        "FindingWork BIT NULL"})
            End If

            If Not APXINV_CheckIfTableExist("apx_returns_putaway_detail") Then
                APXINV_CreateTable("apx_returns_putaway_detail", New List(Of String)() From {
                                                        "UserID NVARCHAR (50) NULL",
                                                        "FromLoad NVARCHAR (50) NULL",
                                                        "FromLocation NVARCHAR (50) NULL",
                                                        "ToLoad NVARCHAR (50) NULL",
                                                        "ToLocation NVARCHAR (50) NULL",
                                                        "Sku NVARCHAR (50) NULL",
                                                        "SkuDesc NVARCHAR (MAX) NULL",
                                                        "Quantity INT NULL",
                                                        "QuantityConfirmed INT NULL",
                                                        "PickUpConfirmed BIT NULL"})
            End If

            If Not APXINV_CheckIfTableExist("apx_returns_putaway_problem") Then
                APXINV_CreateTable("apx_returns_putaway_problem", New List(Of String)() From {
                                                        "UserID NVARCHAR (50) NULL",
                                                        "LoadID NVARCHAR (50) NULL",
                                                        "Reason NVARCHAR (MAX) NULL",
                                                        "AddDate DATETIME NULL"})
            End If
        End If

        If Not Session("Elogout") Is Nothing Then
            InitControls()
            InitButtons()
            DO1.Button(0).Visible = False
            DO1.Button(2).Text = "Logout"
        Else
            WhereToNext()
        End If
    End Sub

    Private Sub InitControls()
        DO1.setVisibility("MODE", False)
        DO1.setVisibility("LOADID", False)
        DO1.setVisibility("SKU", False)
        DO1.setVisibility("SKUDESC", False)
        DO1.setVisibility("LOCATION", False)
        DO1.setVisibility("Msg", False)
        DO1.setVisibility("FROMLOC", False)
        DO1.setVisibility("FROMLOAD", False)
        DO1.setVisibility("FROMSKU", False)
        DO1.setVisibility("FROMCONFIRMQTY", False)
        DO1.setVisibility("ReplenList", False)
        DO1.setVisibility("TOLOC", False)
        DO1.setVisibility("UNITS", False)
        DO1.setVisibility("UNITSCONFIRMED", False)
        DO1.setVisibility("CONFIRMQTY", False)
        DO1.setVisibility("FROMSER", False)
        DO1.setVisibility("PROBREASON", False)
    End Sub

    Private Sub InitButtons()
        DO1.Button(0).Visible = True
        DO1.Button(0).Text = "Next"
        DO1.Button(1).Visible = False
        DO1.Button(2).Visible = True
        DO1.Button(3).Visible = False
    End Sub

    Private Sub WhereToNext()
        InitControls()
        InitButtons()
        Timeout.Visible = False

        If Not Session("LoadProblem") Is Nothing Then
            DO1.setVisibility("LOADID", True)
            DO1.setVisibility("SKU", True)
            DO1.setVisibility("SKUDESC", True)
            DO1.setVisibility("LOCATION", True)

            Dim NonConfirmedDT As DataTable = CheckIfLoadsOnTrolleyConfimed(WMS.Logic.Common.GetCurrentUser)

            If NonConfirmedDT.Rows.Count > 0 Then
                DO1.Value("LOADID") = NonConfirmedDT.Rows(0)("FromLoad")
                DO1.Value("SKU") = NonConfirmedDT.Rows(0)("Sku")
                DO1.Value("SKUDESC") = NonConfirmedDT.Rows(0)("SkuDesc")
                DO1.Value("LOCATION") = NonConfirmedDT.Rows(0)("FromLocation")
            End If

            DO1.Button(0).Text = "Back"
            DO1.setVisibility("PROBREASON", True)
            DO1.Button(1).Visible = True
            DO1.Button(1).Text = "Problem"
        Else
            Dim Collecting As Boolean = CheckModeFromPutAawayTrolley(WMS.Logic.Common.GetCurrentUser)

            If Collecting Then
                Dim CanCarryMore As Boolean = CheckCanCarryMoreLoad(WMS.Logic.Common.GetCurrentUser)

                If CanCarryMore Then
                    Dim SomethingToDeposit As Boolean = CheckIfTheresSomethingToDeposit(WMS.Logic.Common.GetCurrentUser)

                    If SomethingToDeposit Then
                        DO1.Button(3).Visible = True
                    End If
                    PerformCollectionTasks(SomethingToDeposit)
                Else
                    'Give msg that the trolley is full and needs to be deposited
                    DO1.setVisibility("Msg", True)
                    DO1.Value("Msg") = "Maximum Number Of Loads Reached. Please Deposit"
                    DO1.Button(3).Visible = True
                End If
            Else
                Dim SomethingToDeposit As Boolean = CheckIfTheresSomethingToDeposit(WMS.Logic.Common.GetCurrentUser)
                PerformPutAwayTasks()
            End If
        End If
    End Sub

    Private Function CheckIfTheresSomethingToDeposit(ByVal UserID As String) As Boolean
        Dim DT As DataTable = CheckIfAnythingToDeposit(UserID)

        If DT.Rows.Count > 0 Then
            DO1.setVisibility("ReplenList", True)
            DO1.Value("ReplenList") = BuildDisplayItems(DT)
            Return True
        End If

        Return False
    End Function

    Private Function BuildDisplayItems(ByVal DtLines As DataTable) As String
        Dim StartTable As String = "<table border='1'><tr><th nowrap>FROM LOCATION</th><th nowrap>LOADID</th><th nowrap>QUANTITY</th><th nowrap>SKU</th></tr>"
        Dim EndTable As String = "</table>"
        Dim StartRow As String = "<tr bgcolor='#DCDCDC'>"
        Dim EndRow As String = "</tr>"

        Dim x As Integer = 1
        If DtLines.Rows.Count > 0 Then
            For Each dr As DataRow In DtLines.Rows
                StartTable = StartTable & StartRow & "<td nowrap>" & dr("FromLocation") & "</td>" & "<td nowrap>" & dr("FromLoad") & "</td>" & "<td nowrap>" & dr("Quantity") & "</td>" & "<td nowrap>" & dr("Sku") & "</td>" & EndRow
            Next
        End If

        Return StartTable & EndTable
    End Function

    Private Function UsingReturnPutawayMH(ByVal MhType As String) As Boolean
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim Value As String = ""
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'ReturnPutawayMhType'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        If Value IsNot Nothing Then
            If MhType <> Value Then
                MessageQue.Enqueue(t.Translate("Please login with MHType " & Value & " to perform return putaway"))
                ''Made4Net.Mobile.Common.GoToMenu()
                Return False
            End If
        Else
            MessageQue.Enqueue(t.Translate("Return MHType not setup is parameters"))
            ''Made4Net.Mobile.Common.GoToMenu()
            Return False
        End If

        Return True
    End Function

    Private Function CheckCanCarryMoreLoad(ByVal UserID As String) As Boolean
        Dim Value As String = ""
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'ReturnMaxNumLoadsInSingleCarry'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        Dim MaxNumLoads As Integer = 5
        If Value IsNot Nothing And IsNumeric(Value) Then
            MaxNumLoads = CInt(Value)
        End If

        Dim DT As DataTable = CheckIfAnythingToDeposit(UserID)
        If DT.Rows.Count < MaxNumLoads Then
            Return True
        End If

        Return False
    End Function

    Private Function CheckModeFromPutAawayTrolley(ByVal UserID As String) As Boolean
        Dim Collecting As Boolean = True

        Dim Sql As String = "select * from apx_returns_putaway where UserID = '" + UserID + "'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)

        If sDtLines.Rows.Count > 0 Then
            If Not IsDBNull(sDtLines.Rows(0)("Mode")) Then
                Dim Mode As String = CStr(sDtLines.Rows(0)("Mode"))
                If Mode.ToUpper() <> "COLLECTING" Then
                    Dim TrolleyRows As DataTable = CheckIfAnythingToDeposit(UserID)
                    If TrolleyRows.Rows.Count > 0 Then
                        Collecting = False
                    Else
                        UpdateModeForUserTrolley("COLLECTING", UserID)
                    End If
                End If
            Else
                UpdateModeForUserTrolley("COLLECTING", UserID)
            End If
        Else
            CreateTrolleyForUser(UserID)
        End If

        Return Collecting
    End Function

    Private Sub CreateTrolleyForUser(ByVal UserID As String)
        Dim SQL As String = String.Format("Insert Into apx_returns_putaway (Mode, UserID) VALUES({0},{1})", Made4Net.Shared.FormatField("COLLECTING"), Made4Net.Shared.FormatField(UserID))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Sub UpdateModeForUserTrolley(ByVal Mode As String, UserID As String)
        Dim SQL As String = String.Format("Update apx_returns_putaway set Mode={0} where UserID={1}", Made4Net.Shared.FormatField(Mode), Made4Net.Shared.FormatField(UserID))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Function CheckIfAnythingToDeposit(ByVal UserID As String) As DataTable
        Dim Sql As String = "select * from apx_returns_putaway_detail where UserID = '" + UserID + "' and PickUpConfirmed = 1"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)

        Return sDtLines
    End Function

    Private Sub RemovedAnyUnconfirmedPickups(ByVal UserID As String)
        Dim SQL As String = String.Format("Delete from apx_returns_putaway_detail where UserID={0} and PickUpConfirmed = 0", Made4Net.Shared.FormatField(UserID))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Function GetReturnDestinationWarehouseAreas() As String()
        Dim Value As String = ""
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'ReturnDestinationWarehouseAreas'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        Dim whas As String()
        If Value IsNot Nothing Then
            whas = Value.Split(New Char() {","c})
        End If

        Return whas
    End Function

    Private Function GetReturnSourceWarehouseAreas() As String()
        Dim Value As String = ""
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'ReturnSourceWarehouseAreas'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        Dim whas As String()
        If Value IsNot Nothing Then
            whas = Value.Split(New Char() {","c})
        End If

        Return whas
    End Function

    Private Function BuildQueryClause(ByVal ReturnWHAreas As String()) As String
        Dim Clause As String = "('"
        Dim x As Integer = 0

        While x < ReturnWHAreas.Length
            Clause = Clause & ReturnWHAreas(x) & "'"
            If (x + 1) < ReturnWHAreas.Length Then
                Clause = Clause & ","
            Else
                Clause = Clause & ")"
            End If
            x += 1
        End While

        Return Clause
    End Function

    Private Sub LockProcessToUser(ByVal UserID As String)
        Dim SQL As String = String.Format("Update apx_returns_putaway set FindingWork={0} where UserID={1}", Made4Net.Shared.FormatField(True), Made4Net.Shared.FormatField(UserID))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)

        SQL = String.Format("Update apx_returns_putaway set FindingWork={0} where UserID<>{1}", Made4Net.Shared.FormatField(False), Made4Net.Shared.FormatField(UserID))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Sub UnLockProcessToUser(ByVal UserID As String)
        Dim SQL As String = String.Format("Update apx_returns_putaway set FindingWork={0} where UserID={1}", Made4Net.Shared.FormatField(False), Made4Net.Shared.FormatField(UserID))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Sub ConfirmLoadPickupForUser(ByVal UserID As String, ByVal LoadId As String, ByVal Sku As String)
        'Dim SQL As String = String.Format("select * from apx_returns_putaway_detail where Sku={0} and UserID={1} and FromLoad<>{2}", Made4Net.Shared.FormatField(Sku), Made4Net.Shared.FormatField(UserID), Made4Net.Shared.FormatField(LoadId))
        'Dim sDtLines As DataTable = New DataTable()
        'Made4Net.DataAccess.DataInterface.FillDataset(SQL, sDtLines, False, Nothing)

        'If sDtLines.Rows.Count > 0 Then
        '    SQL = String.Format("select * from apx_returns_putaway_detail where UserID={0} and FromLoad={1}", Made4Net.Shared.FormatField(UserID), Made4Net.Shared.FormatField(LoadId))
        '    Dim LdDTLines As DataTable = New DataTable()
        '    Made4Net.DataAccess.DataInterface.FillDataset(SQL, LdDTLines, False, Nothing)
        '    If LdDTLines.Rows.Count > 0 Then
        '        SQL = String.Format("Update apx_returns_putaway_detail set Quantity=Quantity + {0} where FromLoad={1}", Made4Net.Shared.FormatField(LdDTLines.Rows(0)("Quantity")), Made4Net.Shared.FormatField(sDtLines.Rows(0)("FromLoad")))
        '        Made4Net.DataAccess.DataInterface.RunSQL(SQL)

        '        SQL = String.Format("Update apx_serial set loadid={0} where loadid={1} and received=1 and dispatched=0 and SKU={2}", Made4Net.Shared.FormatField(sDtLines.Rows(0)("FromLoad")), Made4Net.Shared.FormatField(LdDTLines.Rows(0)("FromLoad")), Made4Net.Shared.FormatField(Sku))
        '        Made4Net.DataAccess.DataInterface.RunSQL(SQL)

        '        Dim DestLd As Load = New Load(sDtLines.Rows(0)("FromLoad"), True)
        '        Dim SourceLd As Load = New Load(LdDTLines.Rows(0)("FromLoad"), True)

        '        DestLd.Merge(SourceLd)

        '        SQL = String.Format("Delete From apx_returns_putaway_detail where FromLoad={0} and UserID={1}", Made4Net.Shared.FormatField(LdDTLines.Rows(0)("FromLoad")), Made4Net.Shared.FormatField(UserID))
        '        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
        '    End If
        'Else
        Dim SQL As String = String.Format("Update apx_returns_putaway_detail set PickUpConfirmed={0} where UserID={1} and FromLoad={2}", Made4Net.Shared.FormatField(True), Made4Net.Shared.FormatField(UserID), Made4Net.Shared.FormatField(LoadId))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
        'End If
    End Sub

    Private Function CheckLoadAvailableForPickup(ByVal LoadId As String) As Boolean
        Dim SQL As String = String.Format("select * from apx_returns_putaway_detail where FromLoad={0}", Made4Net.Shared.FormatField(LoadId))
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, sDtLines, False, Nothing)

        If sDtLines.Rows.Count > 0 Then
            Return False
        End If

        Return True
    End Function

    Private Function CheckIfCanGetWork(ByVal UserID As String) As Boolean
        Dim CanGetWork As Boolean = False

        Dim Sql As String = "select * from apx_returns_putaway where FindingWork = 1"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)

        If sDtLines.Rows.Count <= 0 Then
            CanGetWork = True
        Else
            If Not IsDBNull(sDtLines.Rows(0)("UserID")) Then
                If CStr(sDtLines.Rows(0)("UserID")) = UserID Then
                    CanGetWork = True
                End If
            End If
        End If

        If CanGetWork Then
            LockProcessToUser(UserID)
        End If

        Return CanGetWork
    End Function

    Private Function BalanceTheLoadQty(ByVal LoadId As String, ByVal Units As Double, ByVal Sku As String, ByVal Uom As String) As Double
        Dim Sql As String = "select ClassName from sku where sku = '" & Sku & "'"
        Dim SkuDt As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, SkuDt, False, Nothing)

        If SkuDt.Rows.Count > 0 Then
            If Not IsDBNull(SkuDt.Rows(0)("ClassName")) Then
                Dim Cname As String = CStr(SkuDt.Rows(0)("ClassName"))
                If Cname.ToUpper() = "SERIAL" Then
                    Sql = "select * from apx_serial where received = 1 and dispatched = 0 and loadid = '" & LoadId & "' and sku = '" & Sku & "' and UOM = '" & Uom & "'"
                    Dim SerDT As DataTable = New DataTable()
                    Made4Net.DataAccess.DataInterface.FillDataset(Sql, SerDT, False, Nothing)
                    Dim SerialCount As Double = CDbl(SerDT.Rows.Count)

                    If Units <> SerialCount Then
                        Sql = String.Format("Update loads set UNITS={0} where LOADID={1}", Made4Net.Shared.FormatField(SerialCount), Made4Net.Shared.FormatField(LoadId))
                        Made4Net.DataAccess.DataInterface.RunSQL(Sql)
                    End If

                    Return SerialCount
                End If
            End If
        End If

        Return Units
    End Function

    Private Sub AddLoadToUserTrolley(ByVal LoadID As String, ByVal Location As String, ByVal Units As Integer, ByVal UserID As String, ByVal Sku As String, ByVal SkuDesc As String, ByVal DestData As String)
        Dim DestValues As String()
        DestValues = DestData.Split(New Char() {"|"c})

        Dim SQL As String = String.Format("Insert Into apx_returns_putaway_detail (UserID, FromLoad, FromLocation, Quantity, Sku, SkuDesc, PickUpConfirmed, ToLoad, ToLocation, QuantityConfirmed) VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8},{9})", Made4Net.Shared.FormatField(UserID), Made4Net.Shared.FormatField(LoadID), Made4Net.Shared.FormatField(Location), Made4Net.Shared.FormatField(Units), Made4Net.Shared.FormatField(Sku), Made4Net.Shared.FormatField(SkuDesc), Made4Net.Shared.FormatField(False), Made4Net.Shared.FormatField(DestValues(1)), Made4Net.Shared.FormatField(DestValues(0)), Made4Net.Shared.FormatField(0))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)

        UnLockProcessToUser(UserID)

        Session.Remove("ScannedLoc")
        Session.Remove("ScannedLoadId")
        Session.Remove("ScannedSku")
        Session.Remove("ConfirmedPickupQty")
        SetupPickupLocScan(LoadID, Location)
    End Sub

    Private Function FindLoadForDestLoc(ByVal Location As String, ByVal SKU As String, ByVal FromLoadId As String) As String
        Dim Query As String = "select * from INVLOAD where STATUS = 'AVAILABLE' and LOCATION = '" & Location & "' and SKU = '" & SKU & "'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Query, sDtLines, False, Nothing)

        If sDtLines.Rows.Count > 0 Then
            Return sDtLines.Rows(0)("LOADID")
        End If

        Return FromLoadId
    End Function

    Private Function DetermineIfHasDestination(ByVal FromLoadId As String, ByVal Sku As String, ByVal ReturnDestWHAreas As String()) As String
        Dim a As Integer = 0
        Dim RetData As String = ""

        While a < ReturnDestWHAreas.Length
            Dim Query As String = "select * from vPickLoc where SKU='" & Sku & "' and WAREHOUSEAREA = '" & ReturnDestWHAreas(a) & "'"
            Dim sDtLines As DataTable = New DataTable()
            Made4Net.DataAccess.DataInterface.FillDataset(Query, sDtLines, False, Nothing)
            Dim Dr As DataRow = Nothing
            Dim BestIndex As Integer = -1

            If sDtLines.Rows.Count > 0 Then
                Dim x As Integer = 0
                While x < sDtLines.Rows.Count
                    If CDbl(sDtLines.Rows(x)("CURRENTQTY")) < CDbl(sDtLines.Rows(x)("MAXIMUMQTY")) Then
                        If BestIndex = -1 Then
                            BestIndex = x
                        End If

                        If (CDbl(sDtLines.Rows(x)("CURRENTQTY"))) < (CDbl(sDtLines.Rows(BestIndex)("CURRENTQTY"))) Then
                            BestIndex = x
                        End If
                    End If
                    x += 1
                End While

                If BestIndex = -1 Then
                    BestIndex = 0
                    x = 0
                    While x < sDtLines.Rows.Count
                        If (CDbl(sDtLines.Rows(x)("CURRENTQTY"))) < (CDbl(sDtLines.Rows(BestIndex)("CURRENTQTY"))) Then
                            BestIndex = x
                        End If
                        x += 1
                    End While
                End If

                Dr = sDtLines.Rows(BestIndex)

                Dim Location As String = sDtLines.Rows(BestIndex)("LOCATION")
                Dim LoadId As String = FindLoadForDestLoc(Location, Sku, FromLoadId)
                RetData = RetData & Location & "|" & LoadId
                Exit While
            End If

            a += 1
        End While

        Return RetData
    End Function

    Private Sub GetWork(ByVal SourceQueryClause As String, ByVal ReturnDestWHAreas As String(), ByVal UserID As String, ByVal SomethingToDeposit As Boolean)
        Dim WhereClause As String = " WHERE ld.STATUS = 'AVAILABLE' AND ld.LOADID NOT IN (select FromLoad from apx_returns_putaway_detail) AND ld.UNITS > 0 AND ld.LOADID NOT IN (select LoadID from apx_returns_putaway_problem) AND loc.WAREHOUSEAREA in " & SourceQueryClause
        Dim Query As String = "select Top(10) ld.LOADID, ld.HARVEST, ld.SKU, ld.LOADUOM, ld.LOCATION, ld.STATUS, ld.CONSIGNEE, ld.UNITS, ld.SKUDESC from vLOADATTRIBUTESDESK ld join location loc on ld.location = loc.location and loc.Bay <> '' and loc.Bay is not null join vPickLoc pic on ld.SKU = pic.SKU " & WhereClause
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Query, sDtLines, False, Nothing)
        Dim Qty As Double = 0
        Dim DestData As String = ""

        If sDtLines.Rows.Count <= 0 Then
            UnLockProcessToUser(UserID)
            DO1.setVisibility("Msg", True)
            If SomethingToDeposit Then
                DO1.Value("Msg") = "No More Return Locations With Available Stock. Deposit Collected Stock"
            Else
                'No Work
                Timeout.Visible = True
                Timeout.Text = 10000
                DO1.Value("Msg") = "No Work"
            End If
        Else
            Dim x As Integer = 0
            Dim index As Integer = -1
            While x < sDtLines.Rows.Count
                Qty = CDbl(sDtLines.Rows(x)("UNITS")) ''BalanceTheLoadQty(CStr(sDtLines.Rows(x)("LOADID")), CDbl(sDtLines.Rows(x)("UNITS")), CStr(sDtLines.Rows(x)("SKU")), CStr(sDtLines.Rows(x)("LOADUOM")))

                If Qty > 0 Then
                    DestData = DetermineIfHasDestination(CStr(sDtLines.Rows(x)("LOADID")), CStr(sDtLines.Rows(x)("SKU")), ReturnDestWHAreas)
                    If Not String.IsNullOrEmpty(DestData) Then
                        Dim LoadAvailableForPickup As Boolean = CheckLoadAvailableForPickup(CStr(sDtLines.Rows(x)("LOADID")))
                        If LoadAvailableForPickup Then
                            index = x
                            Exit While
                        End If
                    End If
                End If
                x += 1
            End While

            If index >= 0 And Not String.IsNullOrEmpty(DestData) Then
                AddLoadToUserTrolley(CStr(sDtLines.Rows(x)("LOADID")), CStr(sDtLines.Rows(x)("LOCATION")), Qty, UserID, CStr(sDtLines.Rows(x)("SKU")), CStr(sDtLines.Rows(x)("SKUDESC")), DestData)
            Else
                UnLockProcessToUser(UserID)
                DO1.setVisibility("Msg", True)
                If SomethingToDeposit Then
                    DO1.Value("Msg") = "No More Return Locations With Valid Stock. Deposit Collected Stock"
                Else
                    'No Work
                    Timeout.Visible = True
                    Timeout.Text = 10000
                    DO1.Value("Msg") = "No Work"
                End If
            End If
        End If
    End Sub

    Private Function CheckIfLoadsOnTrolleyConfimed(ByVal UserID As String) As DataTable
        Dim Sql As String = "select * from apx_returns_putaway_detail where UserID = '" + UserID + "' and PickUpConfirmed = 0"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)

        Return sDtLines
    End Function

    Private Sub SetupDropOffLocationScan(ByVal LoadID As String, ByVal Sku As String, ByVal SkuDesc As String, ByVal Location As String)
        DO1.setVisibility("Msg", True)
        DO1.Value("Msg") = "Proceed To Location " & Location

        DO1.setVisibility("LOADID", True)
        DO1.Value("LOADID") = LoadID

        DO1.setVisibility("SKU", True)
        DO1.Value("SKU") = Sku

        DO1.setVisibility("SKUDESC", True)
        DO1.Value("SKUDESC") = SkuDesc

        DO1.setVisibility("TOLOC", True)
        DO1.FocusField = "TOLOC"

        Session.Add("CompareValue", LoadID)
    End Sub

    Private Sub SetupPickupLocScan(ByVal LoadID As String, ByVal Location As String)
        DO1.setVisibility("Msg", True)
        DO1.Value("Msg") = "Proceed To Location " & Location & "."

        DO1.setVisibility("LOADID", True)
        DO1.Value("LOADID") = LoadID

        DO1.setVisibility("FROMLOC", True)
        DO1.FocusField = "FROMLOC"

        Session.Add("CompareValue", Location)
    End Sub

    Private Sub SetupPickupLocLoadIdScan(ByVal LoadID As String, ByVal Location As String, ByVal Sku As String, ByVal SkuDesc As String)
        DO1.setVisibility("Msg", True)
        DO1.Value("Msg") = "Scan Load " & LoadID & " In Location " & Location & "."

        DO1.setVisibility("LOADID", True)
        DO1.Value("LOADID") = LoadID

        DO1.setVisibility("LOCATION", True)
        DO1.Value("LOCATION") = Location

        DO1.setVisibility("FROMLOAD", True)
        DO1.FocusField = "FROMLOAD"

        Session.Add("CompareValue", LoadID)
    End Sub

    Private Sub SetupPickupLocSkuScan(ByVal LoadID As String, ByVal Location As String, ByVal Sku As String, ByVal SkuDesc As String)
        DO1.setVisibility("Msg", True)
        DO1.Value("Msg") = "Scan Sku On Load " & LoadID & " In Location " & Location & "."

        DO1.setVisibility("LOADID", True)
        DO1.Value("LOADID") = LoadID

        DO1.setVisibility("LOCATION", True)
        DO1.Value("LOCATION") = Location

        DO1.setVisibility("SKU", True)
        DO1.Value("SKU") = Sku

        DO1.setVisibility("SKUDESC", True)
        DO1.Value("SKUDESC") = SkuDesc

        DO1.setVisibility("FROMSKU", True)
        DO1.FocusField = "FROMSKU"

        Session.Add("CompareValue", Sku)
    End Sub

    Private Sub SetupPickupLocQtyConfirm(ByVal LoadID As String, ByVal Location As String, ByVal Sku As String, ByVal SkuDesc As String, ByVal Qty As Integer)
        DO1.setVisibility("Msg", True)
        DO1.Value("Msg") = "Confirm Load Qty Of " & Qty

        DO1.setVisibility("LOADID", True)
        DO1.Value("LOADID") = LoadID

        DO1.setVisibility("LOCATION", True)
        DO1.Value("LOCATION") = Location

        DO1.setVisibility("SKU", True)
        DO1.Value("SKU") = Sku

        DO1.setVisibility("SKUDESC", True)
        DO1.Value("SKUDESC") = SkuDesc

        DO1.setVisibility("FROMCONFIRMQTY", True)
        DO1.FocusField = "FROMCONFIRMQTY"

        Session.Add("CompareValue", Qty)
    End Sub

    Private Sub PerformCollectionTasks(ByVal SomethingToDeposit As Boolean)
        DO1.setVisibility("MODE", True)
        DO1.Value("MODE") = "<b>COLLECTING</b>"

        Dim NonConfirmedDT As DataTable = CheckIfLoadsOnTrolleyConfimed(WMS.Logic.Common.GetCurrentUser)

        If NonConfirmedDT.Rows.Count > 0 Then
            If Session("ScannedLoc") Is Nothing Then
                SetupPickupLocScan(CStr(NonConfirmedDT.Rows(0)("FromLoad")), CStr(NonConfirmedDT.Rows(0)("FromLocation")))
            ElseIf Session("ScannedLoadId") Is Nothing Then
                DO1.Button(1).Visible = True
                DO1.Button(1).Text = "Problem"
                SetupPickupLocLoadIdScan(CStr(NonConfirmedDT.Rows(0)("FromLoad")), CStr(NonConfirmedDT.Rows(0)("FromLocation")), CStr(NonConfirmedDT.Rows(0)("Sku")), CStr(NonConfirmedDT.Rows(0)("SkuDesc")))
            ElseIf Session("ScannedSku") Is Nothing Then
                DO1.Button(1).Visible = True
                DO1.Button(1).Text = "Problem"
                SetupPickupLocSkuScan(CStr(NonConfirmedDT.Rows(0)("FromLoad")), CStr(NonConfirmedDT.Rows(0)("FromLocation")), CStr(NonConfirmedDT.Rows(0)("Sku")), CStr(NonConfirmedDT.Rows(0)("SkuDesc")))
            ElseIf Session("ConfirmedPickupQty") Is Nothing Then
                DO1.Button(1).Visible = True
                DO1.Button(1).Text = "Problem"
                SetupPickupLocQtyConfirm(CStr(NonConfirmedDT.Rows(0)("FromLoad")), CStr(NonConfirmedDT.Rows(0)("FromLocation")), CStr(NonConfirmedDT.Rows(0)("Sku")), CStr(NonConfirmedDT.Rows(0)("SkuDesc")), NonConfirmedDT.Rows(0)("Quantity"))
            End If
        Else
            Dim ReturnSourceWHAreas As String() = GetReturnSourceWarehouseAreas()
            Dim ReturnDestWHAreas As String() = GetReturnDestinationWarehouseAreas()

            If ReturnSourceWHAreas.Length <= 0 Or ReturnDestWHAreas.Length <= 0 Then
                'Give Message that the warehouse area for returns has not been defined
                DO1.setVisibility("Msg", True)
                DO1.Value("Msg") = "Returns Warehouse Areas Not Configured In System Parameters"
            Else
                Dim CanGetWork As Boolean = CheckIfCanGetWork(WMS.Logic.Common.GetCurrentUser)

                If Not CanGetWork Then
                    DO1.setVisibility("Msg", True)
                    'indicate to the user that there is no work for the moment
                    Timeout.Visible = True
                    Timeout.Text = 10000
                    DO1.Value("Msg") = "No Work"
                Else
                    Dim QueryClause As String = BuildQueryClause(ReturnSourceWHAreas)
                    Dim DestQueryClause As String = BuildQueryClause(ReturnDestWHAreas)
                    GetWork(QueryClause, ReturnDestWHAreas, WMS.Logic.Common.GetCurrentUser, SomethingToDeposit)
                End If
            End If
        End If
    End Sub

    Private Function GetToLocHandOff(ByVal OutHandOff As String) As String
        Dim Sql As String = "select * from HANDOFF where FROMHANDOFFREGION = '" + OutHandOff + "'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)
        Dim ToLoc As String = ""

        If sDtLines.Rows.Count > 0 Then
            ToLoc = sDtLines.Rows(0)("HANDOFFLOCATION")
        End If

        Return ToLoc
    End Function

    Private Sub CreateReplenTaskfromHandover(ByVal Dr As DataRow)
        Dim Task As String = ""
        Dim ReplTaskDetail As WMS.Logic.Replenishment
        Dim tm As New WMS.Logic.TaskManager
        Dim ReplenExist As Boolean = True

        Try
            ReplTaskDetail = Replenishment.getReplenishment(Dr("FromLoad"))
        Catch
            ReplenExist = False
        End Try

        If Not ReplenExist Then
            Dim FrmLd As Load = New Load(CStr(Dr("FromLoad")), True)

            Dim FrmLoc As Location = New Location(CStr(Dr("FromLocation")))
            Dim OutHandOff As String = FrmLoc.OUTHANDOFF
            Dim HandOffToLoc As String = GetToLocHandOff(OutHandOff)

            If HandOffToLoc <> "" Then
                ReplTaskDetail = New Replenishment()

                ReplTaskDetail.Units = CDbl(Dr("Quantity"))
                ReplTaskDetail.FromLoad = CStr(Dr("FromLoad"))
                ReplTaskDetail.FromLocation = HandOffToLoc
                ReplTaskDetail.ToLoad = CStr(Dr("ToLoad"))
                ReplTaskDetail.ToLocation = CStr(Dr("ToLocation"))
                ReplTaskDetail.ReplType = Replenishment.ReplenishmentTypes.PartialReplenishment
                ReplTaskDetail.ReplMethod = Replenishment.ReplenishmentMethods.ManualReplenishment
                ReplTaskDetail.UOM = FrmLd.LOADUOM

                ReplTaskDetail.Post(WMS.Logic.Common.GetCurrentUser)

                ReplTaskDetail = Replenishment.getReplenishment(Dr("FromLoad"))
                tm.CreateReplenishmentTask(ReplTaskDetail, FrmLd, 200)
            End If
        Else
            If ReplTaskDetail.hasTask = "0" Or ReplTaskDetail.hasTask = "False" Then
                Dim FrmLd As Load = New Load(CStr(Dr("FromLoad")), True)
                tm.CreateReplenishmentTask(ReplTaskDetail, FrmLd, 200)
            End If
        End If

    End Sub

    Private Sub CheckForAReplenTask(ByVal Dr As DataRow)
        Dim Task As String = ""
        Dim ReplTaskDetail As WMS.Logic.Replenishment
        Dim tm As New WMS.Logic.TaskManager
        Dim ReplenExist As Boolean = True

        Try
            ReplTaskDetail = Replenishment.getReplenishment(Dr("FromLoad"))
        Catch
            ReplenExist = False
        End Try

        If Not ReplenExist Then
            Dim FrmLd As Load = New Load(CStr(Dr("FromLoad")), True)

            Dim FrmLoc As Location = New Location(CStr(Dr("FromLocation")))
            Dim OutHandOff As String = FrmLoc.OUTHANDOFF
            Dim HandOffToLoc As String = GetToLocHandOff(OutHandOff)

            If HandOffToLoc <> "" Then
                ReplTaskDetail = New Replenishment()

                ReplTaskDetail.Units = CDbl(Dr("Quantity"))
                ReplTaskDetail.FromLoad = CStr(Dr("FromLoad"))
                ReplTaskDetail.FromLocation = CStr(Dr("FromLocation"))
                ReplTaskDetail.ToLoad = CStr(Dr("FromLoad"))
                ReplTaskDetail.ToLocation = HandOffToLoc
                ReplTaskDetail.ReplType = Replenishment.ReplenishmentTypes.PartialReplenishment
                ReplTaskDetail.ReplMethod = Replenishment.ReplenishmentMethods.ManualReplenishment
                ReplTaskDetail.UOM = FrmLd.LOADUOM

                ReplTaskDetail.Post(WMS.Logic.Common.GetCurrentUser)

                ReplTaskDetail = Replenishment.getReplenishment(Dr("FromLoad"))
                tm.CreateReplenishmentTask(ReplTaskDetail, FrmLd, 200)
                tm.AssignUser(WMS.Logic.Common.GetCurrentUser, "MANUAL", "")
            End If
        Else
            If ReplTaskDetail.hasTask = "0" Or ReplTaskDetail.hasTask = "False" Then
                Dim FrmLd As Load = New Load(CStr(Dr("FromLoad")), True)
                tm.CreateReplenishmentTask(ReplTaskDetail, FrmLd, 200)
                tm.AssignUser(WMS.Logic.Common.GetCurrentUser, "MANUAL", "")
            End If
        End If
    End Sub

    Private Sub SetupSerialScan(ByVal Qty As Integer, ByVal Location As String, ByVal LoadID As String, ByVal sku As String, ByVal SkuDesc As String)
        DO1.setVisibility("LOADID", True)
        DO1.Value("LOADID") = LoadID

        DO1.setVisibility("LOCATION", True)
        DO1.Value("LOCATION") = Location

        DO1.setVisibility("SKU", True)
        DO1.Value("SKU") = sku

        DO1.setVisibility("SKUDESC", True)
        DO1.Value("SKUDESC") = SkuDesc

        DO1.setVisibility("Msg", True)
        DO1.Value("Msg") = "Scan a serial for product <br />" & SkuDesc & "."

        DO1.setVisibility("FROMSER", True)
        Session.Add("CanScanSerial", True)
    End Sub

    Private Sub SetupQtyConfirm(ByVal Qty As Integer, ByVal Location As String, ByVal LoadID As String, ByVal sku As String, ByVal SkuDesc As String)
        DO1.setVisibility("Msg", True)
        DO1.Value("Msg") = "Confirm qty of " & Qty & " of product <br />" & SkuDesc & "<br /> Into Location " & Location & "."

        DO1.setVisibility("LOADID", True)
        DO1.Value("LOADID") = LoadID

        DO1.setVisibility("LOCATION", True)
        DO1.Value("LOCATION") = Location

        DO1.setVisibility("SKU", True)
        DO1.Value("SKU") = sku

        DO1.setVisibility("SKUDESC", True)
        DO1.Value("SKUDESC") = SkuDesc

        DO1.setVisibility("CONFIRMQTY", True)
        DO1.FocusField = "CONFIRMQTY"

        Session.Add("CompareCQtyValue", Qty)
    End Sub

    Private Sub sendReplAuditMsg(ByVal Units As Double, ByVal Status As String, ByVal pFromStatus As String, ByVal pToLocation As String, ByVal pToLoad As String, ByVal pFromLocation As String, ByVal replId As String, ByVal FromLoad As String)
        Dim dataTable As DataTable = New DataTable()
        DataInterface.FillDataset(String.Format("Select consignee,sku from loads where loadid = '{0}'", FromLoad), dataTable, False, Nothing)
        Dim value As String = CStr(dataTable.Rows(0)("SKU"))
        Dim value2 As String = CStr(dataTable.Rows(0)("CONSIGNEE"))
        Dim eventManagerQ As EventManagerQ = New EventManagerQ()
        Dim num As Integer = 76
        eventManagerQ.Add("ACTIVITYTIME", "0")
        eventManagerQ.Add("ACTIVITYTYPE", "REPLENISHMENT")
        eventManagerQ.Add("DOCUMENT", replId)
        eventManagerQ.Add("FROMLOAD", FromLoad)
        eventManagerQ.Add("CONSIGNEE", value2)
        eventManagerQ.Add("SKU", value)
        eventManagerQ.Add("FROMLOC", pFromLocation)
        eventManagerQ.Add("FROMQTY", CStr(Units))
        eventManagerQ.Add("FROMSTATUS", pFromStatus)
        eventManagerQ.Add("NOTES", "")
        eventManagerQ.Add("TOLOAD", pToLoad)
        eventManagerQ.Add("TOLOC", pToLocation)
        eventManagerQ.Add("TOQTY", CStr(Units))
        eventManagerQ.Add("TOSTATUS", Status)
        eventManagerQ.Add("USERID", WMS.Logic.Common.GetCurrentUser)
        eventManagerQ.Add("ADDDATE", Made4Net.[Shared].Util.DateTimeToWMSString(DateTime.Now))
        eventManagerQ.Add("LASTMOVEUSER", WMS.Logic.Common.GetCurrentUser)
        eventManagerQ.Add("LASTSTATUSUSER", WMS.Logic.Common.GetCurrentUser)
        eventManagerQ.Add("LASTCOUNTUSER", WMS.Logic.Common.GetCurrentUser)
        eventManagerQ.Add("LASTSTATUSDATE", Made4Net.[Shared].Util.DateTimeToWMSString(DateTime.Now))
        eventManagerQ.Add("ADDUSER", WMS.Logic.Common.GetCurrentUser)
        eventManagerQ.Add("EDITDATE", Made4Net.[Shared].Util.DateTimeToWMSString(DateTime.Now))
        eventManagerQ.Add("EDITUSER", WMS.Logic.Common.GetCurrentUser)
        eventManagerQ.Add("EVENT", CStr(num))
        eventManagerQ.Add("REPLID", replId)
        eventManagerQ.Add("ACTIVITYDATE", Made4Net.[Shared].Util.DateTimeToWMSString(DateTime.Now))
        eventManagerQ.Send(WMSEvents.EventDescription(num))
    End Sub

    Private Sub SetupReplenConfirm(ByVal Dr As DataRow)
        Dim Qty As Integer = CInt(Dr("Quantity"))
        Dim ConfirmedQty As Integer = CInt(Dr("QuantityConfirmed"))

        If ConfirmedQty <> Qty Then
            DO1.setVisibility("UNITS", True)
            DO1.Value("UNITS") = Qty

            Dim ld As Load = New Load(CStr(Dr("FromLoad")))
            Dim oSku As SKU = New SKU(ld.CONSIGNEE, ld.SKU)
            Dim ClassName As String = oSku.SKUClass.ClassName
            If ClassName.ToUpper() = "SERIAL" And APXINV_ScanSerialAtDeposit() Then
                DO1.setVisibility("UNITSCONFIRMED", True)
                DO1.Value("UNITSCONFIRMED") = ConfirmedQty

                Session.Add("SerialFromLoad", CStr(Dr("FromLoad")))
                Session.Add("SerialFromSku", CStr(Dr("Sku")))

                SetupSerialScan(Qty, CStr(Dr("ToLocation")), CStr(Dr("ToLoad")), CStr(Dr("Sku")), CStr(Dr("SkuDesc")))
            Else
                SetupQtyConfirm(Qty, CStr(Dr("ToLocation")), CStr(Dr("ToLoad")), CStr(Dr("Sku")), CStr(Dr("SkuDesc")))
            End If
        Else
            Session.Remove("ScannedToLoc")
            Session.Remove("SerialFromLoad")
            Session.Remove("SerialFromSku")
            Dim ReplTask As WMS.Logic.ReplenishmentTask
            Dim ReplTaskDetail As WMS.Logic.Replenishment
            ReplTask = Session("RetReplenTask")
            ReplTaskDetail = Session("ReplTaskDetail")
            Dim replJob As ReplenishmentJob = ReplTask.getReplenishmentJob(ReplTaskDetail)

            Try
                'ReplTask.Replenish(ReplTaskDetail, replJob, WMS.Logic.Common.GetCurrentUser)

                Dim FromLoad As Load = New Load(Dr("FromLoad"), True)
                If Dr("FromLoad") <> Dr("ToLoad") And WMS.Logic.Load.Exists(Dr("ToLoad")) Then
                    Dim ToLoad As Load = New Load(Dr("ToLoad"), True)
                    ToLoad.Merge(FromLoad)
                Else
                    FromLoad.Move(CStr(Dr("ToLocation")), "", WMS.Logic.Common.GetCurrentUser)
                End If

                Dim pSQL As String = String.Format("Update REPLENISHMENT set status={0},editdate={1},edituser={2},tolocation={4} where ReplId={3}", FormatField("COMPLETE"), FormatField(DateTime.Now), FormatField(WMS.Logic.Common.GetCurrentUser), FormatField(ReplTaskDetail.ReplId), FormatField(CStr(Dr("ToLocation"))))
                DataInterface.RunSQL(pSQL)
                sendReplAuditMsg(ReplTaskDetail.Units, "COMPLETE", ReplTaskDetail.Status, ReplTaskDetail.ToLocation, ReplTaskDetail.ToLoad, ReplTaskDetail.FromLocation, ReplTaskDetail.ReplId, ReplTaskDetail.FromLoad)
                ReplTask.Complete()

                Dim SQL As String = String.Format("Update apx_serial set loadid={0}, MDsyncRequired=1, JPsyncRequired=1 where loadid={1} and received=1 and dispatched=0 and SKU={2}", Made4Net.Shared.FormatField(Dr("ToLoad")), Made4Net.Shared.FormatField(Dr("FromLoad")), Made4Net.Shared.FormatField(Dr("Sku")))
                Made4Net.DataAccess.DataInterface.RunSQL(SQL)

                SQL = String.Format("Delete from apx_returns_putaway_detail where UserID={0} and ToLocation={1}", Made4Net.Shared.FormatField(WMS.Logic.Common.GetCurrentUser), Made4Net.Shared.FormatField(Dr("ToLocation")))
                Made4Net.DataAccess.DataInterface.RunSQL(SQL)

                SQL = String.Format("UPDATE ATTRIBUTE SET HARVEST=NULL WHERE PKEY1={0} and PKEYTYPE='LOAD'", Made4Net.Shared.FormatField(Dr("ToLoad")))
                Made4Net.DataAccess.DataInterface.RunSQL(SQL)
            Catch ex As Exception
                Throw ex
            End Try

            WhereToNext()
        End If
    End Sub

    Private Sub DoReplen(ByVal DT As DataTable, ByVal index As Integer)
        Dim ReplTask As WMS.Logic.ReplenishmentTask
        Dim ReplTaskDetail As WMS.Logic.Replenishment

        Try
            ReplTaskDetail = Replenishment.getReplenishment(DT.Rows(index)("FromLoad"))
        Catch
            Dim Dr As DataRow = DT.Rows(index)
            Dim SQL As String = String.Format("Delete from apx_returns_putaway_detail where UserID={0} and ToLocation={1}", Made4Net.Shared.FormatField(WMS.Logic.Common.GetCurrentUser), Made4Net.Shared.FormatField(Dr("ToLocation")))
            Made4Net.DataAccess.DataInterface.RunSQL(SQL)
            ReplTaskDetail = Nothing
        End Try

        'If Session("ScannedToLoc") Is Nothing Then
        '    Dim FrmLoc As Location = New Location(DT.Rows(index)("FromLocation"))
        '    Dim OutHandOff As String = FrmLoc.OUTHANDOFF
        '    Dim HandOffToLoc As String = GetToLocHandOff(OutHandOff)

        '    SetupDropOffLocationScan(DT.Rows(index)("ToLoad"), HandOffToLoc)
        'Else
        '    Dim oLoad As Load = New Load(DT.Rows(index)("FromLoad"), True)
        '    oLoad.PutAway(WMS.Logic.Common.GetCurrentUser)

        '    Dim pwtask As PutawayTask
        '    Dim tm As New WMS.Logic.TaskManager
        '    pwtask = tm.getAssignedTask(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PUTAWAY)
        '    ' Check if the containes Putaway jobs, if no then close task and go bak
        '    pwtask.EDITUSER = WMS.Logic.GetCurrentUser
        '    Dim pwjob As PutawayJob = pwtask.getPutawayJob(DT.Rows(index)("FromLoad"))
        '    pwtask.Put(pwjob, pwjob.toLocation, "")

        '    Dim Dr As DataRow = DT.Rows(index)

        '    Dim SQL As String = String.Format("Delete from apx_returns_putaway_detail where UserID={0} and ToLocation={1}", Made4Net.Shared.FormatField(WMS.Logic.Common.GetCurrentUser), Made4Net.Shared.FormatField(Dr("ToLocation")))
        '    Made4Net.DataAccess.DataInterface.RunSQL(SQL)
        '    Session.Remove("ScannedToLoc")

        '    WhereToNext()

        'End If

        If ReplTaskDetail Is Nothing Then
            index += 1
            DoReplen(DT, index)
        Else
            If Session("ScannedToLoc") Is Nothing Then
                ReplTask = ReplTaskDetail.GetReplTask()
                If Not ReplTask.ASSIGNED Then
                    ReplTask.AssignUser(WMS.Logic.Common.GetCurrentUser, "MANUAL", "")
                End If

                Session.Add("ReplTaskDetail", ReplTaskDetail)
                Session.Add("RetReplenTask", ReplTask)
                SetupDropOffLocationScan(ReplTask.TOLOAD, CStr(DT.Rows(index)("Sku")), CStr(DT.Rows(index)("SkuDesc")), ReplTaskDetail.ToLocation)

                'Dim Sql As String = "select * from vLOADATTRIBUTESDESK where LOADID = '" & DT.Rows(index)("ToLoad") & "' and LOCATION = '" & DT.Rows(index)("ToLocation") & "' and STATUS = 'AVAILABLE'"
                'Dim sDtLines As DataTable = New DataTable()
                'Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)

                'If sDtLines.Rows.Count > 0 Or DT.Rows(index)("FromLoad") = DT.Rows(index)("ToLoad") Then
                '    Session.Add("ReplTaskDetail", ReplTaskDetail)
                '    Session.Add("RetReplenTask", ReplTask)
                '    SetupDropOffLocationScan(DT.Rows(index)("ToLoad"), ReplTaskDetail.ToLocation)
                'Else
                '    Dim ReturnDestWHAreas As String() = GetReturnDestinationWarehouseAreas()
                '    Dim DestQueryClause As String = BuildQueryClause(ReturnDestWHAreas)
                '    Dim DestData As String = DetermineIfHasDestination(CStr(DT.Rows(index)("FromLoad")), CStr(DT.Rows(index)("Sku")), ReturnDestWHAreas)
                '    If Not String.IsNullOrEmpty(DestData) Then
                '        Dim DestValues As String()
                '        DestValues = DestData.Split(New Char() {"|"c})
                '        Sql = String.Format("Update apx_returns_putaway_detail set ToLoad={0}, ToLocation={1} where FromLoad={2}", Made4Net.Shared.FormatField(DestValues(1)), Made4Net.Shared.FormatField(DestValues(0)), Made4Net.Shared.FormatField(DT.Rows(index)("FromLoad")))
                '    Else
                '        Sql = String.Format("Update apx_returns_putaway_detail set ToLoad={0}, ToLocation={1} where FromLoad={2}", Made4Net.Shared.FormatField(DT.Rows(index)("FromLoad")), Made4Net.Shared.FormatField(DT.Rows(index)("FromLocation")), Made4Net.Shared.FormatField(DT.Rows(index)("FromLoad")))
                '    End If

                '    Made4Net.DataAccess.DataInterface.RunSQL(Sql)
                '    ReplTask.Cancel()
                '    PerformPutAwayTasks()
                'End If
            Else
                ReplTask = Session("RetReplenTask")
                ReplTaskDetail = Session("ReplTaskDetail")
                Dim replJob As ReplenishmentJob = ReplTask.getReplenishmentJob(ReplTaskDetail)
                ReplTask.Replenish(ReplTaskDetail, replJob, WMS.Logic.Common.GetCurrentUser)

                Dim Dr As DataRow = DT.Rows(index)

                CreateReplenTaskfromHandover(Dr)

                Dim SQL As String = ""
                If ReplTask.TOLOAD <> ReplTask.FROMLOAD Then
                    SQL = String.Format("Update apx_serial set loadid={0} where loadid={1} and received=1 and dispatched=0 and SKU={2}", Made4Net.Shared.FormatField(ReplTask.TOLOAD), Made4Net.Shared.FormatField(ReplTask.FROMLOAD), Made4Net.Shared.FormatField(replJob.Sku))
                    Made4Net.DataAccess.DataInterface.RunSQL(SQL)
                End If

                SQL = String.Format("Delete from apx_returns_putaway_detail where UserID={0} and ToLocation={1} and FromLoad={2}", Made4Net.Shared.FormatField(WMS.Logic.Common.GetCurrentUser), Made4Net.Shared.FormatField(Dr("ToLocation")), Made4Net.Shared.FormatField(Dr("FromLoad")))
                Made4Net.DataAccess.DataInterface.RunSQL(SQL)

                Session.Remove("ScannedToLoc")
                Session.Remove("SerialFromLoad")
                Session.Remove("SerialFromSku")

                WhereToNext()
                ''SetupReplenConfirm(Dr)
            End If
        End If
    End Sub

    Private Sub PerformPutAwayTasks()
        DO1.setVisibility("MODE", True)
        DO1.Value("MODE") = "<b>DEPOSIT</b>"

        Dim DT As DataTable = CheckIfAnythingToDeposit(WMS.Logic.Common.GetCurrentUser)
        If Session("ScannedToLoc") Is Nothing Then
            RemovedAnyUnconfirmedPickups(WMS.Logic.Common.GetCurrentUser)
            For Each Dr As DataRow In DT.Rows
                CheckForAReplenTask(Dr)
            Next
        End If

        DoReplen(DT, 0)
    End Sub

    Private Function GetSKU(ByVal sku As String) As String
        Dim SQL As String = String.Format("SELECT SKU FROM vSKUCODE WHERE SKUCODE = {0}", Made4Net.Shared.FormatField(sku))
        Return CStr(Made4Net.DataAccess.DataInterface.ExecuteScalar(SQL))
    End Function

    Private Function CheckIfSerialIsValid(ByVal ScannedSerial As String, ByVal FromLoad As String, ByVal Sku As String) As String
        Dim Query As String = "select * from apx_serial where serial='" & ScannedSerial & "' and received=1 and dispatched=0"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Query, sDtLines, False, Nothing)

        If sDtLines.Rows.Count > 0 Then
            If sDtLines.Rows.Count = 1 Then
                If sDtLines.Rows(0)("SKU") <> Sku Then
                    Return "Scanned serial " & ScannedSerial & " does not match destination location load sku"
                End If

                If sDtLines.Rows(0)("loadID") <> FromLoad Then
                    Return "Scanned serial " & ScannedSerial & " not from load " & FromLoad
                End If
            Else
                Dim x As Integer = 0
                Dim LoadMatch As Integer = -1
                While x < sDtLines.Rows.Count
                    If sDtLines.Rows(x)("loadID") = FromLoad Then
                        LoadMatch = x
                    End If
                    x += 1
                End While

                If LoadMatch >= 0 Then
                    If sDtLines.Rows(LoadMatch)("SKU") = Sku Then
                        Return ""
                    End If

                    Return "Scanned serial " & ScannedSerial & " does not match destination location load sku"
                End If

                Return "Scanned serial " & ScannedSerial & " not from load " & FromLoad
            End If
        End If

        Return "Scanned serial " & ScannedSerial & " is not found on the system"
    End Function

    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Not Session("LoadProblem") Is Nothing Then
            Session.Remove("LoadProblem")
        Else
            If Not String.IsNullOrEmpty(DO1.Value("FROMLOC").Trim()) Then
                Dim ScannedLoc As String = DO1.Value("FROMLOC").Trim()
                DO1.Value("FROMLOC") = ""
                Dim CompareVal As String = Session("CompareValue")
                If ScannedLoc.ToUpper() = CompareVal.ToUpper() Then
                    Session.Add("ScannedLoc", CompareVal)
                    Session.Remove("CompareValue")
                Else
                    'Error Message
                    MessageQue.Enqueue(t.Translate("Scanned location does not match requested location"))
                End If
            ElseIf Not String.IsNullOrEmpty(DO1.Value("FROMLOAD").Trim()) Then
                Dim ScannedLoad As String = DO1.Value("FROMLOAD").Trim()
                DO1.Value("FROMLOAD") = ""
                Dim CompareVal As String = Session("CompareValue")
                If ScannedLoad.ToUpper() = CompareVal.ToUpper() Then
                    Session.Add("ScannedLoadId", CompareVal)
                    Session.Remove("CompareValue")
                Else
                    'Error Message
                    MessageQue.Enqueue(t.Translate("Scanned loadId does not match requested loadId"))
                End If
            ElseIf Not String.IsNullOrEmpty(DO1.Value("FROMSKU").Trim()) Then
                Dim ScannedSku As String = GetSKU(DO1.Value("FROMSKU").Trim())
                DO1.Value("FROMSKU") = ""
                Dim CompareVal As String = Session("CompareValue")
                If ScannedSku.ToUpper() = CompareVal.ToUpper() Then
                    Session.Add("ScannedSku", CompareVal)
                    Session.Remove("CompareValue")
                Else
                    'Error Message
                    MessageQue.Enqueue(t.Translate("Scanned product is invalid"))
                End If
            ElseIf Not String.IsNullOrEmpty(DO1.Value("FROMCONFIRMQTY").Trim()) Then
                Dim StrConfirmQty As String = DO1.Value("FROMCONFIRMQTY").Trim()
                DO1.Value("FROMCONFIRMQTY") = ""
                If IsNumeric(StrConfirmQty) Then
                    Dim CompareVal As Integer = Session("CompareValue")
                    Dim ConfirmQty As Integer = CInt(StrConfirmQty)
                    If ConfirmQty = CompareVal Then
                        Session.Remove("CompareValue")
                        Session.Add("ConfirmedPickupQty", CompareVal)
                        ConfirmLoadPickupForUser(WMS.Logic.Common.GetCurrentUser, Session("ScannedLoadId"), Session("ScannedSku"))
                    Else
                        MessageQue.Enqueue(t.Translate("Entered Qty Does Not Match Load Qty"))
                    End If
                Else
                    MessageQue.Enqueue(t.Translate("Invalid Qty Entered"))
                End If
            ElseIf Not String.IsNullOrEmpty(DO1.Value("TOLOC").Trim()) Then
                Dim Scannedloc As String = DO1.Value("TOLOC").Trim()
                DO1.Value("TOLOC") = ""
                If Session("CompareValue") IsNot Nothing Then
                    Dim CompareVal As String = Session("CompareValue")
                    If Scannedloc.ToUpper() = CompareVal.ToUpper() Then
                        Session.Add("ScannedToLoc", CompareVal)
                        Session.Remove("CompareValue")
                        Session.Remove("SerialFromLoad")
                        Session.Remove("SerialFromSku")
                    Else
                        'Error Message
                        MessageQue.Enqueue(t.Translate("Scanned location does not match requested location"))
                    End If
                End If
            ElseIf Not String.IsNullOrEmpty(DO1.Value("CONFIRMQTY").Trim()) Then
                    Dim EnteredQty As String = DO1.Value("CONFIRMQTY").Trim()
                    DO1.Value("CONFIRMQTY") = ""
                    If Session("CompareCQtyValue") IsNot Nothing Then
                        Dim CompareVal As String = CStr(Session("CompareCQtyValue"))
                        If IsNumeric(EnteredQty) And CInt(EnteredQty) = CInt(CompareVal) Then
                            Session.Remove("CompareCQtyValue")
                            Dim Sql As String = String.Format("Update apx_returns_putaway_detail set QuantityConfirmed={0} where UserID={1} and ToLocation={2}", Made4Net.Shared.FormatField(CInt(EnteredQty)), Made4Net.Shared.FormatField(WMS.Logic.Common.GetCurrentUser), Made4Net.Shared.FormatField(Session("ScannedToLoc")))
                            Made4Net.DataAccess.DataInterface.RunSQL(Sql)
                        Else
                            'Error Message 
                            MessageQue.Enqueue(t.Translate("Entered Qty " & DO1.Value("CONFIRMQTY") & " is invalid"))
                        End If
                    End If
                ElseIf Not String.IsNullOrEmpty(DO1.Value("FROMSER").Trim()) Then
                    Dim ScannedSerial As String = DO1.Value("FROMSER").Trim()
                DO1.Value("FROMSER") = ""
                If Session("CanScanSerial") IsNot Nothing Then
                    Dim ErrMsg As String = CheckIfSerialIsValid(ScannedSerial, Session("SerialFromLoad"), Session("SerialFromSku"))
                    If String.IsNullOrEmpty(ErrMsg) Then
                        Dim Sql As String = String.Format("Update apx_returns_putaway_detail set QuantityConfirmed=QuantityConfirmed + 1 where UserID={0} and ToLocation={1} and FromLoad={2}", Made4Net.Shared.FormatField(WMS.Logic.Common.GetCurrentUser), Made4Net.Shared.FormatField(Session("ScannedToLoc")), Made4Net.Shared.FormatField(Session("SerialFromLoad")))
                        Made4Net.DataAccess.DataInterface.RunSQL(Sql)
                    Else
                        'Error Message 
                        MessageQue.Enqueue(t.Translate(ErrMsg))
                    End If
                End If
            End If
        End If

        WhereToNext()
    End Sub

    Private Sub ClearUnConfirmed(ByVal UserID As String)
        Dim SQL As String = String.Format("Delete From apx_returns_putaway_detail where PickUpConfirmed={0} and UserID={1}", Made4Net.Shared.FormatField(False), Made4Net.Shared.FormatField(UserID))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Sub doMenu()
        If Not Session("Elogout") Is Nothing Then
            Session.Remove("Elogout")
            Made4Net.Shared.Web.User.Logout()
            Return
        End If

        RemoveSessions()
        ClearUnConfirmed(WMS.Logic.Common.GetCurrentUser)
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doMarkAsProblem()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Not Session("LoadProblem") Is Nothing Then
            If String.IsNullOrEmpty(DO1.Value("PROBREASON")) Then
                MessageQue.Enqueue(t.Translate("Please insert a reason to mark load as problem"))
                Return
            Else
                Dim NonConfirmedDT As DataTable = CheckIfLoadsOnTrolleyConfimed(WMS.Logic.Common.GetCurrentUser)

                If NonConfirmedDT.Rows.Count > 0 Then
                    Dim SQL As String = String.Format("Insert Into apx_returns_putaway_problem (UserID, LoadID, Reason, AddDate) VALUES({0},{1},{2},{3})", Made4Net.Shared.FormatField(WMS.Logic.Common.GetCurrentUser), Made4Net.Shared.FormatField(NonConfirmedDT.Rows(0)("FromLoad")), Made4Net.Shared.FormatField(DO1.Value("PROBREASON")), Made4Net.Shared.FormatField(DateTime.Now))
                    Made4Net.DataAccess.DataInterface.RunSQL(SQL)

                    ClearUnConfirmed(WMS.Logic.Common.GetCurrentUser)
                    Session.Remove("LoadProblem")
                End If
            End If
        Else
            Session.Add("LoadProblem", True)
        End If
        WhereToNext()

    End Sub

    Private Sub doDeposit()
        Session.Remove("ScannedLoc")
        Session.Remove("ScannedLoadId")
        Session.Remove("ScannedSku")
        Session.Remove("ConfirmedPickupQty")
        Session.Remove("CompareValue")
        Session.Remove("CompareCQtyValue")
        Session.Remove("CanScanSerial")
        Session.Remove("RetReplenTask")
        Session.Remove("ScannedToLoc")
        Session.Remove("SerialFromLoad")
        Session.Remove("SerialFromSku")

        UpdateModeForUserTrolley("DEPOSIT", WMS.Logic.Common.GetCurrentUser)
        WhereToNext()
    End Sub

    Private Sub RemoveSessions()
        Session.Remove("ScannedLoc")
        Session.Remove("ScannedLoadId")
        Session.Remove("ScannedSku")
        Session.Remove("ConfirmedPickupQty")
        Session.Remove("CompareValue")
        Session.Remove("CompareCQtyValue")
        Session.Remove("CanScanSerial")
        Session.Remove("RetReplenTask")
        Session.Remove("ScannedToLoc")
        Session.Remove("SerialFromLoad")
        Session.Remove("SerialFromSku")
    End Sub

    Private Sub DO1_CreatedChildControls(sender As Object, e As EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("MODE", "")
        DO1.AddLabelLine("LOADID")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOCATION")
        DO1.AddLabelLine("UNITS")
        DO1.AddLabelLine("UNITSCONFIRMED")
        DO1.AddSpacer()
        DO1.AddLabelLine("Msg", "")
        DO1.AddSpacer()
        DO1.AddTextboxLine("FROMLOC", "Scan Location")
        DO1.AddTextboxLine("FROMLOAD", "Scan LoadId")
        DO1.AddTextboxLine("FROMSKU", "Scan Product")
        DO1.AddTextboxLine("FROMCONFIRMQTY", "Confirm Qty")
        DO1.AddTextboxLine("TOLOC", "Scan LoadID")
        DO1.AddTextboxLine("CONFIRMQTY", "Enter Qty")
        DO1.AddTextboxLine("FROMSER", "Scan Serial")
        DO1.AddTextboxLine("PROBREASON", "Reason")
        DO1.AddSpacer()
        DO1.AddLabelLine("ReplenList", "")
    End Sub

    Private Sub DO1_ButtonClick(sender As Object, e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "deposit"
                doDeposit()
            Case "menu"
                doMenu()
            Case "logout"
                doMenu()
            Case "problem"
                doMarkAsProblem()
        End Select
    End Sub
End Class
