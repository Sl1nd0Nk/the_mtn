Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

<CLSCompliant(False)> Public Class WHTransferUnloading1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen

    Protected WithEvents ddUOM As Made4Net.WebControls.MobileDropDown
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then WMS.Logic.GotoLogin()

        If Not IsPostBack Then SetScreen()
    End Sub

    Private Sub SetScreen()
        Dim s As Shipment = Session("TransferShipment")

        DO1.Value("TRANSFERID") = s.SHIPMENT
        DO1.Value("VEHICLEID") = s.VEHICLE


        setTotalLoads(s.SHIPMENT)

        DO1.Value("LASTSCAN") = ""
    End Sub


    Private Sub setTotalLoads(ByVal shipmentId As String)
        Dim sql As String = $"select count(1) from SHIPMENTLOADS where loadid not in (select loadid from invload) and shipment='{shipmentId}'"
        DO1.Value("TOTALLOADS") = Made4Net.DataAccess.DataInterface.ExecuteScalar(sql)
    End Sub



    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator()
        Dim ld As String = DO1.Value("LOADID")
        If String.IsNullOrEmpty(ld) Then
            MessageQue.Enqueue(t.Translate("Loadid can not be empty"))
            Return
        End If

        Dim s As Shipment = Session("TransferShipment")

        Dim sql As String = ""
        sql = $"select * from shipmentloads where loadid='{ld}' and shipment='{s.SHIPMENT}'"
        Dim slDT As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, slDT)

        If slDT.Rows.Count = 0 Then
            MessageQue.Enqueue(t.Translate("Load does not exist"))
            Return
        End If

        sql = $"select * from loads where loadid='{ld}'"
        Dim dt As New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)

        If dt.Rows.Count > 0 Then
            If Not String.IsNullOrEmpty(dt.Rows(0)("STATUS").ToString()) Then
                MessageQue.Enqueue(t.Translate("Load was already unloaded"))
                Return
            Else
                sql = $"update loads set location='{Session()("TransferLocation")}', status='{slDT.Rows(0)("INVENTORYSTATUS")}' where loadid='{ld}'"
                Made4Net.DataAccess.DataInterface.ExecuteScalar(sql)
            End If
        Else

        End If

        DO1.Value("LASTSCAN") = ld
        Dim totalLoads As Integer = Integer.Parse(DO1.Value("TOTALLOADS")) - 1
        DO1.Value("TOTALLOADS") = totalLoads
        DO1.Value("LOADID") = ""

        Dim ldObj As New WMS.Logic.Load(ld)
        sendLoadUnloadedEvent(ldObj)

        If Not s.STATUS.Equals(WMS.Lib.Statuses.Shipment.ATDOCK, StringComparison.OrdinalIgnoreCase) Then
            SetAtDock(s, WMS.Logic.GetCurrentUser)
        End If
        If totalLoads = 0 Then
            s.SetStatus("CLOSE", WMS.Logic.GetCurrentUser)
            MessageQue.Enqueue(t.Translate("WH Transfer unloading finished"))
            doBack()
        End If


    End Sub

    Private Sub doBack()
        Session.Remove("TransferShipment")
        Session.Remove("TransferLocation")
        Response.Redirect(MapVirtualPath("Screens/WHTransferUnloading.aspx"))
    End Sub

    Private Sub doClose()
        Dim s As Shipment = Session("TransferShipment")
        s.Ship(WMS.Logic.GetCurrentUser)
        Session.Remove("TransferShipment")
        Response.Redirect(MapVirtualPath("Screens/WHTransferUnloading.aspx"))
    End Sub


    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("TRANSFERID")
        DO1.AddLabelLine("VEHICLEID")
        DO1.AddLabelLine("TOTALLOADS")
        DO1.AddLabelLine("LASTSCAN")
        DO1.AddTextboxLine("LOADID")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "back"
                doBack()
            Case "close"
                doClose()
            Case "next"
                doNext()
        End Select
    End Sub


    Private Sub sendLoadUnloadedEvent(ByVal ld As WMS.Logic.Load)
        Dim aq As New EventManagerQ
        aq.Add("EVENT", "1001")
        aq.Add("ACTIVITYDATE", Made4Net.Shared.Util.DateTimeToWMSString(DateTime.Now))
        aq.Add("ACTIVITYTIME", "0")
        aq.Add("ACTIVITYTYPE", WMS.Lib.Actions.Audit.LOADLOAD)
        aq.Add("CONSIGNEE", ld.CONSIGNEE)
        aq.Add("DOCUMENT", "")
        aq.Add("DOCUMENTLINE", 0)
        aq.Add("FROMLOAD", ld.LOADID)
        aq.Add("FROMLOC", ld.LOCATION)
        aq.Add("FROMQTY", 0)
        aq.Add("FROMSTATUS", ld.STATUS)
        aq.Add("NOTES", "")
        aq.Add("SKU", ld.SKU)
        aq.Add("TOLOAD", ld.LOADID)
        aq.Add("TOLOC", "")
        aq.Add("TOQTY", ld.UNITS)
        aq.Add("TOSTATUS", ld.STATUS)
        aq.Add("USERID", WMS.Logic.GetCurrentUser)
        aq.Add("ADDDATE", Made4Net.[Shared].Util.DateTimeToWMSString(DateTime.Now))
        aq.Add("LASTMOVEUSER", WMS.Logic.GetCurrentUser)
        aq.Add("LASTSTATUSUSER", WMS.Logic.GetCurrentUser)
        aq.Add("LASTCOUNTUSER", WMS.Logic.GetCurrentUser)
        aq.Add("LASTSTATUSDATE", Made4Net.[Shared].Util.DateTimeToWMSString(DateTime.Now))
        aq.Add("ADDUSER", WMS.Logic.GetCurrentUser)
        aq.Add("EDITDATE", Made4Net.[Shared].Util.DateTimeToWMSString(DateTime.Now))
        aq.Add("EDITUSER", WMS.Logic.GetCurrentUser)
        aq.Send("UNLOADLOAD")
    End Sub

    Private Sub SetAtDock(ByVal s As WMS.Logic.Shipment, ByVal pUserId As String)

        Dim oldStatus, SQL As String
        oldStatus = s.STATUS
        s.EDITUSER = pUserId
        s.EDITDATE = DateTime.Now
        s.STATUS = WMS.Lib.Statuses.Shipment.ATDOCK
        SQL = String.Format("UPDATE SHIPMENT SET STATUS ='{0}', EDITDATE ={1}, EDITUSER ={2} WHERE {3}", s.STATUS,
        Made4Net.Shared.Util.FormatField(s.EDITDATE), Made4Net.Shared.Util.FormatField(s.EDITUSER), s.WhereClause)
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)

        Dim aq As EventManagerQ = New EventManagerQ
        aq.Add("EVENT", WMS.Logic.WMSEvents.EventType.ShipmentAtDock)
        aq.Add("ACTIVITYTYPE", WMS.Lib.Actions.Audit.SHIPATDOCK)
        aq.Add("ACTIVITYDATE", Made4Net.Shared.Util.DateTimeToWMSString(DateTime.Now))
        aq.Add("ACTIVITYTIME", "0")
        aq.Add("CONSIGNEE", "")
        aq.Add("DOCUMENT", s.SHIPMENT)
        aq.Add("DOCUMENTLINE", 0)
        aq.Add("FROMLOAD", "")
        aq.Add("FROMLOC", "")
        aq.Add("FROMQTY", 0)
        aq.Add("FROMSTATUS", oldStatus)
        aq.Add("NOTES", "")
        aq.Add("SKU", "")
        aq.Add("TOLOAD", "")
        aq.Add("TOLOC", "")
        aq.Add("TOQTY", 0)
        aq.Add("TOSTATUS", s.STATUS)
        aq.Add("USERID", pUserId)
        aq.Add("ADDDATE", Made4Net.[Shared].Util.DateTimeToWMSString(DateTime.Now))
        aq.Add("ADDUSER", pUserId)
        aq.Add("EDITDATE", Made4Net.[Shared].Util.DateTimeToWMSString(DateTime.Now))
        aq.Add("EDITUSER", pUserId)
        aq.Send(WMS.Lib.Actions.Audit.SHIPATDOCK)

    End Sub



End Class
