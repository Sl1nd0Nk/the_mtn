Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports WMS.MobileWebApp.apexInventory
Imports WMS.MobileWebApp.apexSerial

<CLSCompliant(False)> Public Class REPL6
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            If Not Request.QueryString.Item("sourcescreen") Is Nothing Then
                Session("REPLSRCSCREEN") = Request.QueryString.Item("sourcescreen")
            End If
            SetScreen()
        End If
    End Sub

    Private Sub SkuSerialProd(ByVal consignee As String, ByVal sku As String)
        Dim oSKU As New WMS.Logic.SKU(consignee, sku)

        If oSKU.SKUClass IsNot Nothing Then
            Session.Add("PRODCLASS", oSKU.SKUClass.ClassName)
        Else
            Session.Add("PRODCLASS", "")
        End If
    End Sub

    Private Sub SetScreen()
        Dim ReplTask As WMS.Logic.ReplenishmentTask = Session("REPLTSKTaskId")
        Dim ReplTaskDetail As WMS.Logic.Replenishment = Session("REPLTSKTDetail")
        Dim replJob As ReplenishmentJob = Session("REPLJobDetail")

        SkuSerialProd(replJob.Consignee, replJob.Sku)

        If replJob.IsHandOff Then
            'Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            'DO1.setVisibility("Note", True)
            'DO1.Value("Note") = trans.Translate("Task Destination Location is a Hand Off Location!")
        Else
            'DO1.setVisibility("Note", False)
        End If
        DO1.Value("TASKTYPE") = replJob.TaskType
        DO1.Value("LOCATION") = replJob.toLocation
        DO1.Value("LOADID") = replJob.fromLoad
        DO1.Value("UNITS") = Session("REPLENEDQTY")
        DO1.Value("CONSIGNEE") = replJob.Consignee
        DO1.Value("SKU") = replJob.Sku
        DO1.Value("SKUDESC") = replJob.skuDesc
        DO1.Value("UOMUNITS") = replJob.UOMUnits
        DO1.setVisibility("UOMUNITS", False)

        If Session("PRODCLASS") = "SERIAL" And APXINV_ScanSerialAtDeposit() Then
            DO1.setVisibility("QTY", False)
        Else
            DO1.setVisibility("SERIAL", False)
        End If

    End Sub

    'If the location is the right one then replenish the location else just move
    'the load to new location and leave the Job activ

    Private Sub doNext()
        Dim errStr As String = ""
        If Session("PRODCLASS") = "SERIAL" And APXINV_ScanSerialAtDeposit() Then
            errStr = HandleScannedSerial()
        Else
            errStr = HandleEnteredQty()
        End If

        If Not String.IsNullOrEmpty(errStr) Then
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            MessageQue.Enqueue(trans.Translate(errStr))
            DO1.Value("QTY") = ""
            DO1.Value("SERIAL") = ""
            Return
        Else
            If DO1.Value("UNITS") <= 0 Then
                DoReplen()
            Else
                DO1.Value("QTY") = ""
                DO1.Value("SERIAL") = ""
            End If
        End If
    End Sub

    Private Sub DoReplen()
        Dim repl As WMS.Logic.Replenishment = Session("REPLTSKTDetail")
        Dim ReplTask As WMS.Logic.ReplenishmentTask = Session("REPLTSKTaskId")
        Dim repljob As ReplenishmentJob = Session("REPLJobDetail")
        Try
            Dim units As Decimal = repljob.Units
            Dim ldid As String = repl.FromLoad
            Dim TransferSerial As Boolean = False

            Dim fromLoad As String = repl.FromLoad
            Dim toload As String = repl.ToLoad
            Dim fromloc As String = repl.FromLocation
            Dim toloc As String = repl.ToLocation
            Dim toloadExist As Boolean = True

            If fromloc = "HO-RT-SECURITY-GATE" Then
                If Not WMS.Logic.Load.Exists(toload) Then
                    toloadExist = False
                End If

                If fromLoad = toload Or Not toloadExist Then
                    Dim load As Load = New Load(fromLoad, True)
                    load.Replenish(toloc, WMS.Logic.Common.GetCurrentUser)
                Else
                    Dim LD As Load = New Load(ldid)
                    Dim NewLoad As Load = New Load(toload, True)
                    NewLoad.Merge(LD)

                    Dim SQL As String = String.Format("Update apx_serial set loadid={0} where loadid={1} and received=1 and dispatched=0 and SKU={2}", Made4Net.Shared.FormatField(toload), Made4Net.Shared.FormatField(ldid), Made4Net.Shared.FormatField(repljob.Sku))
                    Made4Net.DataAccess.DataInterface.RunSQL(SQL)
                End If

                Complete(WMS.Logic.Common.GetCurrentUser, repl, fromLoad, fromloc, toload, toloc, ReplTask)
            Else
                ReplTask.Replenish(repl, repljob, WMS.Logic.Common.GetCurrentUser, True)
            End If

        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.ToString())
            Return
        End Try

        APXINV_DequeueReplen(ReplTask.Replenishment)

        Session.Remove("REPLTSKTaskId")
        Session.Remove("REPLTSKTDetail")
        Session.Remove("REPLJobDetail")

        Response.Redirect(MapVirtualPath("Screens/REPL2.aspx"))
    End Sub

    Private Sub Complete(pUser As String, repl As Replenishment, fromLoad As String, fromLocation As String, toLoad As String, toLocation As String, ReplTask As ReplenishmentTask)
        Dim status As String = repl.Status
        repl.Status = "COMPLETE"
        repl.EDITDATE = DateTime.Now
        repl.EDITUSER = pUser
        Dim pSQL As String = String.Format("Update REPLENISHMENT set status={0},editdate={1},edituser={2},tolocation={3} where ReplId={4}",
                                           Made4Net.Shared.FormatField(repl.Status),
                                           Made4Net.Shared.FormatField(repl.EDITDATE),
                                           Made4Net.Shared.FormatField(repl.EDITUSER),
                                           Made4Net.Shared.FormatField(repl.ToLocation),
                                           Made4Net.Shared.FormatField(repl.ReplId))

        Made4Net.DataAccess.DataInterface.RunSQL(pSQL)

        ReplTask.Complete()

        sendReplAuditMsg(status, repl.Status, toLocation, toLoad, fromLocation, fromLoad, repl.ReplId, repl.Units)
    End Sub

    Private Sub sendReplAuditMsg(pFromStatus As String, toStatus As String, pToLocation As String, pToLoad As String, pFromLocation As String, fromLoad As String, replId As String, units As Decimal)
        Dim dataTable As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(String.Format("Select consignee,sku from loads where loadid = '{0}'", fromLoad), dataTable, False, Nothing)
        Dim value As String = dataTable.Rows(0)("SKU")
        Dim value2 As String = dataTable.Rows(0)("CONSIGNEE")
        Dim eventManagerQ As EventManagerQ = New EventManagerQ()
        Dim num As Integer = 76
        eventManagerQ.Add("ACTIVITYTIME", "0")
        eventManagerQ.Add("ACTIVITYTYPE", "REPLENISHMENT")
        eventManagerQ.Add("DOCUMENT", replId)
        eventManagerQ.Add("FROMLOAD", fromLoad)
        eventManagerQ.Add("CONSIGNEE", value2)
        eventManagerQ.Add("SKU", value)
        eventManagerQ.Add("FROMLOC", pFromLocation)
        eventManagerQ.Add("FROMQTY", units.ToString())
        eventManagerQ.Add("FROMSTATUS", pFromStatus)
        eventManagerQ.Add("NOTES", "")
        eventManagerQ.Add("TOLOAD", pToLoad)
        eventManagerQ.Add("TOLOC", pToLocation)
        eventManagerQ.Add("TOQTY", units.ToString())
        eventManagerQ.Add("TOSTATUS", toStatus)
        eventManagerQ.Add("USERID", WMS.Logic.Common.GetCurrentUser)
        eventManagerQ.Add("ADDDATE", Made4Net.Shared.Util.DateTimeToWMSString(DateTime.Now))
        eventManagerQ.Add("LASTMOVEUSER", WMS.Logic.Common.GetCurrentUser)
        eventManagerQ.Add("LASTSTATUSUSER", WMS.Logic.Common.GetCurrentUser)
        eventManagerQ.Add("LASTCOUNTUSER", WMS.Logic.Common.GetCurrentUser)
        eventManagerQ.Add("LASTSTATUSDATE", Made4Net.Shared.Util.DateTimeToWMSString(DateTime.Now))
        eventManagerQ.Add("ADDUSER", WMS.Logic.Common.GetCurrentUser)
        eventManagerQ.Add("EDITDATE", Made4Net.Shared.Util.DateTimeToWMSString(DateTime.Now))
        eventManagerQ.Add("EDITUSER", WMS.Logic.Common.GetCurrentUser)
        eventManagerQ.Add("EVENT", num.ToString())
        eventManagerQ.Add("REPLID", replId)
        eventManagerQ.Add("ACTIVITYDATE", Made4Net.Shared.Util.DateTimeToWMSString(DateTime.Now))
        eventManagerQ.Send("Replenishment Complted")
    End Sub

    Private Function HandleScannedSerial() As String
        Dim SerialQty As Integer = 0
        Dim ToLoad As String = APXINV_GetReplenToLoad(DO1.Value("LOCATION"))

        If Not String.IsNullOrEmpty(ToLoad) Then
            Dim err As String = APXSERIAL_TransferSerial(DO1.Value("SERIAL"), DO1.Value("LOADID"), ToLoad, DO1.Value("SKU"), SerialQty)
            If Not String.IsNullOrEmpty(err) Then
                Return err
            Else
                DO1.Value("UNITS") = DO1.Value("UNITS") - SerialQty
                Return ""
            End If
        Else
            Return "No load found in destination"
        End If
    End Function

    Private Function HandleEnteredQty() As String
        If DO1.Value("UNITS") <> DO1.Value("QTY") Then
            Return "Deposit quantity does not match replen quantity"
        End If

        DO1.Value("UNITS") = DO1.Value("UNITS") - DO1.Value("QTY")
        Return ""
    End Function

    Private Sub doBack()
        'Response.Redirect(MapVirtualPath("Screens/REPL1.aspx"))
        Dim ReplTask As WMS.Logic.ReplenishmentTask = Session("REPLTSKTaskId")
        If Not ReplTask Is Nothing Then
            If ReplTask.ASSIGNMENTTYPE = WMS.Lib.TASKASSIGNTYPE.MANUAL Then
                If Not Session("REPLSRCSCREEN") Is Nothing Then
                    Response.Redirect(MapVirtualPath("Screens/" & Session("REPLSRCSCREEN") & ".aspx"))
                Else
                    Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
                End If
            Else
                Response.Redirect(MapVirtualPath("Screens/TaskManager.aspx"))
            End If
        Else
            Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
        End If
    End Sub

    Private Function CheckSKU() As String
        Dim repljob As ReplenishmentJob = Session("REPLJobDetail")

        If repljob.Sku <> DO1.Value("SKU") Then
            Return "Incorrect SKU scanned."
        Else
            Return ""
        End If
    End Function

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        'DO1.AddLabelLine("Note")
        DO1.AddLabelLine("TASKTYPE")
        DO1.AddLabelLine("LOADID", "From Load")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOCATION")
        DO1.AddLabelLine("UOMUNITS")
        DO1.AddLabelLine("UNITS")
        DO1.AddSpacer()
        DO1.AddTextboxLine("QTY", "Enter Quantity To Deposit")
        DO1.AddTextboxLine("SERIAL", "Scan Serial Number To Deposit")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            'Case "override"
            '    doOverride()
            Case "next"
                doNext()
            Case "back"
                doBack()
        End Select
    End Sub
End Class

