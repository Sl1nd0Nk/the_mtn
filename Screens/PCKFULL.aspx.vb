Imports WMS.Logic
Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

<CLSCompliant(False)> Public Class PCKFULL
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If WMS.Logic.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If

        If Not IsPostBack Then
            If Not Request.QueryString("sourcescreen") Is Nothing Then
                Session("MobileSourceScreen") = Request.QueryString("sourcescreen")
            ElseIf Session("MobileSourceScreen") Is Nothing Then
                Session("MobileSourceScreen") = "PCK"
            ElseIf Not Session("MobileSourceScreen").ToString.ToLower() = "taskmanager" Then
                Session("MobileSourceScreen") = "PCK"
            End If

            If Not WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PICKING) Then
                Response.Redirect(MapVirtualPath("screens/" & Session("MobileSourceScreen") & ".aspx"))
            End If

            Dim pcks As Picklist = Session("PCKPicklist")
            Dim pck As PickJob
            Dim tm As New WMS.Logic.TaskManager(pcks.PicklistID, True)
            Try
                pck = PickTask.getNextPick(pcks)
            Catch ex As Exception
            End Try
            setPick(pck)
        End If
    End Sub

    Private Sub doMenu()
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        If WMS.Logic.TaskManager.isAssigned(UserId, WMS.Lib.TASKTYPE.PICKING) Then
            Dim tm As New WMS.Logic.TaskManager(UserId, WMS.Lib.TASKTYPE.PICKING)
            tm.ExitTask()
        End If
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doBack()
        'Response.Redirect(MapVirtualPath("Screens/PCK.aspx"))
        Response.Redirect(MapVirtualPath("screens/" & Session("MobileSourceScreen") & ".aspx"))
    End Sub

    Private Sub setPick(ByVal pck As PickJob)
        If pck Is Nothing Then
            'Response.Redirect(MapVirtualPath("Screens/DELLBLPRNT.aspx?sourcescreen=PCK"))
            Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=" & Session("MobileSourceScreen")))
        Else
            Session("PCKPicklistPickJob") = pck
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Dim pcks As Picklist = Session("PCKPicklist")
            DO1.Value("LOADID") = pck.fromload
            DO1.Value("LOCATION") = pck.fromlocation
            DO1.Value("PICKMETHOD") = pcks.PickMethod
            DO1.Value("PICKTYPE") = pcks.PickType
            DO1.Value("SKU") = pck.sku
            DO1.Value("SKUDESC") = pck.skudesc
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            'DO1.Value("UOMUNITS") = "(" & pck.units & ")" & " " & trans.Translate(pck.uom) & " " & pck.uomunits
            DO1.Value("UOMUNITS") = SetUomUnitsText(pck)
            If pck.SystemPickShort Then
                MessageQue.Enqueue(trans.Translate("System Pick Short- Quantity available is less than quantity required / Wrong Location!"))
            End If
            'Label Printing
            If MobileUtils.LoggedInMHEID <> String.Empty And MobileUtils.GetMHEDefaultPrinter <> String.Empty Then
                DO1.setVisibility("PRINTER", False)
            Else
                DO1.setVisibility("PRINTER", True)
            End If
        End If
    End Sub
    Private Function SetUnitsDisplay(ByVal pUnits As Decimal, ByVal pUom As String, ByVal pLowsetUom As String) As String
        If Not String.Equals(pUom, pLowsetUom, StringComparison.CurrentCultureIgnoreCase) Then
            Return Math.Round(pUnits, 2).ToString()
        End If
        Dim intUnits As Integer = 0 'only for display
        Int32.TryParse(Int(pUnits), intUnits)
        If intUnits = pUnits Then
            Return intUnits.ToString()
        End If
        Return Math.Round(pUnits, 2).ToString()
    End Function

    Private Function SetUomUnitsText(ByVal pck As WMS.Logic.PickJob) As String
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim sUomUnits As String = String.Empty
        Dim oSku As New WMS.Logic.SKU(pck.consingee, pck.sku, True)
        Dim sWantedUom As String = pck.uom
        Dim uomunits As Int32 = 0
        Dim dLowerUomUnits As Decimal = 0

        Integer.TryParse(Int(pck.uomunits).ToString(), uomunits)
        If (uomunits = pck.uomunits) OrElse String.Equals(pck.uom, oSku.LOWESTUOM, StringComparison.CurrentCultureIgnoreCase) Then '
            If String.Equals(pck.uom, oSku.LOWESTUOM, StringComparison.CurrentCultureIgnoreCase) Then
                sUomUnits = String.Format("{0} {1} ", SetUnitsDisplay(pck.units, pck.uom, oSku.LOWESTUOM), trans.Translate(pck.uom))
            Else
                sUomUnits = String.Format("{2} {3} ({0} {1}) ", SetUnitsDisplay(pck.units, oSku.LOWESTUOM, oSku.LOWESTUOM), oSku.LOWESTUOM, Integer.Parse(Int(pck.uomunits).ToString()), trans.Translate(pck.uom))
            End If

        Else 'need to find lower suitable uom
            ''Dim sql As String = String.Format("select uom,LOWERUOM from SKUUOM where sku='{0}' order by UNITSPERLOWESTUOM desc", String.Format(oSku.SKU))
            '' Dim dt As New DataTable()
            ''Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)
            Dim sUom As String = pck.uom
            Dim currUnits As Decimal
            Dim currUnitsInt As Integer
            Dim continueloop As Boolean = True

            While (continueloop)
                If String.IsNullOrEmpty(WMS.Logic.SKU.SKUUOM.GetSKUUOM(oSku.CONSIGNEE, oSku.SKU, sUom).LOWERUOM) Then
                    sUomUnits = String.Format("{0} {1} ", SetUnitsDisplay(pck.units, sUom, oSku.LOWESTUOM), trans.Translate(sUom))
                    continueloop = False
                Else
                    currUnits = oSku.ConvertUnitsToUom(sUom, pck.units)
                    Integer.TryParse(Int(currUnits), currUnitsInt)
                    If currUnits = currUnitsInt Then
                        sUomUnits = String.Format("{2} {3} ({0} {1}) ", SetUnitsDisplay(pck.units, oSku.LOWESTUOM, oSku.LOWESTUOM), oSku.LOWESTUOM, currUnitsInt, trans.Translate(sUom))
                        continueloop = False
                    Else
                        sUom = WMS.Logic.SKU.SKUUOM.GetSKUUOM(oSku.CONSIGNEE, oSku.SKU, sUom).LOWERUOM
                    End If
                End If

            End While

            'Dim wantedUomFound As Boolean = False
            'For Each dr As DataRow In dt.Rows
            '    If (Not wantedUomFound) Then 'in order to start from the requested uom
            '        If (Not (String.Equals(dr("uom"), pck.uom, StringComparison.CurrentCultureIgnoreCase))) Then
            '            Continue For
            '        Else
            '            wantedUomFound = True
            '        End If
            '    End If

            '    If (uomunits = pck.uomunits) OrElse String.Equals(dr("uom"), oSku.LOWESTUOM, StringComparison.CurrentCultureIgnoreCase) Then '
            '        sUomUnits = String.Format("({0}) ", pck.units)
            '        Exit For
            '    End If
            '    dLowerUomUnits = oSku.ConvertToUnits(dr("uom").ToString())
            '    Integer.TryParse(Int(dLowerUomUnits).ToString(), uomunits)
            '    If (Not dLowerUomUnits = uomunits) Then
            '        Continue For
            '    End If
            '    sUomUnits = String.Format("({0}) {1} {2}", pck.units, trans.Translate(dr("uom")), uomunits)

            'Next
        End If
        Return sUomUnits
    End Function
    Private Sub DoNext()
        Dim pck As PickJob
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim pcklst As Picklist = Session("PCKPicklist")
        pck = Session("PCKPicklistPickJob")
        If pck Is Nothing Then
            'Response.Redirect(MapVirtualPath("Screens/DELLBLPRNT.aspx?sourcescreen=PCK"))
            Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=" & Session("MobileSourceScreen")))
        End If
        Try
            pck.pickedqty = pck.units
            Dim tm As New WMS.Logic.TaskManager(pck.picklist, True)

            'ExtractAttributes()
            MobileUtils.ExtractPickingAttributes(pck, DO1)

            If MobileUtils.LoggedInMHEID.Trim <> String.Empty Then
                pck.LabelPrinterName = MobileUtils.GetMHEDefaultPrinter
            End If
            pck.TaskConfirmation.ConfirmationValues = MobileUtils.ExtractConfirmationValues(pck.TaskConfirmation.ConfirmationType, DO1)
            'pck = PickTask.Pick(DO1.Value("CONFIRM").Trim(), pcklst, pck, WMS.Logic.Common.GetCurrentUser)
            pck = PickTask.Pick(pcklst, pck, WMS.Logic.GetCurrentUser)
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            ClearAttributes()
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Return
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate(ex.Message))
            ClearAttributes()
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Return
        End Try
        ClearAttributes()

        setPick(pck)
    End Sub

    Private Sub pickanotherload()
        Dim pck As PickJob
        Dim pcklst As Picklist = Session("PCKPicklist")
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        pck = Session("PCKPicklistPickJob")
        If pck Is Nothing Then
            'Response.Redirect(MapVirtualPath("Screens/DELLBLPRNT.aspx?sourcescreen=PCK"))
            Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=" & Session("MobileSourceScreen")))
        End If

        Try
            pck.pickedqty = pck.units
            Dim tm As New WMS.Logic.TaskManager(pck.picklist, True)

            'ExtractAttributes()
            MobileUtils.ExtractPickingAttributes(pck, DO1)

            If MobileUtils.LoggedInMHEID.Trim <> String.Empty Then
                pck.LabelPrinterName = MobileUtils.GetMHEDefaultPrinter
            End If
            pck.TaskConfirmation.ConfirmationValues = MobileUtils.ExtractConfirmationValues(pck.TaskConfirmation.ConfirmationType, DO1)
            pck = PickTask.PickAnotherLoad(DO1.Value("SubtituteLoad").Trim(), pcklst, pck, WMS.Logic.GetCurrentUser())
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            ClearAttributes()
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Return
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate(ex.Message))
            ClearAttributes()
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Return
        End Try
        ClearAttributes()
        DO1.Value("SubtituteLoad") = ""
        setPick(pck)
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("PickMethod")
        DO1.AddLabelLine("PickType")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LoadId")
        DO1.AddLabelLine("Location")
        DO1.AddLabelLine("UomUnits")
        DO1.AddSpacer()
        Dim pck As PickJob = Session("PCKPicklistPickJob")
        If Not pck Is Nothing Then
            If Not pck.oAttributeCollection Is Nothing Then
                For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
                    DO1.AddTextboxLine(String.Format("Attr_{0}", pck.oAttributeCollection.Keys(idx)), pck.oAttributeCollection.Keys(idx))
                Next
            End If
        End If
        DO1.AddSpacer()
        addConfirmationFields(pck.TaskConfirmation.ConfirmationType)
        DO1.AddTextboxLine("SubtituteLoad")
        DO1.AddTextboxLine("PRINTER")
    End Sub

    Private Sub addConfirmationFields(ByVal pConfirmType As String)
        Select Case pConfirmType
            Case WMS.Lib.CONFIRMATIONTYPE.LOAD
                DO1.AddTextboxLine("CONFIRM(LOAD)")
            Case WMS.Lib.CONFIRMATIONTYPE.LOCATION
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
            Case WMS.Lib.CONFIRMATIONTYPE.NONE
            Case WMS.Lib.CONFIRMATIONTYPE.SKU
                DO1.AddTextboxLine("CONFIRM(SKU)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATION
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATIONUOM
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
                DO1.AddTextboxLine("CONFIRM(UOM)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKUUOM
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(UOM)")
            Case WMS.Lib.CONFIRMATIONTYPE.UPC
                DO1.AddTextboxLine("CONFIRM(UPC)")
        End Select
    End Sub

    Private Sub clearConfirmationFields(ByVal pConfirmType As String)
        Select Case pConfirmType
            Case WMS.Lib.CONFIRMATIONTYPE.LOAD
                DO1.Value("CONFIRM(LOAD)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.LOCATION
                DO1.Value("CONFIRM(LOCATION)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.NONE
            Case WMS.Lib.CONFIRMATIONTYPE.SKU
                DO1.Value("CONFIRM(SKU)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATION
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(LOCATION)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATIONUOM
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(LOCATION)") = ""
                DO1.Value("CONFIRM(UOM)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKUUOM
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(UOM)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.UPC
                DO1.Value("CONFIRM(UPC)") = ""
        End Select
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                DoNext()
            Case "pickanotherload"
                pickanotherload()
            Case "back"
                doBack()
        End Select
    End Sub

    'Private Function ExtractAttributes() As AttributesCollection
    '    Dim pck As PickJob = Session("PCKPicklistPickJob")
    '    Dim Val As Object
    '    If Not pck Is Nothing Then
    '        If Not pck.oAttributeCollection Is Nothing Then
    '            For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
    '                Val = DO1.Value(pck.oAttributeCollection.Keys(idx))
    '                If Val = "" Then Val = Nothing
    '                pck.oAttributeCollection(idx) = Val
    '            Next
    '            Return pck.oAttributeCollection
    '        End If
    '    End If
    '    Return Nothing
    'End Function

    Private Sub ClearAttributes()
        Dim pck As PickJob = Session("PCKPicklistPickJob")
        If Not pck Is Nothing Then
            If Not pck.oAttributeCollection Is Nothing Then
                For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
                    DO1.Value(String.Format("Attr_{0}", pck.oAttributeCollection.Keys(idx))) = ""
                Next
            End If
        End If
    End Sub
End Class
