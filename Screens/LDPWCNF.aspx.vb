Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess
Imports WMS.Logic

Partial Public Class LDPWCNF
    Inherits System.Web.UI.Page

#Region "ViewState"
    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            If WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.LOADPUTAWAY) Then
                Dim taskid As String = WMS.Logic.TaskManager.getUserAssignedTask(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.LOADPUTAWAY)
                Dim oTask As WMS.Logic.PutawayTask = New WMS.Logic.PutawayTask(taskid)
                oTask.AssignUser(WMS.Logic.Common.GetCurrentUser)
                Session("LoadPWTask") = oTask
                If Not oTask Is Nothing Then
                    setScreen(oTask)
                End If
            End If
        End If
    End Sub

    Private Sub doMenu()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim oTask As WMS.Logic.Task = Session("LoadPWTask")
            Dim ldId As String = getLoadId()
            If ldId <> String.Empty AndAlso String.Equals(ldId, oTask.FROMLOAD, StringComparison.OrdinalIgnoreCase) Then
                Response.Redirect(MapVirtualPath("Screens/RPK.aspx?sourcescreen=LDPWCNF"))
            Else
                ClearForm()
                'MessageQue.Enqueue(t.Translate("Wrong Confirmation"))
                Return
            End If
        Catch ex As Threading.ThreadAbortException
        Catch m4nEx As Made4Net.Shared.M4NException
            ClearForm()
            MessageQue.Enqueue(m4nEx.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
        Catch ex As Exception
            ClearForm()
            MessageQue.Enqueue(ex.ToString)
        End Try
    End Sub

    Private Sub ClearForm()
        DO1.Value("CONFIRMLOAD") = ""
        DO1.Value("CONFIRMLOCATION") = ""
        DO1.Value("CONFIRMCONSIGNEE") = ""
        DO1.Value("CONFIRMSKU") = ""
    End Sub

    Private Function getLoadId() As String
        Try
            Dim oSku As WMS.Logic.SKU
            If DO1.Value("CONFIRMSKU") <> "" And DO1.Value("CONFIRMCONSIGNEE") <> "" Then
                oSku = New WMS.Logic.SKU(DO1.Value("CONFIRMCONSIGNEE"), DO1.Value("CONFIRMSKU"))
            Else
                oSku = New WMS.Logic.SKU
            End If
            Dim SQL As String
            If DO1.Value("CONFIRMLOAD") <> "" Then
                SQL = String.Format("select loadid from invload inner join sku on invload.consignee = sku.consignee and invload.sku = sku.sku where loadid = '{0}' and location like '{1}%' and (sku.sku like '%{2}%' or sku.othersku like '%{2}%') and invload.consignee like '{3}%'", _
                    DO1.Value("CONFIRMLOAD"), DO1.Value("CONFIRMLOCATION"), DO1.Value("CONFIRMSKU"), DO1.Value("CONFIRMCONSIGNEE"))
            ElseIf DO1.Value("CONFIRMLOCATION") <> "" Then
                SQL = String.Format("select loadid from invload inner join sku on invload.consignee = sku.consignee and invload.sku = sku.sku where loadid like '{0}%' and location = '{1}' and (sku.sku like '%{2}%' or sku.othersku like '%{2}%') and invload.consignee like '{3}%'", _
                    DO1.Value("CONFIRMLOAD"), DO1.Value("CONFIRMLOCATION"), DO1.Value("CONFIRMSKU"), DO1.Value("CONFIRMCONSIGNEE"))
            Else
                SQL = String.Format("select loadid from invload inner join sku on invload.consignee = sku.consignee and invload.sku = sku.sku where loadid like '{0}%' and location like '{1}%' and (sku.sku like '%{2}%' or sku.othersku like '%{2}%') and invload.consignee = '{3}'", _
                    DO1.Value("CONFIRMLOAD"), DO1.Value("CONFIRMLOCATION"), DO1.Value("CONFIRMSKU"), DO1.Value("CONFIRMCONSIGNEE"))
            End If
            Dim dt As New DataTable
            DataInterface.FillDataset(SQL, dt)
            If dt.Rows.Count = 0 Then
                Throw New Made4Net.Shared.M4NException(New Exception, "Wrong Confirmation - No Loads were found", "Wrong Confirmation - No Loads were found")
            ElseIf dt.Rows.Count > 1 Then
                Throw New Made4Net.Shared.M4NException(New Exception, "Wrong Confirmation - More than 1 load was found", "Wrong Confirmation - More than 1 load was found")
            End If
            Return dt.Rows(0)("LOADID")
        Catch m4nEx As Made4Net.Shared.M4NException
            Made4Net.Mobile.MessageQue.Enqueue(m4nEx.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
        Catch ex As Exception
            Made4Net.Mobile.MessageQue.Enqueue(ex.Message)
        End Try
        Return String.Empty
    End Function

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("TaskId")
        DO1.AddLabelLine("LoadId")
        DO1.AddLabelLine("Location")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("Units")
        DO1.AddSpacer()
        DO1.AddTextboxLine("CONFIRMLOAD", "Load Id")
        DO1.AddTextboxLine("CONFIRMLOCATION", "Location")
        DO1.AddTextboxLine("CONFIRMCONSIGNEE", "Consignee")
        DO1.AddTextboxLine("CONFIRMSKU", "Sku")
    End Sub

    Private Sub setScreen(ByVal oTask As WMS.Logic.PutawayTask)
        Dim t As New Made4Net.Shared.Translation.Translator
        If Not WMS.Logic.Load.Exists(oTask.FROMLOAD) Then
            MessageQue.Enqueue(t.Translate("Load does not exists"))
            Return
        End If
        Dim oLoad As New WMS.Logic.Load(oTask.FROMLOAD)
        Dim oSku As New WMS.Logic.SKU(oLoad.CONSIGNEE, oLoad.SKU)
        DO1.Value("TaskId") = oTask.TASK
        DO1.Value("LoadId") = oLoad.LOADID
        DO1.Value("Location") = oLoad.LOCATION
        DO1.Value("SKU") = oLoad.SKU
        DO1.Value("SKUDESC") = oSku.SKUDESC
        DO1.Value("Units") = String.Format("{0} ({1} {2})", oLoad.UNITS, oSku.ConvertUnitsToUom(oLoad.LOADUOM, oLoad.UNITS), oLoad.LOADUOM)

        DO1.DefaultButton = "Next"
        DO1.FocusField = "Confirm"
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
        End Select
    End Sub
End Class
