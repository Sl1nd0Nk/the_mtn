Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports WMS.MobileWebApp.apexInbound
Imports WMS.MobileWebApp.apexSerial

<CLSCompliant(False)> Public Class CLDSerial1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session("UserAssignedPrinter") Is Nothing Then
            Dim Printer As String = GetUserPrinter(Logic.GetCurrentUser())

            If Printer = "" Then
                Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

                MessageQue.Enqueue(t.Translate("User " & Logic.GetCurrentUser() & " is not assigned to a label printer"))
            Else
                Session.Add("UserAssignedPrinter", Printer)
            End If
        End If

        If Not IsPostBack Then
            If Session("CreateLoadRCN") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRcnLine.aspx"))
            End If
            'CLDInfo
            DO1.Value("CONSIGNEE") = Session("CreateLoadConsignee")
            DO1.Value("SKU") = Session("CreateLoadSKU")
            DO1.Value("RECEIPT") = Session("CreateLoadRCN")
            DO1.Value("RECEIPTLINE") = Session("CreateLoadRCNLine")
            Dim oSku As New Logic.SKU(Session("CreateLoadConsignee"), Session("CreateLoadSKU"))
            DO1.Value("SKUDESC") = oSku.SKUDESC
            DO1.Value("SKUNOTES") = oSku.NOTES
            DO1.Value("UNITS") = Session("CreateLoadUnits")
            DO1.Value("UOM") = Session("CreateLoadUOM")
            DO1.Value("LOCATION") = Session("CreateLoadLocation")
            DO1.Value("WEIGHT") = Session("CreateLoadWeight")
            DO1.Value("Scanned") = 0
            DO1.setVisibility("SKUNOTES", False)
            DO1.setVisibility("VERMSG", False)
            GetSerialReceivingProgress()

        End If
        SetVisibility()
    End Sub

    Private Function GetUserPrinter(ByVal User As String) As String
        Dim SQL As String = String.Format("SELECT * FROM USERPROFILE WHERE USERID={0}", FormatField(User))
        Dim DtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, DtLines)

        If DtLines.Rows.Count > 0 Then
            If IsDBNull(DtLines.Rows(0)("DEFAULTPRINTER")) Then
                Return ""
            Else
                Return DtLines.Rows(0)("DEFAULTPRINTER")
            End If
        Else
            Return ""
        End If
    End Function

    Private Sub doMenu()
        MobileUtils.ClearCreateLoadProcessSession()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub SetVisibility()
        If CInt(DO1.Value("Scanned")) >= CInt(DO1.Value("UNITS")) Then
            DO1.setVisibility("SERIAL", False)

            If CInt(DO1.Value("Unverified")) <= 0 Then
                DO1.setVisibility("VERMSG", True)
            End If
        Else
            DO1.setVisibility("SERIAL", True)
            Response.Redirect(MapVirtualPath("Screens/CLDSerial.aspx"))
        End If

        'If DO1.Value("Unverified") > 0 Or DO1.Value("VERIFIED") <= 0 Then
        '    DO1.setVisibility("CreateLoad", False)
        'End If

        'DO1.Value("SERIAL") = ""
    End Sub

    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If (Not String.IsNullOrEmpty(DO1.Value("SERIAL"))) And (DO1.Value("SERIAL") <> " ") Then
            Dim err As String = ""
            err = APXINBOUND_AddSerialToStation(DO1.Value("SERIAL"), Logic.GetCurrentUser, Session("CreateLoadRCN"), Session("CreateLoadRCNLine"), _
                                          Session("CreateLoadConsignee"), Session("CreateLoadSKU"), Session("CreateLoadUnits"), _
                                          Session("CreateLoadWeight"), Session("CreateLoadUOM"), Session("CreateLoadLocation"))
            If Not String.IsNullOrEmpty(err) Then
                Made4Net.Mobile.MessageQue.Enqueue(t.Translate(err))
            End If
        Else
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Please scan valid serial number"))
        End If

        GetSerialReceivingProgress()
        SetVisibility()
        DO1.Value("SERIAL") = ""
    End Sub

    Private Function ReceiveValidSerialNumbers(ByVal LoadId As String, ByVal Qty As Integer, ByVal serialStatus As String) As Boolean
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim dtLines As DataTable = APXINBOUND_FindSerialsOnStationByState(Logic.GetCurrentUser, Session("CreateLoadRCN"), _
                                                               Session("CreateLoadRCNLine"), Session("CreateLoadConsignee"), _
                                                               Session("CreateLoadSKU"), Session("CreateLoadLocation"), "V")
        If dtLines.Rows.Count = 0 Then
            Return False
        Else
            If Qty = dtLines.Rows.Count Then
                For Each dr As DataRow In dtLines.Rows
                    Try
                        'strOEMTransIns = "INSERT INTO [SERIAL]([SERIALNUMBER],[SKU],[LOADID],[UOM],[CONTAINER],[DESPATCHED],[UID],[RECEIPT],[RECEIPTLINE])" & _
                        '                                 "VALUES('" & dr("SerialNumber") & "','" & Session("CreateLoadSKU") & "','" & LoadId & "','" & Session("CreateLoadUOM") & "','',0,'" & Logic.GetCurrentUser & "','" & Session("CreateLoadRCN") & "','" & Session("CreateLoadRCNLine") & "')"
                        'DataInterface.RunSQL(strOEMTransIns)
                        APXSERIAL_AddSerialToLoad(dr("SerialNumber"), Session("CreateLoadSKU"), LoadId, Session("CreateLoadUOM"), Logic.GetCurrentUser, CDbl(Session("CreateLoadWeight")), serialStatus, "NEW")
                    Catch ex As Exception
                        Made4Net.Mobile.MessageQue.Enqueue(t.Translate(ex.Message))
                        Return False
                    End Try
                Next
            Else
                Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Scanned count does not match the load qty"))
                Return False
            End If
        End If

        Dim err As String = ""
        err = APXINBOUND_RemoveSerialNumbersFromStation(Logic.GetCurrentUser, Session("CreateLoadRCN"), _
                                             Session("CreateLoadRCNLine"), Session("CreateLoadConsignee"), _
                                             Session("CreateLoadSKU"), Session("CreateLoadLocation"))

        If Not String.IsNullOrEmpty(err) Then
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate(err))
        End If

        Return True
    End Function

    Private Sub doViewSerial()
        Response.Redirect(MapVirtualPath("Screens/CLDSerial2.aspx"))
    End Sub

    Private Sub doAbort()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim err As String = ""
        err = APXINBOUND_RemoveSerialNumbersFromStation(Logic.GetCurrentUser, Session("CreateLoadRCN"), _
                                             Session("CreateLoadRCNLine"), Session("CreateLoadConsignee"), _
                                             Session("CreateLoadSKU"), Session("CreateLoadLocation"))

        If Not String.IsNullOrEmpty(err) Then
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate(err))
        End If

        Session.Remove("CreateLoadRCN")
        Session.Remove("CreateLoadRCNLine")
        Session.Remove("CreateLoadConsignee")
        Session.Remove("CreateLoadSKU")
        Session.Remove("CreateLoadLocation")
        Session.Remove("CreateLoadWeight")
        Session.Remove("CreateLoadLocation")
        Session.Remove("CreateLoadUnits")
        Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRCNLine.aspx"))
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("RECEIPT")
        DO1.AddLabelLine("RECEIPTLINE")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("SKUNOTES")
        DO1.AddLabelLine("LOCATION")
        DO1.AddLabelLine("WEIGHT")
        DO1.AddSpacer()

        DO1.AddTextboxLine("SERIAL", "Scan Serial Number")

        DO1.AddSpacer()
        DO1.AddLabelLine("UNITS", "___Units To Receive____")
        DO1.AddLabelLine("Scanned", "___Scanned Units______")
        DO1.AddLabelLine("VERIFIED", "___Verified Units______")
        'DO1.AddLabelLine("FAILEDVER", "___Failed Verification__")
        DO1.AddLabelLine("Unverified", "___Unverified Units____")
        DO1.AddSpacer()
        DO1.AddLabelLine("VERMSG", "Click the CreateLoad button to complete process")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "createload"
                SubmitCreate(ExtractAttributeValues(), False)
            Case "next"
                doNext()
            Case "serialnumbers"
                doViewSerial()
            Case "abort"
                doAbort()
        End Select
    End Sub

    Private Sub GetSerialReceivingProgress()
        Dim UnVerifiedCount As Integer = 0
        Dim VerifiedCount As Integer = 0
        Dim ValidScanned As Integer = 0
        Dim InvalidScanned As Integer = 0
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim dtLines As DataTable = APXINBOUND_FindSerialsOnStationByState(Logic.GetCurrentUser, Session("CreateLoadRCN"), _
                                                 Session("CreateLoadRCNLine"), Session("CreateLoadConsignee"), _
                                                 Session("CreateLoadSKU"), Session("CreateLoadLocation"), "")

        If dtLines.Rows.Count = 0 Then
            DO1.Value("VERIFIED") = 0
            DO1.Value("Unverified") = 0
            'DO1.Value("FAILEDVER") = 0
        Else
            Dim rowsFound As DataRow()
            Dim rowsFound1 As DataRow()
            Dim rowsFound2 As DataRow()
            'Dim rowsFound3 As DataRow()
            For Each dr As DataRow In dtLines.Rows
                rowsFound = dtLines.Select(String.Format("Verified={0}", FormatField("U")))
                If rowsFound.Length > 0 Then
                    UnVerifiedCount = rowsFound.Length
                End If
                rowsFound1 = dtLines.Select(String.Format("Verified={0}", FormatField("V")))
                If rowsFound1.Length > 0 Then
                    VerifiedCount = rowsFound1.Length
                End If
                rowsFound2 = dtLines.Select(String.Format("Verified<>{0}", FormatField("F")))
                If rowsFound2.Length > 0 Then
                    ValidScanned = rowsFound2.Length
                End If
                'rowsFound3 = dtLines.Select(String.Format("Verified={0}", FormatField("F")))
                'If rowsFound3.Length > 0 Then
                '    InvalidScanned = rowsFound3.Length
                'End If
            Next
            DO1.Value("VERIFIED") = VerifiedCount
            DO1.Value("Unverified") = UnVerifiedCount
            'DO1.Value("FAILEDVER") = InvalidScanned
        End If

        DO1.Value("Scanned") = ValidScanned
    End Sub

    Private Function ExtractAttributeValues() As WMS.Logic.AttributesCollection
        Dim oSku As String = Session("CreateLoadSKU")
        Dim oConsignee As String = Session("CreateLoadConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            Dim req As Boolean = False
            If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Dim val As Object
                Try
                    Select Case oAtt.Type
                        Case Logic.AttributeType.Boolean
                            val = CType(DO1.Value(oAtt.Name), Boolean)
                        Case Logic.AttributeType.DateTime
                            val = CType(DO1.Value(oAtt.Name), DateTime)
                        Case Logic.AttributeType.Decimal
                            val = CType(DO1.Value(oAtt.Name), Decimal)
                        Case Logic.AttributeType.Integer
                            val = CType(DO1.Value(oAtt.Name), Int32)
                        Case Else
                            val = DO1.Value(oAtt.Name)
                    End Select
                    oAttCol.Add(oAtt.Name, val)
                Catch ex As Exception

                End Try
            End If
        Next
        Return oAttCol
    End Function

    Private Sub SubmitCreate(ByVal oAttributes As WMS.Logic.AttributesCollection, Optional ByVal DoPutaway As Boolean = False)
        Dim ResponseMessage As String = ""
        Dim attributesarray As New ArrayList
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim ldId As String = Session("CreateLoadLoadId")
            If ldId = "" Then
                ldId = WMS.Logic.Load.GenerateLoadId()
            End If

            Dim ld() As WMS.Logic.Load
            Dim oRec As New Logic.Receiving
            Dim oReceiptLine As ReceiptDetail

            If Session("CreateLoadNumLoads") Is Nothing Then Session("CreateLoadNumLoads") = 1

            If Session("CreateLoadRCNMultipleLines") Then
                ld = oRec.CreateLoadFromMultipleLines(Session("CreateLoadRCN"), Session("CreateLoadSKU"), _
                    Session("CreateLoadUOM"), Session("CreateLoadLocation"), Session("CreateLoadUnits"), Session("CreateLoadStatus"), _
                    Session("CreateLoadHoldRC"), Logic.GetCurrentUser, oAttributes, Session("CreateLoadLabelPrinter"), "", "")
            Else

                Dim Prn As String = ""
                If Not Session("UserAssignedPrinter") Is Nothing Then
                    Prn = Session("UserAssignedPrinter")
                End If

                oReceiptLine = New ReceiptDetail(Session("CreateLoadRCN").ToString(), CType(Session("CreateLoadRCNLine"), Integer))

                ' Check if number of loads is more than one and if we need to create containers
                ld = oRec.CreateLoad(Session("CreateLoadRCN"), Session("CreateLoadRCNLine"), Session("CreateLoadSKU"), ldId, _
                    Session("CreateLoadUOM"), Session("CreateLoadLocation"), Session("CreateLoadUnits"), "Held", _
                    "", Session("CreateLoadNumLoads"), Logic.GetCurrentUser, oAttributes, Prn, "", "", "")
            End If

            If (ReceiveValidSerialNumbers(ldId, Session("CreateLoadUnits"), "Held")) Then
            Else
                Return
            End If

            ' After Load Creation we will put the loads on his container
            If Session("CreateContainer") = "1" Then
                Dim LoadsCreatedCount As Int32
                For LoadsCreatedCount = 0 To ld.Length() - 1
                    Dim cntr As New WMS.Logic.Container()
                    cntr.ContainerId = Made4Net.Shared.getNextCounter("CONTAINER")
                    cntr.HandlingUnitType = Session("HUTYPE")
                    cntr.Location = Session("CreateLoadLocation")
                    cntr.Post(WMS.Logic.Common.GetCurrentUser)

                    cntr.Place(ld(LoadsCreatedCount), WMS.Logic.Common.GetCurrentUser)
                Next
            Else
                If Session("CreateLoadContainerID") <> "" Then
                    Dim cntr As New WMS.Logic.Container(Session("CreateLoadContainerID"), True)
                    Dim LoadsCreatedCount As Int32
                    For LoadsCreatedCount = 0 To ld.Length() - 1
                        cntr.Place(ld(LoadsCreatedCount), WMS.Logic.Common.GetCurrentUser)
                    Next
                End If
            End If


            ' Create Handling unit transaction if needed
            If DO1.Value("HUTRANS") = "YES" Then
                Try
                    Dim strHUTransIns As String
                    If Session("CreateLoadContainerID") <> "" Then
                        strHUTransIns = "INSERT INTO [HANDLINGUNITTRANSACTION]([TRANSACTIONID],[TRANSACTIONTYPE],[TRANSACTIONTYPEID],[TRANSACTIONDATE],[CONSIGNEE],[ORDERID],[DOCUMENTTYPE],[COMPANY],[COMPANYTYPE],[HUTYPE],[HUQTY],[ADDDATE],[ADDUSER],[EDITDATE],[EDITUSER])" & _
                                                      "VALUES('" & Made4Net.Shared.getNextCounter("HUTRANSID") & "','RECEIPT','" & Session("CreateLoadRCN") & "',GETDATE(),'" & Session("CreateLoadConsignee") & "','" & oReceiptLine.ORDERID & "','" & oReceiptLine.DOCUMENTTYPE & "','" & oReceiptLine.COMPANY & "','" & oReceiptLine.COMPANYTYPE & "','" & DO1.Value("HUTYPE") & "','1',GETDATE(),'" & Logic.GetCurrentUser & "',GETDATE(),'" & Logic.GetCurrentUser & "')"
                    Else
                        strHUTransIns = "INSERT INTO [HANDLINGUNITTRANSACTION]([TRANSACTIONID],[TRANSACTIONTYPE],[TRANSACTIONTYPEID],[TRANSACTIONDATE],[CONSIGNEE],[ORDERID],[DOCUMENTTYPE],[COMPANY],[COMPANYTYPE],[HUTYPE],[HUQTY],[ADDDATE],[ADDUSER],[EDITDATE],[EDITUSER])" & _
                                                      "VALUES('" & Made4Net.Shared.getNextCounter("HUTRANSID") & "','RECEIPT','" & Session("CreateLoadRCN") & "',GETDATE(),'" & Session("CreateLoadConsignee") & "','" & oReceiptLine.ORDERID & "','" & oReceiptLine.DOCUMENTTYPE & "','" & oReceiptLine.COMPANY & "','" & oReceiptLine.COMPANYTYPE & "','" & DO1.Value("HUTYPE") & "','" & Session("CreateLoadNumLoads") & "',GETDATE(),'" & Logic.GetCurrentUser & "',GETDATE(),'" & Logic.GetCurrentUser & "')"
                    End If
                    DataInterface.RunSQL(strHUTransIns)
                Catch ex As Exception
                End Try
            End If

            ' If Create and Putaway then request pickup for each load
            If DoPutaway Then
                Dim pw As New Putaway
                Dim i As Int32
                For i = 0 To ld.Length() - 1
                    ld(i).RequestPickUp(Logic.GetCurrentUser)
                    'pw.RequestDestination(ld(i).LOADID)
                Next
                Response.Redirect(MapVirtualPath("Screens/RPK2.aspx?sourcescreen=cld1"))
            End If
            MessageQue.Enqueue(t.Translate("Load Created"))
            DO1.Value("LOADID") = ""
            DO1.Value("UNITS") = ""
            DO1.Value("CONTAINERID") = ""
            clearAttributeValues()
            doBack()
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As ApplicationException
            MessageQue.Enqueue(ex.Message)
            Return
        End Try

    End Sub

    Private Sub doBack()
        Session.Remove("CreateLoadLocation")
        Session.Remove("CreateLoadUnits")
        Session.Remove("CreateLoadWeight")
        Session.Remove("CreateLoadSKU")
        Session.Remove("CreateLoadRCN")
        Session.Remove("CreateLoadConsignee")
        Session.Remove("CreateLoadNumLoads")
        Session.Remove("CreateContainer")
        Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRCNLine.aspx"))
    End Sub

    Private Sub clearAttributeValues()
        Dim oSku As String = Session("CreateLoadSKU")
        Dim oConsignee As String = Session("CreateLoadConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If objSkuClass Is Nothing Then Return
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Try
                    DO1.Value(oAtt.Name) = ""
                Catch ex As Exception
                End Try
            End If
        Next
    End Sub
End Class
