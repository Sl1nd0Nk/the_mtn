Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports Wms.Logic

<CLSCompliant(False)> Public Class CLD1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If Session("CreateLoadRCN") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRcnLine.aspx"))
            End If
            'CLDInfo
            DO1.Value("CONSIGNEE") = Session("CreateLoadConsignee")
            DO1.Value("SKU") = Session("CreateLoadSKU")
            DO1.Value("RECEIPT") = Session("CreateLoadRCN")
            DO1.Value("RECEIPTLINE") = Session("CreateLoadRCNLine")
            Dim oSku As New Logic.SKU(Session("CreateLoadConsignee"), Session("CreateLoadSKU"))
            DO1.Value("SKUDESC") = oSku.SKUDESC
            DO1.Value("SKUNOTES") = oSku.NOTES
            DO1.Value("CONTAINERID") = Session("CreateLoadContainerID")
            DO1.Value("WEIGHT") = Session("CreateLoadWeight")
            DO1.Value("STATUS") = "HELD"
            DO1.Value("UNITS") = Session("CreateLoadUnits")
            'CLD1
            If Session("CreateLoadLocation") = "" Then
                Dim oCon As New WMS.Logic.Consignee(Session("CreateLoadConsignee"))
                DO1.Value("LOCATION") = oCon.DEFAULTRECEIVINGLOCATION
            Else
                DO1.Value("LOCATION") = Session("CreateLoadLocation")
            End If
            'CLD2
            SetStatusAndPrinter()
            'CLD3
            'SetUOM()
            SetHUTYpe()
            SetHUTrans()
        End If
    End Sub

    Private Sub doMenu()
        MobileUtils.ClearCreateLoadProcessSession()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doNext(ByVal doPutAway As Boolean)
        'If Page.IsValid Then
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        'Dim WeightStr As String = DO1.Value("WEIGHT")
        'Dim Weight As Double = 0.0

        'If String.IsNullOrEmpty(WeightStr) Then
        '    Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Please enter the product weight"))
        '    Return
        'Else
        '    Weight = CDbl(WeightStr)
        'End If

        'Dim sql As String = String.Format("select * from sku where CONSIGNEE={0} and SKU={1}", Made4Net.Shared.FormatField(Session("CreateLoadConsignee")), _
        '                                                                                       Made4Net.Shared.FormatField(Session("CreateLoadSKU")))
        'Dim dtSKU As New DataTable
        'Made4Net.DataAccess.DataInterface.FillDataset(sql, dtSKU)
        'Dim MatchingUOM As String = ""
        'If dtSKU.Rows.Count = 0 Then
        '    Made4Net.Mobile.MessageQue.Enqueue(t.Translate("SKU does not exist"))
        '    Return
        'Else
        '    MatchingUOM = dtSKU.Rows(0)("DEFAULTUOM")
        'End If

        'Dim Weightsql As String = String.Format("select * from skuuom where consignee={0} and uom={2} and sku={1}", Made4Net.Shared.FormatField(Session("CreateLoadConsignee")), _
        '                                                                                                            Made4Net.Shared.FormatField(Session("CreateLoadSKU")), _
        '                                                                                                            Made4Net.Shared.FormatField(MatchingUOM))
        'Dim dtWeight As New DataTable
        'Made4Net.DataAccess.DataInterface.FillDataset(Weightsql, dtWeight)
        'If dtWeight.Rows.Count = 0 Then
        '    Made4Net.Mobile.MessageQue.Enqueue(t.Translate("No weight has been configured for this product in master data"))
        '    Return
        'Else
        '    'Dim rowsFound1 As DataRow()
        '    Dim matchingWeight As Double = dtWeight.Rows(0)("GROSSWEIGHT")

        '    'Need to expand on this as it currently does not use tolerance params
        '    If (matchingWeight <> Weight) Then
        '        Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Weight not ok"))
        '        Return
        '    End If
        'End If

        'If DO1.Value("UNITS") = "" Then
        '    MessageQue.Enqueue(t.Translate("Units can not be blank"))
        '    Return
        'End If
        'CLD1
        Dim oCon As New WMS.Logic.Consignee(Session("CreateLoadConsignee"))
        If Not oCon.GENERATELOADID And DO1.Value("LOADID") = "" Then
            MessageQue.Enqueue(t.Translate("Load ID can not be blank"))
            Return
        End If
        If DO1.Value("LOCATION") = "" Then
            MessageQue.Enqueue(t.Translate("Location can not be blank"))
            Return
        End If
        Session("CreateLoadLoadId") = DO1.Value("LOADID").Trim()
        Session("CreateLoadLocation") = DO1.Value("LOCATION")
        Session("CreateContainer") = "0"
        ' Handling Container 

        If DO1.Value("CONTAINERID") <> "" Then
            Dim CheckCntSql As String = "SELECT * FROM CONTAINER WHERE CONTAINER='" & DO1.Value("CONTAINERID") & "'"
            Dim dt As New DataTable
            Made4Net.DataAccess.DataInterface.FillDataset(CheckCntSql, dt)
            'Checking if container already exists, if not creating new one
            If dt.Rows.Count > 0 Then
                Session("CreateLoadContainerID") = DO1.Value("CONTAINERID")
            Else
                If DO1.Value("HUTYPE") = "" Then
                    MessageQue.Enqueue(t.Translate("HUType is empty"))
                    Return
                End If
                ' Remember it in session if there is one load
                Dim oCont As WMS.Logic.Container
                oCont = New WMS.Logic.Container
                oCont.ContainerId = DO1.Value("CONTAINERID")
                oCont.HandlingUnitType = DO1.Value("HUTYPE")
                oCont.Location = DO1.Value("LOCATION")
                oCont.Post(WMS.Logic.Common.GetCurrentUser)

                Session("CreateLoadContainerID") = DO1.Value("CONTAINERID")

            End If
        Else
            If DO1.Value("HUTYPE") <> "" Then
                ' Create new container with counter

                ' Remember it in session if there is one load
                'If DO1.Value("NUMLOADS") = 1 Then
                Dim oCont1 As WMS.Logic.Container
                oCont1 = New WMS.Logic.Container
                oCont1.ContainerId = Made4Net.Shared.getNextCounter("CONTAINER")
                oCont1.HandlingUnitType = DO1.Value("HUTYPE")
                oCont1.Location = DO1.Value("LOCATION")
                oCont1.Post(WMS.Logic.Common.GetCurrentUser)
                DO1.Value("CONTAINERID") = oCont1.ContainerId
                Session("CreateLoadContainerID") = DO1.Value("CONTAINERID")
                'Else
                '    Session("HUTYPE") = DO1.Value("HUTYPE")
                '    Session("CreateContainer") = "1"
                '    Session("CreateLoadContainerID") = ""
                'End If

            Else
                Session("CreateLoadContainerID") = ""
            End If
        End If
        'Response.Redirect(MapVirtualPath("Screens/CLD2.aspx"))
        ' CLD2
        Session("CreateLoadStatus") = DO1.Value("STATUS")
        Session("CreateLoadHoldRC") = DO1.Value("REASONCODE")
        If DO1.Ctrl("PRINTER").Visible = True Then
            Session("CreateLoadLabelPrinter") = DO1.Value("PRINTER")
        End If
        ' CLD3
        'Session("CreateLoadUOM") = DO1.Value("UOM")
        'Session("CreateLoadUnits") = DO1.Value("UNITS")
        Session("CreateLoadNumLoads") = 1 'DO1.Value("NUMLOADS")
        'Session("CreateLoadDoPutAway") = doPutAway
        SubmitCreate(ExtractAttributeValues(), doPutAway)
        'End If
    End Sub

    Private Sub doBack()
        Session.Remove("CreateLoadLocation")
        Session.Remove("CreateLoadUnits")
        Session.Remove("CreateLoadWeight")
        Session.Remove("CreateLoadSKU")
        Session.Remove("CreateLoadRCN")
        Session.Remove("CreateLoadConsignee")
        Session.Remove("CreateLoadNumLoads")
        Session.Remove("CreateContainer")
        Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRCNLine.aspx"))
    End Sub

    Private Sub doChangeLine()
        MobileUtils.ClearCreateLoadChangeLineSession()
        Session.Remove("CreateContainer")
        Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRCNLine.aspx"))
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("RECEIPT")
        DO1.AddLabelLine("RECEIPTLINE")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("SKUNOTES")
        DO1.AddTextboxLine("LOADID")
        DO1.AddTextboxLine("LOCATION")
        DO1.AddLabelLine("UNITS")
        DO1.AddLabelLine("WEIGHT")
        DO1.AddLabelLine("STATUS")
        setAttributes()
        DO1.AddTextboxLine("CONTAINERID")
        'DO1.AddTextboxLine("NUMLOADS")
        DO1.AddTextboxLine("Printer")
        'DO1.AddTextboxLine("WEIGHT")
        'DO1.AddDropDown("UOM")
        'DO1.AddDropDown("STATUS")
        DO1.AddDropDown("HUTYPE")
        DO1.AddDropDown("HUTRANS")
        DO1.AddDropDown("ReasonCode")
        DO1.AddSpacer()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "changeline"
                doChangeLine()
            Case "create&pickup"
                doNext(True)
            Case "create"
                doNext(False)
            Case "closereceipt"
                doCloseReceipt()
            Case "abort"
                doBack()
        End Select
    End Sub
    'CLD2
    Private Sub SetStatusAndPrinter()
        If Session("CreateLoadLabelPrinter") Is Nothing Or Session("CreateLoadLabelPrinter") = "" Then
            Try
                Session("CreateLoadLabelPrinter") = Convert.ToString(Made4Net.DataAccess.DataInterface.ExecuteScalar("SELECT DEFAULTPRINTER FROM LABELS WHERE LABELNAME = 'LOAD'"))
            Catch ex As Exception
            End Try
        End If
        'Dim dd As Made4Net.WebControls.MobileDropDown
        'dd = DO1.Ctrl("STATUS")
        'dd.AllOption = False
        'dd.TableName = "INVSTATUSES"
        'dd.ValueField = "CODE"
        'dd.TextField = "DESCRIPTION"
        'dd.Where = "CODE <> 'LIMBO'"
        'dd.DataBind()
        'If Not Session("CreateLoadStatus") Is Nothing Then
        '    dd.SelectedValue = Session("CreateLoadStatus")
        'Else
        '    Try
        '        Dim oRecLine As New WMS.Logic.ReceiptDetail(DO1.Value("RECEIPT"), DO1.Value("RECEIPTLINE"), True)
        '        If Not String.IsNullOrEmpty(oRecLine.INVENTORYSTATUS) Then
        '            dd.SelectedValue = oRecLine.INVENTORYSTATUS
        '        Else
        '            Dim oSku As New WMS.Logic.SKU(Session("CreateLoadConsignee"), Session("CreateLoadSKU"))
        '            dd.SelectedValue = oSku.INITIALSTATUS
        '        End If
        '    Catch ex As Exception
        '    End Try
        'End If
        'dd = DO1.Ctrl("REASONCODE")
        'dd.AllOption = True
        'dd.TableName = "CODELISTDETAIL"
        'dd.ValueField = "CODE"
        'dd.TextField = "DESCRIPTION"
        'dd.Where = "CODELISTCODE = 'INVHOLDRC'"
        'dd.DataBind()
        'If Not Session("CreateLoadHoldRC") Is Nothing Then
        '    dd.SelectedValue = Session("CreateLoadHoldRC")
        'End If
        If WMS.Logic.Consignee.AutoPrintLoadIdOnReceiving(Session("CreateLoadConsignee")) Then
            DO1.setVisibility("PRINTER", True)
            Dim prnt As String = MobileUtils.GetMHEDefaultPrinter()
            If prnt <> "" Then
                DO1.Value("PRINTER") = prnt
            Else
                DO1.Value("PRINTER") = Session("CreateLoadLabelPrinter")
            End If
        Else
            DO1.setVisibility("PRINTER", False)
        End If
    End Sub
    'CLD3
    Private Sub SetUOM()
        Dim dd As Made4Net.WebControls.MobileDropDown = DO1.Ctrl("UOM")
        dd.AllOption = False
        dd.TableName = "SKUUOMDESC"
        dd.ValueField = "UOM"
        dd.TextField = "DESCRIPTION"
        dd.Where = String.Format("CONSIGNEE = '{0}' and SKU = '{1}'", Session("CreateLoadConsignee"), Session("CreateLoadSKU"))
        dd.DataBind()
        If Not Session("CreateLoadUOM") Is Nothing Then
            dd.SelectedValue = Session("CreateLoadUOM")
        Else
            Dim dfuom As String = Made4Net.DataAccess.DataInterface.ExecuteScalar("SELECT ISNULL(DEFAULTRECUOM,'') FROM SKU WHERE CONSIGNEE = '" & Session("CreateLoadConsignee") & "' and SKU = '" & Session("CreateLoadSKU") & "'")
            Try
                If dfuom <> "" Then
                    dd.SelectedValue = dfuom
                End If
            Catch ex As Exception
            End Try
        End If
        DO1.Value("UNITS") = Session("CreateLoadUnits")
        'DO1.Value("NUMLOADS") = 1
    End Sub

    Private Sub SetHUTrans()
        Dim dd As Made4Net.WebControls.MobileDropDown = DO1.Ctrl("HUTRANS")
        dd.AllOption = False
        dd.TableName = "CODELISTDETAIL"
        dd.ValueField = "CODE"
        dd.TextField = "DESCRIPTION"
        dd.Where = String.Format("CODELISTCODE = 'ISHUTRANS'")
        dd.DataBind()
        'Dim dfuom As String = Made4Net.DataAccess.DataInterface.ExecuteScalar("SELECT ISNULL(HUTYPE,'') FROM SKU WHERE CONSIGNEE = '" & Session("CreateLoadConsignee") & "' and SKU = '" & Session("CreateLoadSKU") & "'")
        'Try
        '    If dfuom <> "" Then
        '        dd.SelectedValue = "YES"
        '    Else
        '        dd.SelectedValue = "NO"
        '    End If
        'Catch ex As Exception
        'End Try
    End Sub

    Private Sub SetHUTYpe()
        Dim dd As Made4Net.WebControls.MobileDropDown = DO1.Ctrl("HUTYPE")
        dd.AllOption = True
        dd.AllOptionText = ""
        dd.TableName = "HANDELINGUNITTYPE"
        dd.ValueField = "CONTAINER"
        dd.TextField = "CONTAINERDESC"
        dd.DataBind()
        Dim dfuom As String = Made4Net.DataAccess.DataInterface.ExecuteScalar("SELECT ISNULL(HUTYPE,'') FROM SKU WHERE CONSIGNEE = '" & Session("CreateLoadConsignee") & "' and SKU = '" & Session("CreateLoadSKU") & "'")
        Try
            dd.SelectedValue = dfuom
        Catch ex As Exception
        End Try
    End Sub
    'CLD4
    Private Sub setAttributes()
        Dim oSku As String = Session("CreateLoadSKU")
        Dim oConsignee As String = Session("CreateLoadConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If Not objSkuClass Is Nothing Then
            If objSkuClass.CaptureAtReceivingLoadAttributesCount > 0 Then
                For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
                    Dim req As Boolean = False
                    If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Then
                        req = True
                    End If
                    If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                        DO1.AddTextboxLine(oAtt.Name, oAtt.Name)
                    End If
                Next
            Else
                'SubmitCreate(Nothing, Session("CreateLoadDoPutAway"))
            End If
        Else
            'SubmitCreate(Nothing, Session("CreateLoadDoPutAway"))
        End If
    End Sub

    Private Sub SubmitCreate(ByVal oAttributes As WMS.Logic.AttributesCollection, Optional ByVal DoPutaway As Boolean = False)
        Dim ResponseMessage As String = ""
        Dim attributesarray As New ArrayList
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim ld() As WMS.Logic.Load
            Dim oRec As New Logic.Receiving
            If Session("CreateLoadNumLoads") Is Nothing Then Session("CreateLoadNumLoads") = 1
            Dim oReceiptLine As ReceiptDetail
            If Session("CreateLoadRCNMultipleLines") Then
                ld = oRec.CreateLoadFromMultipleLines(Session("CreateLoadRCN"), Session("CreateLoadSKU"), _
                    Session("CreateLoadUOM"), Session("CreateLoadLocation"), Session("CreateLoadUnits"), Session("CreateLoadStatus"), _
                    Session("CreateLoadHoldRC"), Logic.GetCurrentUser, oAttributes, Session("CreateLoadLabelPrinter"), "", "")
            Else
                oReceiptLine = New ReceiptDetail(Session("CreateLoadRCN").ToString(), CType(Session("CreateLoadRCNLine"), Integer))

                ' Check if number of loads is more than one and if we need to create containers
                ld = oRec.CreateLoad(Session("CreateLoadRCN"), Session("CreateLoadRCNLine"), Session("CreateLoadSKU"), Session("CreateLoadLoadId"), _
                    Session("CreateLoadUOM"), Session("CreateLoadLocation"), Session("CreateLoadUnits"), Session("CreateLoadStatus"), _
                    Session("CreateLoadHoldRC"), Session("CreateLoadNumLoads"), Logic.GetCurrentUser, oAttributes, Session("CreateLoadLabelPrinter"), oReceiptLine.DOCUMENTTYPE, "", "")
            End If

            ' After Load Creation we will put the loads on his container
            If Session("CreateContainer") = "1" Then
                Dim LoadsCreatedCount As Int32
                For LoadsCreatedCount = 0 To ld.Length() - 1
                    Dim cntr As New WMS.Logic.Container()
                    cntr.ContainerId = Made4Net.Shared.getNextCounter("CONTAINER")
                    cntr.HandlingUnitType = Session("HUTYPE")
                    cntr.Location = Session("CreateLoadLocation")
                    cntr.Post(WMS.Logic.Common.GetCurrentUser)

                    cntr.Place(ld(LoadsCreatedCount), WMS.Logic.Common.GetCurrentUser)
                Next
            Else
                If Session("CreateLoadContainerID") <> "" Then
                    Dim cntr As New WMS.Logic.Container(Session("CreateLoadContainerID"), True)
                    Dim LoadsCreatedCount As Int32
                    For LoadsCreatedCount = 0 To ld.Length() - 1
                        cntr.Place(ld(LoadsCreatedCount), WMS.Logic.Common.GetCurrentUser)
                    Next
                End If
            End If


            ' Create Handling unit transaction if needed
            If DO1.Value("HUTRANS") = "YES" Then
                Try
                    Dim strHUTransIns As String
                    If Session("CreateLoadContainerID") <> "" Then
                        strHUTransIns = "INSERT INTO [HANDLINGUNITTRANSACTION]([TRANSACTIONID],[TRANSACTIONTYPE],[TRANSACTIONTYPEID],[TRANSACTIONDATE],[CONSIGNEE],[ORDERID],[DOCUMENTTYPE],[COMPANY],[COMPANYTYPE],[HUTYPE],[HUQTY],[ADDDATE],[ADDUSER],[EDITDATE],[EDITUSER])" & _
                                                      "VALUES('" & Made4Net.Shared.getNextCounter("HUTRANSID") & "','RECEIPT','" & Session("CreateLoadRCN") & "',GETDATE(),'" & Session("CreateLoadConsignee") & "','" & oReceiptLine.ORDERID & "','" & oReceiptLine.DOCUMENTTYPE & "','" & oReceiptLine.COMPANY & "','" & oReceiptLine.COMPANYTYPE & "','" & DO1.Value("HUTYPE") & "','1',GETDATE(),'" & Logic.GetCurrentUser & "',GETDATE(),'" & Logic.GetCurrentUser & "')"
                    Else
                        strHUTransIns = "INSERT INTO [HANDLINGUNITTRANSACTION]([TRANSACTIONID],[TRANSACTIONTYPE],[TRANSACTIONTYPEID],[TRANSACTIONDATE],[CONSIGNEE],[ORDERID],[DOCUMENTTYPE],[COMPANY],[COMPANYTYPE],[HUTYPE],[HUQTY],[ADDDATE],[ADDUSER],[EDITDATE],[EDITUSER])" & _
                                                      "VALUES('" & Made4Net.Shared.getNextCounter("HUTRANSID") & "','RECEIPT','" & Session("CreateLoadRCN") & "',GETDATE(),'" & Session("CreateLoadConsignee") & "','" & oReceiptLine.ORDERID & "','" & oReceiptLine.DOCUMENTTYPE & "','" & oReceiptLine.COMPANY & "','" & oReceiptLine.COMPANYTYPE & "','" & DO1.Value("HUTYPE") & "','" & Session("CreateLoadNumLoads") & "',GETDATE(),'" & Logic.GetCurrentUser & "',GETDATE(),'" & Logic.GetCurrentUser & "')"
                    End If
                    DataInterface.RunSQL(strHUTransIns)
                Catch ex As Exception
                End Try
            End If

            ' If Create and Putaway then request pickup for each load
            If DoPutaway Then
                Dim pw As New Putaway
                Dim i As Int32
                For i = 0 To ld.Length() - 1
                    ld(i).RequestPickUp(Logic.GetCurrentUser)
                    'pw.RequestDestination(ld(i).LOADID)
                Next
                Response.Redirect(MapVirtualPath("Screens/RPK2.aspx?sourcescreen=cld1"))
            End If
            MessageQue.Enqueue(t.Translate("Load Created"))
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As ApplicationException
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
        DO1.Value("LOADID") = ""
        DO1.Value("UNITS") = ""
        DO1.Value("CONTAINERID") = ""
        clearAttributeValues()
        doBack()
    End Sub

    Private Function ExtractAttributeValues() As WMS.Logic.AttributesCollection
        Dim oSku As String = Session("CreateLoadSKU")
        Dim oConsignee As String = Session("CreateLoadConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If objSkuClass Is Nothing Then Return Nothing
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            Dim req As Boolean = False
            If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Dim val As Object
                Try
                    Select Case oAtt.Type
                        Case Logic.AttributeType.Boolean
                            val = CType(DO1.Value(oAtt.Name), Boolean)
                        Case Logic.AttributeType.DateTime
                            'val = CType(DO1.Value(oAtt.Name), DateTime)
                            val = DateTime.ParseExact(DO1.Value(oAtt.Name), Made4Net.Shared.AppConfig.DateFormat, Nothing)
                        Case Logic.AttributeType.Decimal
                            val = CType(DO1.Value(oAtt.Name), Decimal)
                        Case Logic.AttributeType.Integer
                            val = CType(DO1.Value(oAtt.Name), Int32)
                        Case Else
                            val = DO1.Value(oAtt.Name)
                    End Select
                    oAttCol.Add(oAtt.Name, val)
                Catch ex As Exception
                End Try
            End If
        Next
        Return oAttCol
    End Function

    Private Sub clearAttributeValues()
        Dim oSku As String = Session("CreateLoadSKU")
        Dim oConsignee As String = Session("CreateLoadConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If objSkuClass Is Nothing Then Return
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Try
                    DO1.Value(oAtt.Name) = ""
                Catch ex As Exception
                End Try
            End If
        Next
    End Sub

    Private Sub doCloseReceipt()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim oReceipt As New WMS.Logic.ReceiptHeader(Session("CreateLoadRCN"))
            oReceipt.close(WMS.Logic.Common.GetCurrentUser)
            MessageQue.Enqueue(t.Translate("Receipt Closed"))
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As ApplicationException
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
        Session.Remove("CreateLoadRCN")
        MobileUtils.ClearCreateLoadProcessSession()
        Response.Redirect(MapVirtualPath("Screens/RPK2.aspx?sourcescreen=cld1"))
    End Sub

End Class
