Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports WMS.Logic
Imports WMS.Lib
Imports WMS.MobileWebApp.apexInventory

<CLSCompliant(False)> Public Class Nav
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents nav As Made4Net.Mobile.WebCtrls.NavigationInterface
    Protected WithEvents NavigationInterface1 As Made4Net.Mobile.WebCtrls.NavigationInterface

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If APXINV_CheckIfTableExist("apx_returns_putaway_detail") Then
                If CheckIfTheresSomethingToDeposit(WMS.Logic.Common.GetCurrentUser) Then
                    Response.Redirect(MapVirtualPath("Screens/ReturnsPutAway.aspx"))
                End If
            End If

            If APXINV_CheckActiveReplens(WMS.Logic.Common.GetCurrentUser, False) = True Then
                Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
            End If
            If WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.CONTPUTAWAY) Then
                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("SCREENS/CNTPWCNF.aspx"))
            End If
            If WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.CONTLOADPUTAWAY) Then
                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("SCREENS/RPKC.aspx"))
            End If
            If WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PUTAWAY) Then
                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("SCREENS/LDPWCNF.aspx?sourcescreen=Main"))
            End If
            Session.Remove("MobileSourceScreen")
        End If
        Screen1.ShowMainMenu = True
    End Sub

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

    Private Sub NavigationInterface1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.NavigationInterface.ButtonClickEventArgs) Handles NavigationInterface1.ButtonClick
        If e.CommandText.ToLower = "logoff" Then
            WMS.Logic.WHActivity.Delete(WMS.Logic.Common.GetCurrentUser)
            Made4Net.Shared.Web.User.Logout()
            Response.Redirect(GetUrlWithParams())
        End If
        If e.CommandText.ToLower = "requesttask" Then
            'Me.Screen1.Warn("Task Requested")
            GetTask()
        End If
    End Sub

    Private Function CheckIfAnythingToDeposit(ByVal UserID As String) As DataTable
        Dim Sql As String = "select * from apx_returns_putaway_detail where UserID = '" + UserID + "' and PickUpConfirmed = 1"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)

        Return sDtLines
    End Function

    Private Function CheckIfTheresSomethingToDeposit(ByVal UserID As String) As Boolean
        Dim DT As DataTable = CheckIfAnythingToDeposit(UserID)

        If DT.Rows.Count > 0 Then
            Return True
        End If

        Return False
    End Function

    Private Function GetUrlWithParams() As String
        Dim ret, urlparams As String
        Dim skin, wh, mheid, mhtype, terminaltype As String
        skin = Session("Skin")
        wh = Session("WH")
        mheid = Session("MHEID")
        mhtype = Session("MHTYPE")
        terminaltype = Session("TERMINALTYPE")
        urlparams = "?"
        If skin <> String.Empty Then
            urlparams = urlparams & String.Format("Skin={0}&", skin)
        End If
        If wh <> String.Empty Then
            urlparams = urlparams & String.Format("WH={0}&", wh)
        End If
        If mheid <> String.Empty Then
            urlparams = urlparams & String.Format("MHEID={0}&", mheid)
        End If
        If mhtype <> String.Empty Then
            urlparams = urlparams & String.Format("MHTYPE={0}&", mhtype)
        End If
        If terminaltype <> String.Empty Then
            urlparams = urlparams & String.Format("TERMINALTYPE={0}&", terminaltype)
        End If
        ret = Made4Net.Shared.Web.MapVirtualPath("m4nScreens/Login.aspx") & urlparams.TrimEnd("&".ToCharArray())
        Return ret
    End Function

#Region "Task Manager Methods"

    Private Function RequestTask() As Task
        Dim ts As New WMS.Logic.TaskManager
        Return ts.GetTaskFromTMService(WMS.Logic.Common.GetCurrentUser)
    End Function

    Private Function CheckAssigned() As Boolean
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        Dim tm As New WMS.Logic.TaskManager
        If Logic.TaskManager.isAssigned(UserId) Then
            Dim oTask As Task
            Try
                oTask = Logic.TaskManager.getUserAssignedTask(UserId)
            Catch ex As Exception
            End Try
            If Not oTask Is Nothing Then
                Session("TMTask") = oTask
                Return True
            End If
        Else
            Return False
        End If
    End Function

    Private Sub GetTask()
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Not WMS.Logic.TaskManager.isAssigned(UserId) Then
            Try
                'Dim tm As New WMS.Logic.TaskManager
                'tm.RequestTask(UserId)
                Dim oTask As Task = RequestTask()
                'CheckAssigned()
                Response.Redirect(MapVirtualPath("Screens/TaskManager.aspx"))
            Catch ex As Made4Net.Shared.M4NException
                'MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Catch ex As Exception
                'MessageQue.Enqueue(t.Translate(ex.Message))
            End Try
        Else
            Response.Redirect(MapVirtualPath("Screens/TaskManager.aspx"))
            'Dim tm As New WMS.Logic.TaskManager
            'Dim oTask As Task = tm.getUserAssignedTask(UserId)
            'If Not oTask Is Nothing Then
            '    Session("TMTaskId") = oTask
            '    Select Case oTask.TASKTYPE.ToUpper
            '        Case WMS.Lib.TASKTYPE.CONSOLIDATION
            '            Response.Redirect(MapVirtualPath("Screens/CONS.aspx"))
            '        Case WMS.Lib.TASKTYPE.CONSOLIDATIONDELIVERY
            '            Response.Redirect(MapVirtualPath("Screens/CONS.aspx"))
            '        Case WMS.Lib.TASKTYPE.CONTCONTDELIVERY
            '            Response.Redirect(MapVirtualPath("Screens/Del.aspx"))
            '        Case WMS.Lib.TASKTYPE.CONTDELIVERY
            '            Response.Redirect(MapVirtualPath("Screens/Del.aspx"))
            '        Case WMS.Lib.TASKTYPE.CONTLOADDELIVERY
            '            Response.Redirect(MapVirtualPath("Screens/Del.aspx"))
            '        Case WMS.Lib.TASKTYPE.CONTLOADPUTAWAY
            '            Response.Redirect(MapVirtualPath("Screens/RPKC.aspx"))
            '        Case WMS.Lib.TASKTYPE.CONTPUTAWAY
            '            Response.Redirect(MapVirtualPath("Screens/RPKC.aspx"))
            '        Case WMS.Lib.TASKTYPE.COUNTING
            '            Response.Redirect(MapVirtualPath("Screens/CNTTASK.aspx"))
            '        Case WMS.Lib.TASKTYPE.DELIVERY
            '            Response.Redirect(MapVirtualPath("Screens/Del.aspx"))
            '        Case WMS.Lib.TASKTYPE.FULLPICKING
            '            Response.Redirect(MapVirtualPath("Screens/PCK.aspx"))
            '        Case WMS.Lib.TASKTYPE.HOTREPL
            '            Response.Redirect(MapVirtualPath("Screens/Repl.aspx"))
            '        Case WMS.Lib.TASKTYPE.LOADDELIVERY
            '            Response.Redirect(MapVirtualPath("Screens/Del.aspx"))
            '        Case WMS.Lib.TASKTYPE.LOADPUTAWAY
            '            Response.Redirect(MapVirtualPath("Screens/RPK.aspx"))
            '        Case WMS.Lib.TASKTYPE.NORMALREPL
            '            Response.Redirect(MapVirtualPath("Screens/Repl.aspx"))
            '        Case WMS.Lib.TASKTYPE.PARALLELPICKING
            '            Response.Redirect(MapVirtualPath("Screens/PARPCK1.aspx"))
            '        Case WMS.Lib.TASKTYPE.PARTIALPICKING
            '            Response.Redirect(MapVirtualPath("Screens/PCK.aspx"))
            '        Case WMS.Lib.TASKTYPE.PICKING
            '            Response.Redirect(MapVirtualPath("Screens/PCK.aspx"))
            '        Case WMS.Lib.TASKTYPE.PUTAWAY
            '            Response.Redirect(MapVirtualPath("Screens/RPK.aspx"))
            '        Case WMS.Lib.TASKTYPE.REPLENISHMENT
            '            Response.Redirect(MapVirtualPath("Screens/Repl.aspx"))
            '    End Select
            'End If
        End If
    End Sub

#End Region

End Class
