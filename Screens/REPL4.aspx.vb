Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports WMS.MobileWebApp.apexInventory

<CLSCompliant(False)> Public Class REPL4
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen

    Protected WithEvents ddUOM As Made4Net.WebControls.MobileDropDown
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            SetScreen()
        End If
    End Sub

    Private Sub SetScreen()
        Dim replJob As ReplenishmentJob = Session("REPLJobDetail")
        DO1.Value("TASKTYPE") = replJob.TaskType
        DO1.Value("LOCATION") = replJob.fromLocation
        DO1.Value("FROMLOADID") = replJob.fromLoad
        DO1.Value("UNITS") = replJob.Units
        DO1.Value("CONSIGNEE") = replJob.Consignee
        DO1.Value("SKU") = replJob.Sku
        DO1.Value("SKUDESC") = replJob.skuDesc
        DO1.Value("UOMUNITS") = replJob.UOMUnits

        DO1.setVisibility("UOMUNITS", False)
        DO1.setVisibility("ITEM", False)
        DO1.setVisibility("CONS", False)
        DO1.setVisibility("LOADID", False)
        DO1.setVisibility("LOC", False)
    End Sub

    Private Sub doNext()
        If Not CheckLoadId() Then
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            MessageQue.Enqueue(trans.Translate("Entered Quantity Does Not Match Required Quantity"))
        Else
            ListPartialReplen()
        End If
    End Sub

    Private Sub ListPartialReplen()
        Dim units As Integer = DO1.Value("CUNITS")
        Dim replJob As ReplenishmentJob = Session("REPLJobDetail")

        APXINV_QueueReplen(Logic.GetCurrentUser, replJob.Replenishment, units)

        Session.Remove("REPLTSKTaskId")
        Session.Remove("REPLTSKTDetail")
        Session.Remove("REPLOCPICKTYPE")
        Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
    End Sub

    Private Function CheckLoadId() As Boolean
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim units As Integer = DO1.Value("CUNITS")

        Dim replJob As ReplenishmentJob = Session("REPLJobDetail")
        Try
            If units <> replJob.Units Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return False
        End Try

    End Function

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("TASKTYPE")
        DO1.AddLabelLine("FROMLOADID", "LOADID")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOCATION")
        'DO1.AddLabelLine("UOMUNITS")
        DO1.AddLabelLine("UNITS")
        DO1.AddSpacer()
        DO1.AddTextboxLine("LOADID")
        DO1.AddTextboxLine("CONS", "CONSIGNEE")
        DO1.AddTextboxLine("ITEM", "Scan SKU")
        DO1.AddTextboxLine("CUNITS", "Enter Number Collected")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
        End Select
    End Sub

End Class
