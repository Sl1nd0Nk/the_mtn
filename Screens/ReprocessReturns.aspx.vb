Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports WMS.MobileWebApp.apexInbound
Imports WMS.MobileWebApp.apexSerial
Imports System.Collections.Generic

<CLSCompliant(False)> Public Class ReprocessReturns
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim flag As Boolean = Not IsPostBack
        If flag Then
            Dim flag2 As Boolean = Not apexInventory.APXINV_CheckIfTableExist("apx_separation_station")
            If flag2 Then
                apexInventory.APXINV_CreateTable("apx_separation_station", New List(Of String)() From {"Location NVARCHAR (50) NULL", "LoadID NVARCHAR (50) NULL", "UserID NVARCHAR (50) NULL", "DestLoadID NVARCHAR (50) NULL", "LastSerScanned NVARCHAR (50) NULL", "FailDeposit NVARCHAR (50) NULL"})
            End If
            Dim Dt As DataTable = apexInventory.APXINV_GetFailRTLocInProgress(WMS.Logic.Common.GetCurrentUser())
            flag2 = (Dt.Rows.Count > 0)
            If flag2 Then
                flag = (Not IsDBNull(Dt.Rows(0)("Process")) AndAlso Not String.IsNullOrEmpty(CStr(Dt.Rows(0)("Process"))))
                If flag Then
                    Dim Dts As DataTable = apexInventory.APXINV_GetSucessFullSerialsForLoad(CStr(Dt.Rows(0)("LoadID")))
                    flag2 = (Dts.Rows.Count <= 0)
                    If flag2 Then
                        Session.Add("SEPLD", Dt.Rows(0)("LoadID"))
                        flag2 = Not IsDBNull(Dt.Rows(0)("DestLoadID"))
                        If flag2 Then
                            Session.Add("SEPDESTLOAD", Dt.Rows(0)("DestLoadID"))
                        End If
                        flag2 = Not IsDBNull(Dt.Rows(0)("FailDeposit"))
                        If flag2 Then
                            Session.Add("FDEP", Dt.Rows(0)("FailDeposit"))
                            Session.Add("SEPDESTLOADID", Dt.Rows(0)("DestLoadID"))
                            Dim LD As Load = New Load(CStr(Dt.Rows(0)("LoadID")), True)
                            Session.Add("SEPCURLD", LD)
                        End If
                    Else
                        apexInventory.APXINV_ClearLocFromStation("", CStr(Dt.Rows(0)("LoadID")), "")
                        Session.Remove("SEPLD")
                    End If
                Else
                    Session.Remove("SEPLD")
                End If
            Else
                Session.Remove("SEPLD")
            End If
            flag2 = (Session("SEPLD") Is Nothing)
            If flag2 Then
                Dim loadID As String = GetNewLoc()
                flag2 = Not String.IsNullOrEmpty(loadID)
                If flag2 Then
                    Session.Add("SEPLD", loadID)
                    Session.Add("NEW", True)
                Else
                    Session.Remove("SEPLD")
                End If
            End If
        End If
        WhereToNext()
    End Sub

    Private Function GetNewLoc() As String
        Return apexInventory.APXINV_GetRepRTNextFailedLoc()
    End Function

    Private Sub InitControls()
        DO1.setVisibility("LOADID", False)
        DO1.setVisibility("SKU", False)
        DO1.setVisibility("SKUDESC", False)
        DO1.setVisibility("LOCATION", False)
        DO1.setVisibility("Msg", False)
        DO1.setVisibility("SEPLOC", False)
        DO1.setVisibility("SEPSKU", False)
        DO1.setVisibility("SEPSER", False)
        DO1.setVisibility("SEPREASON", False)
        DO1.setVisibility("SEPFLOC", False)
        DO1.setVisibility("FAILDEST", False)
    End Sub

    Private Sub InitButtons()
        DO1.Button(0).Text = "Next"
        DO1.Button(1).Visible = False
        DO1.Button(2).Visible = False
        DO1.Button(3).Visible = False
    End Sub

    Private Sub WhereToNext()
        InitControls()
        InitButtons()
        Dim flag As Boolean = Session("GoBack") IsNot Nothing
        If flag Then
            Dim flag2 As Boolean = Session("SEPCURLD") IsNot Nothing
            If flag2 Then
                DO1.Button(0).Text = "Back"
                DO1.Button(2).Text = "Reset Process"
                DO1.Button(3).Text = "Cancel"
                DO1.Button(2).Visible = True
                DO1.Button(3).Visible = True
            Else
                doBack()
            End If
        Else
            Dim flag2 As Boolean = Session("SEPDESTLOADID") IsNot Nothing
            If flag2 Then
                Dim LD As Load = CType(Session("SEPCURLD"), Load)
                Dim dst As String = CStr(Session("SEPDESTLOADID"))
                If dst.ToUpper() = "NONE" Then
                    Dim NewDestLoad As String = FindLoadId(LD.SKU)
                    If String.IsNullOrEmpty(NewDestLoad) Or NewDestLoad.ToUpper() = "NONE" Then
                        Dim ldId As String = WMS.Logic.Load.GenerateLoadId()
                        Session.Remove("SEPDESTLOADID")
                        Session.Add("SEPDESTLOADID", ldId)
                        UpdateDestLoad(ldId, LD.LOADID, WMS.Logic.Common.GetCurrentUser())
                    End If
                End If
                flag2 = (Session("FDEP") IsNot Nothing)
                If flag2 Then
                    ShowSerProcess(LD, CStr(Session("SEPDESTLOADID")))
                Else
                    DO1.setVisibility("Msg", True)
                    flag2 = WMS.Logic.Load.Exists(CStr(Session("SEPDESTLOADID")))
                    If flag2 Then
                        Dim DLD As Load = New Load(CStr(Session("SEPDESTLOADID")), True)
                        DO1.Value("Msg") = "Move Failed Serials to Location " + DLD.LOCATION
                    Else
                        DO1.Value("Msg") = "Find Empty Location to move Failed serial number into"
                        Session.Add("EmptyLoc", True)
                    End If
                    DO1.setVisibility("SEPFLOC", True)
                    DO1.FocusField = "SEPFLOC"
                End If
            Else
                flag2 = (Session("SerialView") IsNot Nothing)
                If flag2 Then
                    DO1.setVisibility("Msg", True)
                    DO1.Value("Msg") = GetScannedSerialNumbers()
                    DO1.Button(0).Text = "Close"
                    DO1.Button(1).Visible = False
                    DO1.Button(2).Visible = False
                    DO1.Button(3).Visible = False
                Else
                    flag2 = (Session("SEPLD") IsNot Nothing)
                    If flag2 Then
                        Dim LD2 As Load = New Load(CStr(Session("SEPLD")), True)
                        Session.Remove("SEPLD")
                        Session.Add("SEPLOADID", LD2.LOADID)
                        flag2 = (Session("NEW") IsNot Nothing)
                        If flag2 Then
                            ShowNewLocProcess(LD2)
                        Else
                            Session.Add("SEPCURLD", LD2)
                            ShowSerProcess(LD2, "")
                        End If
                    Else
                        flag2 = (Session("SEPCURLD") IsNot Nothing)
                        If flag2 Then
                            Dim LD3 As Load = CType(Session("SEPCURLD"), Load)
                            flag2 = (Session("SEPDESTLOAD") Is Nothing)
                            If flag2 Then
                                Dim DestLdId As String = FindLoadId(LD3.SKU)
                                Session.Add("SEPDESTLOAD", DestLdId)
                                UpdateDestLoad(DestLdId, LD3.LOADID, WMS.Logic.Common.GetCurrentUser())
                            End If
                            ShowSerProcess(LD3, "")
                        Else
                            Dim Dt As DataTable = apexInventory.APXINV_GetFailRTLocInProgress(WMS.Logic.Common.GetCurrentUser())
                            flag2 = (Dt.Rows.Count <= 0 OrElse (Dt.Rows.Count > 0 And (IsDBNull(Dt.Rows(0)("Process")) OrElse String.IsNullOrEmpty(CStr(Dt.Rows(0)("Process"))))))
                            If flag2 Then
                                flag = (Session("SEPLOCCONFIRMED") IsNot Nothing)
                                If flag Then
                                    ShowScanSkuProcess()
                                Else
                                    flag2 = (Session("SEPLOADID") IsNot Nothing)
                                    If flag2 Then
                                        Dim LD4 As Load = New Load(CStr(Session("SEPLOADID")), True)
                                        ShowNewLocProcess(LD4)
                                    Else
                                        Dim loadID As String = GetNewLoc()
                                        flag2 = Not String.IsNullOrEmpty(loadID)
                                        If flag2 Then
                                            Session.Add("SEPLOADID", loadID)
                                            Dim LD5 As Load = New Load(loadID, True)
                                            ShowNewLocProcess(LD5)
                                        Else
                                            ShowNoWorkProcess()
                                        End If
                                    End If
                                End If
                            Else
                                Dim LD6 As Load = New Load(CStr(Dt.Rows(0)("LoadID")), True)
                                Session.Add("SEPDESTLOAD", Dt.Rows(0)("DestLoadID"))
                                ShowSerProcess(LD6, "")
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub UpdateDestLoad(DestLoadID As String, LoadID As String, UserID As String)
        Dim Sql As String = String.Concat(New String() {"update apx_separation_station set DestLoadID= '", DestLoadID, "' where LoadID = '", LoadID, "' and UserID= '", UserID, "'"})
        Made4Net.DataAccess.DataInterface.RunSQL(Sql)
    End Sub

    Private Sub UpdateFailDepProcess(loc As String, LoadID As String, UserID As String)
        Dim Sql As String = String.Concat(New String() {"update apx_separation_station set FailDeposit='", loc, "' where LoadID = '", LoadID, "' and UserID= '", UserID, "'"})
        Made4Net.DataAccess.DataInterface.RunSQL(Sql)
    End Sub

    Private Sub ShowNoWorkProcess()
        DO1.setVisibility("Msg", True)
        DO1.Value("Msg") = "No work"
        DO1.Button(2).Visible = True
    End Sub

    Private Sub ShowScanSkuProcess()
        DO1.setVisibility("LOCATION", True)
        DO1.Value("LOCATION") = CStr(Session("SEPLOCCONFIRMED"))
        DO1.setVisibility("SEPSKU", True)
        DO1.FocusField = "SEPSKU"
        DO1.Button(2).Visible = True
    End Sub

    Private Sub ShowSerProcess(load As Load, Optional FLoadID As String = "")
        Dim flag As Boolean = Not String.IsNullOrEmpty(FLoadID)
        Dim flag2 As Boolean
        If flag Then
            flag2 = WMS.Logic.Load.Exists(FLoadID)
            Dim Loc As String
            If flag2 Then
                Dim LD As Load = New Load(FLoadID, True)
                Loc = LD.LOCATION
            Else
                Loc = CStr(Session("FDEP"))
            End If
            DO1.setVisibility("FAILDEST", True)
            DO1.Value("FAILDEST") = Loc
        Else
            DO1.Button(1).Visible = True
        End If
        DO1.setVisibility("LOCATION", True)
        DO1.setVisibility("LOADID", True)
        DO1.setVisibility("SKU", True)
        DO1.setVisibility("SKUDESC", True)
        DO1.setVisibility("Msg", True)
        DO1.setVisibility("SEPSER", True)
        DO1.Value("LOCATION") = load.LOCATION
        DO1.Value("LOADID") = load.LOADID
        DO1.Value("SKU") = load.SKU
        Dim SkuDesc As String = New SKU(load.CONSIGNEE, load.SKU, True).SKUSHORTDESC
        DO1.Value("SKUDESC") = SkuDesc
        DO1.Value("Msg") = GetLastScannedSerial(load.LOADID)
        flag2 = (Session("SepReason") IsNot Nothing)
        If flag2 Then
            DO1.setVisibility("SEPREASON", True)
            DO1.Value("SEPREASON") = CStr(Session("SepReason"))
            Session.Remove("SepReason")
        End If
        DO1.FocusField = "SEPSER"
        DO1.Button(2).Visible = True
        DO1.Button(3).Visible = True
    End Sub

    Private Sub ShowNewLocProcess(load As Load)
        Session.Remove("NEW")
        DO1.setVisibility("Msg", True)
        DO1.setVisibility("SEPLOC", True)
        DO1.FocusField = "SEPLOC"
        Session.Add("SEPCMPLOC", load.LOCATION)
        DO1.Value("Msg") = "Proceed to location " + load.LOCATION
        DO1.Button(2).Visible = True
    End Sub

    Private Function GetLastScannedSerial(loadID As String) As String
        Dim Value As String = ""
        Dim Sql As String = "select LastSerScanned from apx_separation_station where LoadID = '" + loadID + "'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)
        Dim flag As Boolean = sDtLines.Rows.Count > 0
        If flag Then
            Dim flag2 As Boolean = Not IsDBNull(sDtLines.Rows(0)("LastSerScanned"))
            If flag2 Then
                Value = CStr(sDtLines.Rows(0)("LastSerScanned"))
            End If
        End If
        Return Value
    End Function

    Private Function GetFailedDestLoad(loadID As String) As String
        Dim Value As String = ""
        Dim Sql As String = "select DestLoadID from apx_separation_station where LoadID = '" + loadID + "'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)
        Dim flag As Boolean = sDtLines.Rows.Count > 0
        If flag Then
            Dim flag2 As Boolean = Not IsDBNull(sDtLines.Rows(0)("DestLoadID"))
            If flag2 Then
                Value = CStr(sDtLines.Rows(0)("DestLoadID"))
            End If
        End If
        Return Value
    End Function

    Private Sub AddLastScannedSerial(loadID As String, UserID As String, Lastscanned As String)
        Dim Sql As String = String.Concat(New String() {"update apx_separation_station set LastSerScanned= '", Lastscanned, "' where LoadID = '", loadID, "' and UserID= '", UserID, "'"})
        Made4Net.DataAccess.DataInterface.RunSQL(Sql)
    End Sub

    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim flag As Boolean = Not String.IsNullOrEmpty(DO1.Value("SEPLOC"))
        If flag Then
            If DO1.Value("SEPLOC") = Session("SEPCMPLOC") Then
                Session.Remove("SEPCMPLOC")
                Session.Add("SEPLOCCONFIRMED", DO1.Value("SEPLOC"))
            Else
                MessageQue.Enqueue(t.Translate("Scanned Location does not match the requested location"))
            End If
            DO1.Value("SEPLOC") = ""
        Else
            Dim flag2 As Boolean = Not String.IsNullOrEmpty(DO1.Value("SEPSKU"))
            If flag2 Then
                Dim cmpSKU As String = GetSKU(DO1.Value("SEPSKU"))
                Dim LD As Load = New Load(CStr(Session("SEPLOADID")), True)
                If cmpSKU = LD.SKU Then
                    Session.Add("SEPCURLD", LD)
                    apexInventory.APXINV_CreateSeparationStnData(CStr(Session("SEPLOCCONFIRMED")), CStr(Session("SEPLOADID")), WMS.Logic.Common.GetCurrentUser(), "REPROCESS")
                    Session.Remove("SEPLOCCONFIRMED")
                Else
                    MessageQue.Enqueue(t.Translate("Incorrect Product scanned" + cmpSKU + " != " + LD.SKU))
                End If
                DO1.Value("SEPSKU") = ""
            Else
                flag2 = Not String.IsNullOrEmpty(DO1.Value("SEPSER"))
                If flag2 Then
                    Dim Dt As DataTable = apexSerial.APXSERIAL_GetSerialDetailBySerial(DO1.Value("SEPSER"))
                    flag2 = (Dt.Rows.Count > 0)
                    If flag2 Then
                        If Dt.Rows(0)("loadID") = Session("SEPLOADID") Then
                            Dim flag3 As Boolean = Not IsDBNull(Dt.Rows(0)("ReturnStatus"))
                            If flag3 Then
                                Dim flag4 As Boolean = CInt(Dt.Rows(0)("ReturnStatus")) = 0
                                If flag4 Then
                                    Session.Add("SepReason", "Serial " + DO1.Value("SEPSER") + " marked for reprocess")
                                Else
                                    flag4 = (CInt(Dt.Rows(0)("ReturnStatus")) = 1)
                                    If flag4 Then
                                        MessageQue.Enqueue(t.Translate("Scanned Serial Number is not FAILED"))
                                    Else
                                        flag4 = (CInt(Dt.Rows(0)("ReturnStatus")) = -1)
                                        If flag4 Then
                                            MessageQue.Enqueue(t.Translate("Scanned Serial Number is not yet validated by ERP"))
                                        Else
                                            MessageQue.Enqueue(t.Translate("No return status set on the scanned serialnumber"))
                                        End If
                                    End If
                                End If
                                UpdateSerialReturnWithLoadID(CStr(Dt.Rows(0)("serial")), CStr(Dt.Rows(0)("loadID")))
                                AddLastScannedSerial(CStr(Session("SEPLOADID")), WMS.Logic.Common.GetCurrentUser(), CStr("Last Scanned Serial: " & Dt.Rows(0)("serial")))
                            Else
                                MessageQue.Enqueue(t.Translate("No return status found on this serial number"))
                            End If
                        Else
                            MessageQue.Enqueue(t.Translate("Scanned Serial Number is should not be in this load " & Session("SEPLOADID")))
                        End If
                    Else
                        MessageQue.Enqueue(t.Translate("Scanned Serial Number is not found on the system"))
                    End If
                    DO1.Value("SEPSER") = ""
                End If
            End If
        End If
        WhereToNext()
    End Sub

    Private Sub UpdateSerialReturnWithLoadID(serialNumber As String, loadID As String)
        Dim SQL As String = String.Format("Update apx_serial_returns set loadID={0}, reprocess={1} where serial={2}", Made4Net.Shared.FormatField(loadID), Made4Net.Shared.FormatField(loadID, "NULL", False), Made4Net.Shared.FormatField(serialNumber, "NULL", False))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Sub UpdateScannedSerial(notes As String, LoadID As String, id As Integer)
        Dim Reason As String = SplitNotes("FAILREASON", notes)
        Dim flag As Boolean = Not String.IsNullOrEmpty(Reason)
        If flag Then
            Reason = "FAILREASON:" + Reason
        Else
            Reason = ""
        End If
        Dim SQL As String = String.Format("Update apx_serial set loadID={0}, notes={1} where id={2}", Made4Net.Shared.FormatField(LoadID), Made4Net.Shared.FormatField(Reason, "NULL", False), Made4Net.Shared.FormatField(id, "NULL", False))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Function GetSKU(sku As String) As String
        Dim SQL As String = String.Format("SELECT SKU FROM vSKUCODE WHERE SKUCODE = {0}", Made4Net.Shared.FormatField(sku))
        Return CStr(Made4Net.DataAccess.DataInterface.ExecuteScalar(SQL))
    End Function

    Private Function SplitNotes(Keyword As String, Input As String) As String
        Dim Notes As String() = Input.Split(New Char() {"|"c})
        Dim keyFound As Boolean = False
        Dim value As String = ""
        Dim array As String() = Notes
        ' The following expression was wrapped in a checked-statement
        For i As Integer = 0 To array.Length - 1
            Dim note As String = array(i)
            Dim KVPs As String() = note.Split(New Char() {":"c})
            Dim array2 As String() = KVPs
            Dim flag As Boolean
            For j As Integer = 0 To array2.Length - 1
                Dim key As String = array2(j)
                flag = keyFound
                If flag Then
                    value = key
                End If
                If key = Keyword Then
                    keyFound = True
                End If
            Next
            flag = Not String.IsNullOrEmpty(value)
            If flag Then
                Exit For
            End If
        Next
        Return value
    End Function

    Private Function FindLoadId(sku As String) As String
        Dim Value As String = ""
        Dim Sql As String = "select loadId from LOADATTRIBUTES where harvest=0 and sku='" + sku + "' and location is not null and location <> ''"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)
        Dim flag As Boolean = sDtLines.Rows.Count <= 0
        If flag Then
            Value = "NONE"
        Else
            Try
                Dim enumerator As IEnumerator = sDtLines.Rows.GetEnumerator()
                While enumerator.MoveNext()
                    Dim dr As DataRow = CType(enumerator.Current, DataRow)
                    Sql = String.Format("select * from apx_serial where loadID={0} and ReturnStatus is not null and ReturnStatus<>0", Made4Net.Shared.FormatField(dr("loadId")))
                    Dim DtLines As DataTable = New DataTable()
                    Made4Net.DataAccess.DataInterface.FillDataset(Sql, DtLines, False, Nothing)
                    flag = (DtLines.Rows.Count <= 0)
                    If flag Then
                        Value = CStr(dr("loadId"))
                        Exit While
                    End If
                End While
            Catch ex As Exception
                Throw ex
            End Try
            flag = String.IsNullOrEmpty(Value)
            If flag Then
                Value = "NONE"
            End If
        End If
        Return Value
    End Function

    Private Sub UpdateSerialNotes(id As Integer, Notes As String)
        Dim Sql As String = String.Concat(New String() {"update apx_serial set notes='", Notes, "' where id=", CStr(id), ""})
        Made4Net.DataAccess.DataInterface.RunSQL(Sql)
    End Sub

    Private Sub doBack()
        RemoveSessions()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Function ExtractAttributeValues(sku As String, Consignee As String) As WMS.Logic.AttributesCollection
        Dim objSkuClass As SkuClass = New SKU(Consignee, sku, True).SKUClass
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            Dim req As Boolean = False
            If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Dim val As Object
                Try
                    Select Case oAtt.Type
                        Case Logic.AttributeType.Boolean
                            val = CType(DO1.Value(oAtt.Name), Boolean)
                        Case Logic.AttributeType.DateTime
                            val = CType(DO1.Value(oAtt.Name), DateTime)
                        Case Logic.AttributeType.Decimal
                            val = CType(DO1.Value(oAtt.Name), Decimal)
                        Case Logic.AttributeType.Integer
                            val = CType(DO1.Value(oAtt.Name), Int32)
                        Case Else
                            val = DO1.Value(oAtt.Name)
                    End Select
                    oAttCol.Add(oAtt.Name, val)
                Catch ex As Exception

                End Try
            End If
        Next

        oAttCol.Add("HARVEST", 0)
        Return oAttCol
    End Function

    Private Sub UpdateScannedSerialsToReprocess(loadID As String)
        Dim SQL As String = String.Format("Update apx_serial_returns set reprocess={0} where reprocess={1}", Made4Net.Shared.FormatField("YES", "NULL", False), Made4Net.Shared.FormatField(loadID, "NULL", False))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Sub doComplete()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim flag As Boolean = Session("SEPCURLD") IsNot Nothing
        If flag Then
            Dim LD As Load = CType(Session("SEPCURLD"), Load)
            UpdateScannedSerialsToReprocess(LD.LOADID)
            apexInventory.APXINV_ClearLocFromStation("", LD.LOADID, "")
            RemoveSessions()
        End If
        WhereToNext()
    End Sub

    Private Sub RemoveSessions()
        Session.Remove("SEPCMPLOC")
        Session.Remove("SEPLOADID")
        Session.Remove("NEW")
        Session.Remove("SEPLD")
        Session.Remove("SEPLOCCONFIRMED")
        Session.Remove("GoBack")
        Session.Remove("SEPDESTLOAD")
        Session.Remove("SEPDESTLOADID")
        Session.Remove("FDEP")
        Session.Remove("SEPCURLD")
    End Sub

    Private Function GetLoadLowestStatus(loadID As String) As Integer
        Dim IValue As Integer = -1
        Dim Sql As String = "select MIN(ReturnStatus) As LowestStatus from apx_serial where loadID = '" + loadID + "' and ReturnStatus is not null"
        Dim Value As String = CStr(Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql))
        Dim flag As Boolean = Not String.IsNullOrEmpty(Value)
        If flag Then
            IValue = CInt(Value)
        End If
        Return IValue
    End Function

    Private Function GetFailedSerialToMove(LoadID As String) As DataTable
        Dim SQL As String = String.Format("SELECT * FROM apx_serial WHERE loadID={0} and notes like '%{1}%'", Made4Net.Shared.FormatField(LoadID, "NULL", False), "DESTLOAD")
        Dim dtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines, False, Nothing)
        Return dtLines
    End Function

    Private Sub doShowSerials()
        Session.Add("SerialView", True)
        WhereToNext()
    End Sub

    Private Sub doClose()
        Session.Remove("SerialView")
        Session.Remove("GoBack")
        DO1.Button(0).Text = "Next"
        DO1.Button(1).Text = "View Scanned Failed Serials"
        DO1.Button(2).Text = "Back"
        DO1.Button(3).Text = "Complete"
        DO1.Value("Msg") = ""
        WhereToNext()
    End Sub

    Private Sub doCancelProcess()
        Session.Remove("GoBack")
        Dim ld As Load = CType(Session("SEPCURLD"), Load)
        Dim dtLines As DataTable = GetScannedFailedSer(ld.LOADID)
        Dim flag As Boolean = dtLines.Rows.Count > 0
        If flag Then
            Try
                Dim enumerator As IEnumerator = dtLines.Rows.GetEnumerator()
                While enumerator.MoveNext()
                    Dim dr As DataRow = CType(enumerator.Current, DataRow)
                    Dim strReason As String = SplitNotes("FAILREASON", CStr(dtLines.Rows(0)("notes")))
                    strReason = "FAILREASON:" + strReason
                    UpdateSerialNotes(CInt(dtLines.Rows(0)("id")), strReason)
                End While
            Catch ex As Exception
                Throw ex
            End Try
        End If
        apexInventory.APXINV_ClearLocFromStation("", ld.LOADID, "")
        doBack()
    End Sub

    Private Sub doBackOrCancel()
        Session.Add("GoBack", True)
        WhereToNext()
    End Sub

    Private Function GetScannedFailedSer(LoadID As String) As DataTable
        Dim SQL As String = String.Format("SELECT * FROM apx_serial_returns WHERE reprocess={0}", Made4Net.Shared.FormatField(LoadID, "NULL", False))
        Dim dtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines, False, Nothing)
        Return dtLines
    End Function

    Private Function GetScannedSerialNumbers() As String
        Dim flag As Boolean = Session("SEPCURLD") IsNot Nothing
        ' The following expression was wrapped in a checked-statement
        Dim ReturnValue As String
        If flag Then
            Dim ld As Load = CType(Session("SEPCURLD"), Load)
            Dim dtLines As DataTable = GetScannedFailedSer(ld.LOADID)
            flag = (dtLines.Rows.Count > 0)
            If flag Then
                Dim StartTable As String = "<table>"
                Dim EndTable As String = "</table>"
                Dim StartRow As String = "<tr>"
                Dim EndRow As String = "</tr>"
                Dim x As Integer = 1
                Try
                    Dim enumerator As IEnumerator = dtLines.Rows.GetEnumerator()
                    While enumerator.MoveNext()
                        Dim dr As DataRow = CType(enumerator.Current, DataRow)
                        flag = (x > 3)
                        If flag Then
                            x = 1
                            StartTable += EndRow
                        End If
                        flag = (x = 1)
                        If flag Then
                            StartTable += StartRow
                        End If
                        StartTable = StartTable + "<td>" & dr("serial") & "</td><td>&nbsp&nbsp</td>"
                        x += 1
                    End While
                Catch ex As Exception
                    Throw ex
                End Try
                ReturnValue = StartTable + EndTable
            Else
                ReturnValue = ""
            End If
        Else
            ReturnValue = ""
        End If
        Return ReturnValue
    End Function

    Private Sub DO1_CreatedChildControls(sender As Object, e As EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("LOADID")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOCATION")
        DO1.AddLabelLine("FAILDEST", "Failed Serial Location")
        DO1.AddSpacer()
        DO1.AddLabelLine("Msg", "")
        DO1.AddSpacer()
        DO1.AddLabelLine("SEPREASON", "")
        DO1.AddTextboxLine("SEPLOC", "Scan Location")
        DO1.AddTextboxLine("SEPFLOC", "Scan Location")
        DO1.AddTextboxLine("SEPSKU", "Scan Product")
        DO1.AddTextboxLine("SEPSER", "Scan Serial Number")
    End Sub

    Private Sub DO1_ButtonClick(sender As Object, e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Dim left As String = e.CommandText.ToLower()
        If left = "next" Then
            Dim flag2 As Boolean = Session("SerialView") IsNot Nothing
            If flag2 Then
                doClose()
            Else
                flag2 = (Session("GoBack") IsNot Nothing)
                If flag2 Then
                    doBack()
                Else
                    doNext()
                End If
            End If
        Else
            If left = "back" Then
                If Session("GoBack") IsNot Nothing Then
                    doCancelProcess()
                Else
                    doBackOrCancel()
                End If
            Else
                If left = "complete" Then
                    If Session("GoBack") IsNot Nothing Then
                        doClose()
                    Else
                        doComplete()
                    End If
                Else
                    If left = "view scanned failed serials" Then
                        doShowSerials()
                    End If
                End If
            End If
        End If
    End Sub

End Class
