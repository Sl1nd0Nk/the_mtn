Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

<CLSCompliant(False)> Partial Class LOCCONTTASK2
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Dim ld As New WMS.Logic.Load(Convert.ToString(Session("LocationCNTLoadId")))

            Dim oSKU As SKU = New SKU(ld.CONSIGNEE, ld.SKU)

            'Dim ClassName As String = oSKU.SKUClass.ClassName

            'If ClassName.ToUpper() = "SERIAL" Then
            '    Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK2SER.aspx"))
            '    Return
            'End If

            DO1.Value("CONSIGNEE") = ld.CONSIGNEE
            DO1.Value("SKU") = ld.SKU
            If String.IsNullOrEmpty(Session("ToLocationForVerification")) Then
                DO1.Value("LOCATION") = ld.LOCATION
            Else
                DO1.Value("LOCATION") = Session("ToLocationForVerification")
            End If
            'DO1.Value("LOCATION") = ld.LOCATION
            DO1.Value("LOADID") = ld.LOADID

            Dim ctrl As Made4Net.WebControls.MobileDropDown = DO1.Ctrl("UOM")

            ctrl.AllOption = False
            ctrl.TableName = "SKUUOMDESC"
            ctrl.ValueField = "UOM"
            ctrl.TextField = "DESCRIPTION"
            ctrl.Where = String.Format("CONSIGNEE = '{0}' and SKU = '{1}'", ld.CONSIGNEE, ld.SKU)
            ctrl.DataBind()

            Try
                ctrl.SelectedValue = ld.LOADUOM
            Catch ex As Exception
            End Try
            Try
                DO1.Value("SKUDESC") = Made4Net.DataAccess.DataInterface.ExecuteScalar(String.Format("select skudesc from sku where sku = '{0}' and consignee = '{1}'", ld.SKU, ld.CONSIGNEE))
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub doNext()
        Dim CountOk As Boolean = False
        'If Page.IsValid Then
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim ld As New WMS.Logic.Load(Convert.ToString(Session("LocationCNTLoadId")))
            'ld.Count(DO1.Value("TOUNITS"), DO1.Value("UOM"), ld.LOCATION, WMS.Logic.GetCurrentUser)
            Dim oCntTask As WMS.Logic.CountTask
            If Not Session("LocationBulkCountTask") Is Nothing Then
                oCntTask = Session("LocationBulkCountTask")
            Else
                oCntTask = Session("LocationCountTask")
            End If
            Dim oCounting As New WMS.Logic.Counting(oCntTask.COUNTID)
            'Build and fill count job object
            Dim oCountJob As WMS.Logic.CountingJob = oCntTask.getCountJob(oCounting)
            oCountJob.CountedQty = DO1.Value("TOUNITS")
            oCountJob.UOM = DO1.Value("UOM")
            'oCountJob.Location = ld.LOCATION
            If String.IsNullOrEmpty(Session("ToLocationForVerification")) Then
                oCountJob.Location = ld.LOCATION
            Else
                oCountJob.Location = Session("ToLocationForVerification")
            End If
            oCountJob.LoadId = Session("LocationCNTLoadId")
            oCountJob.LoadCountVerified = False
            oCountJob.LocationCountingLoadCounted = True
            Dim oAttrCol As AttributesCollection = ExtractAttributeValues()
            oCountJob.CountingAttributes = oAttrCol
            CountOk = oCntTask.Count(oCounting, oCountJob, WMS.Logic.GetCurrentUser)
            If Not CountOk Then
                MessageQue.Enqueue(t.Translate("The difference between conuted units and load units are greater that counting tolerance. Please Confirm"))
                Session("CountedUnitsForVerification") = oCountJob.CountedQty
                Session("UOMForVerification") = oCountJob.UOM
                Session("AttributesForVerification") = oCountJob.CountingAttributes
            End If
            UpdateLoadCount(ld.LOADID, ld.UNITS)
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
        If Request.QueryString("countsourcescreen") <> "" Then
            If Not CountOk Then
                Response.Redirect(MapVirtualPath("Screens/CNTTASKVERFICATION.aspx?SRCSCREEN=LOCCONTTASK&countsourcescreen=" & Request.QueryString("countsourcescreen")))
            Else
                Session.Remove("ToLocationForVerification")
                Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx?countsourcescreen=" & Request.QueryString("countsourcescreen")))
            End If
        Else
            If Not CountOk Then
                Response.Redirect(MapVirtualPath("Screens/CNTTASKVERFICATION.aspx?SRCSCREEN=LOCCONTTASK"))
            Else
                Session.Remove("ToLocationForVerification")
                Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx"))
            End If
        End If
        'End If
    End Sub

    Private Sub UpdateLoadCount(ByVal pLoadId As String, ByVal pToQty As Decimal)
        Dim dt As DataTable = Session("TaskLocationCNTLoadsDT")
        For Each dr As DataRow In dt.Rows
            If dr("loadid") = pLoadId Then
                dr("counted") = 1
                dr("toqty") = pToQty
            End If
        Next
        Session("TaskLocationCNTLoadsDT") = dt
    End Sub

    Private Sub doBack()
        Session.Remove("LocationCNTLoadId")
        Session.Remove("ToLocationForVerification")
        If Request.QueryString("countsourcescreen") <> "" Then
            Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx?countsourcescreen=" & Request.QueryString("countsourcescreen")))
        Else
            Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx"))
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("LOADID")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOCATION")
        DO1.AddSpacer()
        DO1.AddDropDown("UOM")
        DO1.AddTextboxLine("TOUNITS")
        setAttributes()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "back"
                doBack()
        End Select
    End Sub

    Private Sub setAttributes()
        Dim ld As New WMS.Logic.Load(Convert.ToString(Session("LocationCNTLoadId")))
        Dim oSku As String = ld.SKU
        Dim oConsignee As String = ld.CONSIGNEE
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If Not objSkuClass Is Nothing Then
            If objSkuClass.CaptureAtCountingLoadAttributesCount > 0 Then
                For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
                    Dim req As Boolean = False
                    If oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Required Then
                        req = True
                    End If
                    If oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                        DO1.AddTextboxLine(oAtt.Name, oAtt.Name)
                    End If
                Next
            End If
        End If
    End Sub

    Private Function ExtractAttributeValues() As WMS.Logic.AttributesCollection
        Dim ld As New WMS.Logic.Load(Convert.ToString(Session("LocationCNTLoadId")))
        Dim oSku As String = ld.SKU
        Dim oConsignee As String = ld.CONSIGNEE
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If objSkuClass Is Nothing Then Return Nothing
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            Dim req As Boolean = False
            If oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Dim val As Object
                Try
                    Select Case oAtt.Type
                        Case Logic.AttributeType.Boolean
                            val = CType(DO1.Value(oAtt.Name), Boolean)
                        Case Logic.AttributeType.DateTime
                            val = DateTime.ParseExact(DO1.Value(oAtt.Name), Made4Net.Shared.AppConfig.DateFormat, Nothing)
                        Case Logic.AttributeType.Decimal
                            val = CType(DO1.Value(oAtt.Name), Decimal)
                        Case Logic.AttributeType.Integer
                            val = CType(DO1.Value(oAtt.Name), Int32)
                        Case Else
                            val = DO1.Value(oAtt.Name)
                            If val = String.Empty And oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                                val = ld.LoadAttributes.Attribute(oAtt.Name)
                            End If
                    End Select
                    oAttCol.Add(oAtt.Name, val)
                Catch ex As Exception
                End Try
            End If
        Next
        Return oAttCol
    End Function

End Class
