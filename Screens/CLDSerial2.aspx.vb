Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports WMS.MobileWebApp.apexInbound
Imports WMS.MobileWebApp.apexSerial

<CLSCompliant(False)> Public Class CLDSerial2
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If Session("CreateLoadRCN") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRcnLine.aspx"))
            End If

            DO1.Value("SKU") = Session("CreateLoadSKU")
            DO1.Value("RECEIPT") = Session("CreateLoadRCN")
            DO1.Value("RECEIPTLINE") = Session("CreateLoadRCNLine")
            GetSerialReceivingProgress()

        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("RECEIPT")
        DO1.AddLabelLine("RECEIPTLINE")
        DO1.AddLabelLine("SKU")
        DO1.AddSpacer()
        DO1.AddLabelLine("VERF", "SUCCESSFUL SERIAL NUMBERS")
        DO1.AddLabelLine("VERSER", "")
    End Sub

    Private Sub doback()
        Response.Redirect(MapVirtualPath("Screens/CLDSerial.aspx"))
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "back"
                doback()
        End Select
    End Sub

    Private Sub GetSerialReceivingProgress()
        Dim VerifiedCount As Integer = 0
        Dim ValidScanned As Integer = 0
        Dim StrUnver As String = ""
        Dim StrVer As String = ""
        Dim StrFailedVer As String = ""

        Dim CountV As Integer = 0
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim dtLines As DataTable = APXINBOUND_FindSerialsOnStationByState(Logic.GetCurrentUser, Session("CreateLoadRCN"), _
                                                 Session("CreateLoadRCNLine"), Session("CreateLoadConsignee"), _
                                                 Session("CreateLoadSKU"), Session("CreateLoadLocation"), "")

        If dtLines.Rows.Count = 0 Then
            DO1.Value("VERF") = 0
        Else
            Dim rowsFound1 As DataRow()
            For Each dr As DataRow In dtLines.Rows
                rowsFound1 = dtLines.Select(String.Format("Verified={0}", FormatField("V")))
                If rowsFound1.Length > 0 Then
                    VerifiedCount = rowsFound1.Length
                End If
                Dim sstr As Char = dr("Verified")
                If sstr = "V" Then
                    If CountV = 0 Then
                        StrVer = StrVer + "| <b>" + dr("SerialNumber") + "</b>"
                    Else
                        StrVer = StrVer + " | <b>" + dr("SerialNumber") + "</b>"
                    End If

                    CountV = CountV + 1

                    If CountV = 6 Then
                        CountV = 0
                        StrVer = StrVer + " | <br>"
                    End If
                End If
            Next
            DO1.Value("VERF") = VerifiedCount
            DO1.Value("VERSER") = StrVer
        End If
    End Sub

End Class
