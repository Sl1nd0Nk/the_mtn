Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess
Imports Made4Net.Shared
Imports System.Data

Partial Public Class MultiSelectForm
    Inherits System.Web.UI.Page

#Region "Variables"

    Private _fromscreen As String
    Private _selectedtext As String

#End Region

#Region "Handlers"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' Bind Control to data
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Me.Up.Text = t.Translate("Up")
        Me.Down.Text = t.Translate("Down")
        Me.SelectValue.Text = t.Translate("Select Value")

        If Not IsPostBack Then

            Dim RequestSkuCode As String = Session("SKUCODE")
            _fromscreen = Session("FROMSCREEN")
            Dim key1 As String = Me.Request("key1")
            'Dim dt As DataTable = New DataTable

            'DataInterface.FillDataset("SELECT DISTINCT vSKUCODE.SKU, vSKUCODE.SKU + ' ' + SKUDESC AS DESCR FROM vSKUCODE INNER JOIN SKU ON vSKUCODE.CONSIGNEE=SKU.CONSIGNEE AND vSKUCODE.SKU=SKU.SKU WHERE SKUCODE = '" & RequestSkuCode & "' OR SKU.SKU ='" & RequestSkuCode & "'", dt)
            'DataInterface.FillDataset("SELECT DISTINCT (vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU) as SKU, vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU + ' ' + SKUDESC AS DESCR FROM vSKUCODE INNER JOIN SKU ON vSKUCODE.CONSIGNEE=SKU.CONSIGNEE AND vSKUCODE.SKU=SKU.SKU WHERE SKUCODE = '" & RequestSkuCode & "' OR SKU.SKU ='" & RequestSkuCode & "'", dt)
            ValueList.DataTextField = "DESCR"
            ValueList.DataValueField = "ROW"
            'ValueList.DataValueField = "SKU"
            Dim dt As DataTable = getSKUsDT(RequestSkuCode, _fromscreen, key1)
            ViewState("SKUSDT") = dt
            ValueList.DataSource = dt 'getSKUsDT(RequestSkuCode, _fromscreen, key1)  'dt
            ValueList.DataBind()

            ValueList.SelectedValue = ValueList.Items(0).Value
        End If
    End Sub



    Private Function getSKUsDT(ByVal pSKUCode As String, ByVal pSourceScreen As String, ByVal pKey1 As String) As DataTable
        Dim dt As New DataTable
        Dim sql As String = ""

        Select Case pSourceScreen.ToLower
            Case "CreateLoadSelectRCNLine".ToLower()
                'sql = "SELECT DISTINCT (vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU) as SKU, vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU + ' ' + SKU.SKUDESC AS DESCR FROM vSKUCODE INNER JOIN SKU ON vSKUCODE.CONSIGNEE=SKU.CONSIGNEE AND vSKUCODE.SKU=SKU.SKU INNER JOIN RECEIPTDETAIL RD ON SKU.CONSIGNEE = RD.CONSIGNEE AND SKU.SKU = RD.SKU WHERE (SKUCODE = '" & pSKUCode & "' OR SKU.SKU ='" & pSKUCode & "') AND RD.RECEIPT='" & pKey1 & "'"
                sql = "SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY vSKUCODE.SKU DESC) AS ROW, vSKUCODE.CONSIGNEE, vSKUCODE.SKU, (vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU) as DISPSKU, vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU + ' ' + SKU.SKUDESC AS DESCR FROM vSKUCODE INNER JOIN SKU ON vSKUCODE.CONSIGNEE=SKU.CONSIGNEE AND vSKUCODE.SKU=SKU.SKU INNER JOIN RECEIPTDETAIL RD ON SKU.CONSIGNEE = RD.CONSIGNEE AND SKU.SKU = RD.SKU WHERE (SKUCODE like '" & pSKUCode & "' OR SKU.SKU like '" & pSKUCode & "') AND RD.RECEIPT='" & pKey1 & "'"
            Case "ReqRepl".ToLower()
                If String.IsNullOrEmpty(pKey1) Then pKey1 = "%"
                'sql = "SELECT DISTINCT (vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU) as SKU, vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU + ' ' + SKU.SKUDESC AS DESCR FROM vSKUCODE INNER JOIN SKU ON vSKUCODE.CONSIGNEE=SKU.CONSIGNEE AND vSKUCODE.SKU=SKU.SKU INNER JOIN vPickLoc vpl ON SKU.CONSIGNEE = vpl.CONSIGNEE AND SKU.SKU = vpl.SKU WHERE (SKUCODE = '" & pSKUCode & "' OR SKU.SKU ='" & pSKUCode & "') AND vpl.LOCATION LIKE '" & pKey1 & "'"
                sql = "SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY vSKUCODE.SKU DESC) AS ROW, vSKUCODE.CONSIGNEE, vSKUCODE.SKU,(vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU) as DISPSKU, vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU + ' ' + SKU.SKUDESC AS DESCR FROM vSKUCODE INNER JOIN SKU ON vSKUCODE.CONSIGNEE=SKU.CONSIGNEE AND vSKUCODE.SKU=SKU.SKU INNER JOIN vPickLoc vpl ON SKU.CONSIGNEE = vpl.CONSIGNEE AND SKU.SKU = vpl.SKU WHERE (SKUCODE like '" & pSKUCode & "' OR SKU.SKU like '" & pSKUCode & "') AND vpl.LOCATION LIKE '" & pKey1 & "'"
            Case Else
                'sql = "SELECT DISTINCT (vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU) as SKU, vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU + ' ' + SKU.SKUDESC AS DESCR FROM vSKUCODE INNER JOIN SKU ON vSKUCODE.CONSIGNEE=SKU.CONSIGNEE AND vSKUCODE.SKU=SKU.SKU WHERE SKUCODE = '" & pSKUCode & "' OR SKU.SKU ='" & pSKUCode & "'"
                sql = "SELECT DISTINCT ROW_NUMBER() OVER(ORDER BY vSKUCODE.SKU DESC) AS ROW, vSKUCODE.CONSIGNEE, vSKUCODE.SKU,(vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU) as DISPSKU, vSKUCODE.CONSIGNEE+' '+ vSKUCODE.SKU + ' ' + SKU.SKUDESC AS DESCR FROM vSKUCODE INNER JOIN SKU ON vSKUCODE.CONSIGNEE=SKU.CONSIGNEE AND vSKUCODE.SKU=SKU.SKU WHERE SKUCODE like '" & pSKUCode & "' OR SKU.SKU like '" & pSKUCode & "'"
        End Select
        DataInterface.FillDataset(sql, dt)
        Return dt
    End Function

    Protected Sub SelectValue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SelectValue.Click
        'Session("SELECTEDSKU") = ValueList.SelectedValue
        Dim index As Integer = Integer.Parse(ValueList.SelectedValue)
        Dim dt As DataTable = ViewState("SKUSDT")
        Dim rows As DataRow() = dt.Select("ROW = " & index)


        Session("SELECTEDCONSIGNEE") = rows(0)("CONSIGNEE").ToString() 'ValueList.SelectedValue.Split(" ")(0)
        Session("SELECTEDSKU") = rows(0)("SKU").ToString() 'dt.Rows(index)("SKU") 'ValueList.SelectedValue.Split(" ")(1)
        Response.Redirect(MapVirtualPath("Screens/" & Session("FROMSCREEN") & ".aspx"))
        Session.Remove("FROMSCREEN")
        Session.Remove("SKUCODE")
    End Sub

    'Private Sub ValueList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ValueList.SelectedIndexChanged
    '    _selectedtext = ValueList.SelectedValue
    'End Sub

    Private Sub Up_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Up.Click
        If ValueList.SelectedIndex > 0 Then
            ValueList.SelectedIndex = ValueList.SelectedIndex - 1
        End If
    End Sub

    Private Sub Down_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Down.Click
        If ValueList.SelectedIndex < ValueList.Items.Count - 1 Then
            ValueList.SelectedIndex = ValueList.SelectedIndex + 1
        End If
    End Sub

#End Region




End Class