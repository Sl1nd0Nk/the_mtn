Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports WMS.MobileWebApp.apexInbound

<CLSCompliant(False)> Public Class CLDWEIGHT
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If Session("CreateLoadRCN") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRcnLine.aspx"))
            End If

            DO1.Value("CONSIGNEE") = Session("CreateLoadConsignee")
            DO1.Value("SKU") = Session("CreateLoadSKU")
            DO1.Value("RECEIPT") = Session("CreateLoadRCN")
            DO1.Value("RECEIPTLINE") = Session("CreateLoadRCNLine")

            Dim oSku As New Logic.SKU(Session("CreateLoadConsignee"), Session("CreateLoadSKU"))

            Dim SKUClass As String = ""
            If oSku.SKUClass IsNot Nothing Then
                SKUClass = oSku.SKUClass.ClassName
            End If

            If String.IsNullOrEmpty(DO1.Value("ASNID")) Then
                DO1.setVisibility("ASNID", False)
            End If

            DO1.Value("SKUDESC") = oSku.SKUDESC
            DO1.Value("SKUNOTES") = oSku.NOTES
            DO1.Value("CONTAINERID") = Session("CreateLoadContainerID")

            If Session("CreateLoadASNID") IsNot Nothing Then
                DO1.Value("ASNID") = Session("CreateLoadASNID")
            Else
                If (APXINBOUND_SkuGroupSuitableForASN(oSku.SKUGROUP)) And (SKUClass = "SERIAL") Then
                    DO1.Value("ASNID") = "Build"
                Else
                    DO1.Value("ASNID") = " "
                End If
            End If

            If Session("CreateLoadLocation") = "" Then
                Dim oCon As New WMS.Logic.Consignee(Session("CreateLoadConsignee"))
                DO1.Value("LOCATION") = oCon.DEFAULTRECEIVINGLOCATION
            Else
                DO1.Value("LOCATION") = Session("CreateLoadLocation")
            End If

            SetUOM()
            DO1.setVisibility("UOM", False)
        End If
    End Sub

    Private Sub doBack()
        Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRcnLine.aspx"))
    End Sub

    Private Sub doMenu()
        MobileUtils.ClearCreateLoadProcessSession()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim oSku As New Logic.SKU(Session("CreateLoadConsignee"), Session("CreateLoadSKU"))

        Dim SKUClass As String = ""
        If oSku.SKUClass IsNot Nothing Then
            SKUClass = oSku.SKUClass.ClassName
        End If

        'If (APXINBOUND_SkuGroupSuitableForASN(oSku.SKUGROUP)) And (SkuClass = "SERIAL") And String.IsNullOrEmpty(DO1.Value("ASNID")) Then
        '    Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Please supply ASN ID"))
        '    Return
        'End If

        Dim WeightStr As String = DO1.Value("WEIGHT")
        Dim Weight As Double = 0.0

        If String.IsNullOrEmpty(WeightStr) Then
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate("SKU weight not entered"))
            Return
        Else
            Weight = CDbl(WeightStr)
            If Weight = 0 Then
                Made4Net.Mobile.MessageQue.Enqueue(t.Translate("SKU weight cannot be zero"))
                Return
            End If
        End If

        Dim sql As String = String.Format("select * from sku where CONSIGNEE={0} and SKU={1}", Made4Net.Shared.FormatField(DO1.Value("CONSIGNEE")), _
                                                                                               Made4Net.Shared.FormatField(DO1.Value("SKU")))
        Dim dtSKU As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtSKU)
        Dim MatchingUOM As String = ""
        If dtSKU.Rows.Count = 0 Then
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate("SKU does not exist"))
            Return
        Else
            MatchingUOM = dtSKU.Rows(0)("DEFAULTRECUOM")
        End If

        Dim Weightsql As String = String.Format("select * from skuuom where consignee={0} and uom={2} and sku={1}", Made4Net.Shared.FormatField(DO1.Value("CONSIGNEE")), _
                                                                                                                    Made4Net.Shared.FormatField(DO1.Value("SKU")), _
                                                                                                                    Made4Net.Shared.FormatField(MatchingUOM))
        Dim dtWeight As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(Weightsql, dtWeight)
        If dtWeight.Rows.Count = 0 Then
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate("No weight has been configured for this SKU in master data"))
            Return
        Else
            Dim matchingWeight As Double = dtWeight.Rows(0)("GROSSWEIGHT")

            'Need to expand on this as it currently does not use tolerance params
            If APXINBOUND_CheckWeightInTolerance(Weight, matchingWeight) = False Then
                Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Weight failed! Not within tolerance"))
                Return
            End If
        End If

        If DO1.Value("UNITS") = "" Then
            MessageQue.Enqueue(t.Translate("Units to receive can not be blank"))
            Return
        End If

        If DO1.Value("LOCATION") = "" Then
            MessageQue.Enqueue(t.Translate("Receiving location can not be blank"))
            Return
        End If

        Session("CreateLoadLocation") = DO1.Value("LOCATION")
        Session("CreateContainer") = "0"

        Session("CreateLoadUOM") = DO1.Value("UOM")
        Session("CreateLoadUnits") = DO1.Value("UNITS")
        Session("CreateLoadNumLoads") = 1
        Session("CreateLoadWeight") = DO1.Value("WEIGHT")

        Redirect(Session("CreateLoadRCN"), Session("CreateLoadRCNLine"), Session("CreateLoadConsignee"), DO1.Value("SKU"), MatchingUOM)

    End Sub

    Private Sub Redirect(ByVal pReceipt As String, ByVal pReceiptLine As String, ByVal pConsignee As String, ByVal pSKU As String, ByVal pUOM As String)
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim oSKU As New WMS.Logic.SKU(pConsignee, pSKU)

        Dim SKUClass As String = ""
        If oSKU.SKUClass IsNot Nothing Then
            SKUClass = oSKU.SKUClass.ClassName
        End If

        If String.IsNullOrEmpty(DO1.Value("ASNID")) Or DO1.Value("ASNID") = " " Then
            If SKUClass = "SERIAL" Then
                Response.Redirect(MobileUtils.GetURLByScreenCode("RDTCLDSerial"))
            Else
                Response.Redirect(MobileUtils.GetURLByScreenCode("RDTCLD1"))
            End If
        Else
            If String.Compare(DO1.Value("ASNID"), "Build", True) = 0 Then
                Session.Add("CreateLoadASNID", DO1.Value("ASNID"))
                Response.Redirect(MapVirtualPath("Screens/CLDAsn1.aspx"))
            Else

                Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
                'If Session("CreateLoadASNID") Is Nothing Then
                '    Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Please supply ASN ID"))
                '    Return
                'Else
                Dim err As String = APXINBOUND_ValidateScannedASNID(Session("CreateLoadASNID"), Session("CreateLoadRCN"), Session("CreateLoadRCNLine"), Session("CreateLoadUnits"), Session("CreateLoadSKU"), pUOM)
                If (Not String.IsNullOrEmpty(err)) Then
                    Made4Net.Mobile.MessageQue.Enqueue(t.Translate(err))
                    Return
                Else
                    Response.Redirect(MobileUtils.GetURLByScreenCode("RDTCLDASN"))
                End If
                'End If
            End If
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("RECEIPT")
        DO1.AddLabelLine("RECEIPTLINE")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("SKUNOTES")
        DO1.AddLabelLine("ASNID")
        DO1.AddTextboxLine("LOCATION", "Enter Receiving Location")
        DO1.AddTextboxLine("UNITS", "Enter Quantity To Receive")
        DO1.AddSpacer()
        DO1.AddTextboxLine("WEIGHT", "Enter Unit Weight")
        DO1.AddDropDown("UOM")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "back"
                doBack()
            Case "next"
                doNext()
            Case "menu"
                doMenu()
        End Select
    End Sub

    Private Sub SetUOM()
        Dim dd As Made4Net.WebControls.MobileDropDown = DO1.Ctrl("UOM")
        dd.AllOption = False
        dd.TableName = "SKUUOMDESC"
        dd.ValueField = "UOM"
        dd.TextField = "DESCRIPTION"
        dd.Where = String.Format("CONSIGNEE = '{0}' and SKU = '{1}'", Session("CreateLoadConsignee"), Session("CreateLoadSKU"))
        dd.DataBind()
        If Not Session("CreateLoadUOM") Is Nothing Then
            dd.SelectedValue = Session("CreateLoadUOM")
        Else
            Dim dfuom As String = Made4Net.DataAccess.DataInterface.ExecuteScalar("SELECT ISNULL(DEFAULTRECUOM,'') FROM SKU WHERE CONSIGNEE = '" & Session("CreateLoadConsignee") & "' and SKU = '" & Session("CreateLoadSKU") & "'")
            Try
                If dfuom <> "" Then
                    dd.SelectedValue = dfuom
                End If
            Catch ex As Exception
            End Try
        End If
        DO1.Value("UNITS") = Session("CreateLoadUnits")
    End Sub

End Class
