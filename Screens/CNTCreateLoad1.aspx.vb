Imports Made4Net.Shared.Collections
Imports Made4Net.DataAccess
Imports System.Collections
Imports Made4Net.Mobile
Imports Made4Net.Shared.Web

Partial Public Class CNTCreateLoad1
    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        setAttributes()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "create"
                doCreate(ExtractAttributeValues())
            Case "back"
                doBack()
        End Select
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls

    End Sub

    Private Sub doBack()
        Session("CountMissingSKUBackClicked") = 1
        Response.Redirect(MapVirtualPath("Screens/CNTCreateLoad.aspx"))
    End Sub

    Private Sub setAttributes()
        Dim oSku As String = Session("CountMisSKUSku")
        Dim oConsignee As String = Session("CountMisSKUConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If Not objSkuClass Is Nothing Then
            If objSkuClass.CaptureAtReceivingLoadAttributesCount > 0 Then
                For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
                    Dim req As Boolean = False
                    If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Then
                        req = True
                    End If
                    If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                        DO1.AddTextboxLine(oAtt.Name, oAtt.Name)
                    End If
                Next
            Else
                doCreate(Nothing)
            End If
        Else
            doCreate(Nothing)
        End If
    End Sub

    Private Function ExtractAttributeValues() As WMS.Logic.AttributesCollection
        Dim oSku As String = Session("CountMisSKUSku")
        Dim oConsignee As String = Session("CountMisSKUConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If objSkuClass Is Nothing Then Return Nothing
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            Dim req As Boolean = False
            If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Dim val As Object
                Try
                    Select Case oAtt.Type
                        Case Logic.AttributeType.Boolean
                            val = CType(DO1.Value(oAtt.Name), Boolean)
                        Case Logic.AttributeType.DateTime
                            val = CType(DO1.Value(oAtt.Name), DateTime)
                            'Try
                            '    val = DateTime.ParseExact(DO1.Value(oAtt.Name), Made4Net.Shared.AppConfig.DateFormat, Nothing)
                            'Catch ex As Exception
                            '    val = Nothing
                            'End Try
                            'If val Is Nothing Then
                            '    val = DateTime.ParseExact(DO1.Value(oAtt.Name), "ddMMyyyy", Nothing)
                            'End If
                        Case Logic.AttributeType.Decimal
                            val = CType(DO1.Value(oAtt.Name), Decimal)
                        Case Logic.AttributeType.Integer
                            val = CType(DO1.Value(oAtt.Name), Int32)
                        Case Else
                            val = DO1.Value(oAtt.Name)
                    End Select
                    oAttCol.Add(oAtt.Name, val)
                Catch ex As Exception
                End Try
            End If
        Next
        Return oAttCol
    End Function

    Private Sub doCreate(ByVal oAttributes As WMS.Logic.AttributesCollection)
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim ldId As String = Session("CountMisSKULoadId")
            Dim sConsignee As String = Session("CountMisSKUConsignee")
            Dim sSku As String = Session("CountMisSKUSku")

            Dim oCounting As New WMS.Logic.Counting()
            oCounting.CreateLoad(ldId, sConsignee, sSku, Session("CountMisSKUQty"), Session("CountMisSKUStatus"), Session("CountMisSKULocation"), _
                Session("CountMisSKUUOM"), oAttributes, WMS.Logic.GetCurrentUser)
            MessageQue.Enqueue(t.Translate("Load was successfully created."))
            Session.Remove("CountMisSKUConsignee")
            Session.Remove("CountMisSKULocation")
            Session.Remove("CountMisSKULoadId")
            Session.Remove("CountMisSKUQty")
            Session.Remove("CountMisSKUSku")
            Session.Remove("CountMisSKUStatus")
            Session.Remove("CountMisSKUUOM")
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage())
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
        Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("Screens/CNT.aspx"))
    End Sub

End Class