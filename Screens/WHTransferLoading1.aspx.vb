Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

<CLSCompliant(False)> Public Class WHTransferLoading1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen

    Protected WithEvents ddUOM As Made4Net.WebControls.MobileDropDown
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then WMS.Logic.GotoLogin()

        If Not IsPostBack Then SetScreen()
    End Sub

    Private Sub SetScreen()
        Dim s As Shipment = Session("TransferShipment")

        Dim sql As String = ""
        sql = $"select count(1) from shipmentloads where shipment='{s.SHIPMENT}'"


        DO1.Value("TRANSFERID") = s.SHIPMENT
        DO1.Value("VEHICLEID") = s.VEHICLE
        DO1.Value("TOTALLOADS") = Made4Net.DataAccess.DataInterface.ExecuteScalar(sql) '0
        DO1.Value("LASTSCAN") = ""
    End Sub




    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator()
        Dim ld As String = DO1.Value("LOADID")
        If String.IsNullOrEmpty(ld) Then
            MessageQue.Enqueue(t.Translate("Loadid can not be empty"))
            Return
        End If
        Dim sql As String = ""
        sql = $"select count(1) from invload where loadid='{ld}'"

        If Made4Net.DataAccess.DataInterface.ExecuteScalar(sql) = 0 Then
            MessageQue.Enqueue(t.Translate("Load does not exist"))
            Return
        End If


        Dim s As Shipment = Session("TransferShipment")

        sql = $"select * from shipmentloads where loadid='{ld}' and shipment='{s.SHIPMENT}'"
        Dim dt As New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)



        If dt.Rows.Count > 0 Then
            If dt.Rows(0)("Shipment").ToString().Equals(s.SHIPMENT, StringComparison.OrdinalIgnoreCase) Then
                MessageQue.Enqueue(t.Translate("Load was already loaded"))
                Return
            Else
                MessageQue.Enqueue(t.Translate("Load was already loaded to another shipment"))
                Return
            End If
        End If

        Dim ldObj As New WMS.Logic.Load(ld)


        sql = $"insert into shipmentloads values ('{s.SHIPMENT}', '{ld}', '{ldObj.STATUS}', getdate(), '{WMS.Logic.GetCurrentUser}', getdate(), '{WMS.Logic.GetCurrentUser}' )"
        Made4Net.DataAccess.DataInterface.RunSQL(sql)

        ldObj.SetActivityStatus(WMS.Lib.Statuses.ActivityStatus.LOADED, WMS.Logic.GetCurrentUser)

        DO1.Value("LASTSCAN") = ld
        DO1.Value("TOTALLOADS") = Integer.Parse(DO1.Value("TOTALLOADS")) + 1
        DO1.Value("LOADID") = ""

        sendLoadLoadedEvent(ldObj)

    End Sub

    Private Sub doBack()
        Session.Remove("TransferShipment")
        Response.Redirect(MapVirtualPath("Screens/WHTransferLoading.aspx"))
    End Sub

    Private Sub doClose()
        Dim t As New Made4Net.Shared.Translation.Translator()
        If Integer.Parse(DO1.Value("TOTALLOADS")) = 0 Then
            MessageQue.Enqueue(t.Translate("Can not close. No load was scanned"))
            Return
        End If
        Dim s As Shipment = Session("TransferShipment")
        s.Ship(WMS.Logic.GetCurrentUser)

        Dim sql As String = ""
        sql = $"update loads set status='', location='', activitystatus='' where loadid in (select loadid from shipmentloads where shipment='{s.SHIPMENT}')"
        Made4Net.DataAccess.DataInterface.RunSQL(sql)

        Session.Remove("TransferShipment")
        Response.Redirect(MapVirtualPath("Screens/WHTransferLoading.aspx"))
    End Sub


    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("TRANSFERID")
        DO1.AddLabelLine("VEHICLEID")
        DO1.AddLabelLine("TOTALLOADS")
        DO1.AddLabelLine("LASTSCAN")
        DO1.AddTextboxLine("LOADID")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "back"
                doBack()
            Case "close"
                doClose()
            Case "next"
                doNext()
        End Select
    End Sub

    Private Sub sendLoadLoadedEvent(ByVal ld As WMS.Logic.Load)
        Dim aq As New EventManagerQ
        aq.Add("EVENT", WMS.Logic.WMSEvents.EventType.LoadLoaded)
        aq.Add("ACTIVITYDATE", Made4Net.Shared.Util.DateTimeToWMSString(DateTime.Now))
        aq.Add("ACTIVITYTIME", "0")
        aq.Add("ACTIVITYTYPE", WMS.Lib.Actions.Audit.LOADLOAD)
        aq.Add("CONSIGNEE", ld.CONSIGNEE)
        aq.Add("DOCUMENT", "")
        aq.Add("DOCUMENTLINE", 0)
        aq.Add("FROMLOAD", ld.LOADID)
        aq.Add("FROMLOC", ld.LOCATION)
        aq.Add("FROMQTY", 0)
        aq.Add("FROMSTATUS", ld.STATUS)
        aq.Add("NOTES", "")
        aq.Add("SKU", ld.SKU)
        aq.Add("TOLOAD", ld.LOADID)
        aq.Add("TOLOC", "")
        aq.Add("TOQTY", ld.UNITS)
        aq.Add("TOSTATUS", ld.STATUS)
        aq.Add("USERID", WMS.Logic.GetCurrentUser)
        aq.Add("ADDDATE", Made4Net.[Shared].Util.DateTimeToWMSString(DateTime.Now))
        aq.Add("LASTMOVEUSER", WMS.Logic.GetCurrentUser)
        aq.Add("LASTSTATUSUSER", WMS.Logic.GetCurrentUser)
        aq.Add("LASTCOUNTUSER", WMS.Logic.GetCurrentUser)
        aq.Add("LASTSTATUSDATE", Made4Net.[Shared].Util.DateTimeToWMSString(DateTime.Now))
        aq.Add("ADDUSER", WMS.Logic.GetCurrentUser)
        aq.Add("EDITDATE", Made4Net.[Shared].Util.DateTimeToWMSString(DateTime.Now))
        aq.Add("EDITUSER", WMS.Logic.GetCurrentUser)
        aq.Send(WMS.Lib.Actions.Audit.LOADLOAD)
    End Sub



End Class
