Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports WMS.MobileWebApp.apexInbound
Imports WMS.MobileWebApp.apexSerial

<CLSCompliant(False)> Public Class CLDAsn1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session("UserAssignedPrinter") Is Nothing Then
            Dim Printer As String = GetUserPrinter(Logic.GetCurrentUser())

            If Printer = "" Then
                Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

                MessageQue.Enqueue(t.Translate("User " & Logic.GetCurrentUser() & " is not assigned to a label printer"))
            Else
                Session.Add("UserAssignedPrinter", Printer)
            End If
        End If

        If Not IsPostBack Then
            If Session("CreateLoadRCN") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRcnLine.aspx"))
            End If

            If Session("CreateLoadASNID") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRcnLine.aspx"))
            End If

            DO1.Value("BUILD_PALLET") = ""
            'DO1.Value("CONSIGNEE") = Session("CreateLoadConsignee")
            DO1.Value("SKU") = Session("CreateLoadSKU")
            DO1.Value("RECEIPT") = Session("CreateLoadRCN")
            DO1.Value("RECEIPTLINE") = Session("CreateLoadRCNLine")
            Dim oSku As New Logic.SKU(Session("CreateLoadConsignee"), Session("CreateLoadSKU"))
            DO1.Value("SKUDESC") = oSku.SKUDESC
            'DO1.Value("SKUNOTES") = oSku.NOTES
            DO1.Value("PALLETQTY") = CInt(Session("CreateLoadUnits"))
            'DO1.Value("UOM") = Session("CreateLoadUOM")
            DO1.Value("LOCATION") = Session("CreateLoadLocation")
            'DO1.Value("WEIGHT") = Session("CreateLoadWeight")
            'DO1.setVisibility("SKUNOTES", False)

            CheckConfiguration()
        End If

    End Sub

    Private Function GetUserPrinter(ByVal User As String) As String
        Dim SQL As String = String.Format("SELECT * FROM USERPROFILE WHERE USERID={0}", FormatField(User))
        Dim DtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, DtLines)

        If DtLines.Rows.Count > 0 Then
            If IsDBNull(DtLines.Rows(0)("DEFAULTPRINTER")) Then
                Return ""
            Else
                Return DtLines.Rows(0)("DEFAULTPRINTER")
            End If
        Else
            Return ""
        End If
    End Function

    Private Sub CheckConfiguration()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim HighestUOM As String = ""
        Dim LowestUOM As String = ""
        Dim QtyOfHighest As Integer = 0
        Dim LowerUOM As String = ""
        Dim NoOfLevels As Integer = 0
        Dim SerialNumber As String = ""
        Dim QtyScanned As Integer = 0
        Dim index As Integer = 0
        Dim Range As Boolean = False
        Dim DtLines As New DataTable

        DO1.Value("QTYSCANNED") = APXINBOUND_GetLowestUOMCount(Session("CreateLoadSKU"), Session("CreateLoadRCN"), Session("CreateLoadRCNLine"), Logic.GetCurrentUser)

        Dim QtyRemaining As Integer = CInt(DO1.Value("PALLETQTY")) - CInt(DO1.Value("QTYSCANNED"))
        DtLines = APXINBOUND_FindUOMsForSku(DO1.Value("SKU"))


        If DtLines IsNot Nothing Then
            If DtLines.Rows.Count > 0 Then
                If DtLines.Rows.Count = 1 Then
                    If IsDBNull(DtLines.Rows(0)("LOWERUOM")) Or DtLines.Rows(0)("LOWERUOM") = "" Then
                        MessageQue.Enqueue(t.Translate("Only the lowest UOM has been setup for this SKU - Cannot Continue"))
                    Else
                        MessageQue.Enqueue(t.Translate("UOMs for this SKU not properly setup - Cannot Continue"))
                    End If
                Else
                    Dim x As Integer = 0
                    Dim exclude As String = "-1"
                    If String.Compare(Session("PARENTSERIAL"), "NONE", True) = 0 Then
                        x = DtLines.Rows.Count
                        exclude = Session("UOM")
                        index = FindByLowerUOM(Session("PARENTSERIAL"), DtLines)
                    Else
                        While x < DtLines.Rows.Count
                            If HighestUOM <> "" Then
                                If DtLines.Rows(x)("LOWERUOM") = HighestUOM Then
                                    HighestUOM = DtLines.Rows(x)("UOM")
                                    QtyOfHighest = DtLines.Rows(x)("UNITSPERMEASURE")
                                    index = x
                                    SerialNumber = ""
                                    QtyScanned = 0

                                    APXINBOUND_FindOnStationUncompletedUOM(DO1.Value("SKU"), Logic.GetCurrentUser, HighestUOM, QtyOfHighest, DO1.Value("RECEIPT"), DO1.Value("RECEIPTLINE"), SerialNumber, QtyScanned, LowerUOM)

                                    If SerialNumber <> "" Then
                                        Session.Add("PARENTSERIAL", SerialNumber)

                                        Dim LUOM As String = ""
                                        If Not IsDBNull(DtLines.Rows(index)("LOWERUOM")) Then
                                            LUOM = DtLines.Rows(index)("LOWERUOM")
                                        End If

                                        If LowestUOM = LUOM Then
                                            Range = True
                                        End If

                                        Exit While
                                    Else
                                        x = -1
                                    End If
                                End If
                            Else
                                If IsDBNull(DtLines.Rows(x)("LOWERUOM")) Or DtLines.Rows(x)("LOWERUOM") = "" Then
                                    HighestUOM = DtLines.Rows(x)("UOM")
                                    Session.Add("CreateLoadUOM", HighestUOM)

                                    index = x
                                    LowestUOM = HighestUOM
                                    x = -1
                                End If
                            End If
                            x = x + 1
                        End While
                    End If

                    If x >= DtLines.Rows.Count Then
                        Session.Add("PARENTSERIAL", "")
                        Dim LUOM As String = ""
                        If Not IsDBNull(DtLines.Rows(index)("LOWERUOM")) Then
                            LUOM = DtLines.Rows(index)("LOWERUOM")
                        End If
                        Session.Add("LOWERUOM", LUOM)

                        Session.Add("UOM", DtLines.Rows(index)("UOM"))
                        Session.Add("UNITSPERUOM", DtLines.Rows(index)("UNITSPERMEASURE"))
                        DO1.Value("UNITS") = GethigherUOMQty(DtLines, DtLines.Rows(index)("UOM"), exclude)
                        DO1.Value(" ") = "Scan " & DtLines.Rows(index)("UOM") & " Serial Number"
                        DO1.setVisibility("SERIAL_FROM", False)
                        DO1.setVisibility("SERIAL_TO", False)
                        DO1.setVisibility("SERIAL", True)
                        'End If
                    Else
                        x = 0
                        While x < DtLines.Rows.Count
                            Dim LUOM As String = ""
                            If Not IsDBNull(DtLines.Rows(x)("LOWERUOM")) Then
                                LUOM = DtLines.Rows(x)("LOWERUOM")
                            End If

                            If DtLines.Rows(x)("UOM") = LowerUOM Then
                                Session.Add("UOM", DtLines.Rows(x)("UOM"))
                                DO1.Value("UNITS") = GethigherUOMQty(DtLines, DtLines.Rows(x)("UOM"), exclude)
                                Session.Add("LOWERUOM", LUOM)
                                Session.Add("UNITSPERUOM", DtLines.Rows(x)("UNITSPERMEASURE"))
                                Exit While
                            End If
                            x = x + 1
                        End While

                        If Range = True Then
                            DO1.Value(" ") = "Scan From and To Unit Serial Number Range"
                            DO1.setVisibility("SERIAL_FROM", True)
                            DO1.setVisibility("SERIAL_TO", True)
                            DO1.setVisibility("SERIAL", False)
                        Else
                            DO1.Value(" ") = "Scan " & Session("UOM") & " Serial Number " & (QtyScanned + 1) & " OF " & DO1.Value("UNITS")
                            DO1.setVisibility("SERIAL_FROM", False)
                            DO1.setVisibility("SERIAL_TO", False)
                            DO1.setVisibility("SERIAL", True)
                        End If
                    End If

                    DO1.Value("PARENT") = Session("PARENTSERIAL")
                End If
            Else
                MessageQue.Enqueue(t.Translate("No UOMs setup for this SKU"))
            End If

        Else
            MessageQue.Enqueue(t.Translate("No UOMs setup for this SKU"))
        End If

        If QtyRemaining <= 0 Then
            DO1.Value(" ") = "Push the create load to complete"
            DO1.setVisibility("SERIAL_FROM", False)
            DO1.setVisibility("SERIAL_TO", False)
            DO1.setVisibility("SERIAL", False)
        End If
    End Sub

    Private Function FindByLowerUOM(ByVal LowerUOM As String, ByVal DtLines As DataTable) As Integer
        Dim x As Integer = 0
        While x < DtLines.Rows.Count
            If DtLines.Rows(x)("UOM") = LowerUOM Then
                Return x
            End If
            x = x + 1
        End While

        Return 0
    End Function


    Private Function GethigherUOMQty(ByVal DtLines As DataTable, ByVal UOM As String, ByVal exclude As String) As Integer
        Dim x As Integer = 0
        While x < DtLines.Rows.Count
            Dim LUOM As String = ""
            If Not IsDBNull(DtLines.Rows(x)("LOWERUOM")) Then
                LUOM = DtLines.Rows(x)("LOWERUOM")
            End If

            If LUOM = UOM Then
                If String.Compare(DtLines.Rows(x)("UOM"), exclude, True) <> 0 Then
                    Return CInt(DtLines.Rows(x)("UNITSPERMEASURE"))
                End If
            End If
            x = x + 1
        End While

        Return 1
    End Function

    Private Sub doMenu()
        MobileUtils.ClearCreateLoadProcessSession()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doAbort(ByVal Full As Boolean)
        APXINBOUND_RemoveSerialNumbersFromStation(Logic.GetCurrentUser, Session("CreateLoadRCN"), Session("CreateLoadRCNLine"), Session("CreateLoadConsignee"), Session("CreateLoadSKU"), Session("CreateLoadLocation"))
        Session.Remove("CreateLoadRCN")
        Session.Remove("CreateLoadRCNLine")
        Session.Remove("CreateLoadConsignee")
        Session.Remove("CreateLoadSKU")
        Session.Remove("CreateLoadLocation")
        Session.Remove("CreateLoadWeight")
        Session.Remove("CreateLoadLocation")
        Session.Remove("CreateLoadUnits")
        Session.Remove("CreateLoadASNID")
        Session.Remove("PARENTSERIAL")
        Session.Remove("LOWERUOM")
        Session.Remove("UOM")
        Session.Remove("UNITSPERUOM")

        Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRCNLine.aspx"))
    End Sub

    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If (Not String.IsNullOrEmpty(DO1.Value("SERIAL"))) And (DO1.Value("SERIAL") <> " ") Then
            Dim err As String = ""
            If String.Compare(DO1.Value("SERIAL"), "NONE", True) = 0 Then
                Session.Add("PARENTSERIAL", DO1.Value("SERIAL"))
            Else
                If APXSERIAL_FindSerialNumber(DO1.Value("SERIAL"), Session("CreateLoadSKU")) < 1 Then
                    err = APXINBOUND_AddSerialToStationBuild(DO1.Value("SERIAL"), Logic.GetCurrentUser, Session("CreateLoadRCN"), Session("CreateLoadRCNLine"), _
                                                            Session("CreateLoadConsignee"), Session("CreateLoadSKU"), _
                                                            Session("CreateLoadWeight"), Session("UOM"), Session("CreateLoadLocation"), Session("UNITSPERUOM"), Session("LOWERUOM"), Session("PARENTSERIAL"), 0, "BUILD", CInt(Session("CreateLoadUnits")))
                    If Not String.IsNullOrEmpty(err) Then
                        Made4Net.Mobile.MessageQue.Enqueue(t.Translate(err))
                    End If
                Else
                    Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Serial Number is already in the system"))
                End If

            End If
        Else
            If (Not String.IsNullOrEmpty(DO1.Value("SERIAL_FROM"))) And (DO1.Value("SERIAL_FROM") <> " ") And (Not String.IsNullOrEmpty(DO1.Value("SERIAL_TO"))) And (DO1.Value("SERIAL_TO") <> " ") Then
                Dim err As String = ""
                If APXSERIAL_FindSerialNumber(DO1.Value("SERIAL_FROM"), Session("CreateLoadSKU")) < 1 Then
                    err = APXINBOUND_VerifySerialsUsingSerialAttrib(DO1.Value("SERIAL_FROM"), Session("CreateLoadSKU"))
                    If Not String.IsNullOrEmpty(err) Then
                        Made4Net.Mobile.MessageQue.Enqueue(t.Translate(err))
                    Else
                        If APXSERIAL_FindSerialNumber(DO1.Value("SERIAL_TO"), Session("CreateLoadSKU")) < 1 Then
                            err = APXINBOUND_VerifySerialsUsingSerialAttrib(DO1.Value("SERIAL_TO"), Session("CreateLoadSKU"))
                            If Not String.IsNullOrEmpty(err) Then
                                Made4Net.Mobile.MessageQue.Enqueue(t.Translate(err))
                            Else
                                Dim checkSerial1 As Int64 = Convert.ToInt64(DO1.Value("SERIAL_FROM").Substring(0, DO1.Value("SERIAL_FROM").Length - 1))
                                Dim checkSerial2 As Int64 = Convert.ToInt64(DO1.Value("SERIAL_TO").Substring(0, DO1.Value("SERIAL_TO").Length - 1))

                                If (checkSerial2 - checkSerial1) <> (DO1.Value("UNITS") - 1) Then
                                    Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Entered Range Not Amount to UOM Measure"))
                                Else
                                    Dim SerInBetween As String = DO1.Value("SERIAL_FROM").Substring(0, DO1.Value("SERIAL_FROM").Length - 1)

                                    Dim x As Integer = 0
                                    Dim ISerial As Int64 = Convert.ToInt64(SerInBetween)
                                    While x < DO1.Value("UNITS")
                                        Dim CheckDigit As Integer = APXINBOUND_GenarateSerialCheckDigit(ISerial.ToString())
                                        Dim NewSerial As String = ISerial.ToString()

                                        If CheckDigit > -1 Then
                                            NewSerial = NewSerial & CheckDigit.ToString()
                                        End If

                                        err = APXINBOUND_AddSerialToStationBuild(NewSerial, Logic.GetCurrentUser, Session("CreateLoadRCN"), Session("CreateLoadRCNLine"), _
                                                                                Session("CreateLoadConsignee"), Session("CreateLoadSKU"), _
                                                                                Session("CreateLoadWeight"), Session("UOM"), Session("CreateLoadLocation"), Session("UNITSPERUOM"), Session("LOWERUOM"), Session("PARENTSERIAL"), Session("UNITSPERUOM"), "BUILD", CInt(Session("CreateLoadUnits")))
                                        If Not String.IsNullOrEmpty(err) Then
                                            Made4Net.Mobile.MessageQue.Enqueue(t.Translate(err))
                                        End If

                                        ISerial = ISerial + 1
                                        x = x + 1
                                    End While

                                    APXINBOUND_UpdateParentSerialToStationBuild(Session("PARENTSERIAL"), DO1.Value("UNITS"))
                                End If
                            End If
                        Else
                            Made4Net.Mobile.MessageQue.Enqueue(t.Translate("To Range Serial Number is already on the system"))
                        End If
                    End If
                Else
                    Made4Net.Mobile.MessageQue.Enqueue(t.Translate("From Range Serial Number is already in the system"))
                End If
            Else
                Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Please scan valid serial number"))
            End If
        End If

        CheckConfiguration()
        DO1.Value("SERIAL") = ""
        DO1.Value("SERIAL_TO") = ""
        DO1.Value("SERIAL_FROM") = ""
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("BUILD_PALLET", "BUILD PALLET")
        DO1.AddLabelLine("RECEIPT")
        DO1.AddLabelLine("RECEIPTLINE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOCATION")
        DO1.AddLabelLine("PALLETQTY", "Pallet Qty")
        DO1.AddLabelLine("QTYSCANNED", "Qty Scanned")
        DO1.AddSpacer()
        DO1.AddLabelLine("PARENT")
        DO1.AddLabelLine("UNITS")
        DO1.AddSpacer()
        DO1.AddLabelLine(" ")
        DO1.AddTextboxLine("SERIAL")
        DO1.AddTextboxLine("SERIAL_FROM", "From Serial")
        DO1.AddTextboxLine("SERIAL_TO", "To Serial")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "createload"
                SubmitCreate(ExtractAttributeValues(), False)
            Case "abort"
                doAbort(True)
            Case "next"
                doNext()
        End Select
    End Sub

    Private Function ExtractAttributeValues() As WMS.Logic.AttributesCollection
        Dim oSku As String = Session("CreateLoadSKU")
        Dim oConsignee As String = Session("CreateLoadConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            Dim req As Boolean = False
            If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Dim val As Object
                Try
                    Select Case oAtt.Type
                        Case Logic.AttributeType.Boolean
                            val = CType(DO1.Value(oAtt.Name), Boolean)
                        Case Logic.AttributeType.DateTime
                            val = CType(DO1.Value(oAtt.Name), DateTime)
                        Case Logic.AttributeType.Decimal
                            val = CType(DO1.Value(oAtt.Name), Decimal)
                        Case Logic.AttributeType.Integer
                            val = CType(DO1.Value(oAtt.Name), Int32)
                        Case Else
                            val = DO1.Value(oAtt.Name)
                    End Select
                    oAttCol.Add(oAtt.Name, val)
                Catch ex As Exception

                End Try
            End If
        Next
        Return oAttCol
    End Function

    Private Sub SubmitCreate(ByVal oAttributes As WMS.Logic.AttributesCollection, Optional ByVal DoPutaway As Boolean = False)
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If CInt(DO1.Value("PALLETQTY")) = CInt(DO1.Value("QTYSCANNED")) Then
            Dim ResponseMessage As String = ""
            Dim attributesarray As New ArrayList

            Try
                Dim ldId As String = Session("CreateLoadLoadId")
                If ldId = "" Then
                    ldId = WMS.Logic.Load.GenerateLoadId()
                End If

                Dim ld() As WMS.Logic.Load
                Dim oRec As New Logic.Receiving
                Dim oReceiptLine As ReceiptDetail

                If Session("CreateLoadNumLoads") Is Nothing Then Session("CreateLoadNumLoads") = 1

                If Session("CreateLoadRCNMultipleLines") Then
                    ld = oRec.CreateLoadFromMultipleLines(Session("CreateLoadRCN"), Session("CreateLoadSKU"), _
                        Session("CreateLoadUOM"), Session("CreateLoadLocation"), Session("CreateLoadUnits"), Session("CreateLoadStatus"), _
                        Session("CreateLoadHoldRC"), Logic.GetCurrentUser, oAttributes, Session("CreateLoadLabelPrinter"), "", "")
                Else

                    oReceiptLine = New ReceiptDetail(Session("CreateLoadRCN").ToString(), CType(Session("CreateLoadRCNLine"), Integer))

                    Dim Prn As String = ""
                    If Not Session("UserAssignedPrinter") Is Nothing Then
                        Prn = Session("UserAssignedPrinter")
                    End If

                    ' Check if number of loads is more than one and if we need to create containers
                    ld = oRec.CreateLoad(Session("CreateLoadRCN"), Session("CreateLoadRCNLine"), Session("CreateLoadSKU"), ldId, _
                        Session("CreateLoadUOM"), Session("CreateLoadLocation"), Session("CreateLoadUnits"), "Held", _
                        "", Session("CreateLoadNumLoads"), Logic.GetCurrentUser, oAttributes, Prn, "", "", "")
                End If


                If ReceiveValidSerialNumbers(ldId, "Held") Then
                Else
                End If

                ' After Load Creation we will put the loads on his container
                If Session("CreateContainer") = "1" Then
                    Dim LoadsCreatedCount As Int32
                    For LoadsCreatedCount = 0 To ld.Length() - 1
                        Dim cntr As New WMS.Logic.Container()
                        cntr.ContainerId = Made4Net.Shared.getNextCounter("CONTAINER")
                        cntr.HandlingUnitType = Session("HUTYPE")
                        cntr.Location = Session("CreateLoadLocation")
                        cntr.Post(WMS.Logic.Common.GetCurrentUser)

                        cntr.Place(ld(LoadsCreatedCount), WMS.Logic.Common.GetCurrentUser)
                    Next
                Else
                    If Session("CreateLoadContainerID") <> "" Then
                        Dim cntr As New WMS.Logic.Container(Session("CreateLoadContainerID"), True)
                        Dim LoadsCreatedCount As Int32
                        For LoadsCreatedCount = 0 To ld.Length() - 1
                            cntr.Place(ld(LoadsCreatedCount), WMS.Logic.Common.GetCurrentUser)
                        Next
                    End If
                End If


                ' Create Handling unit transaction if needed
                If DO1.Value("HUTRANS") = "YES" Then
                    Try
                        Dim strHUTransIns As String
                        If Session("CreateLoadContainerID") <> "" Then
                            strHUTransIns = "INSERT INTO [HANDLINGUNITTRANSACTION]([TRANSACTIONID],[TRANSACTIONTYPE],[TRANSACTIONTYPEID],[TRANSACTIONDATE],[CONSIGNEE],[ORDERID],[DOCUMENTTYPE],[COMPANY],[COMPANYTYPE],[HUTYPE],[HUQTY],[ADDDATE],[ADDUSER],[EDITDATE],[EDITUSER])" & _
                                                          "VALUES('" & Made4Net.Shared.getNextCounter("HUTRANSID") & "','RECEIPT','" & Session("CreateLoadRCN") & "',GETDATE(),'" & Session("CreateLoadConsignee") & "','" & oReceiptLine.ORDERID & "','" & oReceiptLine.DOCUMENTTYPE & "','" & oReceiptLine.COMPANY & "','" & oReceiptLine.COMPANYTYPE & "','" & DO1.Value("HUTYPE") & "','1',GETDATE(),'" & Logic.GetCurrentUser & "',GETDATE(),'" & Logic.GetCurrentUser & "')"
                        Else
                            strHUTransIns = "INSERT INTO [HANDLINGUNITTRANSACTION]([TRANSACTIONID],[TRANSACTIONTYPE],[TRANSACTIONTYPEID],[TRANSACTIONDATE],[CONSIGNEE],[ORDERID],[DOCUMENTTYPE],[COMPANY],[COMPANYTYPE],[HUTYPE],[HUQTY],[ADDDATE],[ADDUSER],[EDITDATE],[EDITUSER])" & _
                                                          "VALUES('" & Made4Net.Shared.getNextCounter("HUTRANSID") & "','RECEIPT','" & Session("CreateLoadRCN") & "',GETDATE(),'" & Session("CreateLoadConsignee") & "','" & oReceiptLine.ORDERID & "','" & oReceiptLine.DOCUMENTTYPE & "','" & oReceiptLine.COMPANY & "','" & oReceiptLine.COMPANYTYPE & "','" & DO1.Value("HUTYPE") & "','" & Session("CreateLoadNumLoads") & "',GETDATE(),'" & Logic.GetCurrentUser & "',GETDATE(),'" & Logic.GetCurrentUser & "')"
                        End If
                        DataInterface.RunSQL(strHUTransIns)
                    Catch ex As Exception
                    End Try
                End If

                ' If Create and Putaway then request pickup for each load
                If DoPutaway Then
                    Dim pw As New Putaway
                    Dim i As Int32
                    For i = 0 To ld.Length() - 1
                        ld(i).RequestPickUp(Logic.GetCurrentUser)
                        'pw.RequestDestination(ld(i).LOADID)
                    Next
                    Response.Redirect(MapVirtualPath("Screens/RPK2.aspx?sourcescreen=cld1"))
                End If
                MessageQue.Enqueue(t.Translate("Load Created"))
            Catch ex As Made4Net.Shared.M4NException
                MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
                Return
            Catch ex As ApplicationException
                MessageQue.Enqueue(ex.Message)
                Return
            End Try
            DO1.Value("LOADID") = ""
            DO1.Value("UNITS") = ""
            DO1.Value("CONTAINERID") = ""
            clearAttributeValues()
            doAbort(False)
        Else
            MessageQue.Enqueue(t.Translate("Cannot Create Load - Pallet building still in progress"))
        End If
    End Sub

    Private Function ReceiveValidSerialNumbers(ByVal LoadId As String, ByVal serialStatus As String) As Boolean
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim dtLines As DataTable = APXINBOUND_FindParentSerialsOnStation(Logic.GetCurrentUser, Session("CreateLoadRCN"), _
                                                               Session("CreateLoadRCNLine"), Session("CreateLoadConsignee"), _
                                                               Session("CreateLoadSKU"), Session("CreateLoadLocation"))
        If dtLines.Rows.Count = 0 Then
            Return False
        Else
            For Each dr As DataRow In dtLines.Rows
                Try
                    APXSERIAL_AddSerialToLoad1(dr("SerialNumber"), Session("CreateLoadSKU"), LoadId, dr("UOM"), Logic.GetCurrentUser, CDbl(Session("CreateLoadWeight")), 0, 0, serialStatus, "NEW")
                Catch ex As Exception
                    Made4Net.Mobile.MessageQue.Enqueue(t.Translate(ex.Message))
                    Return False
                End Try
            Next
        End If

        Dim err As String = ""
        err = APXINBOUND_RemoveSerialNumbersFromStation(Logic.GetCurrentUser, Session("CreateLoadRCN"), _
                                                Session("CreateLoadRCNLine"), Session("CreateLoadConsignee"), _
                                                Session("CreateLoadSKU"), Session("CreateLoadLocation"))

        If Not String.IsNullOrEmpty(err) Then
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate(err))
        End If

        Return True
    End Function

    Private Sub clearAttributeValues()
        Dim oSku As String = Session("CreateLoadSKU")
        Dim oConsignee As String = Session("CreateLoadConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If objSkuClass Is Nothing Then Return
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Try
                    DO1.Value(oAtt.Name) = ""
                Catch ex As Exception
                End Try
            End If
        Next
    End Sub
End Class
