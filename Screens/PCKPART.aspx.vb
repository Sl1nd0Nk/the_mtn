Imports WMS.Logic
Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports System.Collections.Generic

<CLSCompliant(False)> Public Class PCKPART
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents lblUnits As Made4Net.WebControls.Label
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If WMS.Logic.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If

        If Not IsPostBack Then
            If Session("MobileSourceScreen") Is Nothing Then
                If Not Request.QueryString("sourcescreen") Is Nothing Then
                    Session("MobileSourceScreen") = Request.QueryString("sourcescreen")
                Else
                    Session("MobileSourceScreen") = "PCK"
                End If
            End If

            If Not WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PICKING) Then
                If WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.DELIVERY) Then
                    Response.Redirect(MapVirtualPath("Screens/DELLBLPRNT.aspx?sourcescreen=PCK"))
                Else
                    If Session("MobileSourceScreen") = "PCKPART" Then
                        Session("MobileSourceScreen") = "PCK"
                    End If
                    Response.Redirect(MapVirtualPath("screens/" & Session("MobileSourceScreen") & ".aspx"))
                End If
            End If

            Dim pcks As Picklist = Session("PCKPicklist")
            Dim pck As PickJob
            Try
                Dim tm As New WMS.Logic.TaskManager(pcks.PicklistID, True)
                pck = PickTask.getNextPick(pcks)
                Session("PCKPicklist") = pcks
            Catch ex As Made4Net.Shared.M4NException
                Made4Net.Mobile.MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
                Return
            Catch ex As Exception
                Made4Net.Mobile.MessageQue.Enqueue(ex.ToString)
                Return
            End Try
            setPick(pck)
            DO1.Value("UOMUNITS") = Session("UomUnits")
        End If
    End Sub

    Private Sub setPick(ByVal pck As PickJob)
        If pck Is Nothing Then
            'Response.Redirect(MapVirtualPath("Screens/DELLBLPRNT.aspx?sourcescreen=PCK"))
            Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=" & Session("MobileSourceScreen")))
        Else
            Session("PCKPicklistPickJob") = pck
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            ClearAttributes(pck.picklist)
            Dim pcks As Picklist = Session("PCKPicklist")
            DO1.Value("Picklist") = pck.picklist
            DO1.Value("LOADID") = pck.fromload
            DO1.Value("LOCATION") = pck.fromlocation
            DO1.Value("PICKMETHOD") = pcks.PickMethod
            DO1.Value("PICKTYPE") = pcks.PickType
            DO1.Value("SKU") = pck.sku
            DO1.Value("SKUDESC") = pck.skudesc
            DO1.Value("UOM") = pck.uom
            Dim sqluom As String = " SELECT DESCRIPTION FROM CODELISTDETAIL " & _
                          " WHERE CODELISTCODE = 'UOM' AND CODE = '" & pck.uom & "'"

            DO1.Value("UOMDesc") = Made4Net.DataAccess.DataInterface.ExecuteScalar(sqluom)
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            Session("UomUnits") = pck.uomunits
            DO1.Value("UOMUNITS") = Session("UomUnits")

            'Check For LowLimitCount
            Dim oLoad As New WMS.Logic.Load(pck.fromload)
            Dim oSku As New WMS.Logic.SKU(oLoad.CONSIGNEE, oLoad.SKU)
            If oLoad.UNITS <= oSku.LOWLIMITCOUNT And oLoad.LASTCOUNTDATE.Date < DateTime.Now.Date Then
                'Redirect to load count and get back
                MessageQue.Enqueue(trans.Translate("Load Units is less than low limit count - Please count load"))
                Session("LoadCNTLoadId") = oLoad.LOADID
                Session("LoadCountingSourceScreen") = "PCKPART"
                Response.Redirect(MapVirtualPath("Screens/CNT2.aspx"))
            End If

            If oSku.SKUClass Is Nothing Then
                setAttributeFieldsVisibility(pck.picklist, Nothing)
            Else
                setAttributeFieldsVisibility(pck.picklist, oSku.SKUClass.LoadAttributes)
            End If

            'Label Printing
            If pcks.ShouldPrintShipLabelOnPickLineCompleted Then
                If MobileUtils.LoggedInMHEID <> String.Empty And MobileUtils.GetMHEDefaultPrinter <> String.Empty Then
                    DO1.setVisibility("PRINTER", False)
                Else
                    DO1.setVisibility("PRINTER", True)
                End If
            Else
                DO1.setVisibility("PRINTER", False)
            End If
            End If
    End Sub

    Private Sub doCloseContainer()
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Not Session("PCKPicklistActiveContainerID") Is Nothing Then
            Try
                Dim pcklist As Picklist = Session("PCKPicklist") 'New Picklist(pck.picklist)
                Dim relStrat As ReleaseStrategyDetail
                relStrat = pcklist.getReleaseStrategy()
                Dim pck As PickJob = Session("PCKPicklistPickJob")
                If Not relStrat Is Nothing Then
                    If relStrat.DeliverContainerOnClosing Then
                        'Should deliver the container now
                        pcklist.CloseContainer(Session("PCKPicklistActiveContainerID"), True, WMS.Logic.GetCurrentUser)

                        Dim srcScreen As String = "PCK"
                        If Session("MobileSourceScreen").ToString().ToLower() = "taskmanager" Then
                            srcScreen = "PCKPART"
                        End If
                        Response.Redirect(MapVirtualPath(String.Format("screens/DELLBLPRNT.aspx?sourcescreen={0}", srcScreen)))

                        'Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=PCK"))
                    Else
                        'Should close the container - go back to PCK to open a new one
                        pcklist.CloseContainer(Session("PCKPicklistActiveContainerID"), False, WMS.Logic.GetCurrentUser)
                        Session.Remove("PCKPicklistActiveContainerID")
                        Response.Redirect(MapVirtualPath("screens/PCK.aspx"))
                    End If
                End If
            Catch ex As Threading.ThreadAbortException
            Catch ex As Made4Net.Shared.M4NException
                MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
                Return
            Catch ex As Exception
                MessageQue.Enqueue(ex.Message)
                Return
            End Try
        Else
            MessageQue.Enqueue(trans.Translate("Cannot Close Cotnainer - Container is blank"))
        End If
    End Sub

    Private Sub doNext()
        Dim pck As PickJob
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim pcklst As Picklist = Session("PCKPicklist")
        pck = Session("PCKPicklistPickJob")
        If pck Is Nothing Then
            'Response.Redirect(MapVirtualPath("Screens/DELLBLPRNT.aspx?sourcescreen=PCK"))
            Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=" & Session("MobileSourceScreen")))
        End If

        Try
            Dim sku As New sku(pck.consingee, pck.sku)
            pck.pickedqty = sku.ConvertToUnits(pck.uom) * Convert.ToDecimal(DO1.Value("UOMUNITS"))
            sku = Nothing
        Catch ex As Exception
            MessageQue.Enqueue(t.Translate("Error cannot get units"))
            Return
        End Try

        Try
            Dim tm As New WMS.Logic.TaskManager(pcklst.PicklistID, True)
            'ExtractAttributes()
            MobileUtils.ExtractPickingAttributes(pck, DO1)
            pck.container = Session("PCKPicklistActiveContainerID")
            If pck.pickedqty > 0 Then
                pck.TaskConfirmation.ConfirmationValues = MobileUtils.ExtractConfirmationValues(pck.TaskConfirmation.ConfirmationType, DO1)
            Else
                pck.TaskConfirmation = New TaskConfirmationNone()
            End If
            pck = PickTask.Pick(pcklst, pck, WMS.Logic.Common.GetCurrentUser)
        Catch ex As Made4Net.Shared.M4NException
            ClearAttributes(pck.picklist)
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Session("UomUnits") = pck.uomunits
            DO1.Value("UOMUNITS") = Session("UomUnits")
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            ClearAttributes(pck.picklist)
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            Session("UomUnits") = pck.uomunits
            DO1.Value("UOMUNITS") = Session("UomUnits")
            MessageQue.Enqueue(t.Translate(ex.Message))
            Return
        End Try

        'ClearAttributes()
        'DO1.Value("CONFIRM") = ""
        'DO1.Value("CONFIRMSKU") = ""
        If pcklst.PickType = WMS.Lib.PICKTYPE.NEGATIVEPALLETPICK And WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PUTAWAY) Then
            If Session("MobileSourceScreen").ToString().ToLower() = "taskmanager" Then
                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("SCREENS/RPK2.aspx?sourcescreen=" & Session("MobileSourceScreen")))
            Else
                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("SCREENS/RPK2.aspx?sourcescreen=PCKPART"))
            End If
        End If
        setPick(pck)
    End Sub

    Private Sub doBack()
        Session.Remove("CONFTYPE")
        Session.Remove("UomUnits")
        'Response.Redirect(MapVirtualPath("Screens/PCK.aspx"))
        If Session("MobileSourceScreen") Is Nothing Then
            Response.Redirect(MapVirtualPath("screens/PCK.aspx"))
        Else
            Response.Redirect(MapVirtualPath("screens/" & Session("MobileSourceScreen") & ".aspx"))
        End If

    End Sub

    Private Sub doMenu()
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        If WMS.Logic.TaskManager.isAssigned(UserId, WMS.Lib.TASKTYPE.PICKING) Then
            Dim tm As New WMS.Logic.TaskManager(UserId, WMS.Lib.TASKTYPE.PICKING)
            tm.ExitTask()
        End If
        Session.Remove("CONFTYPE")
        Session.Remove("UomUnits")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("Picklist")
        DO1.AddLabelLine("PickMethod")
        DO1.AddLabelLine("PickType")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOADID")
        DO1.AddLabelLine("UOM")
        DO1.AddLabelLine("UOMDesc")
        DO1.AddLabelLine("LOCATION")
        DO1.AddSpacer()
        Dim pck As PickJob = Session("PCKPicklistPickJob")

        'For Each attrName As String In getAttributesForAllPickList(pck.picklist)
        '    DO1.AddTextboxLine(attrName)
        'Next
        addAttributesFields(pck.picklist)

        addConfirmationFields(pck.TaskConfirmation.ConfirmationType)
        DO1.AddTextboxLine("UOMUNITS", "UOMUNITS", "")
        DO1.AddTextboxLine("PRINTER")
        DO1.setVisibility("UOMDesc", True)

        'Dim pcklist As Picklist = Session("PCKPicklist") 'New Picklist(pck.picklist)
        'Dim relStrat As ReleaseStrategyDetail
        'relStrat = pcklist.getReleaseStrategy()
        'If Not relStrat Is Nothing Then
        '    Session("CONFTYPE") = relStrat.ConfirmationType
        '    If relStrat.ConfirmationType = WMS.Lib.Release.CONFIRMATIONTYPE.SKULOCATION Then
        '        DO1.setVisibility("CONFIRMSKU", True)
        '    Else
        '        DO1.setVisibility("CONFIRMSKU", False)
        '    End If
        'End If
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "back"
                doBack()
            Case "closecontainer"
                doCloseContainer()
            Case "count"
                doCount()
        End Select
    End Sub

    Private Sub doCount()
        Session("PCKLOADID") = DO1.Value("LOADID")
        Session("PCKSOURCESCREEN") = "PCKPART"
        Response.Redirect(MapVirtualPath("Screens/CNT.aspx"))
    End Sub

    'Private Function ExtractAttributes() As AttributesCollection
    '    Dim pck As PickJob = Session("PCKPicklistPickJob")
    '    Dim Val As Object
    '    If Not pck Is Nothing Then
    '        If Not pck.oAttributeCollection Is Nothing Then
    '            For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
    '                Val = DO1.Value(pck.oAttributeCollection.Keys(idx))
    '                If Val = "" Then Val = Nothing
    '                pck.oAttributeCollection(idx) = Val
    '            Next
    '            Return pck.oAttributeCollection
    '        End If
    '    End If
    '    Return Nothing
    'End Function

    Private Sub ClearAttributes(ByVal pPicklist As String)
        For Each attStr As String In getAttributesForAllPickList(pPicklist)
            DO1.Value(String.Format("Attr_{0}", attStr)) = ""
        Next
    End Sub

    Private Function getAttributesForAllPickList(ByVal pPickListID As String) As System.Collections.Generic.List(Of String)
        If ViewState()("PickListAttributes") Is Nothing Then
            Dim list As New System.Collections.Generic.List(Of String)
            Dim sql As String
            Dim dt As New DataTable()
            sql = String.Format("select distinct attributename from pickdetail pd inner join sku on pd.consignee = sku.consignee and pd.sku = sku.sku inner join skuclsloadatt att on sku.classname = att.classname where pd.picklist={0} and pd.status in ({1},{2}) and att.PICKINGCAPTURE <> {3}", _
            Made4Net.Shared.FormatField(pPickListID), Made4Net.Shared.FormatField(WMS.Lib.Statuses.Picklist.PARTPICKED), _
            Made4Net.Shared.FormatField(WMS.Lib.Statuses.Picklist.RELEASED), Made4Net.Shared.FormatField(WMS.Logic.SkuClassLoadAttribute.CaptureType.NoCapture))
            Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)

            For Each dr As DataRow In dt.Rows
                list.Add(dr("ATTRIBUTENAME").ToString())
            Next
            ViewState("PickListAttributes") = list
        End If
        Return ViewState("PickListAttributes")
    End Function

    'Private Function getAttributesForAllPickList(ByVal pPickListID As String) As Dictionary(Of String, List(Of String))
    '    If ViewState()("PickListAttributes") Is Nothing Then
    '        Dim dict As New Dictionary(Of String, List(Of String))
    '        Dim sql As String
    '        Dim dt As New DataTable()
    '        sql = String.Format("select distinct (pd.consignee +'~'+ pd.sku) AttKey, attributename from pickdetail pd inner join sku on pd.consignee = sku.consignee and pd.sku = sku.sku inner join skuclsloadatt att on sku.classname = att.classname where pd.picklist={0} and pd.status in ({1},{2}) and att.PICKINGCAPTURE <> {3}", _
    '        Made4Net.Shared.FormatField(pPickListID), Made4Net.Shared.FormatField(WMS.Lib.Statuses.Picklist.PARTPICKED), _
    '        Made4Net.Shared.FormatField(WMS.Lib.Statuses.Picklist.RELEASED), Made4Net.Shared.FormatField(WMS.Logic.SkuClassLoadAttribute.CaptureType.NoCapture))
    '        Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)

    '        Dim attrkey As String
    '        Dim attrName As String
    '        For Each dr As DataRow In dt.Rows
    '            attrkey = dr("AttKey").ToString()
    '            attrName = dr("ATTRIBUTENAME").ToString()

    '            If Not dict.ContainsKey(attrkey) Then dict.Add(attrkey, New List(Of String))
    '            If Not dict(attrkey).Contains(attrName) Then dict(attrkey).Add(attrName)
    '        Next
    '        ViewState("PickListAttributes") = dict
    '    End If
    '    Return ViewState("PickListAttributes")
    'End Function

    Private Sub addAttributesFields(ByVal pPickListID As String)
        For Each attrName As String In getAttributesForAllPickList(pPickListID)
            DO1.AddTextboxLine(String.Format("Attr_{0}", attrName), attrName)
        Next
        'Dim addedAttributes As New List(Of String)
        'For Each pair As KeyValuePair(Of String, List(Of String)) In getAttributesForAllPickList(pPickListID)
        '    For Each attrName As String In pair.Value
        '        If Not addedAttributes.Contains(attrName) Then
        '            DO1.AddTextboxLine(String.Format("Attr_{0}", attrName))
        '            addedAttributes.Add(attrName)
        '        End If
        '    Next
        'Next

    End Sub

    Private Sub setAttributeFieldsVisibility(ByVal pPickListID As String, ByVal pSkuClassLoadAttributeColl As SkuClassLoadAttributeCollection)
        'Dim key As String = String.Format("{0}~{1}", pConsignee, pSKU)
        For Each attrName As String In getAttributesForAllPickList(pPickListID)
            DO1.setVisibility(String.Format("Attr_{0}", attrName), False)
            If pSkuClassLoadAttributeColl Is Nothing Then Continue For
            For Each skuClsLoadAtt As SkuClassLoadAttribute In pSkuClassLoadAttributeColl
                If skuClsLoadAtt.Name.Equals(attrName, StringComparison.OrdinalIgnoreCase) Then
                    If skuClsLoadAtt.CaptureAtPicking <> SkuClassLoadAttribute.CaptureType.NoCapture Then
                        DO1.setVisibility(String.Format("Attr_{0}", attrName), True)
                        Continue For
                    End If
                End If
            Next
        Next
    End Sub



    Private Sub addConfirmationFields(ByVal pConfirmType As String)
        Select Case pConfirmType
            Case WMS.Lib.CONFIRMATIONTYPE.LOAD
                DO1.AddTextboxLine("CONFIRM(LOAD)")
            Case WMS.Lib.CONFIRMATIONTYPE.LOCATION
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
            Case WMS.Lib.CONFIRMATIONTYPE.NONE
            Case WMS.Lib.CONFIRMATIONTYPE.SKU
                DO1.AddTextboxLine("CONFIRM(SKU)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATION
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATIONUOM
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
                DO1.AddTextboxLine("CONFIRM(UOM)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKUUOM
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(UOM)")
            Case WMS.Lib.CONFIRMATIONTYPE.UPC
                DO1.AddTextboxLine("CONFIRM(UPC)")
        End Select
    End Sub

    Private Sub clearConfirmationFields(ByVal pConfirmType As String)
        Select Case pConfirmType
            Case WMS.Lib.CONFIRMATIONTYPE.LOAD
                DO1.Value("CONFIRM(LOAD)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.LOCATION
                DO1.Value("CONFIRM(LOCATION)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.NONE
            Case WMS.Lib.CONFIRMATIONTYPE.SKU
                DO1.Value("CONFIRM(SKU)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATION
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(LOCATION)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATIONUOM
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(LOCATION)") = ""
                DO1.Value("CONFIRM(UOM)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKUUOM
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(UOM)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.UPC
                DO1.Value("CONFIRM(UPC)") = ""
        End Select
    End Sub

End Class
