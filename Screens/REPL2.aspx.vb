Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports WMS.MobileWebApp.apexInventory

<CLSCompliant(False)> Public Class REPL2
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            If Not Request.QueryString.Item("sourcescreen") Is Nothing Then
                Session("REPLSRCSCREEN") = Request.QueryString.Item("sourcescreen")
            End If
            SetScreen()
        End If
    End Sub

    Private Function LocalGetTask(ByVal replen As String) As WMS.Logic.ReplenishmentTask
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim ReplTask As WMS.Logic.ReplenishmentTask = Nothing
        Dim sql As String = String.Format("select * from TASKS where STATUS={0} and ASSIGNED={1} and TASKTYPE={2} and REPLENISHMENT={3} and USERID={4}", Made4Net.Shared.FormatField("ASSIGNED"), 1, Made4Net.Shared.FormatField("PARTREPL"), Made4Net.Shared.FormatField(replen), Made4Net.Shared.FormatField(WMS.Logic.GetCurrentUser))

        Dim dtReplen As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)

        If dtReplen.Rows.Count > 0 Then
            ReplTask = New WMS.Logic.ReplenishmentTask(dtReplen.Rows(0)("TASK"))
            Return ReplTask
        End If

        Return ReplTask
    End Function

    Private Function CheckActiveReplens(ByVal user As String) As WMS.Logic.Replenishment
        Dim ReplTaskDetail As WMS.Logic.Replenishment = Nothing

        Dim dtReplen As DataTable = APXINV_FindReplenByUser(user)

        If dtReplen.Rows.Count > 0 Then
            ReplTaskDetail = New WMS.Logic.Replenishment(dtReplen.Rows(0)("REPLENID"))

            Session.Add("REPLENEDQTY", dtReplen.Rows(0)("QTY"))

            Dim ReplTask As WMS.Logic.ReplenishmentTask = LocalGetTask(dtReplen.Rows(0)("REPLENID"))
            If Not ReplTask Is Nothing Then
                ReplTask.TOLOCATION = ReplTaskDetail.ToLocation
                ReplTask.TOLOAD = ReplTaskDetail.ToLoad
                Session("REPLTSKTaskId") = ReplTask
                Session.Add("QUEUEDREPLEN", True)
                Session("REPLTSKTDetail") = ReplTaskDetail
                APXINV_UpdateReplenStatus(dtReplen.Rows(0)("REPLENID"))
            End If
        End If
        Return ReplTaskDetail
    End Function

    Private Function LocalRequestTask(ByVal status As String, ByVal assigned As Integer, ByVal user As String) As WMS.Logic.Replenishment
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim ReplTaskDetail As WMS.Logic.Replenishment = Nothing
        Dim sql As String = String.Format("select * from TASKS where STATUS={0} and ASSIGNED={1} and USERID={2} and TASKTYPE LIKE '%REPL' Order By Priority desc", Made4Net.Shared.FormatField(status), assigned, Made4Net.Shared.FormatField(user))

        Dim dtReplen As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)

        If dtReplen.Rows.Count > 0 Then
            ReplTaskDetail = New WMS.Logic.Replenishment(dtReplen.Rows(0)("REPLENISHMENT"))
            Dim ReplTask As WMS.Logic.ReplenishmentTask = New ReplenishmentTask(dtReplen.Rows(0)("TASK"))

            If Not ReplTask Is Nothing Then
                ReplTask.TOLOCATION = ReplTaskDetail.ToLocation
                ReplTask.TOLOAD = ReplTaskDetail.ToLoad
                Session.Add("REPLTSKTaskId", ReplTask)
                Session.Add("REPLTSKTDetail", ReplTaskDetail)
                Session.Add("QUEUEDREPLEN", True)
                Dim RplQty As Integer = GetQueuedReplenQty(dtReplen.Rows(0)("REPLENISHMENT"), WMS.Logic.GetCurrentUser)
                If RplQty > 0 Then
                    Session.Add("REPLENEDQTY", RplQty)
                Else
                    Session.Remove("QUEUEDREPLEN")
                End If

                Return ReplTaskDetail
            Else
                Session.Remove("QUEUEDREPLEN")
            End If
        Else
            Session.Remove("QUEUEDREPLEN")
        End If

        Return Nothing
    End Function

    Private Function GetQueuedReplenQty(ByVal ReplenID As String, ByVal User As String) As Integer
        Dim sql As String = String.Format("select * from apx_ReplenRdt where ReplenId={0} and UserId={1}", Made4Net.Shared.FormatField(ReplenID), Made4Net.Shared.FormatField(User))
        Dim dtReplen As New DataTable

        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)
        If dtReplen.Rows.Count > 0 Then
            If Not IsDBNull(dtReplen.Rows(0)("QTY")) Then
                Return dtReplen.Rows(0)("QTY")
            Else
                Return 0
            End If
        Else
            Return 0
        End If
    End Function

    Private Sub SetScreen()
        Session.Remove("MULTIREPLEN")
        Dim ReplTask As WMS.Logic.ReplenishmentTask = Session("REPLTSKTaskId")
        Dim ReplTaskDetail As WMS.Logic.Replenishment = Session("REPLTSKTDetail")
        Dim replJob As ReplenishmentJob
        Dim UserReplTaskDetail As WMS.Logic.Replenishment = LocalRequestTask("ASSIGNED", 1, WMS.Logic.GetCurrentUser)
        'Dim UserReplTaskDetail As WMS.Logic.Replenishment = CheckActiveReplens(WMS.Logic.GetCurrentUser)
        If Session("REPLSRCSCREEN") = "RPKC1" Then
            replJob = ReplenishmentTask.getReplenishmentJob(ReplTaskDetail.FromLoad, WMS.Logic.GetCurrentUser)
        Else
            If UserReplTaskDetail Is Nothing Then
                If ReplTaskDetail Is Nothing Then
                    Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
                Else
                    replJob = ReplTask.getReplenishmentJob(ReplTaskDetail)
                End If
            Else
                ReplTask = Session("REPLTSKTaskId")
                replJob = ReplTask.getReplenishmentJob(UserReplTaskDetail)
                replJob.toLocation = ReplTask.TOLOCATION
                Session.Add("MULTIREPLEN", "DEPOSIT")
            End If
        End If

        If replJob Is Nothing Then
            APXINV_ClearReplen(WMS.Logic.GetCurrentUser)
            Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
        End If

        Session("REPLJobDetail") = replJob

        If replJob.IsHandOff Then
            'Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            'DO1.setVisibility("Note", True)
            'DO1.Value("Note") = trans.Translate("Task Destination Location is a Hand Off Location!")
        Else
            'DO1.setVisibility("Note", False)
        End If
        DO1.Value("TASKTYPE") = replJob.TaskType
        DO1.Value("LOCATION") = replJob.toLocation
        DO1.Value("LOADID") = replJob.fromLoad
        DO1.Value("UNITS") = replJob.Units
        DO1.Value("CONSIGNEE") = replJob.Consignee
        DO1.Value("SKU") = replJob.Sku
        DO1.Value("SKUDESC") = replJob.skuDesc
        DO1.Value("UOMUNITS") = replJob.UOMUnits

        If Not Session("QUEUEDREPLEN") Is Nothing Then
            DO1.setVisibility("CONSIGNEE", False)
            DO1.setVisibility("UNITS", False)
            DO1.setVisibility("SKU", False)
            DO1.setVisibility("SKUDESC", False)
        End If

        If ReplTaskDetail IsNot Nothing Then
            If ReplTaskDetail.ReplType = WMS.Logic.Replenishment.ReplenishmentTypes.FullReplenishment Then
                DO1.setVisibility("UOMUNITS", False)
            Else
                DO1.setVisibility("UOMUNITS", False)
            End If
        Else
            DO1.setVisibility("UOMUNITS", False)
        End If
    End Sub

    'If the location is the right one then replenish the location else just move
    'the load to new location and leave the Job active
    Private Sub doOverride()
        Dim errStr As String = CheckLocation(True)
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Not String.IsNullOrEmpty(errStr) Then
            'MessageQue.Enqueue(trans.Translate("Locations Match, use Next button"))
            MessageQue.Enqueue(trans.Translate(errStr))
            Return
        End If

        Dim repl As WMS.Logic.Replenishment = Session("REPLTSKTDetail")
        Dim repljob As ReplenishmentJob = Session("REPLJobDetail")
        Dim ReplTask As WMS.Logic.ReplenishmentTask = Session("REPLTSKTaskId")
        Try
            repl.OverrideReplenish(ReplTask, repljob, DO1.Value("TOLOCATION"), True, WMS.Logic.Common.GetCurrentUser)
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.ToString)
            Return
        End Try

        Session.Remove("REPLTSKTaskId")
        Session.Remove("REPLTSKTDetail")
        Session.Remove("REPLJobDetail")

        If Not ReplTask Is Nothing Then
            If ReplTask.ASSIGNMENTTYPE = WMS.Lib.TASKASSIGNTYPE.MANUAL Then
                If Not Session("REPLSRCSCREEN") Is Nothing Then
                    Response.Redirect(MapVirtualPath("Screens/" & Session("REPLSRCSCREEN") & ".aspx"))
                Else
                    Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
                End If
            Else
                Response.Redirect(MapVirtualPath("Screens/TaskManager.aspx"))
            End If
        Else
            If Not Session("REPLSRCSCREEN") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/" & Session("REPLSRCSCREEN") & ".aspx"))
            Else
                Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
            End If
        End If
    End Sub



    Private Sub doNext()
        Dim errStr As String = CheckLocation(False)
        If Not String.IsNullOrEmpty(errStr) Then
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            MessageQue.Enqueue(trans.Translate(errStr))
            Return
        Else
            If Not Session("QUEUEDREPLEN") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/REPL5.aspx"))
                Return
            End If
        End If

        Dim repl As WMS.Logic.Replenishment = Session("REPLTSKTDetail")
        Dim ReplTask As WMS.Logic.ReplenishmentTask = Session("REPLTSKTaskId")
        Dim repljob As ReplenishmentJob = Session("REPLJobDetail")
        Try
            ReplTask.Replenish(repl, repljob, WMS.Logic.Common.GetCurrentUser, True)
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.ToString())
            Return
        End Try

        Dim sLoadid As String = repl.FromLoad
        'If the replenishment task is a negative replenishment - should pick up the load.
        If repl.ReplType = WMS.Logic.Replenishment.ReplenishmentTypes.NegativeReplenishment Then
            Dim oLoad As New WMS.Logic.Load(sLoadid)
            If Not oLoad.STATUS = WMS.Lib.Statuses.LoadStatus.NONE AndAlso oLoad.UNITS > 0 Then
                oLoad.RequestPickUp(WMS.Logic.Common.GetCurrentUser)
                Dim taskID As String = oLoad.IsAssignedToTask()
                If taskID <> String.Empty Then
                    If oLoad.ACTIVITYSTATUS = WMS.Lib.Statuses.ActivityStatus.PUTAWAYPEND Then
                        Dim srcScreen As String = ""
                        If Not ReplTask Is Nothing Then
                            If ReplTask.ASSIGNMENTTYPE = WMS.Lib.TASKASSIGNTYPE.MANUAL Then
                                If Not Session("REPLSRCSCREEN") Is Nothing Then
                                    srcScreen = Session("REPLSRCSCREEN")
                                Else
                                    srcScreen = "REPL"
                                End If
                            Else
                                srcScreen = "TaskManager"
                            End If
                        Else
                            srcScreen = "REPL"
                        End If
                        Response.Redirect(MapVirtualPath("Screens/rpk2.aspx?sourcescreen=" & srcScreen))
                    ElseIf oLoad.ACTIVITYSTATUS = WMS.Lib.Statuses.ActivityStatus.REPLPENDING Then
                        Dim task As New WMS.Logic.ReplenishmentTask(taskID)
                        task.AssignUser(WMS.Logic.GetCurrentUser())
                        Session("REPLTSKTaskId") = task
                        Session("REPLTSKTDetail") = New WMS.Logic.Replenishment(task.Replenishment)
                        SetScreen()
                        DO1.Value("ToLocation") = ""
                        DO1.Value("TOUNITS") = ""
                        Return
                    End If
                End If
            End If
        End If

        Session.Remove("REPLTSKTaskId")
        Session.Remove("REPLTSKTDetail")
        Session.Remove("REPLJobDetail")
        If Not ReplTask Is Nothing Then
            If ReplTask.ASSIGNMENTTYPE = WMS.Lib.TASKASSIGNTYPE.MANUAL Then
                If Not Session("REPLSRCSCREEN") Is Nothing Then
                    Response.Redirect(MapVirtualPath("Screens/" & Session("REPLSRCSCREEN") & ".aspx"))
                Else
                    Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
                End If
            Else
                Response.Redirect(MapVirtualPath("Screens/TaskManager.aspx"))
            End If
        Else
            If Not Session("REPLSRCSCREEN") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/" & Session("REPLSRCSCREEN") & ".aspx"))
            Else
                Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
            End If
        End If
    End Sub


    Private Sub doBack()
        'Response.Redirect(MapVirtualPath("Screens/REPL1.aspx"))
        Dim ReplTask As WMS.Logic.ReplenishmentTask = Session("REPLTSKTaskId")
        If Not ReplTask Is Nothing Then
            If ReplTask.ASSIGNMENTTYPE = WMS.Lib.TASKASSIGNTYPE.MANUAL Then
                If Not Session("REPLSRCSCREEN") Is Nothing Then
                    Response.Redirect(MapVirtualPath("Screens/" & Session("REPLSRCSCREEN") & ".aspx"))
                Else
                    Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
                End If
            Else
                Response.Redirect(MapVirtualPath("Screens/TaskManager.aspx"))
            End If
        Else
            Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
        End If
    End Sub

    Private Function CheckLocation(ByVal pIsOverride As Boolean) As String
        Dim repljob As ReplenishmentJob = Session("REPLJobDetail")

        Dim strConfirmationLocation As String = ""
        Try
            strConfirmationLocation = Location.CheckLocationConfirmation(repljob.toLocation, DO1.Value("TOLOCATION"))
        Catch ex As Exception
            'Return False
            Return "Location does not exist."
        End Try

        If Not Location.Exists(strConfirmationLocation) Then
            Return "Location does not exist."
        End If
        Dim ReplTaskDetail As WMS.Logic.Replenishment = Session("REPLTSKTDetail")
        If Not pIsOverride Then
            'Dim inpLocation As String = DO1.Value("TOLOCATION")
            If strConfirmationLocation.Trim.ToLower <> repljob.toLocation.Trim.ToLower Then
                Return "Location does not match. Use override instead."
                'Return False
            Else
                Return ""
                'Return True
            End If
        End If

        If strConfirmationLocation.Trim.ToLower = repljob.toLocation.Trim.ToLower Then
            Return "Location matches target location. Use next button instead."
            'Return False
        Else
            Return ""
            'Return True
        End If
    End Function

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        'DO1.AddLabelLine("Note")
        DO1.AddLabelLine("DEPOSITING")
        DO1.AddLabelLine("TASKTYPE")
        DO1.AddLabelLine("LOADID", "From Load")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("UOMUNITS")
        DO1.AddLabelLine("UNITS")
        DO1.AddLabelLine("LOCATION", "Proceed To Location")
        DO1.AddSpacer()
        DO1.AddTextboxLine("TOLOCATION", "Scan location")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            'Case "override"
            '    doOverride()
            Case "next"
                doNext()
            Case "back"
                doBack()
        End Select
    End Sub
End Class

