Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess
Imports WMS.Logic

<CLSCompliant(False)> Public Class LOCCONTTASK
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If WMS.Logic.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            If Session("MobileSourceScreen") Is Nothing Then
                If Not Request.QueryString("sourcescreen") Is Nothing Then
                    Session("MobileSourceScreen") = Request.QueryString("sourcescreen")
                Else
                    Session("MobileSourceScreen") = "LOCCNT"
                End If
            End If

            If Session("CountingSrcScreen") <> "LOCCNT" Then
                Session.Remove("TSKTaskId")
                Session.Remove("TaskLocationCNTLocationId")
                Session.Remove("CountingSrcScreen")
            End If
            If Session("UserInitiatedCounting") = True Then
                'Session.Remove("UserInitiatedCounting")
                doNext()
            Else
                CheckAssigned()
            End If
        End If
    End Sub

    Private Sub doMenu()
        Try
            'Dim cnt As New WMS.Logic.Counting
            'cnt.ExitCount(WMS.Logic.GetCurrentUser)
            Dim UserId As String = WMS.Logic.Common.GetCurrentUser
            If WMS.Logic.TaskManager.isAssigned(UserId, WMS.Lib.TASKTYPE.LOCATIONCOUNTING) Then
                Dim tm As New WMS.Logic.TaskManager(UserId, WMS.Lib.TASKTYPE.LOCATIONCOUNTING)
                tm.ExitTask()
            End If
            Made4Net.Mobile.Common.GoToMenu()
        Catch ex As Exception
        End Try
        Session.Remove("TSKTaskId")
        Session.Remove("TaskLocationCNTLocationId")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Function CheckAssigned() As Boolean

        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID) ', Made4Net.Schema.CONNECTION_NAME)
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        Dim tm As New WMS.Logic.TaskManager
        Dim oCntTask As WMS.Logic.CountTask

        ' Check if we came from reg screen
        If Session("TaskLocationCNTLocationId") <> "" And Not Session("TaskLocationCNTLocationId") Is Nothing Then
            Dim strTaskCheck As String = "SELECT TASK FROM TASKS where STATUS='AVAILABLE' AND TASKTYPE='LOCCNT' AND FROMLOCATION='" & Session("TaskLocationCNTLocationId") & "'"
            Dim strTaskId As String = DataInterface.ExecuteScalar(strTaskCheck)
            If strTaskId <> "" Then
                Dim tm1 As New WMS.Logic.TaskManager(UserId, WMS.Lib.TASKTYPE.LOCATIONCOUNTING)
                While tm1.isAssigned(UserId, WMS.Lib.TASKTYPE.LOCATIONCOUNTING)
                    tm1.ExitTask()
                    tm1 = New WMS.Logic.TaskManager(UserId, WMS.Lib.TASKTYPE.LOCATIONCOUNTING)
                End While

                Dim ts As Task = New Task(strTaskId)
                ts.AssignUser(UserId)
                oCntTask = tm.getAssignedTask(UserId, WMS.Lib.TASKTYPE.LOCATIONCOUNTING)
                Session.Add("LocationCountTask", oCntTask)
                Session.Add("TSKTaskId", oCntTask.TASK)
                Session.Add("TaskLocationCNTLocationId", oCntTask.FROMLOCATION)
                DO1.Value("ASSIGNED") = "Assigned"
                DO1.Value("TASKID") = Session("TSKTaskId")
                Return True
            End If
        End If
        If WMS.Logic.TaskManager.isAssigned(UserId, WMS.Lib.TASKTYPE.LOCATIONCOUNTING) Then
            oCntTask = tm.getAssignedTask(UserId, WMS.Lib.TASKTYPE.LOCATIONCOUNTING)
            Session.Add("LocationCountTask", oCntTask)
            Session.Add("TSKTaskId", oCntTask.TASK)
            Session.Add("TaskLocationCNTLocationId", oCntTask.FROMLOCATION)
            DO1.Value("ASSIGNED") = "Assigned"
            DO1.Value("TASKID") = Session("TSKTaskId")
            Return True
        Else
            oCntTask = tm.RequestTask(UserId, WMS.Lib.TASKTYPE.LOCATIONCOUNTING)
            If Not oCntTask Is Nothing Then
                Session.Add("LocationCountTask", oCntTask)
                Session.Add("TSKTaskId", oCntTask.TASK)
                Session.Add("TaskLocationCNTLocationId", oCntTask.FROMLOCATION)
                DO1.Value("ASSIGNED") = "Assigned"
                DO1.Value("TASKID") = Session("TSKTaskId")
                Return True
            Else
                DO1.Value("ASSIGNED") = "Not Assigned"
                DO1.Value("TASKID") = ""
                Return False
            End If
        End If
    End Function

    Private Sub doNext()
        Try
            Dim UserId As String = WMS.Logic.Common.GetCurrentUser
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            If Not WMS.Logic.TaskManager.isAssigned(UserId, WMS.Lib.TASKTYPE.LOCATIONCOUNTING) Then
                Try
                    Dim tm As New WMS.Logic.TaskManager
                    tm.RequestTask(UserId, WMS.Lib.TASKTYPE.LOCATIONCOUNTING)
                    CheckAssigned()
                Catch ex As Made4Net.Shared.M4NException
                    MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
                Catch ex As Exception
                    MessageQue.Enqueue(trans.Translate(ex.Message))
                End Try
            Else
                CheckAssigned()
            End If
            If Session("TSKTaskId") = "" Or Session("TSKTaskId") = Nothing Then
                MessageQue.Enqueue(trans.Translate("No jobs assigned"))
            Else
                If Not Session("UserInitiatedCounting") = True Then
                    Session("CountingSrcScreen") = "LOCCONTTASK"
                    Session("MobileSourceScreen") = "LOCCONTTASK"
                End If
                Session.Remove("UserInitiatedCounting")
                Session("TaskLocationCNTLoadsDT") = CreateLoadsDatatable()
                Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx?countsourcescreen=" & Session("MobileSourceScreen")))
            End If
        Catch ex As System.Threading.ThreadAbortException
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("ASSIGNED")
        DO1.AddLabelLine("TASKID")
        DO1.AddSpacer()
        DO1.AddSpacer()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
        End Select
    End Sub

    Private Function CreateLoadsDatatable() As DataTable
        Dim oCntTask As WMS.Logic.CountTask = Session("LocationCountTask")
        Dim dt As New DataTable
        'Dim SQL As String = String.Format("select loadid,consignee,sku, units as fromqty, 0 as toqty, 0 as counted from invload where location = '{0}'", oCntTask.FROMLOCATION)
        Dim SQL As String = String.Format("select loadid,consignee,sku, units as fromqty, 0 as toqty, 0 as counted from invload where location = '{0}' and (isnull(activitystatus,'')='' or isnull(activitystatus,'')='{1}' or isnull(activitystatus,'')='{2}')", _
        oCntTask.FROMLOCATION, WMS.Lib.Statuses.ActivityStatus.PUTAWAYPEND, WMS.Lib.Statuses.ActivityStatus.REPLPENDING)
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dt)
        Return dt
    End Function

End Class
