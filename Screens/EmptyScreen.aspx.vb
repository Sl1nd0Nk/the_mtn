Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports WMS.MobileWebApp.apexInbound
Imports WMS.MobileWebApp.apexSerial
Imports System.Collections.Generic

<CLSCompliant(False)> Public Class EmptyScreen
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject
    Protected WithEvents Input As TextBox
    Protected WithEvents PackStn As Label
    Protected WithEvents lblUser As Label
    Protected WithEvents lblInput As Label
    Protected WithEvents InputPanel As Panel
    Protected WithEvents Logout As Button
    Protected WithEvents InfoTable As Table
    Protected WithEvents CurrentOrderPanel As Panel
    Protected WithEvents CurrentOrder As Label
    Protected WithEvents ScannedTotePanel As Panel
    Protected WithEvents ScannedTote As Label
    Protected WithEvents AttribPanel As Panel

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If WMS.Logic.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If

        If Not IsPostBack Then
            Dim ipaddress As String = ""
            ipaddress = Request.ServerVariables("HTTP_X_FORWARDED_FOR")

            If ipaddress = "" Or ipaddress Is Nothing Then
                ipaddress = Request.ServerVariables("REMOTE_ADDR")
            End If

            If GetStationUsingMachineIP(ipaddress) = False Then
                SetInitialState()
                MessageQue.Enqueue("Machine not configured as a packing station")
                Return
            End If
        End If
        Logout.Text = "logout " & WMS.Logic.Common.GetCurrentUser

        WhereToNext()
    End Sub

    Private Function ValidateScannedTote(ByVal Scanned As String) As String
        Dim dtLines As DataTable = GetScannedContainerID(Scanned)
        Dim strUnverSerTransIns As String

        If dtLines.Rows.Count > 0 Then
            strUnverSerTransIns = String.Format("Select * From [apx_packStationDetail] Where PickList={0}", FormatField(dtLines.Rows(0)("PICKLIST")))

            Dim dtLines1 As New DataTable
            Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines1)

            If dtLines1.Rows.Count > 0 Then
                If dtLines1.Rows(0)("StationID") <> Session("StationNumber") Then
                    Return "Tote is currently being packed at station" & dtLines1.Rows(0)("StationID")
                End If
            End If

            For Each dr As DataRow In dtLines.Rows
                Dim SQL As String = String.Format("Insert INTO apx_packStationDetail (StationID, PickList, PiclistLine, OrderID, OrderLine, SKU, SKUDESC, SKUClass, UOM, AdjQty, PickedQty, PackedQty, ToLoad, ToContainer, Consignee) " &
                                                  "VALUES ({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14})", FormatField(Session("StationNumber")), FormatField(dr("PICKLIST")), FormatField(dr("PICKLISTLINE")),
                                                  FormatField(dr("ORDERID")), FormatField(dr("ORDERLINE")), FormatField(dr("SKU")), FormatField(dr("SKUDESC")),
                                                  FormatField(dr("CLASSNAME")), FormatField(dr("UOM")),
                                                  FormatField(dr("ADJQTY")), FormatField(dr("PICKEDQTY")),
                                                  FormatField(0), FormatField(dr("TOLOAD")),
                                                  FormatField(dr("TOCONTAINER")), FormatField(dr("CONSIGNEE")))
                DataInterface.RunSQL(SQL)
            Next

            Dim SQL1 As String = String.Format("Update apx_packStationHeader set ToteID={0}, PickList={1}, CartonID=NULL, DocPrinted=0, DocVerified=0 where StationNumber={2}", FormatField(DO1.Value("ScannedTote")), FormatField(dtLines.Rows(0)("PICKLIST")), FormatField(Session("StationNumber")))
            DataInterface.RunSQL(SQL1)

            Session.Add("ToteID", DO1.Value("ScannedTote"))
            Session.Add("PickList", dtLines.Rows(0)("PICKLIST"))
        Else
            Return "Invalid Tote ID scanned"
        End If

        Return ""
    End Function

    Private Function GetScannedContainerID(ByVal Scanned As String) As DataTable
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [vPackablePicklists] Where ToContainer={0} AND ATTRIB1 is NULL", FormatField(Scanned))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        Return dtLines
    End Function

    Private Sub SetInitialState()
        Input.Visible = False
        ScannedTotePanel.Visible = False
        PackStn.Visible = False
        CurrentOrderPanel.Visible = False
        InfoTable.Visible = False
        AttribPanel.Visible = False
    End Sub

    Private Sub DisplayErrors(ByVal ErrStr As String)
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        MessageQue.Enqueue(t.Translate(ErrStr))
        Input.Focus()
    End Sub

    Private Sub WhereToNext()
        SetInitialState()

        Dim AllPacked As Boolean = False
        Dim ItemsList As String = ""
        Dim ShowAllItems As Boolean = False
        Dim CartonScanned As Boolean = False
        Dim DocPrinted As Boolean = False

        If Session("StationNumber") Is Nothing Then
            If GetStationUsingMachineIP(System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName()).GetValue(0).ToString()) = False Then
                Return
            End If
        End If

        If Session("ToteID") Is Nothing Or Session("PickList") Is Nothing Then
            If Not String.IsNullOrEmpty(Input.Text.Trim()) Then
                Dim Scanned As String = Input.Text.Trim()
                Input.Text = ""
                Dim ErrStr As String = ValidateScannedTote(Scanned)
                If Not String.IsNullOrEmpty(ErrStr) Then
                    DisplayErrors(ErrStr)
                    Return
                End If
            End If
        Else
            If lblInput.Text = "Scan Barcode/Serial" And Not String.IsNullOrEmpty(Input.Text.Trim()) Then
                Dim Scanned As String = Input.Text.Trim()
                Input.Text = ""
                Dim ErrStr As String = ProcessScannedBarcodeOrSerial(Scanned)
                If Not String.IsNullOrEmpty(ErrStr) Then
                    DisplayErrors(ErrStr)
                    Return
                End If
            ElseIf lblInput.Text = "Scan Carton" And Not String.IsNullOrEmpty(Input.Text.Trim()) Then
                Dim Scanned As String = Input.Text.Trim()
                Input.Text = ""
                Dim ErrStr As String = ProcessScannedCarton(Scanned)
                If Not String.IsNullOrEmpty(ErrStr) Then
                    DisplayErrors(ErrStr)
                    Return
                End If
            ElseIf lblInput.Text = "Confirm Qty Packed" And Not String.IsNullOrEmpty(Input.Text.Trim()) Then
                Dim Scanned As String = Input.Text.Trim()
                Input.Text = ""
                Dim ErrStr As String = ProcessEnteredQty(Nothing, Scanned)
                If Not String.IsNullOrEmpty(ErrStr) Then
                    DisplayErrors(ErrStr)
                    Return
                End If
            End If
        End If

        Dim NoTote As Boolean = True
        If Session("ToteID") IsNot Nothing And Session("PickList") IsNot Nothing Then
            If Session("ShowAllItems") IsNot Nothing Then
                ShowAllItems = True
            End If

            Dim dtLines As DataTable = GetPackItems(Session("StationNumber"), Session("PickList"), Session("ToteID"))

            If dtLines.Rows.Count > 0 Then
                NoTote = False
                AllPacked = True
                If ShowAllItems Then
                    ConstructFullItemsList(dtLines, AllPacked)
                Else
                    ConstructSingleItemsList(dtLines, AllPacked)
                End If

                If AllPacked Then
                    Dim CartonID As String = ""
                    CartonScanned = CheckCartonScanned(Session("StationNumber"), Session("PickList"), Session("ToteID"), DocPrinted, CartonID)

                    If CartonScanned Then
                        DispatchScannedSerials(CartonID)
                        ClearScannedSerials()
                        ClearPackingStation()
                    End If
                End If
            Else
                ClearOrderSessions()
                ClearPackingStation()
            End If
        End If

        SetDisplayAcccordingly(NoTote, AllPacked, ItemsList, ShowAllItems, CartonScanned)
    End Sub

    Private Function ProcessScannedBarcodeOrSerial(ByVal Input As String) As String
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim strUnverSerTransIns As String
        Dim Serial As String = Input
        Dim Msg As String = ""

        'Dim SerialResults As DataTable = APXSERIAL_GetSerialData(Serial)
        'MessageQue.Enqueue(t.Translate(SerialResults.Rows(0)("SKU")))

        strUnverSerTransIns = String.Format("Select * From [apx_packStationDetail] Where PickList={0} and StationID={1}", FormatField(Session("PickList")), FormatField(Session("StationNumber")))
        Dim dtLines1 As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines1)

        If dtLines1.Rows.Count > 0 Then
            Dim SerialResults As DataTable = APXSERIAL_GetSerialData(Serial)
            If SerialResults.Rows.Count > 0 Then
                Dim AlreadyScanned As Boolean = False
                Dim PackSer As DataTable = CheckIfSerialAlreadyScanned(SerialResults.Rows(0)("serial"))

                If PackSer.Rows.Count = 0 Then
                    Dim x As Integer = 0
                    While x < dtLines1.Rows.Count
                        If dtLines1.Rows(x)("SKU") = SerialResults.Rows(0)("SKU") And dtLines1.Rows(x)("PickedQty") > dtLines1.Rows(x)("PackedQty") Then
                            Exit While
                        End If
                        x = x + 1
                    End While

                    If x < dtLines1.Rows.Count Then
                        Dim SQL1 As String = String.Format("Update apx_packStationDetail set PackedQty=PackedQty+1 where PickList={0} and StationID={1} and PiclistLine={2} and SKU={3}", FormatField(dtLines1.Rows(x)("PickList")), FormatField(Session("StationNumber")), FormatField(dtLines1.Rows(x)("PiclistLine")), FormatField(dtLines1.Rows(x)("SKU")))
                        DataInterface.RunSQL(SQL1)

                        SQL1 = String.Format("Insert into apx_packStationSerials (SerialNumber, StationID, PickList, PiclistLine, OrderID, OrderLine, SKU, UOM, ToLoad)" &
                                             " VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8})",
                                             FormatField(SerialResults.Rows(0)("serial")), FormatField(Session("StationNumber")), FormatField(Session("PickList")), FormatField(dtLines1.Rows(x)("PiclistLine")),
                                             FormatField(dtLines1.Rows(x)("OrderID")), FormatField(dtLines1.Rows(x)("OrderLine")), FormatField(SerialResults.Rows(0)("SKU")), FormatField(SerialResults.Rows(0)("UOM")),
                                             FormatField(SerialResults.Rows(0)("ToLoad")))
                        DataInterface.RunSQL(SQL1)
                    Else
                        Msg = "Too much packed for SKU " + SerialResults.Rows(0)("SKU")
                    End If
                Else
                    Msg = "Serial Number already scanned to order " + PackSer.Rows(0)("OrderID") + " at station " + PackSer.Rows(0)("StationID")
                End If
            Else
                Dim NonSerSKU As String = GetSKU()

                If String.IsNullOrEmpty(NonSerSKU) Then
                    NonSerSKU = Serial
                End If

                Dim IsNoSerialised As Boolean = False
                Dim oSKU As New WMS.Logic.SKU(dtLines1.Rows(0)("Consignee"), NonSerSKU)

                Dim SKUClass As String = ""
                If oSKU.SKUClass IsNot Nothing Then
                    SKUClass = oSKU.SKUClass.ClassName
                    If SKUClass <> "SERIAL" Then
                        IsNoSerialised = True
                    End If
                Else
                    IsNoSerialised = True
                End If

                If IsNoSerialised Then
                    Dim x As Integer = 0
                    While x < dtLines1.Rows.Count
                        If dtLines1.Rows(x)("SKU") = NonSerSKU And dtLines1.Rows(x)("PickedQty") > dtLines1.Rows(x)("PackedQty") Then
                            Exit While
                        End If
                        x = x + 1
                    End While

                    If x < dtLines1.Rows.Count Then
                        Dim SQL1 As String = String.Format("Update apx_packStationDetail set PackedQty=PackedQty+1 where PickList={0} and StationID={1} and PiclistLine={2} and SKU={3}", FormatField(dtLines1.Rows(x)("PickList")), FormatField(Session("StationNumber")), FormatField(dtLines1.Rows(x)("PiclistLine")), FormatField(dtLines1.Rows(x)("SKU")))
                        DataInterface.RunSQL(SQL1)
                    Else
                        Msg = "Too much packed for SKU " + NonSerSKU
                    End If
                Else
                    Msg = "Scanned Serial Number " + Serial + " is not found  on the system"
                End If
            End If
        Else
            Session.Remove("ToteID")
            Msg = "No order at station"
        End If

        Return Msg
    End Function

    Private Function ProcessEnteredQty(ByVal oAttributes As WMS.Logic.AttributesCollection, ByVal Input As String) As String
        Dim attributesarray As New ArrayList
        Dim Msg As String = ""

        If Not IsNumeric(Input) Then
            Msg = "Invalid Qty Entered"
        Else
            If CDbl(Input) <> Session("QtyToConfirm") Then
                Msg = "Invalid Qty Entered"
            Else
                Dim SQL1 As String = String.Format("Update apx_packStationDetail set PackedQty={0} where PickList={1} and PiclistLine={2} and StationID={3}", FormatField(Session("QtyToConfirm")), FormatField(Session("PickList")), FormatField(Session("PLine")), FormatField(Session("StationNumber")))
                DataInterface.RunSQL(SQL1)
                Session.Remove("QtyToConfirm")
                Session.Remove("PLine")
                Session.Remove("SKU")
                Session.Remove("Consignee")
                'ClearAttributes(Session("PickList"))
            End If
        End If

        Return Msg
    End Function

    Private Function ProcessScannedCarton(ByVal Input As String) As String
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim packlist As New WMS.Logic.PackingListHeader
        Dim Msg As String = ""
        Try
            If WMS.Logic.PackingListHeader.Exists(Input) Then
                Msg = "Packing List Already exists"
            Else
                packlist.PACKINGLISTID = Input

                Dim dtLines As DataTable = GetPackItems(Session("StationNumber"), Session("PickList"), Session("ToteID"))

                Dim oCont1 As WMS.Logic.Container = CreateNewContainer(Input)
                If dtLines.Rows.Count > 0 Then
                    For Each dr As DataRow In dtLines.Rows
                        Dim loadID As String = dr("ToLoad")
                        Dim ld As New WMS.Logic.Load(loadID)
                        ld.RemoveFromContainer()
                        'Dim PCK As New PicklistDetail(Session("PickList"), dr("PiclistLine"), True)
                        'PCK.SetContainer(DO1.Value("ScannedCarton"), WMS.Logic.Common.GetCurrentUser)
                        oCont1.Place(ld, WMS.Logic.Common.GetCurrentUser)
                    Next
                End If

                'oCont1.Status = WMS.Lib.Statuses.Container.PACKED
                'oCont1.Post(WMS.Logic.Common.GetCurrentUser)
                'Dim cntr As New WMS.Logic.Container(DO1.Value("ScannedCarton"), True)
                'cntr.Status = WMS.Lib.Statuses.Container.PACKED
                'cntr.Post(WMS.Logic.Common.GetCurrentUser)

                If dtLines.Rows.Count > 0 Then
                    For Each dr As DataRow In dtLines.Rows
                        Dim oPackList As WMS.Logic.PackingListHeader = GetPackingList(packlist, dr("ToLoad"))

                        oPackList.PackLoad(dr("ToLoad"), WMS.Logic.Common.GetCurrentUser)
                        UpdatePickDetailToPacked(Input, Session("PickList"), dr("PiclistLine"))
                    Next
                End If

                If Session("PrintInvoice") IsNot Nothing Then
                    doPrintPackingList(packlist)
                End If

                doPrintShippingLabel(oCont1)

                UpdateStationHeaderPacked()
            End If
        Catch ex As Made4Net.Shared.M4NException
            Msg = ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Catch ex As Exception
            Msg = ex.ToString()
        End Try

        Return Msg
    End Function

    Private Sub UpdateStationHeaderPacked()
        Dim SQL1 As String = String.Format("Update apx_packStationHeader set CartonID={0}, DocPrinted=1 where StationNumber={1}", FormatField(DO1.Value("ScannedCarton")), FormatField(Session("StationNumber")))
        DataInterface.RunSQL(SQL1)
    End Sub

    Private Sub doPrintShippingLabel(ByVal oCont1 As WMS.Logic.Container)
        Dim prntr As LabelPrinter
        Try
            prntr = New LabelPrinter(Session("LabelPrinter"))
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception

        End Try

        oCont1.PrintShipLabel(prntr.PrinterQName)
    End Sub

    Private Sub doPrintPackingList(ByVal packlist As WMS.Logic.PackingListHeader)
        Try
            Dim oPackList As WMS.Logic.PackingListHeader = packlist
            oPackList.PrintPackingList(Session("LaserPrinter"), Made4Net.Shared.Translation.Translator.CurrentLanguageID, WMS.Logic.Common.GetCurrentUser)
        Catch ex As Exception
            Made4Net.Mobile.MessageQue.Enqueue(ex.ToString())
            Return
        End Try
    End Sub

    Private Sub UpdatePickDetailToPacked(ByVal ToContainer As String, ByVal Picklist As String, ByVal PickListLine As Integer)
        Dim SQL1 As String = String.Format("Update PICKDETAIL set ATTRIB1='PACKED', TOCONTAINER={0} where PICKLIST={1} AND PICKLISTLINE={2}", FormatField(ToContainer), FormatField(Picklist), FormatField(PickListLine))
        DataInterface.RunSQL(SQL1)
    End Sub

    Private Function GetPackingList(ByVal packlist As WMS.Logic.PackingListHeader, ByVal Load As String) As WMS.Logic.PackingListHeader
        Dim oPackList As WMS.Logic.PackingListHeader = packlist
        If oPackList.Lines.Count = 0 Then

            If Not WMS.Logic.Load.Exists(Load) Then
                Throw New Made4Net.Shared.M4NException(New Exception(), "Load does not exist", "Load does not exist")
            End If

            Dim sql As String = String.Format("select oh.consignee,oh.orderid,oh.targetcompany,oh.companytype from orderloads ol inner join outboundorheader oh on oh.orderid = ol.orderid and oh.consignee = ol.consignee where loadid = '{0}'", Load)
            Dim dt As New DataTable
            DataInterface.FillDataset(sql, dt)
            If dt.Rows.Count > 0 Then
                Dim oComp As New WMS.Logic.Company(dt.Rows(0)("consignee"), dt.Rows(0)("targetcompany"), dt.Rows(0)("companytype"))
                If WMS.Logic.PackingListHeader.Exists(oPackList.PACKINGLISTID) Then
                    oPackList.Update(oComp.CONSIGNEE, oComp.COMPANY, oComp.COMPANYTYPE, oComp.DEFAULTCONTACTID, 0, WMS.Logic.Common.GetCurrentUser)
                Else
                    oPackList.Create(oPackList.PACKINGLISTID, oComp.CONSIGNEE, oComp.COMPANY, oComp.COMPANYTYPE, oComp.DEFAULTCONTACTID, 0, WMS.Logic.Common.GetCurrentUser)
                End If
            Else
                Throw New Made4Net.Shared.M4NException(New Exception(), "Load does not belong to an order", "Load does not belong to an order")
            End If
        End If
        Return oPackList
    End Function

    Private Function CreateNewContainer(ByVal ContainerID As String) As WMS.Logic.Container
        Dim oCont1 As WMS.Logic.Container
        oCont1 = New WMS.Logic.Container
        oCont1.UsageType = "SHIPCONT"
        oCont1.ContainerId = ContainerID
        oCont1.Status = WMS.Lib.Statuses.Container.PACKED
        Try
            oCont1.Post(WMS.Logic.Common.GetCurrentUser)
        Catch ex As Made4Net.Shared.M4NException
            Made4Net.Mobile.MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return Nothing
        Catch ex As Exception
            Return Nothing
        End Try
        Return oCont1
    End Function

    Private Function GetSKU() As String
        Dim inpSku As String
        Dim SQL As String

        SQL = String.Format("SELECT SKU FROM vSKUCODE WHERE SKUCODE = '{0}'", DO1.Value("SKU"))

        inpSku = Made4Net.DataAccess.DataInterface.ExecuteScalar(SQL)

        Return inpSku
    End Function

    Private Function CheckIfSerialAlreadyScanned(ByVal Serial As String) As DataTable
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_packStationSerials] Where SerialNumber={0}", FormatField(Serial))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        Return dtLines
    End Function

    Private Sub SetupInput(ByVal Instruction As String)
        InputPanel.Visible = True
        Input.Text = ""
        Input.Focus()
        lblInput.Text = Instruction
    End Sub

    Private Sub ClearOrderSessions()
        Session.Remove("ToteID")
        Session.Remove("PickList")
        Session.Remove("QtyToConfirm")
        Session.Remove("PLine")
        Session.Remove("OrderID")
        Session.Remove("SKU")
        Session.Remove("Consignee")
    End Sub

    Private Sub SetupStation()
        PackStn.Visible = True
        PackStn.Text = "Packing Station " & Session("StationNumber")
    End Sub

    Private Sub SetupCurrentContainerProcessing()
        ScannedTotePanel.Visible = True
        ScannedTote.Text = Session("ToteID")

        CurrentOrderPanel.Visible = True
        CurrentOrder = Session("OrderID")
        InfoTable.Visible = True
    End Sub

    Private Sub SetDisplayAcccordingly(ByVal NoTote As Boolean, ByVal AllPacked As Boolean, ByVal ItemsList As String, ByVal ShowAllItems As Boolean, ByVal CartonScanned As Boolean)
        SetupStation()

        If NoTote Then
            SetupInput("Scan Tote/Container")
        Else
            SetupCurrentContainerProcessing()

            If ShowAllItems Then
                If Not AllPacked Then
                    SetupInput("Scan Barcode/Serial")
                Else
                    DisplaAllPackedAccordingly(CartonScanned)
                End If
            Else
                If Not AllPacked Then
                    Dim oSku As New WMS.Logic.SKU(Session("Consignee"), Session("SKU"))
                    setAttributeFieldsVisibility(Session("PickList"), oSku.SKUClass.LoadAttributes)
                    SetupInput("Confirm Qty Packed")
                Else
                    DisplaAllPackedAccordingly(CartonScanned)
                End If
            End If
        End If
    End Sub

    Private Function getAttributesForAllPickList(ByVal pPickListID As String) As List(Of String)
        Dim list As New List(Of String)
        Dim sql As String
        Dim dt As New DataTable()
        sql = String.Format("select distinct attributename from pickdetail pd inner join sku on pd.consignee = sku.consignee and pd.sku = sku.sku inner join skuclsloadatt att on sku.classname = att.classname where pd.picklist={0} and att.VERIFICATIONCAPTURE <> {1}",
        FormatField(pPickListID), FormatField(SkuClassLoadAttribute.CaptureType.NoCapture))
        DataInterface.FillDataset(sql, dt)

        For Each dr As DataRow In dt.Rows
            list.Add(dr("ATTRIBUTENAME").ToString())
        Next

        Return list
    End Function

    Private Sub setAttributeFieldsVisibility(ByVal pPickListID As String, ByVal pSkuClassLoadAttributeColl As SkuClassLoadAttributeCollection)
        Dim myLabel As Label
        Dim myTextBox As TextBox

        AttribPanel.Controls.Clear()
        For Each attrName As String In getAttributesForAllPickList(pPickListID)
            For Each skuClsLoadAtt As SkuClassLoadAttribute In pSkuClassLoadAttributeColl
                If skuClsLoadAtt.Name.Equals(attrName, StringComparison.OrdinalIgnoreCase) Then
                    If skuClsLoadAtt.CaptureAtVerification <> SkuClassLoadAttribute.CaptureType.NoCapture Then
                        myLabel = New Label()
                        myLabel.ID = String.Format("Attr_{0}", attrName)
                        myLabel.Text = attrName
                        myLabel.Visible = True
                        AttribPanel.Controls.Add(myLabel)
                        AttribPanel.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))

                        myTextBox = New TextBox()
                        myTextBox.ID = attrName
                        myTextBox.Text = ""
                        myTextBox.Visible = True
                        AttribPanel.Controls.Add(myTextBox)
                        AttribPanel.Controls.Add(New LiteralControl("<br />"))
                    End If
                End If
            Next
        Next

        If AttribPanel.Controls.Count > 0 Then
            AttribPanel.Controls.Add(New LiteralControl("<br />"))
            AttribPanel.Visible = True
        End If
    End Sub

    Private Sub DisplaAllPackedAccordingly(ByVal CartonScanned As Boolean)
        If CartonScanned Then
            ClearOrderSessions()
            SetupInput("Scan Tote/Container")
        Else
            SetupInput("Scan Carton")
        End If
    End Sub

    Private Function GetHeaderData() As List(Of String)
        Dim List As List(Of String) = New List(Of String)

        List.Add("PICKLIST LINE")
        List.Add("SKU")
        List.Add("PRODUCT DESCRIPTION")
        List.Add("PICKED QTY")
        List.Add("QTY PACKED")

        Return List
    End Function

    Private Function GetDetailData(ByVal dr As DataRow) As List(Of String)
        Dim List As List(Of String) = New List(Of String)

        List.Add(CStr(dr("PiclistLine")))
        List.Add(CStr(dr("SKU")))
        List.Add(CStr(dr("SKUDESC")))
        List.Add(CStr(dr("PickedQty")))
        List.Add(CStr(dr("PackedQty")))

        Return List
    End Function

    Private Sub ConstructFullItemsList(ByVal dtLines As DataTable, ByRef AllPacked As Boolean)
        Dim thr As TableHeaderRow
        Dim thc As TableHeaderCell
        Dim tr As TableRow
        Dim tc As TableCell

        Dim HeaderList As List(Of String) = New List(Of String)
        HeaderList = GetHeaderData()

        thr = New TableHeaderRow()
        For Each item As String In HeaderList
            thc = New TableHeaderCell()
            thc.Controls.Add(New LiteralControl(item))
            thr.Cells.Add(thc)
        Next

        InfoTable.Rows.Add(thr)

        For Each dr As DataRow In dtLines.Rows
            Dim RowData As List(Of String)
            RowData = GetDetailData(dr)

            tr = New TableRow()
            For Each item As String In RowData
                tc = New TableCell()
                tc.Controls.Add(New LiteralControl(item))
                tr.Cells.Add(tc)
            Next

            InfoTable.Rows.Add(tr)
            If dr("PickedQty") <> dr("PackedQty") And AllPacked = True Then
                AllPacked = False
            End If
        Next

        Session.Add("OrderID", dtLines.Rows(0)("OrderID"))
    End Sub

    Private Sub ConstructSingleItemsList(ByVal dtLines As DataTable, ByRef AllPacked As Boolean)
        Dim index As Integer = 0
        Dim thr As TableHeaderRow
        Dim thc As TableHeaderCell
        Dim tr As TableRow
        Dim tc As TableCell

        Dim HeaderList As List(Of String) = New List(Of String)
        HeaderList = GetHeaderData()

        thr = New TableHeaderRow()
        For Each item As String In HeaderList
            thc = New TableHeaderCell()
            thc.Controls.Add(New LiteralControl(item))
            thr.Cells.Add(thc)
        Next

        InfoTable.Rows.Add(thr)

        While index < dtLines.Rows.Count
            If CInt(dtLines.Rows(index)("PickedQty")) <> CInt(dtLines.Rows(index)("PackedQty")) Then
                Exit While
            End If

            index = index + 1
        End While

        If index < dtLines.Rows.Count Then
            AllPacked = False
            Dim RowData As List(Of String)
            RowData = GetDetailData(dtLines.Rows(index))

            tr = New TableRow()
            For Each item As String In RowData
                tc = New TableCell()
                tc.Controls.Add(New LiteralControl(item))
                tr.Cells.Add(tc)
            Next

            InfoTable.Rows.Add(tr)

            Session.Add("QtyToConfirm", dtLines.Rows(index)("PickedQty"))
            Session.Add("PLine", dtLines.Rows(index)("PiclistLine"))
            Session.Add("SKU", dtLines.Rows(index)("SKU"))
            Session.Add("Consignee", dtLines.Rows(index)("Consignee"))
            Session.Add("OrderID", dtLines.Rows(index)("OrderID"))
        Else
            For Each dr As DataRow In dtLines.Rows
                Dim RowData As List(Of String)
                RowData = GetDetailData(dr)

                tr = New TableRow()
                For Each item As String In RowData
                    tc = New TableCell()
                    tc.Controls.Add(New LiteralControl(item))
                    tr.Cells.Add(tc)
                Next

                InfoTable.Rows.Add(tr)
            Next
            Session.Add("OrderID", dtLines.Rows(0)("OrderID"))
        End If
    End Sub

    Private Sub ClearPackingStation()
        Dim SQL1 As String = String.Format("delete from apx_packStationDetail where StationID={0}", FormatField(Session("StationNumber")))
        If Session("PickList") IsNot Nothing Then
            SQL1 = SQL1 & " and PickList='" & FormatField(Session("PickList")) & "'"
        End If
        DataInterface.RunSQL(SQL1)

        Dim SQL As String = String.Format("Update apx_packStationHeader set ToteID=NULL, PickList=NULL, CartonID=NULL, DocPrinted=0, DocVerified=0 where StationNumber={0}", FormatField(Session("StationNumber")))
        DataInterface.RunSQL(SQL)
    End Sub

    Private Sub ClearScannedSerials()
        Dim SQL1 As String = String.Format("delete from apx_packStationSerials where StationID={0}", FormatField(Session("StationNumber")))
        DataInterface.RunSQL(SQL1)
    End Sub

    Private Sub DispatchScannedSerials(ByVal CartonID As String)
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_packStationSerials] Where StationID={0}", FormatField(Session("StationNumber")))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)
        If dtLines.Rows.Count > 0 Then
            For Each dr As DataRow In dtLines.Rows
                Dim SQL1 As String = String.Format("Update apx_serial set dispatched=1, userid={0}, serialStatus='DISPATCHED', salesOrderID={1}, salesOrderLineID={2}, cartonID={3} where serial={4}",
                                                   FormatField(Logic.GetCurrentUser), FormatField(dr("OrderID")), FormatField(dr("OrderLine")), FormatField(CartonID), FormatField(dr("SerialNumber")))
                DataInterface.RunSQL(SQL1)
            Next
        End If
    End Sub

    Private Function CheckCartonScanned(ByVal StationID As Integer, ByVal PickList As String, ByVal ToContainer As String, ByRef DocPrinted As Boolean, ByRef CartonID As String) As Boolean
        Dim strUnverSerTransIns As String
        Dim CartonScanned As Boolean = False

        strUnverSerTransIns = String.Format("Select * From [apx_packStationHeader] Where StationNumber={0} and PickList={1} and ToteID={2}", FormatField(StationID), FormatField(PickList), FormatField(ToContainer))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        CartonID = ""

        If dtLines.Rows.Count > 0 Then
            If Not IsDBNull(dtLines.Rows(0)("CartonID")) Then
                CartonID = dtLines.Rows(0)("CartonID")
                CartonScanned = True
            End If

            If Not IsDBNull(dtLines.Rows(0)("DocPrinted")) Then
                DocPrinted = dtLines.Rows(0)("DocPrinted")
            End If
        End If

        Return CartonScanned
    End Function

    Private Function GetPackItems(ByVal StationID As Integer, ByVal PickList As String, ByVal ToContainer As String) As DataTable
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_packStationDetail] Where StationID={0} and PickList={1} and ToContainer={2}", FormatField(StationID), FormatField(PickList), FormatField(ToContainer))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        Return dtLines
    End Function

    Private Function GetStationUsingMachineIP(ByVal IPAddress As String) As Boolean
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim Station As Integer = 0
        Dim strUnverSerTransIns As String
        Dim Result As Boolean = False
        strUnverSerTransIns = String.Format("Select * From [apx_packStationHeader] Where StationIP={0}", FormatField(IPAddress))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        If dtLines.Rows.Count > 0 Then
            If Not IsDBNull(dtLines.Rows(0)("StationNumber")) Then
                If Not IsDBNull(dtLines.Rows(0)("LaserPrinter")) Then
                    If Not IsDBNull(dtLines.Rows(0)("LabelPrinter")) Then
                        Session.Add("StationNumber", dtLines.Rows(0)("StationNumber"))
                        Session.Add("LaserPrinter", dtLines.Rows(0)("LaserPrinter"))
                        Session.Add("LabelPrinter", dtLines.Rows(0)("LabelPrinter"))

                        If Not IsDBNull(dtLines.Rows(0)("ToteID")) Then
                            Session.Add("ToteID", dtLines.Rows(0)("ToteID"))
                            If Not IsDBNull(dtLines.Rows(0)("PickList")) Then
                                Session.Add("PickList", dtLines.Rows(0)("PickList"))
                            End If
                        End If

                        ShowAllToteItemsAtPacking()
                        PrintInvoiceAtPacking()
                        Result = True
                    Else
                        MessageQue.Enqueue(t.Translate("Label Printer Not set up at station " + dtLines.Rows(0)("StationNumber")))
                    End If
                Else
                    MessageQue.Enqueue(t.Translate("Invoice Printer Not set up at station " + dtLines.Rows(0)("StationNumber")))
                End If
            Else
                MessageQue.Enqueue(t.Translate("Machine not connfigured to a pack station - IP Address[" + IPAddress + "]"))
            End If
        Else
            MessageQue.Enqueue(t.Translate("Machine not connfigured to a pack station - IP Address[" + IPAddress + "]"))
        End If

        Return Result

    End Function

    Private Sub ShowAllToteItemsAtPacking()
        Dim Value As String = "NO"
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'ShowAllToteItemsAtPacking'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        If Value = "YES" Then
            Session.Add("ShowAllItems", Value)
        Else
            Session.Remove("ShowAllItems")
        End If
    End Sub

    Private Sub PrintInvoiceAtPacking()
        Dim Value As String = "NO"
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'PrintInvoiceAtPacking'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        If Value = "YES" Then
            Session.Add("PrintInvoice", Value)
        Else
            Session.Remove("PrintInvoice")
        End If
    End Sub

    Protected Sub Logout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Logout.Click
        If Not IsPostBack Then
            Made4Net.Shared.Web.User.Logout()
            WMS.Logic.GotoLogin()
        End If
    End Sub

End Class
