Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess
Imports Made4Net.Shared

Partial Public Class MoveYardTrailer
    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub doMenu()
        Session.Remove("YardMovementEquipment")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddSpacer()
        DO1.AddSpacer()
        DO1.AddTextboxLine("TrailerId")
        DO1.AddSpacer()
        DO1.FocusField = "TrailerId"
        DO1.DefaultButton = "Next"
    End Sub

    Private Sub doNext()
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            If DO1.Value("TrailerId").Trim = String.Empty Then
                Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("Please scan trailer id"))
                DO1.Value("TrailerId") = ""
                Return
            End If
            Dim oYardEquipment As WMS.Logic.YardEquipment = WMS.Logic.YardEquipment.GetYardEquipmentByTrailerID(DO1.Value("TrailerId"))
            If oYardEquipment.STATUS <> WMS.Lib.Statuses.YardEquipment.INYARD Then
                Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("Scanned trailer status is incorrect"))
                DO1.Value("TrailerId") = ""
                Return
            End If
            Session("YardMovementEquipment") = oYardEquipment
        Catch m4nEx As Made4Net.Shared.M4NException
            MessageQue.Enqueue(m4nEx.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            DO1.Value("TrailerId") = ""
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            DO1.Value("TrailerId") = ""
            Return
        End Try
        Response.Redirect(MapVirtualPath("Screens/MoveYardTrailer2.aspx"))
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
        End Select
    End Sub

End Class
