Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports WMS.MobileWebApp.apexInbound
Imports WMS.MobileWebApp.apexInventory
Imports WMS.MobileWebApp.apexSerial
Imports System.Collections.Generic

<CLSCompliant(False)> Public Class OrdReserve
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If WMS.Logic.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If

        If Not IsPostBack Then
            If Not APXINV_CheckIfTableExist("apx_reversed_order_header") Then
                APXINV_CreateTable("apx_reversed_order_header", New List(Of String)() From {
                                                         "Consignee NVARCHAR (50) NULL",
                                                         "OrderID NVARCHAR (50) NULL",
                                                         "UserID NVARCHAR (50) NULL",
                                                         "TotalQty INT NULL",
                                                         "QtyShipped INT NULL",
                                                         "QtyReversed INT NULL",
                                                         "FullPallet BIT NULL",
                                                         "ProcessStatus NVARCHAR (50) NULL",
                                                         "AddDate DATETIME NULL",
                                                         "EditDate DATETIME NULL"})
            End If

            If Not APXINV_CheckIfTableExist("apx_reversed_order_detail") Then
                APXINV_CreateTable("apx_reversed_order_detail", New List(Of String)() From {
                                                         "UserID NVARCHAR (50) NULL",
                                                         "OrderID NVARCHAR (50) NULL",
                                                         "CartonID NVARCHAR (50) NULL",
                                                         "Confirmed BIT NULL",
                                                         "AddDate DATETIME NULL",
                                                         "EditDate DATETIME NULL"})
            End If

            If Not APXINV_CheckIfTableExist("apx_reversed_order_stock") Then
                APXINV_CreateTable("apx_reversed_order_stock", New List(Of String)() From {
                                                         "UserID NVARCHAR (50) NULL",
                                                         "OrderID NVARCHAR (50) NULL",
                                                         "OrderLine INT NULL",
                                                         "CartonID NVARCHAR (50) NULL",
                                                         "Serialised BIT NULL",
                                                         "Serial NVARCHAR (50) NULL",
                                                         "Sku NVARCHAR (50) NULL",
                                                         "UOM NVARCHAR (20) NULL",
                                                         "SkuDesc NVARCHAR (MAX) NULL",
                                                         "Quantity INT NULL",
                                                         "Confirmed BIT NULL",
                                                         "Id BIGINT NULL",
                                                         "NewLoadID NVARCHAR (50) NULL",
                                                         "ReturnLocation NVARCHAR (50) NULL",
                                                         "ParentNodeID BIGINT NULL",
                                                         "AddDate DATETIME NULL",
                                                         "EditDate DATETIME NULL"})
            End If

            If Session("UserAssignedPrinter") Is Nothing Then
                Dim Printer As String = GetUserPrinter(Logic.GetCurrentUser())

                If Printer = "" Then
                    Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

                    MessageQue.Enqueue(t.Translate("User " & Logic.GetCurrentUser() & " is not assigned to a label printer"))
                Else
                    Session.Add("UserAssignedPrinter", Printer)
                End If
            End If
        End If

        WhereToNext()
    End Sub

    Private Function GetUserPrinter(ByVal User As String) As String
        Dim SQL As String = String.Format("SELECT * FROM USERPROFILE WHERE USERID={0}", FormatField(User))
        Dim DtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, DtLines)

        If DtLines.Rows.Count > 0 Then
            If IsDBNull(DtLines.Rows(0)("DEFAULTPRINTER")) Then
                Return ""
            Else
                Return DtLines.Rows(0)("DEFAULTPRINTER")
            End If
        Else
            Return ""
        End If
    End Function

    Private Sub InitControls()
        DO1.setVisibility("ORD", False)
        DO1.setVisibility("CTNCOUNT", False)
        DO1.setVisibility("SCANORD", False)
        DO1.setVisibility("SCANCARTON", False)
        DO1.setVisibility("SCANSKU", False)
        DO1.setVisibility("ORDLIST", False)
        DO1.setVisibility("SCANSER", False)
        DO1.setVisibility("ENTQTY", False)
        DO1.setVisibility("SCANLOC", False)
    End Sub

    Private Sub InitButtons()
        DO1.Button(0).Text = "Next"
        DO1.Button(1).Visible = False
        DO1.Button(2).Visible = True
        DO1.Button(3).Visible = False
    End Sub

    Private Sub WhereToNext()
        InitControls()
        InitButtons()
        Dim InprogDT As DataTable = CheckInprogressForUser(WMS.Logic.Common.GetCurrentUser)

        If InprogDT.Rows.Count > 0 Then
            DO1.Button(3).Visible = True
            DO1.setVisibility("ORD", True)
            DO1.Value("ORD") = InprogDT.Rows(0)("OrderID")
            Session.Add("OrdConsignee", InprogDT.Rows(0)("Consignee"))

            Dim CartonCount As Integer = 0
            Dim CartonConfirmed As Integer = 0
            Dim AllCartonsConfimed As Boolean = CheckCartonScanned(DO1.Value("ORD"), WMS.Logic.Common.GetCurrentUser, CartonCount, CartonConfirmed)
            If AllCartonsConfimed Then
                DO1.setVisibility("CTNCOUNT", True)
                DO1.Value("CTNCOUNT") = CartonCount & " carton(s) in total"
                ValidateSkuInprogress(DO1.Value("ORD"), WMS.Logic.Common.GetCurrentUser)

                If Session("CurrentSku") IsNot Nothing Then
                    ShowProductContent(DO1.Value("ORD"), WMS.Logic.Common.GetCurrentUser)
                Else
                    ShowOrderContent(DO1.Value("ORD"), WMS.Logic.Common.GetCurrentUser)
                End If
            Else
                ShowOrderCartonConfirm(CartonCount, CartonConfirmed)
            End If
        Else
            DO1.setVisibility("SCANORD", True)
            DO1.FocusField = "SCANORD"
        End If
    End Sub

    Private Sub ShowProductContent(ByVal OrderID As String, ByVal UserID As String)
        DO1.setVisibility("ORDLIST", True)
        DO1.Value("ORDLIST") = ShowOrdList(OrderID, UserID, Session("CurrentSku"))

        If Session("AddLoad") IsNot Nothing Then
            DO1.setVisibility("SCANLOC", True)
            DO1.FocusField = "SCANLOC"
        Else
            Dim oSku As SKU = New SKU(Session("OrdConsignee"), Session("CurrentSku"))
            Dim ClassName As String = oSku.SKUClass.ClassName
            If ClassName.ToUpper() = "SERIAL" Then
                DO1.setVisibility("SCANSER", True)
                DO1.FocusField = "SCANSER"
            Else
                DO1.setVisibility("ENTQTY", True)
                DO1.FocusField = "ENTQTY"
            End If
        End If
    End Sub

    Private Sub ShowOrderContent(ByVal OrderID As String, ByVal UserID As String)
        DO1.setVisibility("ORDLIST", True)
        DO1.Value("ORDLIST") = ShowOrdList(OrderID, UserID)

        If Session("OrdComplete") Is Nothing Then
            DO1.setVisibility("SCANSKU", True)
            DO1.FocusField = "SCANSKU"
        Else
            DO1.Button(3).Visible = False
            DO1.Button(1).Visible = True
        End If
    End Sub

    Private Function GetLoadID(ByVal sDtLines As DataTable, ByVal Sku As String) As String
        Dim x As Integer = 0
        Dim ld As String = ""

        While x < sDtLines.Rows.Count
            If sDtLines.Rows(x)("Sku") = Sku Then
                If Not IsDBNull(sDtLines.Rows(x)("NewLoadID")) Then
                    ld = sDtLines.Rows(x)("NewLoadID")
                End If
            End If
            x += 1
        End While

        Return ld
    End Function

    Private Sub ValidateSkuInprogress(ByVal OrderId As String, ByVal UserID As String)
        Dim sDtLines As DataTable = GetOrderStock(OrderId, UserID)
        Dim SkuList As List(Of String) = New List(Of String)
        Dim SkuDescList As List(Of String) = New List(Of String)
        Dim SkuLoads As List(Of String) = New List(Of String)
        Dim SkuLocations As List(Of String) = New List(Of String)
        GetDistinctSkuFromResults(SkuList, SkuDescList, SkuLoads, SkuLocations, sDtLines)

        Dim i As Integer = 0
        While i < SkuList.Count
            Dim Qty As Integer = 0
            Dim QtyConfirmed As Integer = 0
            GetSkuQuantities(SkuList(i), Qty, QtyConfirmed, sDtLines)

            If QtyConfirmed > 0 Then
                If QtyConfirmed < Qty Then
                    If Session("CurrentSku") Is Nothing Then
                        Session.Add("CurrentSku", SkuList(i))
                    End If
                    Exit While
                Else
                    If String.IsNullOrEmpty(SkuLoads(i)) Then
                        If Session("CurrentSku") Is Nothing Then
                            Session.Add("CurrentSku", SkuList(i))
                        End If
                        Session.Add("AddLoad", SkuList(i))
                        Exit While
                    End If
                End If
            End If

            i += 1
        End While
    End Sub

    Private Sub GetDistinctSkuFromResults(ByRef SkuList As List(Of String), ByRef SkuDescList As List(Of String), ByRef SkuLoads As List(Of String), ByRef SkuLocations As List(Of String), ByVal sDtLines As DataTable)
        Dim i As Integer = 0
        While i < sDtLines.Rows.Count
            Dim Sku As String = sDtLines.Rows(i)("Sku")
            If SkuList.IndexOf(Sku) < 0 Then
                SkuList.Add(Sku)
                SkuDescList.Add(sDtLines.Rows(i)("SkuDesc"))
                If Not IsDBNull(sDtLines.Rows(i)("NewLoadID")) Then
                    SkuLoads.Add(sDtLines.Rows(i)("NewLoadID"))
                Else
                    SkuLoads.Add("")
                End If
                If Not IsDBNull(sDtLines.Rows(i)("ReturnLocation")) Then
                    SkuLocations.Add(sDtLines.Rows(i)("ReturnLocation"))
                Else
                    SkuLocations.Add("")
                End If
            End If
                i += 1
        End While
    End Sub

    Private Sub GetSkuQuantities(ByVal Sku As String, ByRef Qty As Integer, ByRef QtyConfirmed As Integer, ByVal sDtLines As DataTable)
        Dim a As Integer = 0
        While a < sDtLines.Rows.Count
            If Sku = sDtLines.Rows(a)("Sku") Then
                Dim isParent As Boolean = False
                Dim id As Int64 = sDtLines.Rows(a)("Id")
                Dim y As Integer = 0
                While y < sDtLines.Rows.Count
                    If Not IsDBNull(sDtLines.Rows(y)("ParentNodeID")) Then
                        Dim PNodeId As Int64 = sDtLines.Rows(y)("ParentNodeID")
                        If PNodeId = id Then
                            isParent = True
                            Exit While
                        End If
                    End If
                    y += 1
                End While

                If Not isParent Then
                    Qty += CInt(sDtLines.Rows(a)("Quantity"))
                    Dim Confirmed As Boolean = False
                    If Not IsDBNull(sDtLines.Rows(a)("Confirmed")) Then
                        Confirmed = CBool(sDtLines.Rows(a)("Confirmed"))
                    End If

                    If Confirmed Then
                        QtyConfirmed += CInt(sDtLines.Rows(a)("Quantity"))
                    End If
                End If
            End If
            a += 1
        End While
    End Sub

    Private Function GetOrderStock(ByVal OrderID As String, ByVal UserID As String, Optional OrdSku As String = Nothing) As DataTable
        Dim SQL As String = String.Format("select * from apx_reversed_order_stock where OrderID={0} and UserID={1}", FormatField(OrderID), FormatField(UserID))

        If OrdSku IsNot Nothing Then
            SQL = SQL & " and Sku='" & OrdSku & "'"
        End If

        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, sDtLines, False, Nothing)

        Return sDtLines
    End Function

    Private Function ShowOrdList(ByVal OrderID As String, ByVal UserID As String, Optional OrdSku As String = Nothing) As String
        Dim sDtLines As DataTable = GetOrderStock(OrderID, UserID, OrdSku)

        Dim SkuList As List(Of String) = New List(Of String)
        Dim SkuDescList As List(Of String) = New List(Of String)
        Dim SkuLoads As List(Of String) = New List(Of String)
        Dim SkuLocations As List(Of String) = New List(Of String)
        Dim StartTable As String = "<table border='1'><tr bgcolor='#DCDCDC'><th nowrap>SKU</th><th nowrap>QTY</th><th nowrap>QTY SCANNED</th><th nowrap>LOCATION</th><th nowrap>LOAD</th><th nowrap>DESCRIPTION</th></tr>"
        Dim EndTable As String = "</table>"
        Dim StartRow As String = "<tr>"
        Dim StartRowComplete As String = "<tr bgcolor='#7CFC00'>"
        Dim EndRow As String = "</tr>"
        Dim CompletedSku As Integer = 0

        If sDtLines.Rows.Count > 0 Then
            GetDistinctSkuFromResults(SkuList, SkuDescList, SkuLoads, SkuLocations, sDtLines)

            Dim i As Integer = 0
            While i < SkuList.Count
                Dim Qty As Integer = 0
                Dim QtyConfirmed As Integer = 0
                GetSkuQuantities(SkuList(i), Qty, QtyConfirmed, sDtLines)

                If OrdSku Is Nothing Then
                    If Qty = QtyConfirmed Then
                        CompletedSku += 1
                        StartTable = StartTable & StartRowComplete
                    Else
                        StartTable = StartTable & StartRow
                    End If
                Else
                    StartTable = StartTable & StartRow
                End If

                StartTable = StartTable & "<td nowrap>" & SkuList(i) & "</td>" & "<td nowrap>" & Qty & "</td>" & "<td nowrap>" & QtyConfirmed & "</td>" & "<td nowrap>" & SkuLocations(i) & "</td>" & "<td nowrap>" & SkuLoads(i) & "</td>" & "<td nowrap>" & SkuDescList(i) & "</td>" & EndRow
                i += 1
            End While

            If CompletedSku > 0 And CompletedSku = SkuList.Count Then
                Session.Add("OrdComplete", True)
            End If
        End If

        Return StartTable & EndTable
    End Function

    Private Sub ShowOrderCartonConfirm(ByVal CartonCount As Integer, ByVal CartonConfirmed As Integer)
        DO1.setVisibility("SCANCARTON", True)
        DO1.FocusField = "SCANCARTON"

        DO1.setVisibility("CTNCOUNT", True)
        DO1.Value("CTNCOUNT") = "Order Carton " & CartonConfirmed + 1 & " Of " & CartonCount
    End Sub

    Private Function CheckCartonScanned(ByVal OrderID As String, ByVal UserID As String, ByRef CartonCount As Integer, ByRef CartonConfirmed As Integer) As Boolean
        Dim Sql As String = "select * from apx_reversed_order_detail where UserID = '" + UserID + "' and OrderID = '" & OrderID & "'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)

        CartonCount = sDtLines.Rows.Count

        Dim AllConfirmed As Boolean = True
        Dim x As Integer = 0
        While x < sDtLines.Rows.Count
            Dim Confirmed As Boolean = CBool(sDtLines.Rows(x)("Confirmed"))
            If Confirmed Then
                CartonConfirmed += 1
            Else
                If AllConfirmed Then
                    AllConfirmed = False
                End If
            End If
            x += 1
        End While

        Return AllConfirmed
    End Function

    Private Function CheckInprogressForUser(ByVal UserID As String) As DataTable
        Dim Sql As String = "select * from apx_reversed_order_header where UserID = '" + UserID + "' and ProcessStatus = 'INPROGRESS'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, sDtLines, False, Nothing)

        Return sDtLines
    End Function

    Private Sub DO1_CreatedChildControls(sender As Object, e As EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("ORD", "Order ID")
        DO1.AddLabelLine("CTNCOUNT", "")
        DO1.AddSpacer()
        DO1.AddTextboxLine("SCANORD", "Scan/Enter OrderId")
        DO1.AddTextboxLine("SCANCARTON", "Scan Carton For Order")
        DO1.AddTextboxLine("SCANSKU", "Scan Product To Process")
        DO1.AddTextboxLine("SCANSER", "Scan Serials For Product")
        DO1.AddTextboxLine("ENTQTY", "Confirm Qty For Product")
        DO1.AddTextboxLine("SCANLOC", "Scan Return Location To Place Load")
        DO1.AddSpacer()
        DO1.AddLabelLine("ORDLIST", "")
    End Sub

    Private Sub DO1_ButtonClick(sender As Object, e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "abort"
                doAbort()
            Case "menu"
                doMenu()
            Case "complete"
                doComplete()
        End Select
    End Sub

    Private Sub doComplete()
        Dim sDtLines As DataTable = GetOrderStock(DO1.Value("ORD"), WMS.Logic.Common.GetCurrentUser)
        Dim SkuList As List(Of String) = New List(Of String)
        Dim SkuDescList As List(Of String) = New List(Of String)
        Dim SkuLoads As List(Of String) = New List(Of String)
        Dim SkuLocations As List(Of String) = New List(Of String)
        GetDistinctSkuFromResults(SkuList, SkuDescList, SkuLoads, SkuLocations, sDtLines)

        Dim i As Integer = 0
        While i < SkuList.Count
            Dim Qty As Integer = 0
            Dim QtyConfirmed As Integer = 0
            Dim x As Integer = 0
            Dim index As Integer = -1
            GetSkuQuantities(SkuList(i), Qty, QtyConfirmed, sDtLines)

            While x < sDtLines.Rows.Count
                If sDtLines.Rows(x)("Sku") = SkuList(i) And Not IsDBNull(sDtLines.Rows(x)("NewLoadID")) And Not IsDBNull(sDtLines.Rows(x)("ReturnLocation")) Then
                    index = x
                    Exit While
                End If
                x += 1
            End While

            If index >= 0 Then
                Dim Dr As DataRow = sDtLines.Rows(index)
                Try
                    Dim ld As New WMS.Logic.Load
                    Dim oSku As SKU = New SKU(Session("OrdConsignee"), SkuList(i))
                    Dim SkuUom As String = oSku.getLowestUom()
                    ld.CreateLoad(Dr("NewLoadID"), Session("OrdConsignee"), SkuList(i), SkuUom, Dr("ReturnLocation"), "AVAILABLE", "", CDec(QtyConfirmed), DO1.Value("ORD"), 1, "", Nothing, WMS.Logic.Common.GetCurrentUser, "", DateTime.Now, Nothing, Nothing)
                    Dim Prn As String = ""
                    If Not Session("UserAssignedPrinter") Is Nothing Then
                        Prn = Session("UserAssignedPrinter")
                    End If
                    ld.PrintLabel(Prn)

                    x = 0
                    While x < sDtLines.Rows.Count
                        Dim Serialised As Boolean = CBool(sDtLines.Rows(x)("Serialised"))
                        Dim Confirmed As Boolean = CBool(sDtLines.Rows(x)("Confirmed"))
                        If sDtLines.Rows(x)("Sku") = SkuList(i) And Serialised And Confirmed Then
                            Dim Id As Int64 = CInt(sDtLines.Rows(x)("Id"))
                            Dim SQL As String = String.Format("Update apx_serial set loadID={0}, dispatched=0, received=1, userid={1}, serialStatus={2}, salesOrderID=null, salesOrderLineID=null, cartonID=null, cartonNo=null, noOfCartons=null, lastEditDate={3}, notes=null, MDsyncRequired=1, ReturnStatus=1, JPsyncRequired=1 where id={4}", FormatField(Dr("NewLoadID")), FormatField(WMS.Logic.Common.GetCurrentUser), FormatField("AVAILABLE"), FormatField(DateTime.Now), FormatField(Id))
                            Made4Net.DataAccess.DataInterface.RunSQL(SQL)
                        End If
                        x += 1
                    End While
                Catch ex As Exception
                    Throw ex
                End Try
            End If

            i += 1
        End While

        Dim OrdSQL As String = String.Format("Update apx_reversed_order_header set ProcessStatus='COMPLETE', EditDate={2} where OrderID={0} and UserID={1}", FormatField(DO1.Value("ORD")), FormatField(WMS.Logic.Common.GetCurrentUser), FormatField(DateTime.Now))
        Made4Net.DataAccess.DataInterface.RunSQL(OrdSQL)

        DO1.Value("ORD") = ""
        Session.Remove("OrdConsignee")
        Session.Remove("OrdComplete")
        WhereToNext()
    End Sub

    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Not String.IsNullOrEmpty(DO1.Value("SCANORD").Trim()) Then
            Dim ScannedOrder As String = DO1.Value("SCANORD").Trim()
            DO1.Value("SCANORD") = ""
            Dim ErrMsg As String = ValidateOrd(ScannedOrder, WMS.Logic.Common.GetCurrentUser)

            If Not String.IsNullOrEmpty(ErrMsg) Then
                MessageQue.Enqueue(t.Translate(ErrMsg))
            End If
        ElseIf Not String.IsNullOrEmpty(DO1.Value("SCANCARTON").Trim()) Then
            Dim ScannedCarton As String = DO1.Value("SCANCARTON").Trim()
            Dim OrderId As String = DO1.Value("ORD")
            DO1.Value("SCANCARTON") = ""

            Dim ErrMsg As String = ValidateCarton(ScannedCarton, OrderId)

            If Not String.IsNullOrEmpty(ErrMsg) Then
                MessageQue.Enqueue(t.Translate(ErrMsg))
            End If
        ElseIf Not String.IsNullOrEmpty(DO1.Value("SCANSKU").Trim()) Then
            Dim ScannedSku As String = DO1.Value("SCANSKU").Trim()
            Dim OrderId As String = DO1.Value("ORD")
            DO1.Value("SCANSKU") = ""

            Dim ErrMsg As String = ValidateSku(ScannedSku, OrderId, WMS.Logic.Common.GetCurrentUser)

            If Not String.IsNullOrEmpty(ErrMsg) Then
                MessageQue.Enqueue(t.Translate(ErrMsg))
            End If
        ElseIf Not String.IsNullOrEmpty(DO1.Value("SCANSER").Trim()) Then
            Dim ScannedSer As String = DO1.Value("SCANSER").Trim()
            Dim OrderId As String = DO1.Value("ORD")
            DO1.Value("SCANSER") = ""

            Dim ErrMsg As String = ValidateSerial(ScannedSer, OrderId, WMS.Logic.Common.GetCurrentUser)

            If Not String.IsNullOrEmpty(ErrMsg) Then
                MessageQue.Enqueue(t.Translate(ErrMsg))
            End If
        ElseIf Not String.IsNullOrEmpty(DO1.Value("ENTQTY").Trim()) Then
            Dim EnteredQty As String = DO1.Value("ENTQTY").Trim()
            Dim OrderId As String = DO1.Value("ORD")
            DO1.Value("ENTQTY") = ""

            Dim ErrMsg As String = ValidateQty(EnteredQty, OrderId, WMS.Logic.Common.GetCurrentUser)

            If Not String.IsNullOrEmpty(ErrMsg) Then
                MessageQue.Enqueue(t.Translate(ErrMsg))
            End If
        ElseIf Not String.IsNullOrEmpty(DO1.Value("SCANLOC").Trim()) Then
            Dim EnteredLoc As String = DO1.Value("SCANLOC").Trim()
            Dim OrderId As String = DO1.Value("ORD")
            DO1.Value("SCANLOC") = ""

            Dim ErrMsg As String = ValidateLoc(EnteredLoc)

            If Not String.IsNullOrEmpty(ErrMsg) Then
                MessageQue.Enqueue(t.Translate(ErrMsg))
            Else
                GenLoadIdForSku(EnteredLoc, OrderId, Session("CurrentSku"), WMS.Logic.Common.GetCurrentUser)
            End If
        End If

        WhereToNext()
    End Sub

    Private Sub doMenu()
        RemoveSessions()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doAbort()
        RemoveStockData(DO1.Value("ORD"), WMS.Logic.Common.GetCurrentUser)
        RemoveDetailData(DO1.Value("ORD"), WMS.Logic.Common.GetCurrentUser)
        AbortHeader(DO1.Value("ORD"), WMS.Logic.Common.GetCurrentUser)
        WhereToNext()
    End Sub

    Private Sub RemoveSessions()
        Session.Remove("CurrentSku")
        Session.Remove("OrdConsignee")
        Session.Remove("OrdConsignee")
        Session.Remove("OrdComplete")
    End Sub

    Private Sub GenLoadIdForSku(ByVal Loc As String, ByVal OrderId As String, ByVal Sku As String, ByVal UserID As String)
        Dim ldId As String = WMS.Logic.Load.GenerateLoadId()

        Dim SQL As String = String.Format("Update apx_reversed_order_stock Set NewLoadID={0}, EditDate={1}, ReturnLocation={5} where OrderID={2} And UserID={3} and Sku={4} and Confirmed=1", FormatField(ldId), FormatField(DateTime.Now), FormatField(OrderId), FormatField(UserID), FormatField(Sku), FormatField(Loc))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)

        Session.Remove("CurrentSku")
        Session.Remove("AddLoad")
    End Sub

    Private Sub RemoveStockData(ByVal orderID As String, ByVal UserID As String)
        Dim SQL As String = String.Format("Delete From apx_reversed_order_stock where OrderID={0} and UserID={1}", FormatField(orderID), FormatField(UserID))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Sub RemoveDetailData(ByVal orderID As String, ByVal UserID As String)
        Dim SQL As String = String.Format("Delete From apx_reversed_order_detail where OrderID={0} and UserID={1}", FormatField(orderID), FormatField(UserID))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Sub AbortHeader(ByVal orderID As String, ByVal UserID As String)
        Dim SQL As String = String.Format("Update apx_reversed_order_header set ProcessStatus='ABORTED', EditDate={2} where OrderID={0} and UserID={1}", FormatField(orderID), FormatField(UserID), FormatField(DateTime.Now))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Function ValidateCarton(ByVal ScannedCarton As String, ByVal OrderID As String) As String
        Dim Query As String = "select * from apx_reversed_order_detail where OrderID='" & OrderID & "' and CartonID='" & ScannedCarton & "'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Query, sDtLines, False, Nothing)

        Dim ErrStr As String = ""
        If sDtLines.Rows.Count > 0 Then
            Dim Confirmed As Boolean = False
            If Not IsDBNull(sDtLines.Rows(0)("Confirmed")) Then
                Confirmed = CBool(sDtLines.Rows(0)("Confirmed"))
            End If

            If Not Confirmed Then
                UpdateCartonConfirmed(ScannedCarton, OrderID)
            Else
                ErrStr = ScannedCarton & " is already confirmed to belong to order " & OrderID
            End If
        Else
            ErrStr = ScannedCarton & " is not part of order " & OrderID
        End If

        Return ErrStr
    End Function

    Private Sub UpdateCartonConfirmed(ByVal ScannedCarton As String, ByVal OrderID As String)
        Dim SQL As String = String.Format("Update apx_reversed_order_detail set Confirmed=1, EditDate={2} where OrderID={0} and CartonID={1}", FormatField(OrderID), FormatField(ScannedCarton), FormatField(DateTime.Now))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Function GetSKU(ByVal sku As String) As String
        Dim SQL As String = String.Format("SELECT SKU FROM vSKUCODE WHERE SKUCODE = {0}", Made4Net.Shared.FormatField(sku))
        Return CStr(Made4Net.DataAccess.DataInterface.ExecuteScalar(SQL))
    End Function

    Private Function FindProdInOrder(ByVal Sku As String, ByVal OrderId As String, ByVal UserID As String) As String
        Dim sDtLines As DataTable = GetOrderStock(OrderId, UserID, Sku)
        Dim ErrStr As String = ""

        If sDtLines.Rows.Count > 0 Then
            Dim x As Integer = 0
            Dim Total As Integer = 0
            Dim Scanned As Integer = 0
            GetSkuQuantities(Sku, Total, Scanned, sDtLines)

            If Scanned < Total Then
                Session.Add("CurrentSku", Sku)
            Else
                ErrStr = "All stock for product " & Sku & " in order " & OrderId & " has been received"
            End If
        Else
            ErrStr = "Scanned product " & Sku & " does not belong to order " & OrderId
        End If

        Return ErrStr
    End Function

    Private Function GetReturnWHA() As String()
        Dim Value As String = ""
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'ReturnSourceWarehouseAreas'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        Dim whas As String()
        If Value IsNot Nothing Then
            whas = Value.Split(New Char() {","c})
        End If

        Return whas
    End Function

    Private Function ValidateLoc(ByVal EnteredLoc As String) As String
        Dim ErrStr As String = ""
        Dim Loc As String = EnteredLoc
        If Location.Exists(Loc) Then
            Dim oLoc As Location = New Location(Loc, True)
            Dim LocArea As String = oLoc.WAREHOUSEAREA
            Dim RetArea As String() = GetReturnWHA()
            Dim x As Integer = 0
            Dim LocValid As Boolean = False

            While x < RetArea.Length
                If RetArea(x) = LocArea Then
                    LocValid = True
                    Exit While
                End If
                x += 1
            End While

            If Not LocValid Then
                ErrStr = "Entered/Scanned Location is not valid in the returns warehouse area"
            End If
        Else
            ErrStr = "Entered/Scanned Return Location does Not exist"
        End If

        Return ErrStr
    End Function

    Private Function ValidateQty(ByVal EnterdQty As String, ByVal OrderId As String, ByVal UserID As String) As String
        Dim Sku As String = Session("CurrentSku")
        Dim sDtLines As DataTable = GetOrderStock(OrderId, UserID, Sku)
        Dim ErrStr As String = ""
        Dim iQty As Integer = CInt(EnterdQty)
        Dim SerialFound As Integer = -1

        If sDtLines.Rows.Count > 0 Then
            Dim SkuQty As Integer = CInt(sDtLines.Rows(0)("Quantity"))
            If SkuQty = iQty Then
                Dim SQL As String = String.Format("Update apx_reversed_order_stock Set Confirmed=1, EditDate={2} where OrderID={0} And UserID={1} and Sku={3}", FormatField(OrderId), FormatField(UserID), FormatField(DateTime.Now), FormatField(Sku))
                Made4Net.DataAccess.DataInterface.RunSQL(SQL)
            Else
                ErrStr = "Entered qty does Not match As required "
            End If
        Else
            ErrStr = "Failed To find details For sku " & Sku
        End If

        Return ErrStr
    End Function

    Private Function ValidateSerial(ByVal ScannedSerial As String, ByVal OrderId As String, ByVal UserID As String) As String
        Dim Sku As String = Session("CurrentSku")
        Dim sDtLines As DataTable = GetOrderStock(OrderId, UserID, Sku)
        Dim ErrStr As String = ""
        Dim SerialFound As Integer = -1
        Dim AltSerial As String = "892700000" & ScannedSerial

        If sDtLines.Rows.Count > 0 Then
            Dim x As Integer = 0
            While x < sDtLines.Rows.Count
                If ScannedSerial = sDtLines.Rows(x)("Serial") Or AltSerial = sDtLines.Rows(x)("Serial") Then
                    SerialFound = x
                    Exit While
                End If
                x += 1
            End While

            If SerialFound >= 0 Then
                Dim Confirmed As Boolean = False
                If Not IsDBNull(sDtLines.Rows(SerialFound)("Confirmed")) Then
                    Confirmed = CBool(sDtLines.Rows(SerialFound)("Confirmed"))
                End If

                If Not Confirmed Then
                    ConfirmScannedSerial(sDtLines, SerialFound, OrderId, UserID, Sku)
                Else
                    ErrStr = "Serial " & ScannedSerial & " Is already scanned"
                End If
            Else
                ErrStr = "Scanned serial " & ScannedSerial & " Is Not For sku " & Sku
            End If
        Else
            ErrStr = "Scanned serial " & ScannedSerial & " Is Not For sku " & Sku
        End If

        Return ErrStr
    End Function

    Private Sub ConfirmScannedSerial(ByVal DT As DataTable, ByVal index As Integer, ByVal OrderId As String, ByVal UserID As String, ByVal Sku As String)
        Dim Dr As DataRow = DT.Rows(index)

        Dim SQL As String = String.Format("Update apx_reversed_order_stock Set Confirmed=1, EditDate={2} where OrderID={0} And UserID={1} and Serial={3}", FormatField(OrderId), FormatField(UserID), FormatField(DateTime.Now), FormatField(Dr("Serial")))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
        Dim id As Int64 = Dr("Id")
        Dim x As Integer = 0
        While x < DT.Rows.Count
            If Not IsDBNull(DT.Rows(x)("ParentNodeID")) Then
                Dim PNodeId As Int64 = DT.Rows(x)("ParentNodeID")
                If PNodeId = id Then
                    SQL = String.Format("Update apx_reversed_order_stock Set Confirmed=1, EditDate={2} where OrderID={0} And UserID={1} and Serial={3}", FormatField(OrderId), FormatField(UserID), FormatField(DateTime.Now), FormatField(DT.Rows(x)("Serial")))
                    Made4Net.DataAccess.DataInterface.RunSQL(SQL)
                End If
            End If
            x += 1
        End While
    End Sub

    Private Function ValidateSku(ByVal ScannedSku As String, ByVal OrderId As String, ByVal UserID As String) As String
        Dim ErrStr As String = ""
        Dim Sku As String = GetSKU(ScannedSku)

        If Sku IsNot Nothing Then
            ErrStr = FindProdInOrder(Sku, OrderId, UserID)
        Else
            ErrStr = "Invalid product " & ScannedSku & " scanned"
        End If

        Return ErrStr
    End Function

    Private Function ValidateOrd(ByVal ScannedOrder As String, ByVal UserID As String) As String
        Dim Query As String = "Select * from apx_reversed_order_header where OrderID='" & ScannedOrder & "'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Query, sDtLines, False, Nothing)
        Dim ErrStr As String = ""

        If sDtLines.Rows.Count > 0 Then
            Dim x As Integer = 0

            While x < sDtLines.Rows.Count
                If sDtLines.Rows(x)("ProcessStatus") = "COMPLETE" Then
                    ErrStr = "Order " & ScannedOrder & " has already been processed by " & sDtLines.Rows(x)("UserID")
                    Exit While
                End If

                If sDtLines.Rows(x)("ProcessStatus") = "INPROGRESS" Then
                    ErrStr = "Order " & ScannedOrder & " is currently being processed by " & sDtLines.Rows(x)("UserID")
                    Exit While
                End If
                x += 1
            End While
        End If

        If String.IsNullOrEmpty(ErrStr) Then
            Query = "select * from OUTBOUNDORHEADER where OrderID='" & ScannedOrder & "'"
            Dim ordDT As DataTable = New DataTable()
            Made4Net.DataAccess.DataInterface.FillDataset(Query, ordDT, False, Nothing)

            If ordDT.Rows.Count <= 0 Then
                ErrStr = "Invalid order " & ScannedOrder & " scanned"
            Else
                Dim shipmentSent As Boolean = False
                If Not IsDBNull(ordDT.Rows(0)("SHIPMENTSENT")) Then
                    shipmentSent = CBool(ordDT.Rows(0)("SHIPMENTSENT"))
                End If

                Dim status As String = ordDT.Rows(0)("STATUS")
                If status.ToUpper() = "SHIPPED" Or status.ToUpper() = "CANCELLED" Or status.ToUpper() = "CANCELED" Then
                    If shipmentSent Then
                        ErrStr = "Order " & ScannedOrder & " was fully shipped and cannot be returned using this process"
                    Else
                        Dim FullPallet As Boolean = False
                        If Not IsDBNull(ordDT.Rows(0)("FULLPALLETORDER")) Then
                            FullPallet = CBool(ordDT.Rows(0)("FULLPALLETORDER"))
                        End If
                        ErrStr = AddOrdToHeader(ScannedOrder, UserID, FullPallet)
                    End If
                Else
                    ErrStr = "Invalid order " & ScannedOrder & " is not in a shipped or cancelled state"
                End If
            End If
        End If

        Return ErrStr
    End Function

    Private Function AddOrdToHeader(ByVal ScannedOrder As String, ByVal UserID As String, ByVal FullPallet As Boolean) As String
        Dim ErrMsg As String = ""
        Dim LineDT As DataTable = GetOrderLine(ScannedOrder)

        If LineDT.Rows.Count > 0 Then
            Dim TotalQty As Integer = 0
            Dim TotalShipped As Integer = 0
            Dim x As Integer = 0
            Dim Consignee As String = Nothing
            While x < LineDT.Rows.Count
                TotalQty += CInt(LineDT.Rows(x)("QTYORIGINAL"))

                If Consignee Is Nothing Then
                    Consignee = LineDT.Rows(x)("CONSIGNEE")
                End If

                Dim oSku As SKU = New SKU(Consignee, LineDT.Rows(x)("SKU"))
                Dim ClassName As String = oSku.SKUClass.ClassName
                If Not String.IsNullOrEmpty(ClassName) And ClassName.ToUpper() = "SERIAL" Then
                    Dim UOM As String = "EA"
                    If Not IsDBNull(LineDT.Rows(x)("INPUTQTYUOM")) Then
                        If Not String.IsNullOrEmpty(LineDT.Rows(x)("INPUTQTYUOM")) Then
                            UOM = LineDT.Rows(x)("INPUTQTYUOM")
                        End If
                    End If

                    Dim SerDT As DataTable = GetSerQty(ScannedOrder, CInt(LineDT.Rows(x)("ORDERLINE")), UOM)
                    TotalShipped += SerDT.Rows.Count
                Else
                    TotalShipped += CInt(LineDT.Rows(x)("QTYSHIPPED"))
                End If
                x += 1
            End While

            Dim CartonDT As DataTable = GetShippedCartons(ScannedOrder)
            If CartonDT.Rows.Count > 0 Then
                x = 0
                While x < CartonDT.Rows.Count
                    If Not IsDBNull(CartonDT.Rows(x)("cartonID")) Then
                        Dim CartonID As String = CartonDT.Rows(x)("cartonID")

                        AddCartonToDetail(CartonID, UserID, ScannedOrder)
                        Dim SerDT As DataTable = GetSerialForCarton(ScannedOrder, CartonID, LineDT.Rows(x)("INPUTQTYUOM"))
                        AddSerialsToDetail(SerDT, UserID, ScannedOrder, Consignee, CartonID)
                    End If

                    x += 1
                End While

                Dim SQL As String = String.Format("Insert Into apx_reversed_order_header (Consignee, OrderID, UserID, TotalQty, QtyShipped, QtyReversed, FullPallet, ProcessStatus, AddDate) VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8})", FormatField(Consignee), FormatField(ScannedOrder), FormatField(UserID), FormatField(TotalQty), FormatField(TotalShipped), FormatField(0), FormatField(FullPallet), Made4Net.Shared.FormatField("INPROGRESS"), Made4Net.Shared.FormatField(DateTime.Now))
                Made4Net.DataAccess.DataInterface.RunSQL(SQL)
                Session.Remove("OrdComplete")
            Else
                ErrMsg = "Failed to find cartons for order " & ScannedOrder
            End If
        Else
            ErrMsg = "Order " & ScannedOrder & " has no order lines"
        End If

        Return ErrMsg
    End Function

    Private Function GetOrderLine(ByVal ScannedOrder As String) As DataTable
        Dim Query As String = "select * from OUTBOUNDORDETAIL where OrderID='" & ScannedOrder & "'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Query, sDtLines, False, Nothing)

        Return sDtLines
    End Function

    Private Function GetSerQty(ByVal OrderID As String, ByVal OrderLine As Integer, ByVal UOM As String) As DataTable
        Dim Query As String = "select * from apx_serial where salesOrderID = '" & OrderID & "' and salesOrderLineID = " & OrderLine & " and uom = '" & UOM & "'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Query, sDtLines, False, Nothing)
        Dim NewDtLines As DataTable = New DataTable()

        NewDtLines = sDtLines.Copy()

        Dim x As Integer = 0
        Dim total As Integer = sDtLines.Rows.Count
        While x < total
            Dim y As Integer = x + 1
            While y < total
                If sDtLines.Rows(x)("serial") = sDtLines.Rows(y)("serial") Then
                    Dim xId As Int64 = CInt(sDtLines.Rows(x)("id"))
                    Dim yId As Int64 = CInt(sDtLines.Rows(y)("id"))
                    If xId > yId Then
                        NewDtLines.Rows.RemoveAt(y)
                    Else
                        NewDtLines.Rows.RemoveAt(x)
                    End If
                    Exit While
                End If
                y += 1
            End While
            x += 1
        End While

        Return NewDtLines
    End Function

    Private Function GetShippedCartons(ByVal OrderID As String) As DataTable
        Dim Query As String = "select distinct cartonID from apx_serial where salesOrderID = '" & OrderID & "'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Query, sDtLines, False, Nothing)

        Return sDtLines
    End Function

    Private Function GetSerialForCarton(ByVal OrderID As String, ByVal CartonID As String, ByVal UOM As String) As DataTable
        Dim Query As String = "select * from apx_serial where salesOrderID = '" & OrderID & "' and cartonID = '" & CartonID & "'" ' and UOM = '" & UOM & "'"
        Dim sDtLines As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Query, sDtLines, False, Nothing)

        Dim NewDtLines As DataTable = New DataTable()

        NewDtLines = sDtLines.Copy()

        Dim x As Integer = 0
        Dim total As Integer = sDtLines.Rows.Count
        While x < total
            Dim y As Integer = x + 1
            While y < total
                If sDtLines.Rows(x)("serial") = sDtLines.Rows(y)("serial") Then
                    Dim xId As Int64 = CInt(sDtLines.Rows(x)("id"))
                    Dim yId As Int64 = CInt(sDtLines.Rows(y)("id"))
                    If xId > yId Then
                        NewDtLines.Rows.RemoveAt(y)
                    Else
                        NewDtLines.Rows.RemoveAt(x)
                    End If
                    Exit While
                End If
                y += 1
            End While
            x += 1
        End While

        Return NewDtLines
    End Function

    Private Sub AddCartonToDetail(ByVal CartonID As String, ByVal UserID As String, ByVal OrderID As String)
        Dim SQL As String = String.Format("Insert Into apx_reversed_order_detail (UserID, OrderID, CartonID, Confirmed, AddDate) VALUES({0},{1},{2},{3},{4})", FormatField(UserID), FormatField(OrderID), FormatField(CartonID), FormatField(False), FormatField(DateTime.Now))
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Private Sub AddSerialsToDetail(ByVal SerDT As DataTable, ByVal UserID As String, ByVal OrderID As String, ByVal Consignee As String, ByVal CartonID As String)
        Dim x As Integer = 0
        While x < SerDT.Rows.Count
            Dim oSku As SKU = New SKU(Consignee, SerDT.Rows(x)("SKU"))
            Dim SkuDesc As String = oSku.SKUSHORTDESC
            Dim OrderLine As Integer = CInt(SerDT.Rows(x)("salesOrderLineID"))
            Dim Serial As String = SerDT.Rows(x)("serial")
            Dim UOM As String = SerDT.Rows(x)("UOM")
            Dim Id As Int64 = SerDT.Rows(x)("id")
            Dim ParentNodeId As Int64 = SerDT.Rows(x)("parentNodeID")
            Dim SQL As String = String.Format("Insert Into apx_reversed_order_stock (UserID, OrderID, OrderLine, CartonID, Serialised, Serial, Sku, UOM, SkuDesc, Quantity, Confirmed, Id, ParentNodeID, AddDate) VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13})", FormatField(UserID), FormatField(OrderID), FormatField(OrderLine), FormatField(CartonID), FormatField(True), FormatField(Serial), FormatField(oSku.SKU), FormatField(UOM), FormatField(SkuDesc), FormatField(1), FormatField(False), FormatField(Id), FormatField(ParentNodeId), FormatField(DateTime.Now))
            Made4Net.DataAccess.DataInterface.RunSQL(SQL)
            x += 1
        End While
    End Sub
End Class
