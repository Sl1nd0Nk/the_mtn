Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports System.Windows.Forms
Imports WMS.MobileWebApp.apexInventory
Imports WMS.MobileWebApp.apexSerial

<CLSCompliant(False)> Public Class SPLITLOAD
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If WMS.Logic.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If


        If Not IsPostBack Then
            If Session("LoadSplitting") Is Nothing Then
                Dim dt As DataTable
                Dim LoadSplitInProgress As Boolean = CheckUserActiveLoadSplit(Logic.GetCurrentUser(), dt)

                If LoadSplitInProgress Then
                    Dim fromLd As Load = New Load(dt.Rows(0)("FromLoad"), True)
                    Dim toLd As Load = New Load(dt.Rows(0)("ToLoad"), True)

                    Session.Add("SlFromLoad", fromLd)
                    Session.Add("SlLoc", toLd.LOCATION)
                    Session.Add("SlToLoad", toLd)
                End If
            End If
        End If

        WhereToNext()
    End Sub

    Private Function CheckUserActiveLoadSplit(ByVal User As String, ByRef dt As DataTable) As Boolean
        Dim Result As Boolean = False

        If APXINV_CheckIfTableExist("apx_load_split_progress") Then
            Dim SQL As String = String.Format("Select * from apx_load_split_progress where UserID={0}", FormatField(User))
            Dim dtData As New DataTable
            Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtData)

            If dtData.Rows.Count > 0 Then
                Result = True
                dt = dtData
            End If
        Else
            ContructTable()
        End If

        Return Result
    End Function

    Private Sub ContructTable()
        Dim list As New Generic.List(Of String)
        list.Add("FromLoad NVARCHAR (20) NULL")
        list.Add("ToLoad NVARCHAR (20) NULL")
        list.Add("UserID NVARCHAR (50) NULL")
        APXINV_CreateTable("apx_load_split_progress", list)
    End Sub

    Private Sub CreateSplitLoadInProgress(ByVal FromLoad As Load, ByVal ToLoad As Load, ByVal User As String)
        If APXINV_CheckIfTableExist("apx_load_split_progress") = False Then
            ContructTable()
        End If

        Dim SQL As String = String.Format("Insert into apx_load_split_progress (FromLoad, ToLoad, UserID) values({0},{1},{2})", FormatField(FromLoad.LOADID), FormatField(ToLoad.LOADID), FormatField(User))
        DataInterface.RunSQL(SQL)
    End Sub

    Private Sub CompleteSplitLoadInProgress(ByVal FromLoad As String, ByVal ToLoad As String, ByVal User As String)
        Dim SQL As String = String.Format("delete from apx_load_split_progress where FromLoad={0} and ToLoad={1} and UserID={2}", FormatField(FromLoad), FormatField(ToLoad), FormatField(User))
        DataInterface.RunSQL(SQL)
    End Sub

    Private Sub WhereToNext()
        InitComponents()
        InitButtons()

        If Not Session("SerialView") Is Nothing Then
            DO1.Button(0).Text = "Close"
            DO1.Button(1).Visible = False
            DO1.Button(2).Visible = False
            DO1.Button(3).Visible = False

            DO1.setVisibility("STScannedSer", True)
            DO1.Value("STScannedSer") = GetScannedSerialNumbers()
        Else
            If Not Session("SlFromLoad") Is Nothing Then
                Dim FromLoad As Load = Session("SlFromLoad")
                DO1.setVisibility("SLSKU", True)
                DO1.Value("SLSKU") = FromLoad.SKU

                DO1.setVisibility("SLFROMLOAD", True)
                DO1.Value("SLFROMLOAD") = FromLoad.LOADID

                If Not Session("SlLoc") Is Nothing Then
                    If Not Session("SlToLoad") Is Nothing Then
                        Dim ToLoad As Load = Session("SlToLoad")
                        DO1.setVisibility("SLTOLOAD", True)
                        DO1.Value("SLTOLOAD") = ToLoad.LOADID

                        DO1.setVisibility("SLTOLOADLOC", True)
                        DO1.Value("SLTOLOADLOC") = Session("SlLoc")

                        DO1.setVisibility("SLSCANSERIAL", True)
                        DO1.Value("SLSCANSERIAL") = ""
                        DO1.FocusField = "SLSCANSERIAL"
                    Else
                        DO1.setVisibility("SLSCANTOLOAD", True)
                        DO1.Value("SLSCANTOLOAD") = ""
                        DO1.FocusField = "SLSCANTOLOAD"
                    End If
                Else
                    DO1.setVisibility("SLSCANLOCATION", True)
                    DO1.Value("SLSCANLOCATION") = ""
                    DO1.FocusField = "SLSCANLOCATION"
                End If
            Else
                DO1.setVisibility("SLSCANFROMLOAD", True)
                DO1.Value("SLSCANFROMLOAD") = ""
                DO1.FocusField = "SLSCANFROMLOAD"
            End If
        End If
    End Sub

    Private Sub InitComponents()
        DO1.setVisibility("SLSKU", False)
        DO1.setVisibility("SLFROMLOAD", False)
        DO1.setVisibility("SLTOLOAD", False)
        DO1.setVisibility("SLTOLOADLOC", False)
        DO1.setVisibility("SLSCANFROMLOAD", False)
        DO1.setVisibility("SLSCANTOLOAD", False)
        DO1.setVisibility("SLSCANSERIAL", False)
        DO1.setVisibility("SLSCANLOCATION", False)
        DO1.setVisibility("STScannedSer", False)
    End Sub

    Private Sub InitButtons()
        DO1.Button(0).Text = "Next"
        DO1.Button(0).Visible = True
        DO1.Button(1).Visible = False
        DO1.Button(2).Visible = False
        DO1.Button(3).Visible = True
    End Sub

    Private Function GetScannedSerialNumbers() As String
        Dim ld As Load = Session("SlToLoad")

        Dim SQL As String = String.Format("SELECT * FROM apx_serial WHERE loadID={0}", FormatField(ld.LOADID))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        If dtLines.Rows.Count > 0 Then
            Dim StartTable As String = "<table>"
            Dim EndTable As String = "</table>"
            Dim StartRow As String = "<tr>"
            Dim EndRow As String = "</tr>"

            Dim x As Integer = 1

            For Each dr1 As DataRow In dtLines.Rows
                If x > 3 Then
                    x = 1
                    StartTable = StartTable & EndRow
                End If

                If x = 1 Then
                    StartTable = StartTable & StartRow
                End If

                StartTable = StartTable & "<td>" & dr1("serial") & "</td><td>&nbsp&nbsp</td>"

                x += 1
            Next

            Return StartTable & EndTable
        Else
            Return ""
        End If
    End Function

    Private Sub CreateToLoad(ByVal ld As Load, ByVal location As String)
        Dim NewLoad As Load = New Load()
        NewLoad.CreateLoad(WMS.Logic.Load.GenerateLoadId(), ld.CONSIGNEE, ld.SKU, ld.LOADUOM, location, ld.STATUS, "", 0, "", 1, "", Nothing, Logic.GetCurrentUser(), "", DateTime.Now, Nothing, Nothing)
        Session.Add("SlToLoad", NewLoad)
        CreateSplitLoadInProgress(ld, NewLoad, Logic.GetCurrentUser())
    End Sub

    Private Sub doMenu()
        Session.Remove("SlFromLoad")
        Session.Remove("SlLoc")
        Session.Remove("SlToLoad")
        Session.Remove("SerialView")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doComplete()
        Dim FromLoad As Load = Session("SlFromLoad")
        Dim ToLoad As Load = Session("SlToLoad")
        Dim FromSerialCount As Integer = GetLoadCountFromSerials(FromLoad.LOADID)
        Dim ToSerialCount As Integer = GetLoadCountFromSerials(ToLoad.LOADID)

        FromLoad.Count(FromSerialCount, FromLoad.LOCATION, Nothing, Logic.GetCurrentUser())
        ToLoad.Count(ToSerialCount, ToLoad.LOCATION, Nothing, Logic.GetCurrentUser())

        CompleteSplitLoadInProgress(FromLoad.LOADID, ToLoad.LOADID, Logic.GetCurrentUser())
        WhereToNext()
    End Sub

    Private Sub doShowSerials()
        Session.Add("SerialView", True)
        WhereToNext()
    End Sub

    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Not String.IsNullOrEmpty(DO1.Value("SLSCANFROMLOAD")) Then
            Try
                Dim ld As Load = New Load(DO1.Value("SLSCANFROMLOAD"), True)
                Session.Add("SlFromLoad", ld)
                DO1.Value("SLSCANFROMLOAD") = ""
            Catch Ex As Exception
                MessageQue.Enqueue(t.Translate(Ex.Message))
                DO1.Value("SLSCANFROMLOAD") = ""
            End Try
        ElseIf Not String.IsNullOrEmpty(DO1.Value("SLSCANTOLOAD")) Then
            If DO1.Value("SLSCANTOLOAD").ToUpper() = "NEW" Then
                CreateToLoad(Session("SlFromLoad"), Session("SlLoc"))
            Else
                Try
                    Dim ld As Load = New Load(DO1.Value("SLSCANTOLOAD"), True)
                    Session.Add("SlToLoad", ld)
                    CreateSplitLoadInProgress(Session("SlFromLoad"), ld, Logic.GetCurrentUser())
                    DO1.Value("SLSCANTOLOAD") = ""
                Catch Ex As Exception
                    MessageQue.Enqueue(t.Translate(Ex.Message))
                    DO1.Value("SLSCANTOLOAD") = ""
                End Try
            End If
        ElseIf Not String.IsNullOrEmpty(DO1.Value("SLSCANLOCATION")) Then
            Try
                Dim loc As Location = New Location(DO1.Value("SLSCANLOCATION"), True)
                Session.Add("SlLoc", loc.Location)
                DO1.Value("SLSCANLOCATION") = ""
            Catch Ex As Exception
                MessageQue.Enqueue(t.Translate(Ex.Message))
                DO1.Value("SLSCANLOCATION") = ""
            End Try
        ElseIf Not String.IsNullOrEmpty(DO1.Value("SLSCANSERIAL")) Then
            HandleScannedSerial(DO1.Value("SLSCANSERIAL"))
            DO1.Value("SLSCANSERIAL") = ""
        End If
        WhereToNext()
    End Sub

    Private Function GetLoadCountFromSerials(ByVal loadID As String) As Integer
        Dim Count As Integer = 0

        APEXSERIAL_GetLoadSerialCount(loadID, Count)

        Return Count
    End Function

    Private Sub HandleScannedSerial(ByVal SerialNumber As String)
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim ld As Load = Session("SlFromLoad")
        Dim SerDetails As DataTable = APXSERIAL_GetSerialDetailBySerial(SerialNumber)
        If SerDetails.Rows.Count > 0 Then
            If SerDetails.Rows(0)("received") Then
                If SerDetails.Rows(0)("dispatched") = False Then
                    If SerDetails.Rows(0)("serialStatus") <> "BROKEN" Then
                        If SerDetails.Rows(0)("loadID") = ld.LOADID Then
                            If SerDetails.Rows(0)("SKU") = ld.SKU Then
                                If SerDetails.Rows(0)("parentNodeID") > 0 Then
                                    CheckIfParentIsInTacked(ld, SerDetails.Rows(0)("id"), SerDetails.Rows(0)("parentNodeID"), SerialNumber)
                                Else
                                    AddSerialToLoad(SerialNumber, SerDetails.Rows(0)("id"), ld, False, 0)
                                End If
                            Else
                                MessageQue.Enqueue(t.Translate("Scanned serial number sku is different from load sku"))
                            End If
                        Else
                            MessageQue.Enqueue(t.Translate("Scanned serial number is not in selected load"))
                        End If
                    Else
                        MessageQue.Enqueue(t.Translate("Scanned conatiner serial number is no longer valid for use"))
                    End If
                Else
                    MessageQue.Enqueue(t.Translate("Scanned serial number is dispatched"))
                End If
            Else
                MessageQue.Enqueue(t.Translate("Scanned serial number is not received"))
            End If
        End If
    End Sub

    Private Sub CheckIfParentIsInTacked(ByVal ld As Load, ByVal id As Integer, ByVal parentID As Integer, ByVal serial As String)
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim SQL As String = String.Format("Select * apx_serial where SKU={0} and dispatched={1} and received={2} and id={3}", FormatField(ld.SKU), FormatField(False), FormatField(True), FormatField(parentID))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        If dtLines.Rows.Count > 0 Then
            If dtLines.Rows(0)("serialStatus") <> "BROKEN" Then
                Dim result As Integer = MessageBox.Show("Scanned serial number is still attached to a parent serial number, would you like to break the parent?", "Serial in container", MessageBoxButtons.YesNoCancel)
                If result = DialogResult.Cancel Then
                    'MessageQue.Enqueue(t.Translate("Please scan serial number"))
                ElseIf result = DialogResult.No Then
                    'MessageQue.Enqueue(t.Translate("Please scan serial number"))
                ElseIf result = DialogResult.Yes Then
                    AddSerialToLoad(serial, id, ld, True, parentID)
                End If
            Else
                AddSerialToLoad(serial, id, ld, True, parentID)
            End If
        Else
            AddSerialToLoad(serial, id, ld, True, parentID)
        End If
    End Sub

    Private Sub AddSerialToLoad(ByVal Serial As String, ByVal id As Integer, ByVal ld As Load, ByVal breakContainer As Boolean, ByVal parentID As Integer)
        APAXSERIAL_MoveSerialToLoad(Serial, id, ld.SKU, ld.LOADID, breakContainer, parentID)

        APXSERIAL_MoveChildSerials(ld.SKU, ld.LOADID, id)
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("SLSKU", "Product")
        DO1.AddLabelLine("SLFROMLOAD", "From Load")
        DO1.AddLabelLine("SLTOLOAD", "To Load")
        DO1.AddLabelLine("SLTOLOADLOC", "To Load Location")
        DO1.AddSpacer()
        DO1.AddLabelLine("STScannedSer", "")
        DO1.AddTextboxLine("SLSCANFROMLOAD", "Scan From Load")
        DO1.AddTextboxLine("SLSCANTOLOAD", "Scan To Load (Type NEW to create new Load)")
        DO1.AddTextboxLine("SLSCANLOCATION", "Scan Location For To load")
        DO1.AddTextboxLine("SLSCANSERIAL", "Scan Serial Number To Move")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                If Not Session("SerialView") Is Nothing Then
                    Session.Remove("SerialView")
                Else
                    doNext()
                End If
            Case "menu"
                doMenu()
            Case "view serial numbers"
                doShowSerials()
            Case "complete"
                doComplete()
        End Select
    End Sub
End Class
