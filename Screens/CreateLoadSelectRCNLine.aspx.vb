Imports Made4Net.Shared.Web
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports Made4Net.Shared
Imports WMS.MobileWebApp.apexInbound


<CLSCompliant(False)> Public Class CreateLoadSelectRCNLine
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            CheckIfUserCurrentlyReceiving()

            DO1.Value("RCN") = Session("CreateLoadRCN")
            If Session("LineChanged") <> 1 Then
                MobileUtils.ClearCreateLoadProcessSession()
            End If
            Session.Remove("LineChanged")
        End If
        If Session("SELECTEDSKU") <> "" Then
            DO1.Value("Product") = Session("SELECTEDSKU")
            DO1.Value("RCN") = Session("SKUSEL_RECEIPT")
            'DO1.Value("RCNLINE") = Session("SKUSEL_RECEIPTLINE")

            Session.Remove("SKUSEL_RECEIPT")
            Session.Remove("SKUSEL_RECEIPTLINE")
            Session.Remove("SELECTEDSKU")
            doNext()
        End If
    End Sub

    Private Sub CheckIfUserCurrentlyReceiving()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim dtLines As DataTable = APXINBOUND_FindSerialsOnStationByState(Logic.GetCurrentUser, "", "", "", "", "", "")

        If dtLines.Rows.Count = 0 Then
            Return
        Else
            'If IsDBNull(dtLines.Rows(0)("Type")) Or dtLines.Rows(0)("Type") = "" Then
            Session("CreateLoadConsignee") = dtLines.Rows(0)("Consignee")
            Session("CreateLoadSKU") = dtLines.Rows(0)("SKU")
            Session("CreateLoadRCN") = dtLines.Rows(0)("Receipt")
            Session("CreateLoadRCNLine") = dtLines.Rows(0)("Line")
            Session("CreateLoadUnits") = dtLines.Rows(0)("TotalUnits")
            Session("CreateLoadUOM") = dtLines.Rows(0)("UOM")
            Session("CreateLoadLocation") = dtLines.Rows(0)("Location")
            Session("CreateLoadWeight") = dtLines.Rows(0)("Weight")
            Response.Redirect(MobileUtils.GetURLByScreenCode("RDTCLDSerial"))
            'Else
            '    Session("CreateLoadConsignee") = dtLines.Rows(0)("Consignee")
            '    Session("CreateLoadSKU") = dtLines.Rows(0)("SKU")
            '    Session("CreateLoadRCN") = dtLines.Rows(0)("Receipt")
            '    Session("CreateLoadRCNLine") = dtLines.Rows(0)("Line")
            '    Session("CreateLoadUnits") = dtLines.Rows(0)("TotalUnits")
            '    Session("CreateLoadLocation") = dtLines.Rows(0)("Location")
            '    Session("CreateLoadWeight") = dtLines.Rows(0)("Weight")
            '    Session.Add("CreateLoadASNID", dtLines.Rows(0)("Type"))
            '    Response.Redirect(MobileUtils.GetURLByScreenCode("RDTCLDASN1"))
            'End If
        End If
    End Sub

    Private Sub doNext()
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If String.IsNullOrEmpty(DO1.Value("RCN")) Then
            Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("Receipt field must not be empty"))
            Return
        End If
        Dim receipt As String = DO1.Value("RCN")
        Dim receiptLine As Integer = Integer.MinValue
        Dim consigneeStr As String = ""
        Dim skuStr As String = DO1.Value("Product")

        If String.IsNullOrEmpty(DO1.Value("Product")) Then
            Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("Please scan/enter a product"))
            DO1.FocusField = "Product"
            Return
        End If

        Dim ASNErr As String = APXINBOUND_FindASNUsingIDAndSku(DO1.Value("RCN"), DO1.Value("Product"), receipt, receiptLine)

        If Not String.IsNullOrEmpty(ASNErr) Then
            receipt = DO1.Value("RCN")
            receiptLine = Integer.MinValue

            'If String.IsNullOrEmpty(DO1.Value("RCNLINE")) AndAlso String.IsNullOrEmpty(skuStr) Then
            '    Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("Receipt must come with another parameter for line selection"))
            '    DO1.FocusField = "RCNLINE"
            '    Return
            'End If

        Else
            Session.Add("CreateLoadASNID", DO1.Value("RCN"))
            'DO1.Value("RCNLINE") = receiptLine.ToString()
        End If

        Dim receiptHeader As WMS.Logic.ReceiptHeader = validateReceipt(receipt)
        If receiptHeader Is Nothing Then
            Return
        End If

        'If Not String.IsNullOrEmpty(DO1.Value("RCNLINE")) Then
        '    Dim errStr As String = ""
        '    Try
        '        receiptLine = Integer.Parse(DO1.Value("RCNLINE"))
        '        If receiptLine < 0 Then errStr = "Receipt line must be positive"
        '        If receiptHeader.LINES.Count < receiptLine Then errStr = "Receipt line does not exist"
        '    Catch
        '        errStr = "Invalid receipt line"
        '    End Try
        '    If Not String.IsNullOrEmpty(errStr) Then
        '        Made4Net.Mobile.MessageQue.Enqueue(trans.Translate(errStr))
        '        DO1.Value("RCNLINE") = ""
        '        DO1.FocusField = "RCNLINE"
        '        Return
        '    End If
        'End If

        Dim dtSKU As DataTable = MobileUtils.GetSKUsDTFromSKUCODE(consigneeStr, skuStr)
        If dtSKU.Rows.Count = 0 Then
            Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("SKU does not exist"))
            Return
        End If


        Dim sql As String = String.Format("select * from receiptdetail where receipt={0}", Made4Net.Shared.FormatField(receipt))
        'If receiptLine > Integer.MinValue Then
        '    sql = String.Format("{0} and receiptline = {1}", sql, Made4Net.Shared.FormatField(receiptLine))
        'End If
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtLines)

        If dtLines.Rows.Count = 0 Then
            Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("Receipt line does not exist"))
            Return
        End If

        Dim MultipleLinesWithSameSKU As Boolean = False

        Dim matchingConsignee As String = ""
        Dim matchingSKU As String = ""
        Dim matchingLine As Integer = -1

        Dim rowsFound As DataRow()
        For Each dr As DataRow In dtSKU.Rows
            rowsFound = dtLines.Select(String.Format("CONSIGNEE={0} AND SKU={1}", FormatField(dr("CONSIGNEE")), FormatField(dr("SKU"))))
            If rowsFound.Length = 1 Then
                If String.IsNullOrEmpty(matchingSKU) Then
                    matchingConsignee = rowsFound(0)("CONSIGNEE")
                    matchingSKU = rowsFound(0)("SKU")
                    matchingLine = rowsFound(0)("RECEIPTLINE")
                Else
                    ''Means that the sku code entered matches more than one sku that exists in the receipt, need to go to multiselectform.aspx
                    redirectToSKUMultiSelectForm(receipt, receiptLine, consigneeStr, skuStr)
                End If
            ElseIf rowsFound.Length > 1 Then
                MultipleLinesWithSameSKU = True
                If String.IsNullOrEmpty(matchingSKU) Then
                    matchingConsignee = rowsFound(0)("CONSIGNEE")
                    matchingSKU = rowsFound(0)("SKU")
                Else
                    ''Means that the sku code entered matches more than one sku that exists in the receipt, need to go to multiselectform.aspx
                    redirectToSKUMultiSelectForm(receipt, receiptLine, consigneeStr, skuStr)
                End If
            End If
        Next
        '' Means we couldn't find a line with the sku entered
        If String.IsNullOrEmpty(matchingSKU) Then
            Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("No line was found with given SKU"))
            Return
        End If
        '' Means we found one matching sku, but the receipt has many lines with it, and you need to select which line
        If MultipleLinesWithSameSKU Then
            Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("SKU belongs to more than one line, please enter receipt line"))
            Return
        End If

        redirect(receipt, matchingLine, matchingConsignee, matchingSKU)
    End Sub

    Private Sub redirectToSKUMultiSelectForm(ByVal pReceipt As String, ByVal pReceiptLine As Integer, ByVal pConsignee As String, ByVal pSKU As String)
        Session("FROMSCREEN") = "CreateLoadSelectRCNLine"
        Session("SKUCODE") = pSKU
        ' Add all controls to session for restoring them when we back from that sreen
        Session("SKUSEL_RECEIPT") = pReceipt
        If pReceiptLine = Integer.MinValue Then
            Session("SKUSEL_RECEIPTLINE") = ""
        Else
            Session("SKUSEL_RECEIPTLINE") = pReceiptLine
        End If

        Session("SKUSEL_CONSIGNEE") = pConsignee
        Response.Redirect(MapVirtualPath("Screens/MultiSelectForm.aspx?key1=" & pReceipt))
    End Sub

    Private Sub redirect(ByVal pReceipt As String, ByVal pReceiptLine As String, ByVal pConsignee As String, ByVal pSKU As String)
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim oSKU As New WMS.Logic.SKU(pConsignee, pSKU)

        If Not oSKU.STATUS Then
            Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("Inactive SKU, cannot receive"))
            Return
        End If

        If oSKU.NEWSKU Then
            Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("New sku, cannot receive"))
            Return
        End If

        Session("CreateLoadRCN") = pReceipt
        If String.IsNullOrEmpty(pReceiptLine) Then
            Session("CreateLoadRCNLine") = -1
            Session("CreateLoadRCNMultipleLines") = True
        Else
            Session("CreateLoadRCNLine") = pReceiptLine
            Session("CreateLoadRCNMultipleLines") = False
        End If
        Session("CreateLoadConsignee") = pConsignee
        Session("CreateLoadSKU") = pSKU

        Response.Redirect(MobileUtils.GetURLByScreenCode("RDTCLDWEIGHT"))
    End Sub

    Private Function validateReceipt(ByVal pReceipt As String) As WMS.Logic.ReceiptHeader
        Dim sql As String = String.Format("select * from receiptheader where receipt like '{0}%'", pReceipt)
        Dim dt As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)
        Dim err As String = ""
        If dt.Rows.Count = 0 Then
            err = "Receipt does not exist"
        ElseIf dt.Rows.Count > 1 Then
            err = "More than one receipt was found"
        End If
        If Not String.IsNullOrEmpty(err) Then
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            Made4Net.Mobile.MessageQue.Enqueue(trans.Translate(err))
            DO1.Value("RCN") = ""
            DO1.FocusField = "RCN"
            Return Nothing
        End If
        Dim receiptHeader As New WMS.Logic.ReceiptHeader(dt.Rows(0)("RECEIPT"))
        If receiptHeader.STATUS.Equals(WMS.Lib.Statuses.Receipt.CLOSED, StringComparison.OrdinalIgnoreCase) OrElse _
            receiptHeader.STATUS.Equals(WMS.Lib.Statuses.Receipt.CANCELLED, StringComparison.OrdinalIgnoreCase) Then
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("Incorrect receipt status"))
            DO1.Value("RCN") = ""
            DO1.FocusField = "RCN"
            Return Nothing
        End If
        Return receiptHeader

    End Function

    Private Sub GoToCLDInfo(ByVal dr As DataRow, Optional ByVal pReceiveMultipleLines As Boolean = False)
        Session("CreateLoadRCN") = dr("Receipt")
        If Session("CreateLoadRCNMultipleLines") = True And pReceiveMultipleLines Then
            Session("CreateLoadRCNLine") = -1
        Else
            Session("CreateLoadRCNLine") = dr("ReceiptLine")
            Session("CreateLoadRCNMultipleLines") = False
        End If
        Session("CreateLoadConsignee") = dr("Consignee")
        Session("CreateLoadSKU") = dr("SKU")
        Response.Redirect(MobileUtils.GetURLByScreenCode("RDTCLD1"))
    End Sub

    Private Sub doMenu()
        MobileUtils.ClearCreateLoadProcessSession()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doClose()
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If DO1.Value("RCN") <> "" Then
            Try
                Dim rc As ReceiptHeader = New ReceiptHeader(DO1.Value("RCN"), True)
                rc.close(WMS.Logic.GetCurrentUser)
            Catch ex As Exception
                Made4Net.Mobile.MessageQue.Enqueue(trans.Translate(ex.Message))
            End Try
        Else
            Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("Receipt not found"))
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddTextboxLine("RCN", "Receipt Or ASN ID")
        'DO1.AddTextboxLine("RCNLINE")
        DO1.AddTextboxLine("Product")
        DO1.AddSpacer()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
            Case "close receipt"
                doClose()
        End Select
    End Sub
End Class
