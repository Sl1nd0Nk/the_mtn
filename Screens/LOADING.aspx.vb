Imports WMS.Logic
Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.DataAccess

<CLSCompliant(False)> Public Class LOADING
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Session.Remove("LoadingJob")
            DO1.Value("SHIPMENT") = Session("LOADINGSHIPMENT")
            DO1.Value("DOOR") = Session("LOADINGDOOR")
        End If
    End Sub

    Private Sub doNext()
        Try
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            Dim oLoading As New WMS.Logic.Loading
            Dim oLoadingJob As LoadingJob
            If WMS.Logic.Load.Exists(DO1.Value("PALLETID")) Then
                oLoadingJob = oLoading.PickupLoad(DO1.Value("PALLETID"), True, WMS.Logic.Common.GetCurrentUser)
            ElseIf WMS.Logic.Container.Exists(DO1.Value("PALLETID")) Then
                oLoadingJob = oLoading.PickupContainer(DO1.Value("PALLETID"), True, WMS.Logic.Common.GetCurrentUser)
            End If
            If oLoadingJob Is Nothing Then
                Made4Net.Mobile.MessageQue.Enqueue(trans.Translate("Pallet was not found"))
                DO1.Value("PALLETID") = ""
                Return
            Else
                Session("LoadingJob") = oLoadingJob
                Response.Redirect(MapVirtualPath("Screens/LOADING1.aspx"))
            End If
        Catch ex As Threading.ThreadAbortException
            'Do Nothing
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
    End Sub

    Private Sub doMenu()
        If Not Session("LoadingJob") Is Nothing Then
            Dim oLoadindTask As New LoadingTask
            oLoadindTask.ReleaseTask(Session("LoadingJob"), WMS.Logic.Common.GetCurrentUser)
        End If
        Session.Remove("LoadingJob")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddSpacer()
        DO1.AddLabelLine("DOOR")
        DO1.AddLabelLine("SHIPMENT")
        DO1.AddTextboxLine("PALLETID")
        DO1.AddSpacer()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
        End Select
    End Sub

End Class

