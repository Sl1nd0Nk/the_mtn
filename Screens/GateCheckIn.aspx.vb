Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports WMS.Logic

Partial Public Class GateCheckIn
    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Session.Remove("YardAppointment")
            Session.Remove("YardAppointmentCheckInLocation")
            Session.Remove("YardGateCheckSourceScreen")
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddTextboxLine("AppointmentId")
        DO1.AddTextboxLine("Vehicle")
        DO1.AddTextboxLine("Trailer")
        DO1.AddSpacer()
        DO1.AddSpacer()
        'DO1.AddTextboxLine("YardLocation", "Scan Gate")
    End Sub

    Private Sub CleatForm()
        DO1.Value("AppointmentId") = ""
        DO1.Value("Vehicle") = ""
        DO1.Value("Trailer") = ""
        DO1.Value("YardLocation") = ""
    End Sub

    Private Sub NextClicked()
        Try
            Dim oTranslator As New Made4Net.Shared.Translation.Translator
            If DO1.Value("AppointmentId") = String.Empty AndAlso DO1.Value("Vehicle") = String.Empty AndAlso DO1.Value("Trailer") = String.Empty Then
                MessageQue.Enqueue(oTranslator.Translate("Appointment / Vehicle / Trailer cannot be blank"))
                Return
            End If
            
            'Checking in with appointment id
            Dim sAppointmentId As String = GetAppointmentId()
            If sAppointmentId <> String.Empty Then
                'Dim oAppointment As New WMS.Logic.YardAppointment(DO1.Value("AppointmentId"))
                'oAppointment.CheckIn(DO1.Value("YardLocation"), WMS.Logic.Common.GetCurrentUser)
                'MessageQue.Enqueue(oTranslator.Translate(String.Format("Appoimtment checked in successfully. Appointment location {0}", oAppointment.YARDLOCATION)))

                Dim oAppointment As New WMS.Logic.YardAppointment(sAppointmentId)
                Session("YardAppointment") = oAppointment
                Session("YardAppointmentCheckInLocation") = DO1.Value("YardLocation")
                Session("YardGateCheckSourceScreen") = "GateCheckIn"
                Response.Redirect(MapVirtualPath(String.Format("Screens/GateCheckInAppointmentInfo.aspx")))
            ElseIf DO1.Value("Vehicle") <> String.Empty OrElse DO1.Value("Trailer") <> String.Empty Then 'Checking in with vehicle/trailer id
                'Dim oCheckIn As New WMS.Logic.YardCheckIn()
                'oCheckIn.Create("", DateTime.Now, DO1.Value("Vehicle"), DO1.Value("Trailer"), "", "", DO1.Value("YardLocation"), "", "", WMS.Logic.Common.GetCurrentUser)
                'MessageQue.Enqueue(oTranslator.Translate("Check in created successfully"))
                'CleatForm()

                Session("YardGateCheckVehicleId") = DO1.Value("Vehicle")
                Session("YardGateCheckTrailerId") = DO1.Value("Trailer")
                Session("YardAppointmentCheckInLocation") = DO1.Value("YardLocation")
                Session("YardGateCheckSourceScreen") = "GateCheckIn"
                Response.Redirect(MapVirtualPath(String.Format("Screens/GateCheckInAppointmentInfo.aspx")))
            End If
        Catch ex As Threading.ThreadAbortException
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            CleatForm()
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            CleatForm()
            Return
        End Try
    End Sub

    Private Function GetAppointmentId() As String
        Dim sWhereClause As String
        If DO1.Value("AppointmentId") <> String.Empty Then
            sWhereClause = String.Format("APPOINTMENTID = '{0}'", DO1.Value("AppointmentId"))
        End If
        If DO1.Value("Vehicle") <> String.Empty Then
            sWhereClause = sWhereClause + String.Format("and ISNULL(VEHICLE,'') = '{0}'", DO1.Value("Vehicle"))
        End If
        If DO1.Value("Trailer") <> String.Empty Then
            sWhereClause = sWhereClause + String.Format("and ISNULL(Trailer,'') = '{0}'", DO1.Value("Trailer"))
        End If
        sWhereClause = sWhereClause.TrimStart("and".ToCharArray)
        Dim sSql As String = String.Format("select ISNULL(appointmentid,'') as appointmentid, status from YARDAPPOINTMENT where {0}", sWhereClause)
        Dim dt As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sSql, dt)
        If dt.Rows.Count = 0 Then
            If DO1.Value("AppointmentId") <> String.Empty Then
                Throw New Made4Net.Shared.M4NException(New Exception, "Appointment not found", "Appointment not found")
            End If
            Return String.Empty
        ElseIf dt.Rows.Count > 1 Then
            Dim cnt As Int32 = 0
            Dim sAppId As String
            For Each dr As DataRow In dt.Rows
                Dim sTempStat As String = dr("status").ToString
                If sTempStat.Equals(WMS.Lib.Statuses.YardAppointments.SCHEDULED) OrElse sTempStat.Equals(WMS.Lib.Statuses.YardAppointments.PLANNED) Then
                    cnt += 1
                    sAppId = dr("appointmentid")
                End If
            Next
            If cnt > 1 Then
                Throw New Made4Net.Shared.M4NException(New Exception, "More than one row was found", "More than one row was found")
            End If
            Return sAppId
        End If
        Dim sStatus As String = dt.Rows(0)("status").ToString
        If Not sStatus.Equals(WMS.Lib.Statuses.YardAppointments.SCHEDULED) AndAlso Not sStatus.Equals(WMS.Lib.Statuses.YardAppointments.PLANNED) Then
            Throw New Made4Net.Shared.M4NException(New Exception, "Yard appointment status incorrect for checking in", "Yard appointment status incorrect for checking in")
        End If
        Return dt.Rows(0)("appointmentid").ToString
    End Function

    Private Sub GoToMenu()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                NextClicked()
            Case "menu"
                GoToMenu()
        End Select
    End Sub

End Class