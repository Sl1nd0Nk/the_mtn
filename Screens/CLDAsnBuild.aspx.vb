Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports WMS.MobileWebApp.apexInbound
Imports WMS.MobileWebApp.apexSerial

<CLSCompliant(False)> Public Class CLDAsnBuild
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If Session("CreateLoadRCN") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRcnLine.aspx"))
            End If
            'CLDInfo
            DO1.Value("CONSIGNEE") = Session("CreateLoadConsignee")
            DO1.Value("SKU") = Session("CreateLoadSKU")
            DO1.Value("RECEIPT") = Session("CreateLoadRCN")
            DO1.Value("RECEIPTLINE") = Session("CreateLoadRCNLine")
            Dim oSku As New Logic.SKU(Session("CreateLoadConsignee"), Session("CreateLoadSKU"))
            DO1.Value("SKUDESC") = oSku.SKUDESC
            DO1.Value("SKUNOTES") = oSku.NOTES
            DO1.Value("UNITS") = Session("CreateLoadUnits")
            DO1.Value("UOM") = Session("CreateLoadUOM")
            DO1.Value("LOCATION") = Session("CreateLoadLocation")
            DO1.Value("WEIGHT") = Session("CreateLoadWeight")
            DO1.Value("Scanned") = 0
            DO1.setVisibility("SKUNOTES", False)
            DO1.setVisibility("VERMSG", False)
            GetSerialReceivingProgress()
            If (CInt(DO1.Value("Scanned")) >= CInt(DO1.Value("UNITS"))) And (CInt(DO1.Value("Unverified")) <= 0) Then
                Response.Redirect(MapVirtualPath("Screens/CLDSerial1.aspx"))
            End If
        End If
        SetVisibility()
    End Sub

    Private Sub doMenu()
        MobileUtils.ClearCreateLoadProcessSession()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub SetVisibility()
        If CInt(DO1.Value("Scanned")) >= CInt(DO1.Value("UNITS")) Then
            DO1.setVisibility("SERIAL", False)

            If CInt(DO1.Value("Unverified")) > 0 Then
                DO1.setVisibility("VERMSG", True)
            End If
        Else
            DO1.setVisibility("SERIAL", True)
        End If

        'If DO1.Value("Unverified") > 0 Or DO1.Value("VERIFIED") <= 0 Then
        '    DO1.setVisibility("CreateLoad", False)
        'End If

        'DO1.Value("SERIAL") = ""
    End Sub

    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If (Not String.IsNullOrEmpty(DO1.Value("SERIAL"))) And (DO1.Value("SERIAL") <> " ") Then
            Dim err As String = ""
            err = APXINBOUND_AddSerialToStation(DO1.Value("SERIAL"), Logic.GetCurrentUser, Session("CreateLoadRCN"), Session("CreateLoadRCNLine"), _
                                          Session("CreateLoadConsignee"), Session("CreateLoadSKU"), Session("CreateLoadUnits"), _
                                          Session("CreateLoadWeight"), Session("CreateLoadUOM"), Session("CreateLoadLocation"))
            If Not String.IsNullOrEmpty(err) Then
                Made4Net.Mobile.MessageQue.Enqueue(t.Translate(err))
            End If
        Else
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Please scan valid serial number"))
        End If

        GetSerialReceivingProgress()
        SetVisibility()
        DO1.Value("SERIAL") = ""
    End Sub

    Private Sub doVerify()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If CInt(DO1.Value("Unverified")) > 0 Then
            Dim dtLines As DataTable = APXINBOUND_FindSerialsOnStationByState(Logic.GetCurrentUser, Session("CreateLoadRCN"), _
                                                             Session("CreateLoadRCNLine"), Session("CreateLoadConsignee"), _
                                                             Session("CreateLoadSKU"), Session("CreateLoadLocation"), "U")

            If dtLines.Rows.Count = 0 Then
                Return
            Else
                Dim UnVerifiedCount As Integer = 0
                Dim VerifiedCount As Integer = 0
                For Each dr As DataRow In dtLines.Rows
                    If APXINBOUND_OEMVerifySerialsAtStation(dr("SerialNumber"), Session("CreateLoadSKU")) >= 1 Then
                        If APXSERIAL_FindSerialNumber(dr("SerialNumber"), Session("CreateLoadSKU")) >= 1 Then
                            APXINBOUND_UpdateSerialStatusAtStation("F", dr("SerialNumber"), "Already in System")
                        Else
                            APXINBOUND_UpdateSerialStatusAtStation("V", dr("SerialNumber"), "")
                        End If
                    Else
                        APXINBOUND_UpdateSerialStatusAtStation("F", dr("SerialNumber"), "Not in OEM table")
                    End If
                Next
                GetSerialReceivingProgress()
                SetVisibility()
                If (CInt(DO1.Value("Scanned")) >= CInt(DO1.Value("UNITS"))) And (CInt(DO1.Value("Unverified")) <= 0) Then
                    Response.Redirect(MapVirtualPath("Screens/CLDSerial1.aspx"))
                End If
            End If
        Else
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Nothing to verify!!"))
        End If
    End Sub

    Private Sub doAbort()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim err As String = ""
        err = APXINBOUND_RemoveSerialNumbersFromStation(Logic.GetCurrentUser, Session("CreateLoadRCN"), _
                                             Session("CreateLoadRCNLine"), Session("CreateLoadConsignee"), _
                                             Session("CreateLoadSKU"), Session("CreateLoadLocation"))

        If Not String.IsNullOrEmpty(err) Then
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate(err))
        End If

        Session.Remove("CreateLoadRCN")
        Session.Remove("CreateLoadRCNLine")
        Session.Remove("CreateLoadConsignee")
        Session.Remove("CreateLoadSKU")
        Session.Remove("CreateLoadLocation")
        Session.Remove("CreateLoadWeight")
        Session.Remove("CreateLoadLocation")
        Session.Remove("CreateLoadUnits")
        Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRCNLine.aspx"))
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("RECEIPT")
        DO1.AddLabelLine("RECEIPTLINE")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddSpacer()

        DO1.AddTextboxLine("SERIAL", "Scan Serial Number")

        DO1.AddSpacer()
        DO1.AddLabelLine("UNITS", "___Units To Receive____")
        DO1.AddLabelLine("Scanned", "___Scanned Units______")
        DO1.AddLabelLine("VERIFIED", "___Verified Units______")
        DO1.AddLabelLine("FAILEDVER", "___Failed Verification__")
        DO1.AddLabelLine("Unverified", "___Unverified Units____")
        DO1.AddSpacer()
        DO1.AddLabelLine("VERMSG", "Click the verify button to verify unverfied serial numbers")
    End Sub

    Private Sub doViewSerial()
        Response.Redirect(MapVirtualPath("Screens/CLDSerial2.aspx"))
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "serialnumbers"
                doViewSerial()
            Case "next"
                doNext()
            Case "verify"
                doVerify()
            Case "abort"
                doAbort()
        End Select
    End Sub

    Private Sub GetSerialReceivingProgress()
        Dim UnVerifiedCount As Integer = 0
        Dim VerifiedCount As Integer = 0
        Dim ValidScanned As Integer = 0
        Dim InvalidScanned As Integer = 0
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim dtLines As DataTable = APXINBOUND_FindSerialsOnStationByState(Logic.GetCurrentUser, Session("CreateLoadRCN"), _
                                                 Session("CreateLoadRCNLine"), Session("CreateLoadConsignee"), _
                                                 Session("CreateLoadSKU"), Session("CreateLoadLocation"), "")

        If dtLines.Rows.Count = 0 Then
            DO1.Value("VERIFIED") = 0
            DO1.Value("Unverified") = 0
            DO1.Value("FAILEDVER") = 0
        Else
            Dim rowsFound As DataRow()
            Dim rowsFound1 As DataRow()
            Dim rowsFound2 As DataRow()
            Dim rowsFound3 As DataRow()
            For Each dr As DataRow In dtLines.Rows
                rowsFound = dtLines.Select(String.Format("Verified={0}", FormatField("U")))
                If rowsFound.Length > 0 Then
                    UnVerifiedCount = rowsFound.Length
                End If
                rowsFound1 = dtLines.Select(String.Format("Verified={0}", FormatField("V")))
                If rowsFound1.Length > 0 Then
                    VerifiedCount = rowsFound1.Length
                End If
                rowsFound2 = dtLines.Select(String.Format("Verified<>{0}", FormatField("F")))
                If rowsFound2.Length > 0 Then
                    ValidScanned = rowsFound2.Length
                End If
                rowsFound3 = dtLines.Select(String.Format("Verified={0}", FormatField("F")))
                If rowsFound3.Length > 0 Then
                    InvalidScanned = rowsFound3.Length
                End If
            Next
            DO1.Value("VERIFIED") = VerifiedCount
            DO1.Value("Unverified") = UnVerifiedCount
            DO1.Value("FAILEDVER") = InvalidScanned
        End If

        DO1.Value("Scanned") = ValidScanned
    End Sub
End Class
