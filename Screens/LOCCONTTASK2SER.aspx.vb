Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports System.Windows.Forms
Imports WMS.MobileWebApp.apexSerial

<CLSCompliant(False)> Partial Class LOCCONTTASK2SER
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Dim ld As New WMS.Logic.Load(Convert.ToString(Session("LocationCNTLoadId")))
            DO1.Value("CONSIGNEE") = ld.CONSIGNEE
            DO1.Value("SKU") = ld.SKU
            If String.IsNullOrEmpty(Session("ToLocationForVerification")) Then
                DO1.Value("LOCATION") = ld.LOCATION
            Else
                DO1.Value("LOCATION") = Session("ToLocationForVerification")
            End If
            DO1.Value("LOADID") = ld.LOADID

            If Session("LocInProgress") Is Nothing Then
                If CheckIfLocInStockTakeProgress(ld.LOCATION, ld.SKU, Logic.GetCurrentUser()) = False Then
                    SetupLocationForCount(ld, Logic.GetCurrentUser())
                End If

                Session.Add("LocInProgress", True)
                Session.Add("LocInProgressld", ld)
                DO1.setVisibility("STProd", False)
                DO1.setVisibility("STScannedSer", False)
            End If
        End If
    End Sub

    Private Function CheckIfLocInStockTakeProgress(ByVal Location As String, ByVal SKU As String, ByVal user As String) As Boolean
        Dim SQL As String = String.Format("Select * from apx_stocktake_Loc_inprogress where Location={0} and SKU={1}", FormatField(Location), FormatField(SKU))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        If dtLines.Rows.Count > 0 Then
            Return True
        End If

        Return False
    End Function

    Private Sub CompleteLocInStockTakeProgress(ByVal Location As String, ByVal SKU As String)
        Dim SQL As String = String.Format("delete from apx_stocktake_Loc_inprogress where Location={0} and SKU={1}", FormatField(Location), FormatField(SKU))
        DataInterface.RunSQL(SQL)
    End Sub

    Private Sub SetupLocationForCount(ByVal LocLoad As Load, ByVal user As String)
        Dim SQL As String = String.Format("Select * from apx_serial where loadID={0} and SKU={1} and dispatched={2} and received={3}", FormatField(LocLoad.LOADID), FormatField(LocLoad.SKU), FormatField(False), FormatField(True))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        If dtLines.Rows.Count > 0 Then
            Dim ld As Load
            SQL = String.Format("Select * from LOADS where STATUS={0} and SKU={1} and Location={2}", FormatField("LIMBO"), FormatField(LocLoad.SKU), FormatField(""))
            Dim dtLoadLines As New DataTable
            Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLoadLines)
            If dtLoadLines.Rows.Count > 0 Then
                ld = New Load(dtLoadLines.Rows(0)("LOADID"), True)
            Else
                ld = New Load()
                ld.CreateLoad(WMS.Logic.Load.GenerateLoadId(), LocLoad.CONSIGNEE, LocLoad.SKU, LocLoad.LOADUOM, "", "LIMBO", "", 0, "", 1, "", Nothing, "SYSTEM", "", DateTime.Now, Nothing, Nothing)
            End If

            If Not ld Is Nothing Then
                SQL = String.Format("Update apx_serial set loadID={4} where loadID={0} and SKU={1} and dispatched={2} and received={3}", FormatField(LocLoad.LOADID), FormatField(LocLoad.SKU), FormatField(False), FormatField(True), FormatField(ld.LOADID))
                DataInterface.RunSQL(SQL)
            End If
        End If

        SQL = String.Format("Insert into apx_stocktake_Loc_inprogress (Location, SKU, UserID) values({0},{1},{2})", FormatField(LocLoad.LOCATION), FormatField(LocLoad.SKU), FormatField(user))
        DataInterface.RunSQL(SQL)
    End Sub

    Private Sub HandleAddNewSerialNumber()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Not Session("SKUScanned") Is Nothing Then
            If Not String.IsNullOrEmpty(DO1.Value("STSerial")) Then
                AddSerialNumberToSystem()
                Session.Remove("SKUScanned")
                Session.Remove("SerialToAdd")
            Else
                MessageQue.Enqueue(t.Translate("Please scan serial number to add"))
            End If
            DO1.Value("STSerial") = ""
        Else
            If Not String.IsNullOrEmpty(DO1.Value("STProd")) Then
                Dim sku As String = GetSKU(DO1.Value("STProd"))
                If sku = Session("LocInProgressSku") Then
                    Session.Add("SKUScanned", True)
                    DO1.setVisibility("STProd", False)
                    DO1.setVisibility("STSerial", True)
                Else
                    MessageQue.Enqueue(t.Translate("Scanned Product does not match location load product"))
                End If
            Else
                MessageQue.Enqueue(t.Translate("Please scan product of the serial number to add"))
            End If
            DO1.Value("STProd") = ""
        End If
    End Sub

    Private Sub doNext()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Not Session("SerialToAdd") Is Nothing Then
            HandleAddNewSerialNumber()
        Else
            If Not String.IsNullOrEmpty(DO1.Value("STSerial")) Then
                Dim SerDetails As DataTable = APXSERIAL_GetSerialDetailBySerial(DO1.Value("STSerial"))

                If SerDetails.Rows.Count > 0 Then
                    If SerDetails.Rows(0)("received") Then
                        If SerDetails.Rows(0)("dispatched") = False Then
                            Dim ld As Load = Session("LocInProgressld")
                            If SerDetails.Rows(0)("SKU") = ld.SKU Then
                                If SerDetails.Rows(0)("serialStatus") <> "BROKEN" Then
                                    If SerDetails.Rows(0)("parentNodeID") > 0 Then
                                        CheckIfParentIsInTacked(ld, SerDetails.Rows(0)("id"), SerDetails.Rows(0)("parentNodeID"), DO1.Value("STSerial"))
                                    Else
                                        AddSerialToLoad(DO1.Value("STSerial"), SerDetails.Rows(0)("id"), ld, False, 0)
                                    End If
                                Else
                                    MessageQue.Enqueue(t.Translate("Scanned parent serial number is no longer a valid container serial"))
                                End If
                            Else
                                MessageQue.Enqueue(t.Translate("Scanned serial number is not for the product"))
                            End If
                        Else
                            MessageQue.Enqueue(t.Translate("Scanned serial number is dispatched"))
                        End If
                    Else
                        MessageQue.Enqueue(t.Translate("Scanned serial number is not received"))
                    End If
                Else
                    SerialNotOnSystem(DO1.Value("STSerial"))
                End If
            Else
                MessageQue.Enqueue(t.Translate("Please scan serial number"))
            End If

            DO1.Value("STSerial") = ""
        End If
    End Sub

    Private Sub AddSerialNumberToSystem()
        Dim ld As Load = Session("LocInProgressld")

        Dim SUom As SKU.SKUUOM = SKU.SKUUOM.GetSKUUOM(ld.CONSIGNEE, ld.SKU, ld.LOADUOM)

        APXSERIAL_AddSerialToLoad(Session("SerialToAdd"), ld.SKU, ld.LOADID, ld.LOADUOM, Logic.GetCurrentUser(), SUom.GROSSWEIGHT, ld.STATUS, "STOCKTAKE_FOUND")
    End Sub

    Private Sub AddSerialToLoad(ByVal Serial As String, ByVal id As Integer, ByVal ld As Load, ByVal breakContainer As Boolean, ByVal parentID As Integer)
        APAXSERIAL_MoveSerialToLoad(Serial, id, ld.SKU, ld.LOADID, breakContainer, parentID)

        APXSERIAL_MoveChildSerials(ld.SKU, ld.LOADID, id)
    End Sub

    Private Sub CheckIfParentIsInTacked(ByVal ld As Load, ByVal id As Integer, ByVal parentID As Integer, ByVal serial As String)
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim SQL As String = String.Format("Select * apx_serial where SKU={0} and dispatched={1} and received={2} and id={3}", FormatField(ld.SKU), FormatField(False), FormatField(True), FormatField(parentID))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        If dtLines.Rows.Count > 0 Then
            If dtLines.Rows(0)("serialStatus") <> "BROKEN" Then
                Dim result As Integer = MessageBox.Show("Scanned serial number is still attached to a parent serial number, would you like to break the parent?", "Serial in container", MessageBoxButtons.YesNoCancel)
                If result = DialogResult.Cancel Then
                    'MessageQue.Enqueue(t.Translate("Please scan serial number"))
                ElseIf result = DialogResult.No Then
                    'MessageQue.Enqueue(t.Translate("Please scan serial number"))
                ElseIf result = DialogResult.Yes Then
                    AddSerialToLoad(serial, id, ld, True, parentID)
                End If
            Else
                AddSerialToLoad(serial, id, ld, True, parentID)
            End If
        Else
            AddSerialToLoad(serial, id, ld, True, parentID)
        End If
    End Sub

    Private Sub SerialNotOnSystem(ByVal serial As String)
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim result As Integer = MessageBox.Show("Serial Number not found on the system, would you like to add it?", "Serial Not Found", MessageBoxButtons.YesNoCancel)
        If result = DialogResult.Cancel Then
            'MessageQue.Enqueue(t.Translate("Please scan serial number"))
        ElseIf result = DialogResult.No Then
            'MessageQue.Enqueue(t.Translate("Please scan serial number"))
        ElseIf result = DialogResult.Yes Then
            'MessageBox.Show("Yes pressed")
            DO1.setVisibility("STProd", True)
            DO1.setVisibility("STSerial", False)
            DO1.Value("STSerial") = ""
            Session.Add("SerialToAdd", serial)
        End If
    End Sub

    Private Function GetSKU(ByVal product As String) As String
        Dim inpSku As String
        Dim SQL As String

        SQL = String.Format("SELECT SKU FROM vSKUCODE WHERE SKUCODE = '{0}'", product)

        inpSku = Made4Net.DataAccess.DataInterface.ExecuteScalar(SQL)

        Return inpSku
    End Function

    Private Sub doBack()
        Session.Remove("LocationCNTLoadId")
        Session.Remove("ToLocationForVerification")
        If Request.QueryString("countsourcescreen") <> "" Then
            Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx?countsourcescreen=" & Request.QueryString("countsourcescreen")))
        Else
            Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx"))
        End If
    End Sub

    Private Sub doComplete()
        Dim CountOk As Boolean = False
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim ld As WMS.Logic.Load = Session("LocInProgressld")

            If Not ld Is Nothing Then
                Dim SerialCount As Integer = GetLoadCountFromSerials(ld.LOADID)

                Dim oCntTask As WMS.Logic.CountTask
                If Not Session("LocationBulkCountTask") Is Nothing Then
                    oCntTask = Session("LocationBulkCountTask")
                Else
                    oCntTask = Session("LocationCountTask")
                End If
                Dim oCounting As New WMS.Logic.Counting(oCntTask.COUNTID)
                'Build and fill count job object
                Dim oCountJob As WMS.Logic.CountingJob = oCntTask.getCountJob(oCounting)
                oCountJob.CountedQty = SerialCount
                oCountJob.UOM = ld.LOADID
                'oCountJob.Location = ld.LOCATION
                If String.IsNullOrEmpty(Session("ToLocationForVerification")) Then
                    oCountJob.Location = ld.LOCATION
                Else
                    oCountJob.Location = Session("ToLocationForVerification")
                End If

                oCountJob.LoadId = Session("LocationCNTLoadId")
                oCountJob.LoadCountVerified = False
                oCountJob.LocationCountingLoadCounted = True
                Dim oAttrCol As AttributesCollection = ExtractAttributeValues()
                oCountJob.CountingAttributes = oAttrCol
                CountOk = oCntTask.Count(oCounting, oCountJob, WMS.Logic.GetCurrentUser)
                If Not CountOk Then
                    MessageQue.Enqueue(t.Translate("The difference between conuted units and load units are greater that counting tolerance. Please Confirm"))
                    Session("CountedUnitsForVerification") = oCountJob.CountedQty
                    Session("UOMForVerification") = oCountJob.UOM
                    Session("AttributesForVerification") = oCountJob.CountingAttributes
                End If
                UpdateLoadCount(ld.LOADID, SerialCount)
                CompleteLocInStockTakeProgress(ld.LOCATION, ld.SKU)
            End If
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(ex.Message)
            Return
        End Try

        If Request.QueryString("countsourcescreen") <> "" Then
            If Not CountOk Then
                Response.Redirect(MapVirtualPath("Screens/CNTTASKVERFICATION.aspx?SRCSCREEN=LOCCONTTASK&countsourcescreen=" & Request.QueryString("countsourcescreen")))
            Else
                Session.Remove("ToLocationForVerification")
                Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx?countsourcescreen=" & Request.QueryString("countsourcescreen")))
            End If
        Else
            If Not CountOk Then
                Response.Redirect(MapVirtualPath("Screens/CNTTASKVERFICATION.aspx?SRCSCREEN=LOCCONTTASK"))
            Else
                Session.Remove("ToLocationForVerification")
                Response.Redirect(MapVirtualPath("Screens/LOCCONTTASK1.aspx"))
            End If
        End If
    End Sub

    Private Sub doShowSerials()
        Session.Add("SerialView", True)

        DO1.setVisibility("STSerial", False)
        DO1.setVisibility("STProd", False)
        DO1.setVisibility("STScannedSer", True)

        DO1.Value("STScannedSer") = GetScannedSerialNumbers()

        DO1.Button(0).Text = "Close"
        DO1.Button(1).Visible = False
        DO1.Button(2).Visible = False
        DO1.Button(3).Visible = False
    End Sub

    Private Sub doCloseSerialView()
        Session.Remove("SerialView")
        DO1.Value("STScannedSer") = ""

        If Not Session("SerialToAdd") Is Nothing Then
            If Not Session("SKUScanned") Is Nothing Then
                DO1.setVisibility("STSerial", True)
                DO1.setVisibility("STProd", False)
            Else
                DO1.setVisibility("STSerial", False)
                DO1.setVisibility("STProd", True)
            End If
        Else
            DO1.setVisibility("STSerial", True)
            DO1.setVisibility("STProd", False)
        End If
        DO1.setVisibility("STScannedSer", False)

        DO1.Button(0).Text = "Next"
        DO1.Button(1).Visible = True
        DO1.Button(2).Visible = True
        DO1.Button(3).Visible = True
    End Sub

    Private Function GetScannedSerialNumbers() As String
        Dim ld As Load = Session("LocInProgressld")

        Dim SQL As String = String.Format("SELECT * FROM apx_serial WHERE loadID={0}", FormatField(ld.LOADID))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        If dtLines.Rows.Count > 0 Then
            Dim StartTable As String = "<table>"
            Dim EndTable As String = "</table>"
            Dim StartRow As String = "<tr>"
            Dim EndRow As String = "</tr>"

            Dim x As Integer = 1

            For Each dr1 As DataRow In dtLines.Rows
                If x > 3 Then
                    x = 1
                    StartTable = StartTable & EndRow
                End If

                If x = 1 Then
                    StartTable = StartTable & StartRow
                End If

                StartTable = StartTable & "<td>" & dr1("serial") & "</td><td>&nbsp&nbsp</td>"

                x += 1
            Next

            Return StartTable & EndTable
        Else
            Return ""
        End If
    End Function

    Private Function GetLoadCountFromSerials(ByVal loadID As String) As Integer
        Dim Count As Integer = 0

        APEXSERIAL_GetLoadSerialCount(loadID, Count)

        Return Count
    End Function

    Private Sub UpdateLoadCount(ByVal pLoadId As String, ByVal pToQty As Decimal)
        Dim dt As DataTable = Session("TaskLocationCNTLoadsDT")
        For Each dr As DataRow In dt.Rows
            If dr("loadid") = pLoadId Then
                dr("counted") = 1
                dr("toqty") = pToQty
            End If
        Next
        Session("TaskLocationCNTLoadsDT") = dt
    End Sub

    Private Function ExtractAttributeValues() As WMS.Logic.AttributesCollection
        Dim ld As New WMS.Logic.Load(Convert.ToString(Session("LocationCNTLoadId")))
        Dim oSku As String = ld.SKU
        Dim oConsignee As String = ld.CONSIGNEE
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If objSkuClass Is Nothing Then Return Nothing
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            Dim req As Boolean = False
            If oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Dim val As Object
                Try
                    Select Case oAtt.Type
                        Case Logic.AttributeType.Boolean
                            val = CType(DO1.Value(oAtt.Name), Boolean)
                        Case Logic.AttributeType.DateTime
                            val = DateTime.ParseExact(DO1.Value(oAtt.Name), Made4Net.Shared.AppConfig.DateFormat, Nothing)
                        Case Logic.AttributeType.Decimal
                            val = CType(DO1.Value(oAtt.Name), Decimal)
                        Case Logic.AttributeType.Integer
                            val = CType(DO1.Value(oAtt.Name), Int32)
                        Case Else
                            val = DO1.Value(oAtt.Name)
                            If val = String.Empty And oAtt.CaptureAtCounting = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                                val = ld.LoadAttributes.Attribute(oAtt.Name)
                            End If
                    End Select
                    oAttCol.Add(oAtt.Name, val)
                Catch ex As Exception
                End Try
            End If
        Next
        Return oAttCol
    End Function

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("LOADID")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOCATION")
        DO1.AddSpacer()
        DO1.AddTextboxLine("STSerial", "Scan Serial Number")
        DO1.AddTextboxLine("STProd", "Scan Product")
        DO1.AddTextboxLine("STScannedSer", "")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                If Not Session("SerialView") Is Nothing Then
                    doCloseSerialView()
                Else
                    doNext()
                End If
            Case "back"
                doBack()
            Case "complete"
                doComplete()
            Case "view serial numbers"
                doShowSerials()
        End Select
    End Sub

End Class
