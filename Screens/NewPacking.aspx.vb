Imports Made4Net.Shared.Web
Imports WMS.Logic
Imports Made4Net.Shared
Imports System.Data
Imports Made4Net.DataAccess
Imports Made4Net.Mobile
Imports WMS.MobileWebApp.apexSerial

Partial Public Class NewPacking
    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If WMS.Logic.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            If GetStationUsingMachineIP(System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName()).GetValue(0).ToString()) = False Then
                SetInitialState()
                DO1.Value("PackCompleteMsg") = "Machine not configured as a packing station"
                DO1.setVisibility("PackCompleteMsg", True)
                Return
            End If
        End If

        If Not String.IsNullOrEmpty(DO1.Value("ScannedTote")) Then
            Dim dtLines1 As DataTable = GetScannedContainerID()
            Dim Picklist As String = ""
            If dtLines1.Rows.Count > 0 Then
                Picklist = dtLines1.Rows(0)("PickList")
            End If
            PostAddChildControls(Picklist)
        End If

        WhereToNext()
    End Sub

    Private Function GetStationUsingMachineIP(ByVal IPAddress As String) As Boolean
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim Station As Integer = 0
        Dim strUnverSerTransIns As String
        Dim Result As Boolean = False
        strUnverSerTransIns = String.Format("Select * From [apx_packStationHeader] Where StationIP={0}", FormatField(IPAddress))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        If dtLines.Rows.Count > 0 Then
            If Not IsDBNull(dtLines.Rows(0)("StationNumber")) Then
                If Not IsDBNull(dtLines.Rows(0)("LaserPrinter")) Then
                    If Not IsDBNull(dtLines.Rows(0)("LabelPrinter")) Then
                        Session.Add("StationNumber", dtLines.Rows(0)("StationNumber"))
                        Session.Add("LaserPrinter", dtLines.Rows(0)("LaserPrinter"))
                        Session.Add("LabelPrinter", dtLines.Rows(0)("LabelPrinter"))

                        If Not IsDBNull(dtLines.Rows(0)("ToteID")) Then
                            Session.Add("ToteID", dtLines.Rows(0)("ToteID"))
                            If Not IsDBNull(dtLines.Rows(0)("PickList")) Then
                                Session.Add("PickList", dtLines.Rows(0)("PickList"))
                            End If
                        End If

                        ShowAllToteItemsAtPacking()
                        PrintInvoiceAtPacking()
                        Result = True
                    Else
                        MessageQue.Enqueue(t.Translate("Label Printer Not set up at station " + dtLines.Rows(0)("StationNumber")))
                    End If
                Else
                    MessageQue.Enqueue(t.Translate("Invoice Printer Not set up at station " + dtLines.Rows(0)("StationNumber")))
                End If
            Else
                MessageQue.Enqueue(t.Translate("Machine not connfigured to a pack station - IP Address[" + IPAddress + "]"))
            End If
        Else
            MessageQue.Enqueue(t.Translate("Machine not connfigured to a pack station - IP Address[" + IPAddress + "]"))
        End If

        Return Result

    End Function

    Private Sub WhereToNext()
        Dim AllPacked As Boolean = False
        Dim ItemsList As String = ""
        Dim ShowAllItems As Boolean = False
        Dim CartonScanned As Boolean = False
        Dim DocPrinted As Boolean = False

        If Session("StationNumber") Is Nothing Then
            If GetStationUsingMachineIP(System.Net.Dns.GetHostAddresses(System.Net.Dns.GetHostName()).GetValue(0).ToString()) = False Then
                Return
            End If
        End If

        Dim NoTote As Boolean = True
        If Session("ToteID") IsNot Nothing And Session("PickList") IsNot Nothing Then
            NoTote = False

            If Session("ShowAllItems") IsNot Nothing Then
                ShowAllItems = True
            End If

            Dim dtLines As DataTable = GetPackItems(Session("StationNumber"), Session("PickList"), Session("ToteID"))

            If dtLines.Rows.Count > 0 Then
                AllPacked = True
                If ShowAllItems Then
                    ItemsList = ConstructFullItemsList(dtLines, AllPacked)
                Else
                    ItemsList = ConstructSingleItemsList(dtLines, AllPacked)
                End If

                If AllPacked Then
                    Dim CartonID As String = ""
                    CartonScanned = CheckCartonScanned(Session("StationNumber"), Session("PickList"), Session("ToteID"), DocPrinted, CartonID)

                    If CartonScanned Then
                        DispatchScannedSerials(CartonID)
                        ClearScannedSerials()
                        ClearPackingStation()
                    End If
                End If
            End If
        End If

        SetDisplayAcccordingly(NoTote, AllPacked, ItemsList, ShowAllItems, CartonScanned)
    End Sub

    Private Function ConstructSingleItemsList(ByVal dtLines As DataTable, ByRef AllPacked As Boolean) As String
        Dim index As Integer = 0
        Dim ItemStr As String = "<table border='1'><tr><th>PICKLIST LINE</th><th>SKU</th><th>PRODUCT DESCRIPTION</th><th>PICKED QTY</th><th>QTY PACKED</th></tr>"
        While index < dtLines.Rows.Count
            If dtLines.Rows(index)("PickedQty") <> dtLines.Rows(index)("PackedQty") Then
                Exit While
            End If

            index = index + 1
        End While

        If index < dtLines.Rows.Count Then
            AllPacked = False
            ItemStr = ItemStr + "<tr><td>" + CStr(dtLines.Rows(index)("PiclistLine")) + "</td><td>" + dtLines.Rows(index)("SKU") + "</td><td>" + dtLines.Rows(index)("SKUDESC") + "</td><td>" + CStr(dtLines.Rows(index)("PickedQty")) + "</td><td>" + CStr(dtLines.Rows(index)("PackedQty")) + "</td></tr>"
            Session.Add("QtyToConfirm", dtLines.Rows(index)("PickedQty"))
            Session.Add("PLine", dtLines.Rows(index)("PiclistLine"))
            Session.Add("SKU", dtLines.Rows(index)("SKU"))
            Session.Add("Consignee", dtLines.Rows(index)("Consignee"))
            Session.Add("OrderID", dtLines.Rows(index)("OrderID"))
        Else
            For Each dr As DataRow In dtLines.Rows
                ItemStr = ItemStr + "<tr><td>" + CStr(dr("PiclistLine")) + "</td><td>" + dr("SKU") + "</td><td>" + dr("SKUDESC") + "</td><td>" + CStr(dr("PickedQty")) + "</td><td>" + CStr(dr("PackedQty")) + "</td></tr>"
            Next
            Session.Add("OrderID", dtLines.Rows(0)("OrderID"))
        End If

        ItemStr = ItemStr + "</table>"
        Return ItemStr
    End Function

    Private Function ConstructFullItemsList(ByVal dtLines As DataTable, ByRef AllPacked As Boolean) As String
        Dim ItemStr As String = "<table border='1'><tr><th>PICKLIST LINE</th><th>SKU</th><th>PRODUCT DESCRIPTION</th><th>PICKED QTY</th><th>QTY PACKED</th></tr>"
        For Each dr As DataRow In dtLines.Rows
            If dr("PickedQty") <> dr("PackedQty") And AllPacked = True Then
                AllPacked = False
            End If
            ItemStr = ItemStr + "<tr><td>" + CStr(dr("PiclistLine")) + "</td><td>" + dr("SKU") + "</td><td>" + dr("SKUDESC") + "</td><td>" + CStr(dr("PickedQty")) + "</td><td>" + CStr(dr("PackedQty")) + "</td></tr>"
        Next

        Session.Add("OrderID", dtLines.Rows(0)("OrderID"))

        ItemStr = ItemStr + "</table>"
        Return ItemStr
    End Function

    Private Sub SetDisplayAcccordingly(ByVal NoTote As Boolean, ByVal AllPacked As Boolean, ByVal ItemsList As String, ByVal ShowAllItems As Boolean, ByVal CartonScanned As Boolean)
        SetInitialState()
        DO1.setVisibility("PackStation", True)
        DO1.Value("PackStation") = Session("StationNumber")

        If NoTote Then
            DO1.setVisibility("ScannedTote", True)
            DO1.setVisibility("ToteID", False)
            DO1.setVisibility("OrderID", False)
            DO1.FocusField = "ScannedTote"
        Else
            DO1.setVisibility("ScannedTote", False)
            DO1.setVisibility("ToteID", True)
            DO1.setVisibility("OrderID", True)
            DO1.setVisibility("PackItems", True)
            DO1.Value("ToteID") = Session("ToteID")
            DO1.Value("OrderID") = Session("OrderID")
            DO1.Value("PackItems") = ItemsList

            If ShowAllItems Then
                If AllPacked = False Then
                    DO1.setVisibility("ScannedBS", True)
                    DO1.FocusField = "ScannedBS"
                Else
                    DisplaAllPackedAccordingly(CartonScanned)
                End If
            Else
                If AllPacked = False Then
                    DO1.setVisibility("EnteredQty", True)
                    DO1.FocusField = "EnteredQty"
                    Dim oSku As New WMS.Logic.SKU(Session("Consignee"), Session("SKU"))
                    setAttributeFieldsVisibility(Session("PickList"), oSku.SKUClass.LoadAttributes)
                Else
                    DisplaAllPackedAccordingly(CartonScanned)
                End If
            End If
        End If
    End Sub

    Private Sub DisplaAllPackedAccordingly(ByVal CartonScanned As Boolean)
        If CartonScanned Then
            DO1.setVisibility("ToteID", False)
            DO1.setVisibility("OrderID", False)
            DO1.setVisibility("PackItems", False)
            DO1.Value("PackCompleteMsg") = "All Items in Tote Packed. Scan New Tote"
            DO1.setVisibility("PackCompleteMsg", True)
            DO1.setVisibility("ScannedTote", True)
            DO1.FocusField = "ScannedTote"
            Session.Remove("ToteID")
            Session.Remove("PickList")
            Session.Remove("QtyToConfirm")
            Session.Remove("PLine")
            Session.Remove("OrderID")
            Session.Remove("SKU")
            Session.Remove("Consignee")
        Else
            ClearAttributes(Session("PickList"))
            DO1.setVisibility("ScannedCarton", True)
            DO1.FocusField = "ScannedCarton"
        End If
    End Sub

    Private Sub SetInitialState()
        DO1.setVisibility("PackStation", False)
        DO1.setVisibility("ScannedTote", False)
        DO1.setVisibility("ToteID", False)
        DO1.setVisibility("OrderID", False)
        DO1.setVisibility("PackItems", False)
        DO1.setVisibility("ScannedBS", False)
        DO1.setVisibility("ScannedCarton", False)
        DO1.setVisibility("EnteredQty", False)
        DO1.setVisibility("PackCompleteMsg", False)
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("PackStation", "Packing Station")
        DO1.AddLabelLine("ToteID")
        DO1.AddLabelLine("OrderID")
        DO1.AddSpacer()
        DO1.AddTextboxLine("ScannedTote", "Scan Tote/Container")
        DO1.AddTextboxLine("ScannedBS", "Scan Serial/Barcode")
        DO1.AddTextboxLine("EnteredQty", "Confirm Qty Packed")
        Dim picklist As String = Session("PickList")
        If picklist IsNot Nothing Or picklist <> "" Then
            PostAddChildControls(picklist)
        End If

    End Sub

    Private Sub PostAddChildControls(ByVal pickList As String)
        addAttributesFields(pickList)
        DO1.AddTextboxLine("ScannedCarton", "Scan Carton")
        DO1.AddSpacer()
        DO1.AddLabelLine("PackItems", "")
        DO1.AddLabelLine("PackCompleteMsg", "")
    End Sub

    Private Sub addAttributesFields(ByVal pPickListID As String)
        For Each attrName As String In getAttributesForAllPickList(pPickListID)
            DO1.AddTextboxLine(String.Format("Attr_{0}", attrName), attrName)
        Next
    End Sub

    Private Function getAttributesForAllPickList(ByVal pPickListID As String) As System.Collections.Generic.List(Of String)
        If ViewState()("PickListAttributes") Is Nothing Then
            Dim list As New System.Collections.Generic.List(Of String)
            Dim sql As String
            Dim dt As New DataTable()
            sql = String.Format("select distinct attributename from pickdetail pd inner join sku on pd.consignee = sku.consignee and pd.sku = sku.sku inner join skuclsloadatt att on sku.classname = att.classname where pd.picklist={0} and att.VERIFICATIONCAPTURE <> {1}", _
            Made4Net.Shared.FormatField(pPickListID), Made4Net.Shared.FormatField(WMS.Logic.SkuClassLoadAttribute.CaptureType.NoCapture))
            Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)

            For Each dr As DataRow In dt.Rows
                list.Add(dr("ATTRIBUTENAME").ToString())
            Next
            ViewState("PickListAttributes") = list
        End If
        Return ViewState("PickListAttributes")
    End Function

    Private Sub setAttributeFieldsVisibility(ByVal pPickListID As String, ByVal pSkuClassLoadAttributeColl As SkuClassLoadAttributeCollection)
        'Dim key As String = String.Format("{0}~{1}", pConsignee, pSKU)
        For Each attrName As String In getAttributesForAllPickList(pPickListID)
            DO1.setVisibility(String.Format("Attr_{0}", attrName), False)
            If pSkuClassLoadAttributeColl Is Nothing Then Continue For
            For Each skuClsLoadAtt As SkuClassLoadAttribute In pSkuClassLoadAttributeColl
                If skuClsLoadAtt.Name.Equals(attrName, StringComparison.OrdinalIgnoreCase) Then
                    If skuClsLoadAtt.CaptureAtVerification <> SkuClassLoadAttribute.CaptureType.NoCapture Then
                        DO1.setVisibility(String.Format("Attr_{0}", attrName), True)
                        Continue For
                    End If
                End If
            Next
        Next
    End Sub

    Private Sub ClearAttributes(ByVal pPicklist As String)
        For Each attStr As String In getAttributesForAllPickList(pPicklist)
            DO1.Value(String.Format("Attr_{0}", attStr)) = ""
            DO1.setVisibility(String.Format("Attr_{0}", attStr), False)
        Next
    End Sub

    Private Function ExtractAttributeValues() As WMS.Logic.AttributesCollection
        Dim oSku As String = Session("SKU")
        Dim oConsignee As String = Session("Consignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If objSkuClass Is Nothing Then Return Nothing
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            Dim req As Boolean = False
            If oAtt.CaptureAtVerification = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtVerification = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Dim val As Object
                Try
                    Select Case oAtt.Type
                        Case Logic.AttributeType.Boolean
                            val = CType(DO1.Value(oAtt.Name), Boolean)
                        Case Logic.AttributeType.DateTime
                            'val = CType(DO1.Value(oAtt.Name), DateTime)
                            val = DateTime.ParseExact(DO1.Value(oAtt.Name), Made4Net.Shared.AppConfig.DateFormat, Nothing)
                        Case Logic.AttributeType.Decimal
                            val = CType(DO1.Value(oAtt.Name), Decimal)
                        Case Logic.AttributeType.Integer
                            val = CType(DO1.Value(oAtt.Name), Int32)
                        Case Else
                            val = DO1.Value(oAtt.Name)
                    End Select
                    oAttCol.Add(oAtt.Name, val)
                Catch ex As Exception
                End Try
            End If
        Next
        Return oAttCol
    End Function

    Private Sub ShowAllToteItemsAtPacking()
        Dim Value As String = "NO"
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'ShowAllToteItemsAtPacking'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        If Value = "YES" Then
            Session.Add("ShowAllItems", Value)
        Else
            Session.Remove("ShowAllItems")
        End If
    End Sub

    Private Sub PrintInvoiceAtPacking()
        Dim Value As String = "NO"
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'PrintInvoiceAtPacking'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        If Value = "YES" Then
            Session.Add("PrintInvoice", Value)
        Else
            Session.Remove("PrintInvoice")
        End If
    End Sub

    Private Function CheckCartonScanned(ByVal StationID As Integer, ByVal PickList As String, ByVal ToContainer As String, ByRef DocPrinted As Boolean, ByRef CartonID As String) As Boolean
        Dim strUnverSerTransIns As String
        Dim CartonScanned As Boolean = False

        strUnverSerTransIns = String.Format("Select * From [apx_packStationHeader] Where StationNumber={0} and PickList={1} and ToteID={2}", FormatField(StationID), FormatField(PickList), FormatField(ToContainer))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        CartonID = ""

        If dtLines.Rows.Count > 0 Then
            If Not IsDBNull(dtLines.Rows(0)("CartonID")) Then
                CartonID = dtLines.Rows(0)("CartonID")
                CartonScanned = True
            End If

            If Not IsDBNull(dtLines.Rows(0)("DocPrinted")) Then
                DocPrinted = dtLines.Rows(0)("DocPrinted")
            End If
        End If

        Return CartonScanned
    End Function

    Private Function GetPackItems(ByVal StationID As Integer, ByVal PickList As String, ByVal ToContainer As String) As DataTable
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_packStationDetail] Where StationID={0} and PickList={1} and ToContainer={2}", FormatField(StationID), FormatField(PickList), FormatField(ToContainer))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        Return dtLines
    End Function

    Private Sub ClearPackingStation()
        Dim SQL1 As String = String.Format("delete from apx_packStationDetail where StationID={0} and PickList={1}", FormatField(Session("StationNumber")), FormatField(Session("PickList")))
        DataInterface.RunSQL(SQL1)

        Dim SQL As String = String.Format("Update apx_packStationHeader set ToteID=NULL, PickList=NULL, CartonID=NULL, DocPrinted=0, DocVerified=0 where StationNumber={0}", FormatField(Session("StationNumber")))
        DataInterface.RunSQL(SQL)
    End Sub

    Private Sub doMenu()
        Session.Remove("StationNumber")
        Session.Remove("PackStation")
        Session.Remove("LaserPrinter")
        Session.Remove("LabelPrinter")
        Session.Remove("PrintInvoice")
        Session.Remove("ToteID")
        Session.Remove("PickList")
        Session.Remove("QtyToConfirm")
        Session.Remove("PLine")
        Session.Remove("OrderID")
        Session.Remove("SKU")
        Session.Remove("Consignee")

        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doReset()
        If Session("StationNumber") IsNot Nothing And Session("PickList") IsNot Nothing Then
            ClearScannedSerials()
            ClearPackingStation()

            Session.Remove("ToteID")
            Session.Remove("PickList")
            Session.Remove("OrderID")
            Session.Remove("QtyToConfirm")
            Session.Remove("PLine")
            Session.Remove("SKU")
            Session.Remove("Consignee")
        End If
        Response.Redirect(MapVirtualPath("Screens/NewPacking.aspx"))

    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText
            Case "Next"
                doDetermineProcess()
            Case "Reset"
                doReset()
            Case "Menu"
                doMenu()
        End Select
    End Sub

    Private Sub doDetermineProcess()
        If Session("StationNumber") IsNot Nothing Then
            If Not String.IsNullOrEmpty(DO1.Value("ScannedTote")) Then
                ProcessScannedTote()
            ElseIf Not String.IsNullOrEmpty(DO1.Value("ScannedBS")) Then
                ProcessScannedBarcodeOrSerial()
            ElseIf Not String.IsNullOrEmpty(DO1.Value("EnteredQty")) Then
                ProcessEnteredQty(ExtractAttributeValues())
            ElseIf Not String.IsNullOrEmpty(DO1.Value("ScannedCarton")) Then
                ProcessScannedCarton()
            End If

            WhereToNext()
        Else
            Response.Redirect(MapVirtualPath("Screens/NewPacking.aspx"))
        End If
    End Sub

    Private Function GetScannedContainerID() As DataTable
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [vPackablePicklists] Where ToContainer={0} AND ATTRIB1 is NULL", FormatField(DO1.Value("ScannedTote")))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        Return dtLines
    End Function

    Private Sub ProcessScannedTote()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim strUnverSerTransIns As String

        Dim dtLines As DataTable = GetScannedContainerID()

        If dtLines.Rows.Count > 0 Then

            strUnverSerTransIns = String.Format("Select * From [apx_packStationDetail] Where PickList={0}", FormatField(dtLines.Rows(0)("PICKLIST")))

            Dim dtLines1 As New DataTable
            Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines1)

            If dtLines1.Rows.Count > 0 Then
                If dtLines1.Rows(0)("StationID") <> Session("StationNumber") Then
                    MessageQue.Enqueue(t.Translate("Tote is currently being packed at station" + dtLines1.Rows(0)("StationID")))
                    Return
                End If
            End If

            For Each dr As DataRow In dtLines.Rows
                Dim SQL As String = String.Format("Insert INTO apx_packStationDetail (StationID, PickList, PiclistLine, OrderID, OrderLine, SKU, SKUDESC, SKUClass, UOM, AdjQty, PickedQty, PackedQty, ToLoad, ToContainer, Consignee) " & _
                                                  "VALUES ({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14})", FormatField(Session("StationNumber")), FormatField(dr("PICKLIST")), FormatField(dr("PICKLISTLINE")), _
                                                  FormatField(dr("ORDERID")), FormatField(dr("ORDERLINE")), FormatField(dr("SKU")), FormatField(dr("SKUDESC")), _
                                                  FormatField(dr("CLASSNAME")), FormatField(dr("UOM")), _
                                                  FormatField(dr("ADJQTY")), FormatField(dr("PICKEDQTY")), _
                                                  FormatField(0), FormatField(dr("TOLOAD")), _
                                                  FormatField(dr("TOCONTAINER")), FormatField(dr("CONSIGNEE")))
                DataInterface.RunSQL(SQL)
            Next

            Dim SQL1 As String = String.Format("Update apx_packStationHeader set ToteID={0}, PickList={1}, CartonID=NULL, DocPrinted=0, DocVerified=0 where StationNumber={2}", FormatField(DO1.Value("ScannedTote")), FormatField(dtLines.Rows(0)("PICKLIST")), FormatField(Session("StationNumber")))
            DataInterface.RunSQL(SQL1)

            Session.Add("ToteID", DO1.Value("ScannedTote"))
            Session.Add("PickList", dtLines.Rows(0)("PICKLIST"))
        Else
            MessageQue.Enqueue(t.Translate("Invalid Tote ID scanned"))
        End If

        DO1.Value("ScannedTote") = ""
    End Sub

    Private Function CheckIfSerialAlreadyScanned(ByVal Serial As String) As DataTable
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_packStationSerials] Where SerialNumber={0}", FormatField(Serial))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        Return dtLines
    End Function

    Private Sub ClearScannedSerials()
        Dim SQL1 As String = String.Format("delete from apx_packStationSerials where StationID={0}", FormatField(Session("StationNumber")))
        DataInterface.RunSQL(SQL1)
    End Sub

    Private Sub DispatchScannedSerials(ByVal CartonID As String)
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_packStationSerials] Where StationID={0}", FormatField(Session("StationNumber")))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)
        If dtLines.Rows.Count > 0 Then
            For Each dr As DataRow In dtLines.Rows
                Dim SQL1 As String = String.Format("Update apx_serial set dispatched=1, userid={0}, serialStatus='DISPATCHED', salesOrderID={1}, salesOrderLineID={2}, cartonID={3} where serial={4}", _
                                                   FormatField(Logic.GetCurrentUser), FormatField(dr("OrderID")), FormatField(dr("OrderLine")), FormatField(CartonID), FormatField(dr("SerialNumber")))
                DataInterface.RunSQL(SQL1)
            Next
        End If
    End Sub

    Private Sub ProcessScannedBarcodeOrSerial()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim strUnverSerTransIns As String
        Dim Serial As String = DO1.Value("ScannedBS")

        'Dim SerialResults As DataTable = APXSERIAL_GetSerialData(Serial)
        'MessageQue.Enqueue(t.Translate(SerialResults.Rows(0)("SKU")))

        strUnverSerTransIns = String.Format("Select * From [apx_packStationDetail] Where PickList={0} and StationID={1}", FormatField(Session("PickList")), FormatField(Session("StationNumber")))
        Dim dtLines1 As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines1)

        If dtLines1.Rows.Count > 0 Then
            Dim SerialResults As DataTable = APXSERIAL_GetSerialData(Serial)
            If SerialResults.Rows.Count > 0 Then
                Dim AlreadyScanned As Boolean = False
                Dim PackSer As DataTable = CheckIfSerialAlreadyScanned(SerialResults.Rows(0)("serial"))

                If PackSer.Rows.Count = 0 Then
                    Dim x As Integer = 0
                    While x < dtLines1.Rows.Count
                        If dtLines1.Rows(x)("SKU") = SerialResults.Rows(0)("SKU") And dtLines1.Rows(x)("PickedQty") > dtLines1.Rows(x)("PackedQty") Then
                            Exit While
                        End If
                        x = x + 1
                    End While

                    If x < dtLines1.Rows.Count Then
                        Dim SQL1 As String = String.Format("Update apx_packStationDetail set PackedQty=PackedQty+1 where PickList={0} and StationID={1} and PiclistLine={2} and SKU={3}", FormatField(dtLines1.Rows(x)("PickList")), FormatField(Session("StationNumber")), FormatField(dtLines1.Rows(x)("PiclistLine")), FormatField(dtLines1.Rows(x)("SKU")))
                        DataInterface.RunSQL(SQL1)

                        SQL1 = String.Format("Insert into apx_packStationSerials (SerialNumber, StationID, PickList, PiclistLine, OrderID, OrderLine, SKU, UOM, ToLoad)" & _
                                             " VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8})", _
                                             FormatField(SerialResults.Rows(0)("serial")), FormatField(Session("StationNumber")), FormatField(Session("PickList")), FormatField(dtLines1.Rows(x)("PiclistLine")), _
                                             FormatField(dtLines1.Rows(x)("OrderID")), FormatField(dtLines1.Rows(x)("OrderLine")), FormatField(SerialResults.Rows(0)("SKU")), FormatField(SerialResults.Rows(0)("UOM")), _
                                             FormatField(SerialResults.Rows(0)("ToLoad")))
                        DataInterface.RunSQL(SQL1)
                    Else
                        MessageQue.Enqueue(t.Translate("Too much packed for SKU " + SerialResults.Rows(0)("SKU")))
                    End If
                Else
                    MessageQue.Enqueue(t.Translate("Serial Number already scanned to order " + PackSer.Rows(0)("OrderID") + " at station " + PackSer.Rows(0)("StationID")))
                End If
            Else
                Dim NonSerSKU As String = GetSKU()

                If String.IsNullOrEmpty(NonSerSKU) Then
                    NonSerSKU = Serial
                End If

                Dim IsNoSerialised As Boolean = False
                Dim oSKU As New WMS.Logic.SKU(dtLines1.Rows(0)("Consignee"), NonSerSKU)

                Dim SKUClass As String = ""
                If oSKU.SKUClass IsNot Nothing Then
                    SKUClass = oSKU.SKUClass.ClassName
                    If SKUClass <> "SERIAL" Then
                        IsNoSerialised = True
                    End If
                Else
                    IsNoSerialised = True
                End If

                If IsNoSerialised Then
                    Dim x As Integer = 0
                    While x < dtLines1.Rows.Count
                        If dtLines1.Rows(x)("SKU") = NonSerSKU And dtLines1.Rows(x)("PickedQty") > dtLines1.Rows(x)("PackedQty") Then
                            Exit While
                        End If
                        x = x + 1
                    End While

                    If x < dtLines1.Rows.Count Then
                        Dim SQL1 As String = String.Format("Update apx_packStationDetail set PackedQty=PackedQty+1 where PickList={0} and StationID={1} and PiclistLine={2} and SKU={3}", FormatField(dtLines1.Rows(x)("PickList")), FormatField(Session("StationNumber")), FormatField(dtLines1.Rows(x)("PiclistLine")), FormatField(dtLines1.Rows(x)("SKU")))
                        DataInterface.RunSQL(SQL1)
                    Else
                        MessageQue.Enqueue(t.Translate("Too much packed for SKU " + NonSerSKU))
                    End If
                Else
                    MessageQue.Enqueue(t.Translate("Scanned Serial Number " + Serial + " is not found  on the system"))
                End If
            End If
        Else
            MessageQue.Enqueue(t.Translate("No order at station"))
            Session.Remove("ToteID")
        End If

        DO1.Value("ScannedBS") = ""
    End Sub

    Private Sub ProcessEnteredQty(ByVal oAttributes As WMS.Logic.AttributesCollection)
        Dim attributesarray As New ArrayList
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If CDbl(DO1.Value("EnteredQty")) <> Session("QtyToConfirm") Then
            MessageQue.Enqueue(t.Translate("Invalid Qty Entered"))
        Else
            Dim SQL1 As String = String.Format("Update apx_packStationDetail set PackedQty={0} where PickList={1} and PiclistLine={2} and StationID={3}", FormatField(Session("QtyToConfirm")), FormatField(Session("PickList")), FormatField(Session("PLine")), FormatField(Session("StationNumber")))
            DataInterface.RunSQL(SQL1)
            Session.Remove("QtyToConfirm")
            Session.Remove("PLine")
            Session.Remove("SKU")
            Session.Remove("Consignee")
            ClearAttributes(Session("PickList"))
        End If

        DO1.Value("EnteredQty") = ""
    End Sub

    Private Sub ProcessScannedCarton()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim packlist As New WMS.Logic.PackingListHeader
        Try
            If WMS.Logic.PackingListHeader.Exists(DO1.Value("ScannedCarton")) Then
                Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Packing List Already exists"))
                DO1.Value("ScannedCarton") = ""
            Else
                packlist.PACKINGLISTID = DO1.Value("ScannedCarton")

                Dim dtLines As DataTable = GetPackItems(Session("StationNumber"), Session("PickList"), Session("ToteID"))

                Dim oCont1 As WMS.Logic.Container = CreateNewContainer(DO1.Value("ScannedCarton"))
                If dtLines.Rows.Count > 0 Then
                    For Each dr As DataRow In dtLines.Rows
                        Dim loadID As String = dr("ToLoad")
                        Dim ld As New WMS.Logic.Load(loadID)
                        ld.RemoveFromContainer()
                        'Dim PCK As New PicklistDetail(Session("PickList"), dr("PiclistLine"), True)
                        'PCK.SetContainer(DO1.Value("ScannedCarton"), WMS.Logic.Common.GetCurrentUser)
                        oCont1.Place(ld, WMS.Logic.Common.GetCurrentUser)
                    Next
                End If

                'oCont1.Status = WMS.Lib.Statuses.Container.PACKED
                'oCont1.Post(WMS.Logic.Common.GetCurrentUser)
                'Dim cntr As New WMS.Logic.Container(DO1.Value("ScannedCarton"), True)
                'cntr.Status = WMS.Lib.Statuses.Container.PACKED
                'cntr.Post(WMS.Logic.Common.GetCurrentUser)

                If dtLines.Rows.Count > 0 Then
                    For Each dr As DataRow In dtLines.Rows
                        Dim oPackList As WMS.Logic.PackingListHeader = GetPackingList(packlist, dr("ToLoad"))

                        oPackList.PackLoad(dr("ToLoad"), WMS.Logic.Common.GetCurrentUser)
                        UpdatePickDetailToPacked(DO1.Value("ScannedCarton"), Session("PickList"), dr("PiclistLine"))
                    Next
                End If

                If Session("PrintInvoice") IsNot Nothing Then
                    doPrintPackingList(packlist)
                End If

                doPrintShippingLabel(oCont1)

                UpdateStationHeaderPacked()

                'Session.Remove("ToteID")
                'Session.Remove("PickList")
                'Session.Remove("OrderID")
                DO1.Value("ScannedCarton") = ""
            End If
        Catch ex As Made4Net.Shared.M4NException
            Made4Net.Mobile.MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            DO1.Value("ScannedCarton") = ""
        Catch ex As Exception
            Made4Net.Mobile.MessageQue.Enqueue(ex.ToString())
            DO1.Value("ScannedCarton") = ""
        End Try
    End Sub

    Private Function CreateNewContainer(ByVal ContainerID As String) As WMS.Logic.Container
        Dim oCont1 As WMS.Logic.Container
        oCont1 = New WMS.Logic.Container
        oCont1.UsageType = "SHIPCONT"
        oCont1.ContainerId = ContainerID
        oCont1.Status = WMS.Lib.Statuses.Container.PACKED
        Try
            oCont1.Post(WMS.Logic.Common.GetCurrentUser)
        Catch ex As Made4Net.Shared.M4NException
            Made4Net.Mobile.MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return Nothing
        Catch ex As Exception
            Return Nothing
        End Try
        Return oCont1
    End Function

    Private Sub doPrintShippingLabel(ByVal oCont1 As WMS.Logic.Container)
        Dim prntr As LabelPrinter
        Try
            prntr = New LabelPrinter(Session("LabelPrinter"))
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As Exception

        End Try

        oCont1.PrintShipLabel(prntr.PrinterQName)
    End Sub

    Private Function GetPackingList(ByVal packlist As WMS.Logic.PackingListHeader, ByVal Load As String) As WMS.Logic.PackingListHeader
        Dim oPackList As WMS.Logic.PackingListHeader = packlist
        If oPackList.Lines.Count = 0 Then

            If Not WMS.Logic.Load.Exists(Load) Then
                Throw New Made4Net.Shared.M4NException(New Exception(), "Load does not exist", "Load does not exist")
            End If

            Dim sql As String = String.Format("select oh.consignee,oh.orderid,oh.targetcompany,oh.companytype from orderloads ol inner join outboundorheader oh on oh.orderid = ol.orderid and oh.consignee = ol.consignee where loadid = '{0}'", Load)
            Dim dt As New DataTable
            DataInterface.FillDataset(sql, dt)
            If dt.Rows.Count > 0 Then
                Dim oComp As New WMS.Logic.Company(dt.Rows(0)("consignee"), dt.Rows(0)("targetcompany"), dt.Rows(0)("companytype"))
                If WMS.Logic.PackingListHeader.Exists(oPackList.PACKINGLISTID) Then
                    oPackList.Update(oComp.CONSIGNEE, oComp.COMPANY, oComp.COMPANYTYPE, oComp.DEFAULTCONTACTID, 0, WMS.Logic.Common.GetCurrentUser)
                Else
                    oPackList.Create(oPackList.PACKINGLISTID, oComp.CONSIGNEE, oComp.COMPANY, oComp.COMPANYTYPE, oComp.DEFAULTCONTACTID, 0, WMS.Logic.Common.GetCurrentUser)
                End If
            Else
                Throw New Made4Net.Shared.M4NException(New Exception(), "Load does not belong to an order", "Load does not belong to an order")
            End If
        End If
        Return oPackList
    End Function

    Private Sub doPrintPackingList(ByVal packlist As WMS.Logic.PackingListHeader)
        Try
            Dim oPackList As WMS.Logic.PackingListHeader = packlist
            oPackList.PrintPackingList(Session("LaserPrinter"), Made4Net.Shared.Translation.Translator.CurrentLanguageID, WMS.Logic.Common.GetCurrentUser)
        Catch ex As Exception
            Made4Net.Mobile.MessageQue.Enqueue(ex.ToString())
            Return
        End Try
    End Sub

    Private Sub UpdateStationHeaderPacked()
        Dim SQL1 As String = String.Format("Update apx_packStationHeader set CartonID={0}, DocPrinted=1 where StationNumber={1}", FormatField(DO1.Value("ScannedCarton")), FormatField(Session("StationNumber")))
        DataInterface.RunSQL(SQL1)
    End Sub

    Private Sub UpdatePickDetailToPacked(ByVal ToContainer As String, ByVal Picklist As String, ByVal PickListLine As Integer)
        Dim SQL1 As String = String.Format("Update PICKDETAIL set ATTRIB1='PACKED', TOCONTAINER={0} where PICKLIST={1} AND PICKLISTLINE={2}", FormatField(ToContainer), FormatField(Picklist), FormatField(PickListLine))
        DataInterface.RunSQL(SQL1)
    End Sub

    Private Function GetSKU() As String
        Dim inpSku As String
        Dim SQL As String

        SQL = String.Format("SELECT SKU FROM vSKUCODE WHERE SKUCODE = '{0}'", DO1.Value("SKU"))

        inpSku = Made4Net.DataAccess.DataInterface.ExecuteScalar(SQL)

        Return inpSku
    End Function

    Private Sub ProcessScannedDoc()
        'DO1.Value("ScannedDoc")
    End Sub

End Class