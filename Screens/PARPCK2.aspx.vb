Imports WMS.Logic
Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

<CLSCompliant(False)> Public Class PARPCK2
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If WMS.Logic.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If

        If Not IsPostBack Then
            If Not WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PARALLELPICKING) Then
                Response.Redirect(MapVirtualPath("Screens/PARPCK1.aspx"))
            End If

            Dim pcks As ParallelPicking = Session("PARPCKPicklist")
            Dim pck As PickJob
            Try
                pck = pcks.GetNextPick()
                Session("PARPCKPicklist") = pcks
                Session("ParallelPCKPickJob") = pck
            Catch ex As Exception
            End Try

            setPick(pck)
        End If
    End Sub

    Private Sub setPick(ByVal pck As PickJob)
        If pck Is Nothing Then
            'Response.Redirect(MapVirtualPath("Screens/DELLBLPRNT.aspx?sourcescreen=PARPCK1"))
            Session("PCKPicklistActiveContainerID") = Nothing
            Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=" & Session("MobileSourceScreen")))
        Else
            Dim pcklist As Picklist = New Picklist(pck.picklist)
            'Dim relStrat As ReleaseStrategyDetail
            'relStrat = pcklist.getReleaseStrategy()
            'If Not relStrat Is Nothing Then
            '    Session("CONFTYPE") = relStrat.ConfirmationType
            '    If relStrat.ConfirmationType = WMS.Lib.Release.CONFIRMATIONTYPE.SKULOCATION Then
            '        DO1.setVisibility("CONFIRMSKU", True)
            '    Else
            '        DO1.setVisibility("CONFIRMSKU", False)
            '    End If
            'End If

            Session("PARPCKPicklistPickJob") = pck
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            DO1.Value("ParallelPickListId") = pck.parallelpicklistid
            DO1.Value("ParallelPickListSeq") = pck.parallelpicklistseq
            DO1.Value("Picklist") = pck.picklist
            Session("PCKPicklistID") = pck.picklist
            DO1.Value("LOADID") = pck.fromload
            DO1.Value("LOCATION") = pck.fromlocation
            DO1.Value("PICKMETHOD") = pcklist.PickMethod
            DO1.Value("SKU") = pck.sku
            DO1.Value("SKUDESC") = pck.skudesc
            DO1.Value("UOM") = pck.uom
            Dim sqluom As String = " SELECT DESCRIPTION FROM CODELISTDETAIL " & _
                          " WHERE CODELISTCODE = 'UOM' AND CODE = '" & pck.uom & "'"

            DO1.Value("UOMDesc") = Made4Net.DataAccess.DataInterface.ExecuteScalar(sqluom)
            DO1.Value("UOMUNITS") = pck.uomunits
            'Label Printing
            If MobileUtils.LoggedInMHEID <> String.Empty And MobileUtils.GetMHEDefaultPrinter <> String.Empty Then
                DO1.setVisibility("PRINTER", False)
            Else
                DO1.setVisibility("PRINTER", True)
            End If
            DO1.setVisibility("Picklist", False)
            'DO1.setVisibility("ParallelPickListSeq", False)

            Dim oSku As New WMS.Logic.SKU(pck.consingee, pck.sku)
            ClearAttributes(pck.parallelpicklistid)
            If oSku.SKUClass Is Nothing Then
                setAttributeFieldsVisibility(pck.picklist, Nothing)
            Else
                setAttributeFieldsVisibility(pck.picklist, oSku.SKUClass.LoadAttributes)
            End If

            SetContainerID(pcklist)
            If Not pcklist.ShouldPrintShipLabelOnPickLineCompleted Then
                DO1.setVisibility("PRINTER", False)
            End If
        End If
    End Sub

    Private Sub SetContainerID(ByVal pcklist As Picklist)
        If Session("PCKPicklistActiveContainerID") Is Nothing Or (pcklist.ActiveContainer <> Session("PCKPicklistActiveContainerID") AndAlso WMS.Logic.Container.Exists(Session("PCKPicklistActiveContainerID"))) Then
            If Not pcklist Is Nothing Then
                If Not pcklist.PickType = WMS.Lib.PICKTYPE.FULLPICK Then
                    DO1.setVisibility("ContainerType", False)
                    DO1.setVisibility("ContainerTypeDesc", True)
                    DO1.setVisibility("ContainerID", True)
                    Dim contid As String = pcklist.ActiveContainer
                    If contid.Trim = "" Then
                        contid = Made4Net.Shared.Util.getNextCounter("CONTAINER")
                    End If
                    DO1.Value("ContainerId") = contid
                    Session("PCKPicklistActiveContainerID") = contid
                End If
            End If
        Else
            DO1.setVisibility("ContainerType", False)
            DO1.setVisibility("ContainerTypeDesc", True)
            DO1.setVisibility("ContainerID", True)
            DO1.Value("ContainerId") = Session("PCKPicklistActiveContainerID")
            Dim sqltype As String = " select containerdesc from handelingunittype " & _
                          " WHERE container = '" & Session("PCKPicklistActiveContainerID") & "'"
            DO1.Value("ContainerTypeDesc") = Made4Net.DataAccess.DataInterface.ExecuteScalar(sqltype)
        End If
    End Sub

    Private Sub doNext()
        Dim pck As PickJob
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim pcks As ParallelPicking = New ParallelPicking(DO1.Value("ParallelPickListId"))
        Session("PARPCKPicklist") = pcks
        pck = Session("PARPCKPicklistPickJob")
        If pck Is Nothing Then
            Session.Remove("PCKPicklistActiveContainerID")
            Response.Redirect(MapVirtualPath("Screens/DEL.aspx"))
        End If

        Try
            Dim sku As New sku(pck.consingee, pck.sku)
            pck.pickedqty = sku.ConvertToUnits(pck.uom) * Convert.ToDecimal(DO1.Value("UOMUNITS"))
            sku = Nothing
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate("Error cannot get units"))
            Return
        End Try

        Try
            Dim pcklsts As ParallelPicking = Session("PARPCKPicklist")
            pck.container = Session("PCKPicklistActiveContainerID")
            pck.oncontainer = pcklsts.ToContainer
            If MobileUtils.LoggedInMHEID.Trim <> String.Empty Then
                pck.LabelPrinterName = MobileUtils.GetMHEDefaultPrinter
            End If
            'ExtractAttributes()
            MobileUtils.ExtractPickingAttributes(pck, DO1)
            If (pck.pickedqty > 0) Then
                pck.TaskConfirmation.ConfirmationValues = MobileUtils.ExtractConfirmationValues(pck.TaskConfirmation.ConfirmationType, DO1)
            Else
                pck.TaskConfirmation = New TaskConfirmationNone()
            End If
            Session("PARPCKPicklist") = pcklsts
            pck = pcks.Pick(pck, WMS.Logic.GetCurrentUser)
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            ClearAttributes(pck.parallelpicklistid)
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            SetContainerID(New Picklist(pck.picklist))
            Return
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate(ex.Message))
            ClearAttributes(pck.parallelpicklistid)
            clearConfirmationFields(pck.TaskConfirmation.ConfirmationType)
            SetContainerID(New Picklist(pck.picklist))
            Return
        End Try

        'DO1.Value("CONFIRMSKU") = ""
        'DO1.Value("CONFIRM") = ""
        setPick(pck)
    End Sub

    Private Sub doBack()
        Session("PCKPicklistActiveContainerID") = Nothing
        Response.Redirect(MapVirtualPath("Screens/PARPCK1.aspx"))
    End Sub

    Private Sub doMenu()
        Session("PCKPicklistActiveContainerID") = Nothing
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        If WMS.Logic.TaskManager.isAssigned(UserId, WMS.Lib.TASKTYPE.PARALLELPICKING) Then
            Dim tm As New WMS.Logic.TaskManager(UserId, WMS.Lib.TASKTYPE.PARALLELPICKING)
            tm.ExitTask()
        End If
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doCloseContainer()
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Not Session("PCKPicklistActiveContainerID") Is Nothing Then
            Try
                'If Not WMS.Logic.Container.Exists(Session("PCKPicklistActiveContainerID")) Then
                '    MessageQue.Enqueue(trans.Translate("Cannot Close Container. Container does not exist"))
                '    Return
                'Else
                '    Dim oCont As New WMS.Logic.Container(Session("PCKPicklistActiveContainerID"), True)
                '    If oCont.Loads.Count = 0 Then
                '        MessageQue.Enqueue(trans.Translate("Cannot Close Container. No loads are placed on the container"))
                '        Return
                '    End If
                'End If
                Dim pcklist As Picklist = New Picklist(Session("PCKPicklistID"))
                Dim relStrat As ReleaseStrategyDetail
                relStrat = pcklist.getReleaseStrategy()
                'Dim pck As PickJob = Session("PARPCKPicklistPickJob")
                Dim sSrcScreen As String
                If Not Session("MobileSourceScreen") Is Nothing Then
                    sSrcScreen = Session("MobileSourceScreen")
                Else
                    sSrcScreen = "PARPCK2"
                End If

                If Not relStrat Is Nothing Then
                    If relStrat.DeliverContainerOnClosing Then
                        'Should deliver the container now
                        pcklist.CloseContainer(Session("PCKPicklistActiveContainerID"), True, WMS.Logic.GetCurrentUser)
                        Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=" & sSrcScreen))
                    Else
                        'Should close the container - go back to PCK to open a new one
                        pcklist.CloseContainer(Session("PCKPicklistActiveContainerID"), False, WMS.Logic.GetCurrentUser)
                        Session.Remove("PCKPicklistActiveContainerID")
                        Response.Redirect(MapVirtualPath("screens/PARPCK2.aspx"))
                    End If
                End If
            Catch ex As Threading.ThreadAbortException
            Catch ex As Made4Net.Shared.M4NException
                MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
                setPick(Session("PARPCKPicklistPickJob"))
                Return
            Catch ex As Exception
                MessageQue.Enqueue(ex.Message)
                setPick(Session("PARPCKPicklistPickJob"))
                Return
            End Try
        Else
            MessageQue.Enqueue(trans.Translate("Cannot Close Cotnainer - Container is blank"))
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("ParallelPickListId")
        DO1.AddLabelLine("ParallelPickListSeq")
        DO1.AddLabelLine("Picklist")
        DO1.AddLabelLine("PickMethod")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOADID")
        DO1.AddLabelLine("UOM")
        DO1.AddLabelLine("UOMDesc")
        DO1.AddLabelLine("LOCATION")
        DO1.AddSpacer()
        DO1.AddTextboxLine("ContainerID")
        DO1.AddLabelLine("ContainerType")
        DO1.AddLabelLine("ContainerTypeDesc")
        Dim pck As PickJob = Session("ParallelPCKPickJob")
        'If Not pck Is Nothing Then
        '    If Not pck.oAttributeCollection Is Nothing Then
        '        For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
        '            DO1.AddTextboxLine(pck.oAttributeCollection.Keys(idx))
        '        Next
        '    End If
        'End If
        'DO1.AddTextboxLine("CONFIRM")
        'DO1.AddTextboxLine("CONFIRMSKU")
        For Each attrName As String In getAttributesForAllPickList(pck.parallelpicklistid)
            DO1.AddTextboxLine(String.Format("Attr_{0}", attrName), attrName)
        Next
        addConfirmationFields(pck.TaskConfirmation.ConfirmationType)
        DO1.AddTextboxLine("UOMUNITS")
        DO1.AddTextboxLine("PRINTER")
        DO1.setVisibility("ContainerType", False)
        DO1.setVisibility("ContainerTypeDesc", False)
        DO1.setVisibility("ContainerID", False)
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "back"
                doBack()
            Case "closecontainer"
                doCloseContainer()
            Case "menu"
                doMenu()
        End Select
    End Sub

    'Private Function ExtractAttributes() As AttributesCollection
    '    Dim pck As PickJob = Session("ParallelPCKPickJob")
    '    Dim Val As Object
    '    If Not pck Is Nothing Then
    '        If Not pck.oAttributeCollection Is Nothing Then
    '            For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
    '                Val = DO1.Value(pck.oAttributeCollection.Keys(idx))
    '                If Val = "" Then Val = Nothing
    '                pck.oAttributeCollection(idx) = Val
    '            Next
    '            Return pck.oAttributeCollection
    '        End If
    '    End If
    '    Return Nothing
    'End Function

    Private Sub ClearAttributes(ByVal pParallelPickList As String)
        'Dim pck As PickJob = Session("ParallelPCKPickJob")
        'If Not pck Is Nothing Then
        '    If Not pck.oAttributeCollection Is Nothing Then
        '        For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
        '            DO1.Value(pck.oAttributeCollection.Keys(idx)) = ""
        '        Next
        '    End If
        'End If
        For Each attStr As String In getAttributesForAllPickList(pParallelPickList)
            'DO1.Value(attStr) = ""
            DO1.Value(String.Format("Attr_{0}", attStr)) = ""
        Next
    End Sub

    Private Sub addConfirmationFields(ByVal pConfirmType As String)
        Select Case pConfirmType
            Case WMS.Lib.CONFIRMATIONTYPE.LOAD
                DO1.AddTextboxLine("CONFIRM(LOAD)")
            Case WMS.Lib.CONFIRMATIONTYPE.LOCATION
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
            Case WMS.Lib.CONFIRMATIONTYPE.NONE
            Case WMS.Lib.CONFIRMATIONTYPE.SKU
                DO1.AddTextboxLine("CONFIRM(SKU)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATION
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATIONUOM
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(LOCATION)")
                DO1.AddTextboxLine("CONFIRM(UOM)")
            Case WMS.Lib.CONFIRMATIONTYPE.SKUUOM
                DO1.AddTextboxLine("CONFIRM(SKU)")
                DO1.AddTextboxLine("CONFIRM(UOM)")
            Case WMS.Lib.CONFIRMATIONTYPE.UPC
                DO1.AddTextboxLine("CONFIRM(UPC)")
        End Select
    End Sub

    Private Sub clearConfirmationFields(ByVal pConfirmType As String)
        Select Case pConfirmType
            Case WMS.Lib.CONFIRMATIONTYPE.LOAD
                DO1.Value("CONFIRM(LOAD)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.LOCATION
                DO1.Value("CONFIRM(LOCATION)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.NONE
            Case WMS.Lib.CONFIRMATIONTYPE.SKU
                DO1.Value("CONFIRM(SKU)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATION
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(LOCATION)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKULOCATIONUOM
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(LOCATION)") = ""
                DO1.Value("CONFIRM(UOM)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.SKUUOM
                DO1.Value("CONFIRM(SKU)") = ""
                DO1.Value("CONFIRM(UOM)") = ""
            Case WMS.Lib.CONFIRMATIONTYPE.UPC
                DO1.Value("CONFIRM(UPC)") = ""
        End Select
    End Sub

    Private Function getAttributesForAllPickList(ByVal pParallelPickId As String) As System.Collections.Generic.List(Of String)
        If ViewState()("PickListAttributes") Is Nothing Then
            Dim list As New System.Collections.Generic.List(Of String)
            Dim sql As String
            Dim dt As New DataTable()
            sql = String.Format("select distinct attributename from parallelpickdetail inner join pickdetail pd on parallelpickdetail.picklist = pd.picklist inner join sku on pd.consignee = sku.consignee and pd.sku = sku.sku inner join skuclsloadatt att on sku.classname = att.classname where parallelpickdetail.parallelpickid={0} and pd.status in ({1},{2}) and att.PICKINGCAPTURE <> {3}", _
            Made4Net.Shared.FormatField(pParallelPickId), Made4Net.Shared.FormatField(WMS.Lib.Statuses.Picklist.PARTPICKED), _
            Made4Net.Shared.FormatField(WMS.Lib.Statuses.Picklist.RELEASED), Made4Net.Shared.FormatField(WMS.Logic.SkuClassLoadAttribute.CaptureType.NoCapture))
            Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)

            For Each dr As DataRow In dt.Rows
                list.Add(dr("ATTRIBUTENAME").ToString())
            Next
            ViewState("PickListAttributes") = list
        End If
        Return ViewState("PickListAttributes")
    End Function

    Private Sub setAttributeFieldsVisibility(ByVal pPickListID As String, ByVal pSkuClassLoadAttributeColl As SkuClassLoadAttributeCollection)
        'Dim key As String = String.Format("{0}~{1}", pConsignee, pSKU)
        For Each attrName As String In getAttributesForAllPickList(pPickListID)
            DO1.setVisibility(String.Format("Attr_{0}", attrName), False)
            If pSkuClassLoadAttributeColl Is Nothing Then Continue For
            For Each skuClsLoadAtt As SkuClassLoadAttribute In pSkuClassLoadAttributeColl
                If skuClsLoadAtt.Name.Equals(attrName, StringComparison.OrdinalIgnoreCase) Then
                    If skuClsLoadAtt.CaptureAtPicking <> SkuClassLoadAttribute.CaptureType.NoCapture Then
                        DO1.setVisibility(String.Format("Attr_{0}", attrName), True)
                        Continue For
                    End If
                End If
            Next
        Next
    End Sub

End Class
