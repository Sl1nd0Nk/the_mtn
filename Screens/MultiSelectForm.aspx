<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MultiSelectForm.aspx.vb" Inherits="WMS.MobileWebApp.MultiSelectForm" %>
<%@ Register TagPrefix="cc1" Namespace="WMS.MobileWebApp.WebCtrls" Assembly="WMS.MobileWebApp" %>
<%@ Register TagPrefix="cc3" Namespace="Made4Net.WebControls" Assembly="Made4Net.WebControls" %>
<%@ Register TagPrefix="cc2" Namespace="Made4Net.Mobile.WebCtrls" Assembly="Made4Net.Mobile" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>

	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form2" method="post" runat="server">
			<DIV align="center"><cc1:screen id="Screen1" runat="server" title="Select Value" ScreenId="MULSEL"></cc1:screen></DIV>
            <div align="center">
                <asp:ListBox ID="ValueList" runat="server" Rows="15" TabIndex=0></asp:ListBox>
                <br />
                <asp:button ID="SelectValue" text = "Select Value" runat ="server" TabIndex=1/>
                <asp:button ID="Up" text = "Up" runat ="server" TabIndex=2/>
                <asp:button ID="Down" text = "Down" runat ="server" TabIndex=3/>
            </div>
		</form>
	</body>

</html>
