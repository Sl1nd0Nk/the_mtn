Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports WMS.MobileWebApp.apexInbound
Imports WMS.MobileWebApp.apexSerial
Imports System.Collections.Generic

<CLSCompliant(False)> Public Class CUSTRET
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Protected Structure DataStruct
        Public ReceiptLine As Integer
        Public SKU As String
        Public ProdDesc As String
        Public ExpectedQty As Integer
        Public collectionType As String
        Public QtyReceived As Integer
        Public UOM As String
        Public Consignee As String

        Public Sub New(ByVal ReceiptLine As Integer, ByVal SKU As String, ByVal ProdDesc As String, ByVal ExpectedQty As Integer,
                       ByVal collectionType As String, ByVal QtyReceived As Integer, ByVal UOM As String, ByVal Consignee As String)
            Me.ReceiptLine = ReceiptLine
            Me.SKU = SKU
            Me.ProdDesc = ProdDesc
            Me.ExpectedQty = ExpectedQty
            Me.collectionType = collectionType
            Me.QtyReceived = QtyReceived
            Me.UOM = UOM
            Me.Consignee = Consignee
        End Sub

        Public Sub AddToQtyReceived(ByVal QtyToAdd As Integer)
            QtyReceived += 1
        End Sub
    End Structure

    'Protected TableRow As New List(Of DataStruct)

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If Session("UserAssignedPrinter") Is Nothing Then
                Dim Printer As String = GetUserPrinter(Logic.GetCurrentUser())

                If Printer = "" Then
                    Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

                    MessageQue.Enqueue(t.Translate("User " & Logic.GetCurrentUser() & " is not assigned to a label printer"))
                Else
                    Session.Add("UserAssignedPrinter", Printer)
                End If
            End If
        End If

        'Set all controller conponents visibility to false
        SetInitialState()

        If Not IsPostBack Then
            Dim DT As New DataTable

            'Are we busy with anytthing
            If UserCurrentlyBusyWithTote(Logic.GetCurrentUser, DT) Then
                'Currently busy processing a tote/container
                LoadContainerDetails(DT)
            End If

            WhereToNext()
        End If
    End Sub

    Private Function GetUserPrinter(ByVal User As String) As String
        Dim SQL As String = String.Format("SELECT * FROM USERPROFILE WHERE USERID={0}", FormatField(User))
        Dim DtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, DtLines)

        If DtLines.Rows.Count > 0 Then
            If IsDBNull(DtLines.Rows(0)("DEFAULTPRINTER")) Then
                Return ""
            Else
                Return DtLines.Rows(0)("DEFAULTPRINTER")
            End If
        Else
            Return ""
        End If
    End Function

    Private Function UserCurrentlyBusyWithTote(ByVal user As String, ByRef DT As DataTable) As Boolean
        Dim sql As String = String.Format("select * from apx_serial_returns where inProgressUser={0}", FormatField(user))
        Dim localDt As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, localDt)

        If localDt.Rows.Count > 0 Then
            DT = localDt
            Return True
        End If

        Return False
    End Function

    Private Sub LoadContainerDetails(ByVal DT As DataTable)
        DO1.Value("TOTE") = DT.Rows(0)("containerID")
        ProcessScannedTote()
        DO1.Value("SKU") = DT.Rows(0)("SKU")
        ProcessScannedSku()
        DO1.Value("WEIGHT") = DT.Rows(0)("weight")
        ProcessEnteredWeight()
        CheckIfSkuAllReceived()
    End Sub

    Private Sub SetInitialState()
        DO1.setVisibility("TOTE", False)
        DO1.setVisibility("CONT_ITEMS", False)
        DO1.setVisibility("SKU", False)
        DO1.setVisibility("SERIAL", False)
        DO1.setVisibility("CONTAINER", False)
        DO1.setVisibility("Receipt", False)
        DO1.setVisibility("SkuInProg", False)
        DO1.setVisibility("MSG", False)
        DO1.setVisibility("WEIGHT", False)
        DO1.setVisibility("RET_WEIGHT", False)
        DO1.setVisibility("SUG_RETLOC", False)
        DO1.setVisibility("RET_LOC", False)
    End Sub

    Private Sub InitButtons()
        DO1.Button(0).Text = "Next"
        DO1.Button(0).Visible = True
        DO1.Button(1).Visible = False
        DO1.Button(2).Visible = True
        DO1.Button(3).Visible = False
    End Sub

    Private Sub WhereToNext()
        SetInitialState()
        InitButtons()

        If Not Session("ReturnContainer") Is Nothing Then
            DO1.Value("CONTAINER") = Session("ReturnContainer")
            DO1.setVisibility("CONTAINER", True)

            If Not Session("ReturnRC") Is Nothing Then
                DO1.Value("Receipt") = Session("ReturnRC")
                DO1.setVisibility("Receipt", True)
            End If

            If Not Session("ReturnSKU") Is Nothing Then
                DO1.Value("SkuInProg") = Session("ReturnSKU")
                DO1.setVisibility("SkuInProg", True)
                DO1.Button(1).Visible = True

                If Not Session("ReturnWeight") Is Nothing Then
                    DO1.setVisibility("RET_WEIGHT", True)
                    DO1.Value("RET_WEIGHT") = Session("ReturnWeight")

                    If Not Session("ShowScannedSerials") Is Nothing Then
                        DO1.Button(0).Text = "Close"
                        DO1.Button(1).Visible = False
                        DO1.Button(2).Visible = False
                    Else
                        DO1.Button(3).Visible = True
                        If Not Session("ReadyToCreate") Is Nothing Then
                            DO1.Value("MSG") = "Received all serials for this Product, proceed to create load"
                            DO1.setVisibility("MSG", True)
                            DO1.Value("SUG_RETLOC") = Session("ReadyToCreate")
                            DO1.setVisibility("SUG_RETLOC", True)
                            DO1.setVisibility("RET_LOC", True)
                            DO1.FocusField = "RET_LOC"
                            DO1.Button(0).Text = "CreateLoad"
                        Else
                            DO1.setVisibility("SERIAL", True)
                            DO1.FocusField = "SERIAL"
                            DO1.Value("SERIAL") = ""
                        End If
                    End If
                Else
                    DO1.setVisibility("WEIGHT", True)
                    DO1.FocusField = "WEIGHT"
                    DO1.Value("WEIGHT") = ""
                End If
            Else
                DO1.setVisibility("SKU", True)
                DO1.FocusField = "SKU"
                DO1.Value("SKU") = ""
            End If

            If Not Session("ShowScannedSerials") Is Nothing Then
                DO1.Value("CONT_ITEMS") = GetScannedSerialsDisplayItems()
            Else
                DO1.Value("CONT_ITEMS") = GetDisplayItems()
            End If
            DO1.setVisibility("CONT_ITEMS", True)
        Else
            DO1.setVisibility("TOTE", True)
            DO1.FocusField = "TOTE"
            DO1.Value("TOTE") = ""
        End If
    End Sub

    Private Function GetScannedSerialsDisplayItems() As String
        Dim StartTable As String = "<table>"
        Dim EndTable As String = "</table>"
        Dim StartRow As String = "<tr>"
        Dim EndRow As String = "</tr>"

        Dim SQL As String = String.Format("SELECT serial FROM apx_serial_returns WHERE containerID={0} And received={1} And SKU={2}", FormatField(Session("ReturnContainer")), FormatField(True), FormatField(Session("ReturnSKU")))
        Dim DtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, DtLines)

        Dim x As Integer = 1
        If DtLines.Rows.Count > 0 Then
            For Each dr1 As DataRow In DtLines.Rows
                If x > 3 Then
                    x = 1
                    StartTable = StartTable & EndRow
                End If

                If x = 1 Then
                    StartTable = StartTable & StartRow
                End If

                StartTable = StartTable & "<td>" & dr1("serial") & "</td><td>&nbsp&nbsp</td>"

                x += 1
            Next
        End If

        Return StartTable & EndTable
    End Function

    Private Function GetDisplayItems() As String
        Dim TableRow As List(Of DataStruct) = Session("RawDisplay")
        Dim StartTable As String = "<table border='1'>"
            Dim EndTable As String = "</table>"
        Dim StartRow As String = "<tr>"
        Dim StartTHRow As String = "<tr bgcolor='#DCDCDC'>"
        Dim EndRow As String = "</tr>"

        StartTable = StartTable & StartTHRow & "<th nowrap>SKU</th><th nowrap>PRODUCT DESCRIPTION</th><th nowrap>EXPECTED QTY</th><th nowrap>COLLECTIONTYPE</th><th nowrap>QTY RECEIVED</th>" & EndRow

        Dim TableCollection As ICollection = TableRow
        For Each s As DataStruct In TableCollection
            If s.QtyReceived = s.ExpectedQty Then
                StartRow = "<tr bgcolor='#7CFC00'>"
            Else
                StartRow = "<tr>"
            End If

            If Not Session("ReturnSKU") Is Nothing Then
                If s.SKU = Session("ReturnSKU") Then
                    StartTable = StartTable & StartRow & "<td nowrap>" & s.SKU & "</td><td nowrap>" & s.ProdDesc & "</td><td nowrap>" & CStr(s.ExpectedQty) & "</td><td nowrap>" & s.collectionType & "</td><td nowrap>" & CStr(s.QtyReceived) & "</td>" & EndRow
                End If
            Else
                StartTable = StartTable & StartRow & "<td nowrap>" & s.SKU & "</td><td nowrap>" & s.ProdDesc & "</td><td nowrap>" & CStr(s.ExpectedQty) & "</td><td nowrap>" & s.collectionType & "</td><td nowrap>" & CStr(s.QtyReceived) & "</td>" & EndRow
            End If
        Next

        Return StartTable & EndTable
    End Function


    Private Sub ProcessRetrievedContainer(ByVal dtLines As DataTable)
        Dim TableRow As New List(Of DataStruct)
        For Each dr As DataRow In dtLines.Rows
            Dim sku As String = dr("SKU")
            Dim SKUDescrip As String = GetSKUDescription(sku)

            Dim ExpectedQty As Integer = 0
            Dim QtyReceived As Integer = 0
            For Each dr1 As DataRow In dtLines.Rows
                If (dr1("SKU") = sku) Then
                    ExpectedQty = ExpectedQty + dr1("qtyExpected")
                    Dim received As Boolean = dr("received")
                    If received Then
                        QtyReceived = QtyReceived + 1
                    End If
                End If
            Next

            Dim line As New DataStruct(dr("receiptline"), sku, SKUDescrip, ExpectedQty, dr("CollectionType"), QtyReceived, dr("UOM"), dr("consignee"))

            If TableRow.IndexOf(line) < 0 Then
                TableRow.Add(line)
            End If
        Next

        Session.Add("RawDisplay", TableRow)
    End Sub

    Private Sub ProcessScannedTote()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim dtLines As DataTable = APXINBOUND_FindReturnByContainer(DO1.Value("TOTE"))

        If dtLines.Rows.Count = 0 Then
            MessageQue.Enqueue(t.Translate("Scanned Container Not Found"))
            DO1.Value("TOTE") = ""
            DO1.FocusField = "TOTE"
        Else
            Dim InUseBy As String
            For Each dr1 As DataRow In dtLines.Rows
                If Not IsDBNull(dr1("inProgressUser")) Then
                    If dr1("inProgressUser") <> Logic.GetCurrentUser Then
                        InUseBy = dr1("inProgressUser")
                        Exit For
                    End If
                End If
            Next

            If String.IsNullOrEmpty(InUseBy) Then
                ProcessRetrievedContainer(dtLines)
                Session.Add("ReturnContainer", DO1.Value("TOTE"))
                Session.Add("ReturnRC", dtLines.Rows(0)("receipt"))
            Else
                MessageQue.Enqueue(t.Translate("Scanned Container is currently being processed by user" & InUseBy))
                DO1.FocusField = "TOTE"
            End If

            DO1.Value("TOTE") = ""
        End If
    End Sub

    Private Function GetSKU(ByVal sku As String) As String
        Dim inpSku As String
        Dim SQL As String
        Dim trimmedSku As String = sku.Trim()

        SQL = String.Format("SELECT SKU FROM vSKUCODE WHERE SKUCODE = {0}", FormatField(trimmedSku))

        inpSku = Made4Net.DataAccess.DataInterface.ExecuteScalar(SQL)

        Return inpSku
    End Function

    Private Sub ProcessScannedSku()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim QuerySku As String = GetSKU(DO1.Value("SKU"))

        If String.IsNullOrEmpty(QuerySku) Then
            MessageQue.Enqueue(t.Translate("Scanned product does not exist on the system"))
            Return
        End If

        If Not Session("RawDisplay") Is Nothing Then
            Dim TableRow As List(Of DataStruct) = Session("RawDisplay")

            Dim x As Integer = 0
            Dim AllReceivedProd As String
            Dim TableCollection As ICollection = TableRow
            For Each s As DataStruct In TableCollection
                If s.SKU = QuerySku Then
                    If s.QtyReceived = s.ExpectedQty Then
                        AllReceivedProd = s.ProdDesc
                        If Session("ReturnSKU") Is Nothing Then
                            Session.Add("ReturnSKU", s.SKU)
                            Session.Add("ReturnRCLine", s.ReceiptLine)
                            Session.Add("ReturnUOM", s.UOM)
                            Session.Add("Consignee", s.Consignee)
                            Session.Add("TableIndex", x)
                            Session.Add("ReturnColType", s.collectionType)
                        End If
                    Else
                        Session.Add("ReturnSKU", s.SKU)
                        Session.Add("ReturnRCLine", s.ReceiptLine)
                        Session.Add("ReturnUOM", s.UOM)
                        Session.Add("Consignee", s.Consignee)
                        Session.Add("TableIndex", x)
                        Session.Add("ReturnColType", s.collectionType)
                    End If
                    Exit For
                End If

                x += 1
            Next

            If Not Session("ReturnSKU") Is Nothing Then
                DO1.Value("SKU") = ""
            Else
                If Not String.IsNullOrEmpty(AllReceivedProd) Then
                    MessageQue.Enqueue(t.Translate("All Serial Numbers have been received for Product " & AllReceivedProd))
                Else
                    MessageQue.Enqueue(t.Translate("Scanned Product Is Not Part Of This Container"))
                    DO1.Value("SKU") = ""
                    DO1.FocusField = "SKU"
                End If
            End If
        Else
            'Error.....
        End If
    End Sub

    Private Sub ProcessEnteredWeight()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim WeightStr As String = DO1.Value("WEIGHT")
        Dim Weight As Double = 0.0

        Weight = CDbl(WeightStr)
        If Weight = 0 Then
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Product weight cannot be zero"))
            DO1.Value("WEIGHT") = ""
            DO1.FocusField = "WEIGHT"
            Return
        End If

        Dim Weightsql As String = String.Format("select * from skuuom where consignee={0} and uom={2} and sku={1}", FormatField(Session("Consignee")),
                                                                                                                    FormatField(Session("ReturnSKU")),
                                                                                                                    FormatField(Session("ReturnUOM")))
        Dim dtWeight As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(Weightsql, dtWeight)
        If dtWeight.Rows.Count = 0 Then
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate("No weight has been configured for this Product in master data"))
            DO1.Value("WEIGHT") = ""
            DO1.FocusField = "WEIGHT"
            Return
        Else
            Dim matchingWeight As Double = dtWeight.Rows(0)("GROSSWEIGHT")

            'Need to expand on this as it currently does not use tolerance params
            If APXINBOUND_CheckWeightInTolerance(Weight, matchingWeight) = False Then
                Made4Net.Mobile.MessageQue.Enqueue(t.Translate("Weight failed! Not within tolerance"))
                DO1.Value("WEIGHT") = ""
                DO1.FocusField = "WEIGHT"
                Return
            End If
        End If

        DO1.Value("WEIGHT") = ""
        Session.Add("ReturnWeight", Weight)
    End Sub

    Private Sub ProcessScannedSerial()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim QuerySku As String = Session("ReturnSKU")

        Dim dtLines As DataTable = APXINBOUND_FindReturnByContainerSKUSerial(DO1.Value("SERIAL"), QuerySku, Session("ReturnContainer"))

        If dtLines.Rows.Count = 0 Then
            MessageQue.Enqueue(t.Translate("Scanned Serial Number Is Not Part Of This Container"))
            DO1.Value("SERIAL") = ""
            DO1.FocusField = "SERIAL"
        ElseIf dtLines.Rows(0)("SKU") <> Session("ReturnSKU") Then
            MessageQue.Enqueue(t.Translate("Scanned Serial Number Is Not For This Product"))
            DO1.Value("SERIAL") = ""
            DO1.FocusField = "SERIAL"
        ElseIf dtLines.Rows(0)("received") Then
            MessageQue.Enqueue(t.Translate("Serial number is already scanned"))
            DO1.Value("SERIAL") = ""
            DO1.FocusField = "SERIAL"
        Else
            Dim strUnverSerTransIns As String = String.Format("UPDATE [apx_serial_returns] Set received={0}, inProgressUser={1}, weight={2} where serial={3}", FormatField(True), FormatField(Logic.GetCurrentUser), FormatField(Session("ReturnWeight")), FormatField(DO1.Value("SERIAL")))
            DataInterface.RunSQL(strUnverSerTransIns)

            Dim TableRow As List(Of DataStruct) = Session("RawDisplay")
            Dim sss As DataStruct = TableRow.Item(Session("TableIndex"))
            sss.QtyReceived += 1
            TableRow.Item(Session("TableIndex")) = sss
            Session("RawDisplay") = TableRow

            DO1.Value("SERIAL") = ""

            CheckIfSkuAllReceived()
        End If
    End Sub

    Private Function FindASuitableLocForLoad(ByVal Sku As String, ByVal Hold As String, ByVal Release As String) As String
        Dim sql As String = String.Format("select distinct ld.LOCATION from vLOADATTRIBUTESDESK ld join location loc on ld.LOCATION = loc.LOCATION and loc.WAREHOUSEAREA = '311-RT' AND loc.AISLE is not null AND loc.AISLE <> '' AND loc.BAY is not null AND loc.BAY <> '' where ld.SKU = {0} AND ld.Status in ('{1}', '{2}')", FormatField(Sku), Hold, Release)
        Dim localDt As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, localDt)
        Dim SuitableLoc As String = ""
        Dim SuitableIndex As Integer = -1
        Dim x As Integer = 0
        If localDt.Rows.Count > 0 Then
            For Each Dr As DataRow In localDt.Rows
                Dim strLoc As String = Dr("LOCATION")
                Dim Loc As Location = New Location(strLoc)
                Dim LocLoadCount As Integer = Loc.getNumberOfLoads()

                If SuitableIndex >= 0 Then
                    Dim CompLoc As Location = New Location(localDt.Rows(SuitableIndex)("LOCATION"))
                    Dim CompLoadCount As Integer = CompLoc.getNumberOfLoads()
                    If LocLoadCount < CompLoadCount Then
                        SuitableIndex = x
                    End If
                Else
                    SuitableIndex = x
                End If

                x += 1
            Next

            SuitableLoc = localDt.Rows(SuitableIndex)("LOCATION")
        End If

        Return SuitableLoc
    End Function

    Private Sub GetHoldReleaseCodes(ByVal CollectionType As String, ByRef Hold As String, ByRef Release As String)
        Dim SQL As String = String.Format("SELECT * FROM apx_returns_collection_types WHERE ShortDesc like '%" + CollectionType + "%'")
        Dim DtLinees As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, DtLinees, False, Nothing)

        If DtLinees.Rows.Count > 0 Then
            Hold = CStr(DtLinees.Rows(0)("HoldCode"))
            Release = CStr(DtLinees.Rows(0)("ReleaseCode"))
        End If
    End Sub

    Private Sub CheckIfSkuAllReceived()
        Dim TableRow As List(Of DataStruct) = Session("RawDisplay")
        Dim sss As DataStruct = TableRow.Item(Session("TableIndex"))

        If sss.QtyReceived = sss.ExpectedQty Then
            Dim ThisSku As String = sss.SKU
            Dim Hold As String = "HELD"
            Dim Release As String = "AVAILABLE"

            GetHoldReleaseCodes(sss.collectionType, Hold, Release)

            Dim SuggestedLoc As String = FindASuitableLocForLoad(ThisSku, Hold, Release)
            Session.Add("ReadyToCreate", SuggestedLoc)
        Else
            DO1.FocusField = "SERIAL"
        End If
    End Sub

    Private Sub doFindContainer()
        If Not String.IsNullOrEmpty(DO1.Value("TOTE")) Then
            ProcessScannedTote()
        ElseIf Not String.IsNullOrEmpty(DO1.Value("SKU")) Then
            ProcessScannedSku()
        ElseIf Not String.IsNullOrEmpty(DO1.Value("WEIGHT")) Then
            ProcessEnteredWeight()
        ElseIf Not String.IsNullOrEmpty(DO1.Value("SERIAL")) Then
            ProcessScannedSerial()
        End If

        WhereToNext()
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddTextboxLine("TOTE", "Scan Container")
        DO1.AddLabelLine("CONTAINER")
        DO1.AddLabelLine("Receipt", "Container Return Receipt")
        DO1.AddLabelLine("SkuInProg", "Current Product")
        DO1.AddLabelLine("RET_WEIGHT", "Product Weight")
        DO1.AddLabelLine("SUG_RETLOC", "Suggested Return Location")
        DO1.AddSpacer()
        DO1.AddTextboxLine("SKU", "Scan Product")
        DO1.AddTextboxLine("WEIGHT", "Enter Product weight")
        DO1.AddTextboxLine("SERIAL", "Scan Serial")
        DO1.AddTextboxLine("RET_LOC", "Enter Return Location")
        DO1.AddSpacer()
        DO1.AddLabelLine("CONT_ITEMS", "")
        DO1.AddLabelLine("MSG", "")
    End Sub

    Private Sub doMenu()
        Session.Remove("ReturnWeight")
        Session.Remove("ReadyToCreate")
        Session.Remove("ReturnSKU")
        Session.Remove("ReturnRC")
        Session.Remove("ReturnRCLine")
        Session.Remove("ReturnUOM")
        Session.Remove("ReturnColType")
        Session.Remove("Consignee")
        Session.Remove("ReturnContainer")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doReset()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Session("ReturnSKU") IsNot Nothing And Session("ReturnSKU") <> "" Then
            Dim strUnverSerTransIns As String = String.Format("UPDATE [apx_serial_returns] Set received={0}, inProgressUser= null, weight=null where containerID={1} AND SKU={2} AND receipt={3} AND receiptline={4}", FormatField(False), FormatField(Session("ReturnContainer")), FormatField(Session("ReturnSKU")), FormatField(Session("ReturnRC")), FormatField(Session("ReturnRCLine")))
            DataInterface.RunSQL(strUnverSerTransIns)

            Session.Remove("ReturnWeight")
            Session.Remove("ReadyToCreate")
            Session.Remove("ReturnUOM")
            Session.Remove("ReturnColType")
            Session.Remove("Consignee")
            Session.Remove("ReturnSKU")

            Dim TableRow As List(Of DataStruct) = Session("RawDisplay")
            Dim sss As DataStruct = TableRow.Item(Session("TableIndex"))
            sss.QtyReceived = 0
            TableRow.Item(Session("TableIndex")) = sss
            Session("RawDisplay") = TableRow

            Session.Remove("TableIndex")

            WhereToNext()
            'Response.Redirect(MapVirtualPath("Screens/CUSTRET.aspx"))
        Else
            MessageQue.Enqueue(t.Translate("Nothing to Reset"))
        End If
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                If Not Session("ShowScannedSerials") Is Nothing Then
                    Session.Remove("ShowScannedSerials")
                    WhereToNext()
                Else
                    If Not Session("ReadyToCreate") Is Nothing Then
                        SubmitCreate(Nothing, False)
                    Else
                        doFindContainer()
                    End If
                End If
            Case "view serial number"
                Session.Add("ShowScannedSerials", True)
                WhereToNext()
            Case "reset return"
                doReset()
            Case "menu"
                doMenu()
        End Select
    End Sub

    Private Function ExtractAttributeValues(sku As String, Consignee As String) As WMS.Logic.AttributesCollection
        Dim objSkuClass As SkuClass = New SKU(Consignee, sku, True).SKUClass
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            Dim req As Boolean = False
            If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Dim val As Object
                Try
                    Select Case oAtt.Type
                        Case Logic.AttributeType.Boolean
                            val = CType(DO1.Value(oAtt.Name), Boolean)
                        Case Logic.AttributeType.DateTime
                            val = CType(DO1.Value(oAtt.Name), DateTime)
                        Case Logic.AttributeType.Decimal
                            val = CType(DO1.Value(oAtt.Name), Decimal)
                        Case Logic.AttributeType.Integer
                            val = CType(DO1.Value(oAtt.Name), Int32)
                        Case Else
                            val = DO1.Value(oAtt.Name)
                    End Select
                    oAttCol.Add(oAtt.Name, val)
                Catch ex As Exception

                End Try
            End If
        Next

        oAttCol.Add("HARVEST", 0)
        Return oAttCol
    End Function

    Private Sub SetStatusAndPrinter()
        If Session("CreateLoadLabelPrinter") Is Nothing Or Session("CreateLoadLabelPrinter") = "" Then
            Try
                Session("CreateLoadLabelPrinter") = Convert.ToString(Made4Net.DataAccess.DataInterface.ExecuteScalar("SELECT DEFAULTPRINTER FROM LABELS WHERE LABELNAME = 'LOAD'"))
            Catch ex As Exception
            End Try
        End If

        If WMS.Logic.Consignee.AutoPrintLoadIdOnReceiving(Session("CreateLoadConsignee")) Then
            DO1.setVisibility("PRINTER", True)
            Dim prnt As String = MobileUtils.GetMHEDefaultPrinter()
            If prnt <> "" Then
                DO1.Value("PRINTER") = prnt
            Else
                DO1.Value("PRINTER") = Session("CreateLoadLabelPrinter")
            End If
        Else
            DO1.setVisibility("PRINTER", False)
        End If
    End Sub

    Private Sub SetHUTrans()
        Dim dd As Made4Net.WebControls.MobileDropDown = DO1.Ctrl("HUTRANS")
        dd.AllOption = False
        dd.TableName = "CODELISTDETAIL"
        dd.ValueField = "CODE"
        dd.TextField = "DESCRIPTION"
        dd.Where = String.Format("CODELISTCODE = 'ISHUTRANS'")
        dd.DataBind()
    End Sub

    Private Function GetReturnWHA() As String()
        Dim Value As String = ""
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'ReturnSourceWarehouseAreas'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        Dim whas As String()
        If Value IsNot Nothing Then
            whas = Value.Split(New Char() {","c})
        End If

        Return whas
    End Function

    Private Sub SubmitCreate(ByVal oAttributes As WMS.Logic.AttributesCollection, Optional ByVal DoPutaway As Boolean = False)
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Session("ReadyToCreate") IsNot Nothing Then
            If String.IsNullOrEmpty(DO1.Value("RET_LOC")) Then
                MessageQue.Enqueue(t.Translate("Please Scan/Enter Return Location"))
                WhereToNext()
                Return
            Else
                Dim Loc As String = DO1.Value("RET_LOC").Trim()
                If Location.Exists(Loc) Then
                    Dim oLoc As Location = New Location(Loc, True)
                    Dim LocArea As String = oLoc.WAREHOUSEAREA
                    Dim RetArea As String() = GetReturnWHA()
                    Dim x As Integer = 0
                    Dim LocValid As Boolean = False

                    While x < RetArea.Length
                        If RetArea(x) = LocArea Then
                            LocValid = True
                            Exit While
                        End If
                        x += 1
                    End While

                    If Not LocValid Then
                        MessageQue.Enqueue(t.Translate("Entered/Scanned Location is not valid in the returns warehouse area"))
                        WhereToNext()
                        Return
                    End If
                Else
                    MessageQue.Enqueue(t.Translate("Entered/Scanned Return Location does not exist"))
                    WhereToNext()
                    Return
                End If

                'Find out if location is occupied
                Dim LocAvailableForUse As Boolean = True 'ReturnLocValidForUsage(DO1.Value("RET_LOC"))
                If LocAvailableForUse = False Then
                    MessageQue.Enqueue(t.Translate("Entered/Scanned Return Location is already occupied"))
                    WhereToNext()
                    Return
                End If
            End If

            Dim ResponseMessage As String = ""
            Dim attributesarray As New ArrayList

            Try
                Dim WmsHeldType As String = GetWmsCollectionType(Session("ReturnColType"))

                Dim ldId As String = ""
                If ldId = "" Then
                    ldId = WMS.Logic.Load.GenerateLoadId()
                End If

                'TEMP
                Dim ld() As WMS.Logic.Load
                Dim oRec As New Logic.Receiving
                Dim oReceiptLine As ReceiptDetail

                oReceiptLine = New ReceiptDetail(Session("ReturnRC").ToString(), CType(Session("ReturnRCLine"), Integer))

                Dim oCon As New WMS.Logic.Consignee(Session("Consignee"))

                Dim TableRow As List(Of DataStruct) = Session("RawDisplay")
                Dim sss As DataStruct = TableRow.Item(Session("TableIndex"))
                Dim Qty As Integer = sss.QtyReceived

                Dim Prn As String = ""
                If Not Session("UserAssignedPrinter") Is Nothing Then
                    Prn = Session("UserAssignedPrinter")
                End If

                oAttributes = ExtractAttributeValues(Session("ReturnSKU"), oCon.CONSIGNEE)

                'Check if number of loads is more than one and if we need to create containers
                ld = oRec.CreateLoad(Session("ReturnRC"), Session("ReturnRCLine"), Session("ReturnSKU"), ldId,
                    Session("ReturnUOM"), DO1.Value("RET_LOC"), CDbl(Qty), WmsHeldType,
                    "", 1, Logic.GetCurrentUser, oAttributes, Prn, "", "", "")

                If ReceiveValidSerialNumbers(ldId, Session("ReturnSKU"), WmsHeldType) Then
                Else
                End If

                MessageQue.Enqueue(t.Translate("Load " & ldId & " Created"))
            Catch ex As Made4Net.Shared.M4NException
                MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
                WhereToNext()
                Return
            Catch ex As ApplicationException
                MessageQue.Enqueue(ex.Message)
                WhereToNext()
                Return
            End Try

            Session.Remove("ReturnWeight")
            Session.Remove("ReadyToCreate")
            Session.Remove("ReturnSKU")
            Session.Remove("ReturnUOM")
            Session.Remove("ReturnColType")
            Session.Remove("Consignee")
            Session.Remove("TableIndex")
            DO1.Value("SkuInProg") = ""
            DO1.Value("RET_WEIGHT") = ""
            DO1.Value("SUG_RETLOC") = ""
            DO1.Value("RET_LOC") = ""
            DO1.Value("MSG") = ""

            Dim dtLines As DataTable = APXINBOUND_FindReturnByContainer(Session("ReturnContainer"))
            UpdateReceiptTrigger(Session("ReturnRC"), Session("ReturnRCLine"))

            If dtLines.Rows.Count = 0 Then
                Dim oRecptHdr As ReceiptHeader = New ReceiptHeader(Session("ReturnRC"))

                'close receipt  when all lines are receipted
                oRecptHdr.close(Logic.GetCurrentUser)

                Session.Remove("ReturnContainer")
                Session.Remove("ReturnRC")
                Session.Remove("ReturnRCLine")
            End If

            WhereToNext()
        Else
            MessageQue.Enqueue(t.Translate("Cannot Create Load - Product Return in progress"))
            WhereToNext()
        End If
    End Sub

    Private Function ReturnLocValidForUsage(ByVal Loc As String) As Boolean
        Dim oLocation As WMS.Logic.Location = New WMS.Logic.Location(Loc, True)
        If oLocation.LOCUSAGETYPE = "STORAGE" Then
            If oLocation.getNumberOfLoads() >= 1 Then
                Return False
            Else
                Return True
            End If
        Else
            Return True
        End If
    End Function

    Private Function GetWmsCollectionType(CollectionType As String) As String
        Dim SQL As String = String.Format("SELECT * FROM apx_returns_collection_types WHERE ShortDesc like '%" + CollectionType + "%'")
            Dim DtLinees As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, DtLinees, False, Nothing)
        Dim flag As Boolean = DtLinees.Rows.Count > 0
        Dim ReturnValue As String
        If flag Then
            ReturnValue = CStr(DtLinees.Rows(0)("HoldCode"))
        Else
            ReturnValue = "Held"
        End If
        Return ReturnValue
    End Function

    Private Function ReceiveValidSerialNumbers(ByVal LoadId As String, ByVal SKU As String, ByVal serialStatus As String) As Boolean
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        Dim strUnverSerTransIns As String = String.Format("UPDATE [apx_serial_returns] Set loadID={0}, inProgressUser = null where SKU={1} and containerID={2} and receipt={3} and receiptline={4}", FormatField(LoadId), FormatField(SKU), FormatField(Session("ReturnContainer")), FormatField(Session("ReturnRC")), FormatField(Session("ReturnRCLine")))
        DataInterface.RunSQL(strUnverSerTransIns)

        'Now return the serials into apex_serial_table
        APXSERIAL_UpdateSerialsIntoDB(LoadId, SKU, Session("ReturnContainer"), Session("ReturnRC"), Session("ReturnRCLine"), Session("ReturnWeight"), Session("ReturnUOM"), Logic.GetCurrentUser, serialStatus, "RETURN")

        Return True
    End Function

    Private Sub UpdateReceiptTrigger(ByVal Receipt As String, ByVal ReceiptLine As Integer)
        Dim DBReceipt As String
        Dim SQL As String

        SQL = String.Format("SELECT * FROM apx_return_receipt_trigger WHERE receipt = '{0}' and ReceiptLine={1}", Receipt, ReceiptLine)

        DBReceipt = Made4Net.DataAccess.DataInterface.ExecuteScalar(SQL)
        If IsDBNull(DBReceipt) Or DBReceipt Is Nothing Then
            SQL = String.Format("Insert INTO apx_return_receipt_trigger (receipt, ReceiptLine) VALUES ('{0}', {1})", Receipt, ReceiptLine)
            DataInterface.RunSQL(SQL)
        End If

    End Sub

    Private Function GetSKUDescription(ByVal SKU As String) As String
        Dim inpSku As String
        Dim SQL As String

        SQL = String.Format("SELECT SKUSHORTDESC FROM SKU WHERE SKU = '{0}'", SKU)

        inpSku = Made4Net.DataAccess.DataInterface.ExecuteScalar(SQL)

        Return inpSku
    End Function

End Class
