Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports WMS.MobileWebApp.apexInventory

<CLSCompliant(False)> Public Class REPL
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If WMS.Logic.GetCurrentUser = "" Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            Session.Remove("REPLTSKTaskId")
            Session.Remove("REPLTSKTDetail")
            If Not Request.QueryString.Item("sourcescreen") Is Nothing Then
                Session("REPLSRCSCREEN") = Request.QueryString.Item("sourcescreen")
            End If
            'If WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PUTAWAY) Then
            '    Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("SCREENS/RPK2.aspx?sourcescreen=Repl"))
            'End If

            'CheckAssigned()
            CheckAssignedNew()
            doNext()
        End If
    End Sub

    Private Function CheckIfReplenIsToPickface(ByVal Location As String) As Boolean
        Dim sql As String = String.Format("select * from LOCATION where LOCATION={0}", Made4Net.Shared.FormatField(Location))

        Dim dtReplen As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)

        If dtReplen.Rows.Count > 0 Then
            If APXINV_CheckWHASuitableMultiRePlenProcess(dtReplen.Rows(0)("WAREHOUSEAREA")) Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    Private Function LocalRequestTaskNew(ByVal status As String, ByVal assigned As Integer, ByVal user As String) As WMS.Logic.ReplenishmentTask
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim ReplTask As WMS.Logic.ReplenishmentTask = Nothing
        Dim sql As String = String.Format("select * from TASKS where STATUS={0} and ASSIGNED={1} and TASKTYPE LIKE '%REPL'", Made4Net.Shared.FormatField(status), Made4Net.Shared.FormatField(assigned))

        If user <> "" Then
            sql = sql & String.Format(" and USERID={0}", Made4Net.Shared.FormatField(user))
        End If

        sql = sql & "  Order By Priority desc"

        Dim dtReplen As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)

        Dim x As Integer = 0
        If dtReplen.Rows.Count > 0 Then
            While (x < dtReplen.Rows.Count)
                If CheckIfReplenIsToPickface(dtReplen.Rows(x)("FROMLOCATION")) And Not IsDBNull(dtReplen.Rows(x)("REPLENISHMENT")) And dtReplen.Rows(x)("REPLENISHMENT") <> "" Then
                    ReplTask = New WMS.Logic.ReplenishmentTask(dtReplen.Rows(x)("TASK"))
                    ReplTask.AssignUser(WMS.Logic.Common.GetCurrentUser, "MANUAL", "")

                    Return ReplTask
                End If
                x = x + 1
            End While
        End If

        Return ReplTask
    End Function

    Private Function LocalRequestTask(ByVal status As String, ByVal assigned As Integer, ByVal user As String) As WMS.Logic.ReplenishmentTask
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim ReplTask As WMS.Logic.ReplenishmentTask = Nothing
        Dim sql As String = String.Format("select * from TASKS where STATUS={0} and ASSIGNED={1} and USERID={2} and TASKTYPE LIKE '%REPL' Order By Priority desc", Made4Net.Shared.FormatField(status), assigned, Made4Net.Shared.FormatField(user))

        Dim dtReplen As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)

        Dim x As Integer = 0
        If dtReplen.Rows.Count > 0 Then
            While (x < dtReplen.Rows.Count)
                If CheckIfReplenIsToPickface(dtReplen.Rows(x)("FROMLOCATION")) Then
                    ReplTask = New WMS.Logic.ReplenishmentTask(dtReplen.Rows(0)("TASK"))

                    Return ReplTask
                End If
                x = x + 1
            End While

        End If

        Return ReplTask
    End Function

    Private Function CheckAssignedNew() As Boolean
        Dim ReplTask As WMS.Logic.ReplenishmentTask
        Dim tm As New WMS.Logic.TaskManager
        Dim ReplTaskDetail As WMS.Logic.Replenishment

        If Not Session("MULTIREPLEN") Is Nothing Then
            If Session("MULTIREPLEN") = "DEPOSIT" Then
                Response.Redirect(MapVirtualPath("Screens/REPL2.aspx"))
                Return False
            End If

            ReplTask = LocalRequestTaskNew("AVAILABLE", 0, "")

            If ReplTask Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/REPL2.aspx"))
                Return False
            End If
        Else
            If AnythingToDeposit(WMS.Logic.GetCurrentUser) Then
                Response.Redirect(MapVirtualPath("Screens/REPL2.aspx"))
                Return False
            End If
            ReplTask = tm.RequestTask(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.REPLENISHMENT)
        End If

        If Not ReplTask Is Nothing Then
            If Session("MULTIREPLEN") Is Nothing And CheckIfReplenIsToPickface(ReplTask.FROMLOCATION) Then
                Session.Add("MULTIREPLEN", "COLLECT")
            End If

            Session.Add("REPLTSKTaskId", ReplTask)
            ReplTaskDetail = New WMS.Logic.Replenishment(ReplTask.Replenishment)
            Session.Add("REPLTSKTDetail", ReplTaskDetail)
            DO1.Value("ASSIGNED") = "ASSIGNED"
            DO1.Value("TASKID") = ReplTask.TASK
            Return True
        Else
            DO1.Value("ASSIGNED") = "Not Assigned"
            Return False
        End If

    End Function

    Private Function AnythingToDeposit(ByVal User As String) As Boolean
        Dim ReplTask As WMS.Logic.ReplenishmentTask
        Dim ReplTaskDetail As WMS.Logic.Replenishment
        Dim ReturnValue As Boolean = False
        Dim sql As String = String.Format("select * from apx_ReplenRdt where UserId={0}", Made4Net.Shared.FormatField(User))
        Dim dtReplen As New DataTable

        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)

        Dim x As Integer = 0
        If dtReplen.Rows.Count > 0 Then
            While x < dtReplen.Rows.Count
                ReplTaskDetail = New WMS.Logic.Replenishment(dtReplen.Rows(x)("ReplenId"))
                If ReplTaskDetail.hasTask Then
                    ReplTask = ReplTaskDetail.GetReplTask()
                    If Not ReplTask Is Nothing Then
                        If ReplTask.STATUS = "ASSIGNED" Then
                            ReturnValue = True
                        Else
                            APXINV_DequeueReplen(dtReplen.Rows(x)("ReplenId"))
                        End If
                    Else
                        APXINV_DequeueReplen(dtReplen.Rows(x)("ReplenId"))
                    End If
                Else
                    APXINV_DequeueReplen(dtReplen.Rows(x)("ReplenId"))
                End If
                x = x + 1
            End While
        End If

        Return ReturnValue
    End Function

    Private Function CheckAssigned() As Boolean
        Dim ReplTask As WMS.Logic.ReplenishmentTask
        Dim Task As String = ""
        Dim ReplTaskDetail As WMS.Logic.Replenishment
        Dim tm As New WMS.Logic.TaskManager
        Dim Depositing As Boolean = False

        If APXINV_CheckActiveReplens(WMS.Logic.GetCurrentUser, Depositing) Then
            ReplTask = tm.RequestTask(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.REPLENISHMENT)
        Else
            If Depositing Then
                Response.Redirect(MapVirtualPath("Screens/REPL2.aspx"))
            Else
                ReplTask = LocalRequestTask("AVAILABLE", 0, "")
            End If
        End If

        If Not ReplTask Is Nothing Then
            Session.Add("REPLTSKTaskId", ReplTask)
            ReplTaskDetail = New WMS.Logic.Replenishment(ReplTask.Replenishment)
            Session.Add("REPLTSKTDetail", ReplTaskDetail)
            DO1.Value("ASSIGNED") = "ASSIGNED"
            DO1.Value("TASKID") = ReplTask.TASK 'Session("REPLTSKTaskId")
            Return True
        Else
            ReplTask = LocalRequestTask("ASSIGNED", 1, WMS.Logic.GetCurrentUser)

            If Not ReplTask Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/REPL2.aspx"))
            Else
                DO1.Value("ASSIGNED") = "Not Assigned"
                Return False
            End If
        End If
    End Function

    Private Sub doMenu()
        If Not Session("REPLTSKTaskId") Is Nothing Then
            Dim tm As New WMS.Logic.TaskManager(CType(Session("REPLTSKTaskId"), WMS.Logic.ReplenishmentTask).TASK)
            Try
                tm.ExitTask()
            Catch ex As Exception
            End Try
        End If
        Session.Remove("REPLTSKTaskId")
        Session.Remove("REPLTSKTDetail")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doNext()
        If Session("REPLTSKTaskId") Is Nothing Then
            Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            Try
                Dim ts As New WMS.Logic.TaskManager
                ts.RequestTask(WMS.Logic.GetCurrentUser, WMS.Lib.TASKTYPE.REPLENISHMENT)
                'CheckAssigned()
                CheckAssignedNew()
            Catch ex As Made4Net.Shared.M4NException
                MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Catch ex As Exception
                MessageQue.Enqueue(t.Translate(ex.Message))
            End Try
        End If
        If Not Session("REPLTSKTaskId") Is Nothing Then
            Response.Redirect(MapVirtualPath("Screens/REPL1.aspx"))
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("ASSIGNED")
        DO1.AddLabelLine("TASKID")
        DO1.AddSpacer()
        DO1.AddSpacer()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
        End Select
    End Sub
End Class
