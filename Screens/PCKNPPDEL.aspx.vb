Imports WMS.Logic
Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Partial Public Class PCKNPPDEL
    Inherits System.Web.UI.Page


#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If String.IsNullOrEmpty(WMS.Logic.GetCurrentUser) Then
            WMS.Logic.GotoLogin()
        End If

        If Not IsPostBack Then
            If Not Request.QueryString("sourcescreen") Is Nothing Then
                Session("MobileSourceScreen") = Request.QueryString("sourcescreen")
            ElseIf Session("MobileSourceScreen") Is Nothing Then
                Session("MobileSourceScreen") = "PCK"
            ElseIf Not Session("MobileSourceScreen").ToString.ToLower() = "taskmanager" Then
                Session("MobileSourceScreen") = "PCK"
            End If

            If Not WMS.Logic.TaskManager.isAssigned(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.PICKING) Then
                Dim sourceScreen As String = Session("MobileSourceScreen")
                Session.Remove("MobileSourceScreen")
                If sourceScreen.Contains("_") Then
                    sourceScreen = sourceScreen.Split("_")(1)
                End If
                'Response.Redirect(MapVirtualPath("screens/" & Session("MobileSourceScreen") & ".aspx"))
                Response.Redirect(MapVirtualPath("screens/" & sourceScreen & ".aspx"))
            End If

            Dim pcks As Picklist = Session("PCKPicklist")
            Dim pck As PickJob = Session("PCKPicklistPickJob")
            'Dim tm As New WMS.Logic.TaskManager(pcks.PicklistID, True)
            'Try
            '    pck = PickTask.getNextPick(pcks)
            'Catch ex As Exception
            'End Try
            setPick(pck)
        End If
    End Sub

    Private Sub doMenu()
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        If WMS.Logic.TaskManager.isAssigned(UserId, WMS.Lib.TASKTYPE.PICKING) Then
            Dim tm As New WMS.Logic.TaskManager(UserId, WMS.Lib.TASKTYPE.PICKING)
            tm.ExitTask()
        End If
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doBack()
        'Response.Redirect(MapVirtualPath("Screens/PCK.aspx"))
        Response.Redirect(MapVirtualPath("screens/PCKNPP.aspx"))
    End Sub

    Private Sub setPick(ByVal pck As PickJob)
        If pck Is Nothing Then
            'Response.Redirect(MapVirtualPath("Screens/DELLBLPRNT.aspx?sourcescreen=PCK"))
            Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=PCKNPP")) ' & Session("MobileSourceScreen")))
        Else
            Session("PCKPicklistPickJob") = pck
            Dim pcks As Picklist = Session("PCKPicklist")
            DO1.Value("LOADID") = pck.fromload
            DO1.Value("LOCATION") = WMS.Logic.TaskManager.GetFinalDestinationLocation(WMS.Logic.GetCurrentUser, pck.fromlocation, pcks.Lines(0).ToLocation)
            DO1.Value("PICKMETHOD") = pcks.PickMethod
            DO1.Value("PICKTYPE") = pcks.PickType
            DO1.Value("SKU") = pck.sku
            DO1.Value("SKUDESC") = pck.skudesc
            DO1.Value("UOM") = pck.uom
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            DO1.Value("UOMUNITS") = pck.uomunits  '"(" & pck.units & ")" & " " & trans.Translate(pck.uom) & " " & pck.uomunits
            'Label Printing
            If MobileUtils.LoggedInMHEID <> String.Empty And MobileUtils.GetMHEDefaultPrinter <> String.Empty Then
                DO1.setVisibility("PRINTER", False)
            Else
                DO1.setVisibility("PRINTER", True)
            End If

            DO1.setVisibility("HANDLINGUNIT", True)
            DO1.setVisibility("HANDLINGUNITTYPE", True)
            Dim dd As Made4Net.WebControls.MobileDropDown
            dd = DO1.Ctrl("HANDLINGUNITTYPE")
            dd.AllOption = False
            dd.TableName = "handelingunittype"
            dd.ValueField = "container"
            dd.TextField = "containerdesc"
            dd.DataBind()
        End If
    End Sub

    Private Sub DoNext(ByVal pOverride As Boolean)
        Dim pck As PickJob
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim pcklst As Picklist = Session("PCKPicklist")
        pck = Session("PCKPicklistPickJob")
        Try
            Dim err As String = confirm(pOverride, pck)
            If Not String.IsNullOrEmpty(err) Then
                MessageQue.Enqueue(trans.Translate(err))
                Return
            End If
            doPick(pck, pcklst)
            doDeliver()
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            ClearAttributes()
            Return
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate(ex.Message))
            ClearAttributes()
            Return
        End Try
        'ClearAttributes()
        Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=PCKNPP_" & Session("MobileSourceScreen")))
        'setPick(pck)
    End Sub

    Private Function confirm(ByVal pOverride As Boolean, ByVal pPickJob As PickJob) As String
        Try
            Dim sku As New SKU(pPickJob.consingee, pPickJob.sku)
            pPickJob.pickedqty = sku.ConvertToUnits(pPickJob.uom) * Convert.ToDecimal(DO1.Value("UOMUNITS"))
            sku = Nothing
        Catch ex As Exception
            Return "Error cannot get units"
        End Try
        If Not WMS.Logic.Location.Exists(DO1.Value("CONFIRM")) Then
            Return "Location does not exist"
        End If
        If pOverride Then
            If String.Equals(DO1.Value("CONFIRM"), DO1.Value("LOCATION"), StringComparison.OrdinalIgnoreCase) Then
                Return "Confirmation location equals to job target location, use next button instead"
            End If
        Else
            If Not String.Equals(DO1.Value("CONFIRM"), DO1.Value("LOCATION"), StringComparison.OrdinalIgnoreCase) Then
                Return "Confirmation location not equals to job target location, use override button instead"
            End If
        End If
        'Location.CheckLocationConfirmation(DO1.Value("LOCATION"), DO1.Value("CONFIRM"))

        Return ""
    End Function

    Private Sub doPick(ByVal pPickJob As PickJob, ByVal pPickList As Picklist)
        'ExtractAttributes()
        MobileUtils.ExtractPickingAttributes(pPickJob, DO1)
        If MobileUtils.LoggedInMHEID.Trim <> String.Empty Then
            pPickJob.LabelPrinterName = MobileUtils.GetMHEDefaultPrinter
        End If
        'pck.TaskConfirmation.ConfirmationValues = MobileUtils.ExtractConfirmationValues(pck.TaskConfirmation.ConfirmationType, DO1)
        pPickJob.TaskConfirmation = New TaskConfirmationNone
        pPickJob = PickTask.Pick(pPickList, pPickJob, WMS.Logic.GetCurrentUser)
    End Sub

    Private Sub doDeliver()
        Dim tm As New WMS.Logic.TaskManager
        Dim deltsk As DeliveryTask = tm.getAssignedTask(WMS.Logic.Common.GetCurrentUser, WMS.Lib.TASKTYPE.DELIVERY)
        Dim delobj As DeliveryJob = deltsk.getDeliveryJob()
        Dim oCont As WMS.Logic.Container
        If Not delobj.isContainer Then
            If WMS.Logic.Container.Exists(DO1.Value("HANDLINGUNIT")) Then
                oCont = New WMS.Logic.Container(DO1.Value("HANDLINGUNIT"), True)
            Else
                If DO1.Value("HANDLINGUNIT") <> "" Then
                    oCont = New WMS.Logic.Container
                    oCont.ContainerId = DO1.Value("HANDLINGUNIT")
                    oCont.HandlingUnitType = DO1.Value("HANDLINGUNITTYPE")
                    oCont.Location = delobj.toLocation
                    oCont.Post(WMS.Logic.Common.GetCurrentUser)
                End If
            End If
        End If
        deltsk.Deliver(DO1.Value("CONFIRM"), delobj.IsHandOff, oCont)
    End Sub


    Private Sub pickanotherload()
        Dim pck As PickJob
        Dim pcklst As Picklist = Session("PCKPicklist")
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        pck = Session("PCKPicklistPickJob")
        If pck Is Nothing Then
            'Response.Redirect(MapVirtualPath("Screens/DELLBLPRNT.aspx?sourcescreen=PCK"))
            Response.Redirect(MapVirtualPath("screens/DELLBLPRNT.aspx?sourcescreen=" & Session("MobileSourceScreen")))
        End If

        Try
            pck.pickedqty = pck.units
            Dim tm As New WMS.Logic.TaskManager(pck.picklist, True)
            'ExtractAttributes()
            MobileUtils.ExtractPickingAttributes(pck, DO1)
            If MobileUtils.LoggedInMHEID.Trim <> String.Empty Then
                pck.LabelPrinterName = MobileUtils.GetMHEDefaultPrinter
            End If
            pck.TaskConfirmation.ConfirmationValues = MobileUtils.ExtractConfirmationValues(pck.TaskConfirmation.ConfirmationType, DO1)
            pck = PickTask.PickAnotherLoad(DO1.Value("SubtituteLoad").Trim(), pcklst, pck, WMS.Logic.GetCurrentUser())
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            ClearAttributes()
            Return
        Catch ex As Exception
            MessageQue.Enqueue(trans.Translate(ex.Message))
            ClearAttributes()
            Return
        End Try
        ClearAttributes()
        DO1.Value("SubtituteLoad") = ""
        setPick(pck)
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("PickMethod")
        DO1.AddLabelLine("PickType")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LoadId")
        DO1.AddLabelLine("Location")
        DO1.AddLabelLine("UOM")
        DO1.AddSpacer()
        Dim pck As PickJob = Session("PCKPicklistPickJob")
        If Not pck Is Nothing Then
            If Not pck.oAttributeCollection Is Nothing Then
                For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
                    DO1.AddTextboxLine(String.Format("Attr_{0}", pck.oAttributeCollection.Keys(idx)), pck.oAttributeCollection.Keys(idx))
                Next
            End If
        End If
        DO1.AddSpacer()
        DO1.AddTextboxLine("UOMUNITS", "UOMUNITS", "")
        DO1.AddSpacer()
        DO1.AddTextboxLine("HANDLINGUNIT")
        DO1.AddDropDown("HANDLINGUNITTYPE")
        DO1.AddTextboxLine("CONFIRM")
        'DO1.AddTextboxLine("SubtituteLoad")
        DO1.AddTextboxLine("PRINTER")
    End Sub


    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                DoNext(False)
            Case "override"
                DoNext(True)
            Case "back"
                doBack()
        End Select
    End Sub

    'Private Function ExtractAttributes() As AttributesCollection
    '    Dim pck As PickJob = Session("PCKPicklistPickJob")
    '    Dim Val As Object
    '    If Not pck Is Nothing Then
    '        If Not pck.oAttributeCollection Is Nothing Then
    '            For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
    '                Val = DO1.Value(pck.oAttributeCollection.Keys(idx))
    '                If Val = "" Then Val = Nothing
    '                pck.oAttributeCollection(idx) = Val
    '            Next
    '            Return pck.oAttributeCollection
    '        End If
    '    End If
    '    Return Nothing
    'End Function

    Private Sub ClearAttributes()
        Dim pck As PickJob = Session("PCKPicklistPickJob")
        If Not pck Is Nothing Then
            If Not pck.oAttributeCollection Is Nothing Then
                For idx As Int32 = 0 To pck.oAttributeCollection.Count - 1
                    'DO1.Value(pck.oAttributeCollection.Keys(idx)) = ""
                    DO1.Value(String.Format("Attr_{0}", pck.oAttributeCollection.Keys(idx))) = ""
                Next
            End If
        End If
    End Sub
End Class