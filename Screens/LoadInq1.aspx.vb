Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
<CLSCompliant(False)> Public Class LoadInq1
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            Dim ld As New WMS.Logic.Load(Convert.ToString(Session("LoadInqLoadId")))
            DO1.Value("CONSIGNEE") = ld.CONSIGNEE
            DO1.Value("SKU") = ld.SKU
            DO1.Value("UOM") = ld.LOADUOM
            DO1.Value("UNITS") = ld.UNITS
            DO1.Value("LOCATION") = ld.LOCATION
            DO1.Value("LOADID") = ld.LOADID

            Dim val As Boolean = False
            Dim sAttrib As Object = ld.GetAttribute("KOSHER")
            If Not String.IsNullOrEmpty(CStr(sAttrib)) Then
                val = CBool(sAttrib)
            End If

            DO1.Value("SEALED") = CStr(val)

            If val = True Then
                DO1.Button(2).Visible = False
            Else
                If ld.UNITS > 0 Then
                    Dim skuObj As SKU = New SKU(ld.CONSIGNEE, ld.SKU, True)
                    Dim classname As String = ""
                    If skuObj.SKUClass IsNot Nothing Then
                        classname = skuObj.SKUClass.ClassName
                    End If

                    Dim Valid As Boolean = False
                    If String.Compare(classname, "SERIAL", True) Then
                        Dim serialCount As Integer = GetLoadCountFromSerials(ld.LOADID)
                        If CDec(serialCount) = ld.UNITS Then
                            Valid = True
                        End If
                    Else
                        Valid = True
                    End If

                    If Valid Then
                        DO1.Button(2).Visible = True
                    Else
                        DO1.Button(2).Visible = False
                    End If
                Else
                    DO1.Button(2).Visible = False
                End If
            End If

            Try
                DO1.Value("SKUDESC") = Made4Net.DataAccess.DataInterface.ExecuteScalar(String.Format("select skudesc from sku where sku = '{0}' and consignee = '{1}'", ld.SKU, ld.CONSIGNEE))
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Function GetLoadCountFromSerials(loadID As String) As Integer
        Dim Count As Integer = 0
        apexSerial.APEXSERIAL_GetLoadSerialCount(loadID, Count)
        Return Count
    End Function

    Private Sub doNext()
        Session.Remove("LoadInqLoadId")
        Response.Redirect(MapVirtualPath("Screens/LoadInq.aspx"))
    End Sub

    Private Sub doMakeVirgin()
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Dim ld As Load = New Load(Convert.ToString(Session("LoadInqLoadId")), True)
        Dim oAttCol As AttributesCollection = New AttributesCollection()
        Dim Val As Object = True
        oAttCol.Add("KOSHER", Val)
        ld.setAttributes(oAttCol, WMS.Logic.Common.GetCurrentUser())
        MessageQue.Enqueue(t.Translate("Pallet now sealed"))
        Response.Redirect(MapVirtualPath("Screens/LoadInq1.aspx"))
    End Sub

    Private Sub doMenu()
        Session.Remove("LoadInqLoadId")
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("LOADID")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("UOM")
        DO1.AddLabelLine("UNITS")
        DO1.AddLabelLine("LOCATION")
        DO1.AddLabelLine("SEALED")
        DO1.AddSpacer()
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                doNext()
            Case "menu"
                doMenu()
            Case "convert to sealed pallet"
                doMakeVirgin()
        End Select
    End Sub
End Class
