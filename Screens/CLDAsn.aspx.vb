Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports WMS.MobileWebApp.apexInbound
Imports WMS.MobileWebApp.apexSerial

<CLSCompliant(False)> Public Class CLDAsn
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If Session("CreateLoadRCN") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRcnLine.aspx"))
            End If

            If Session("CreateLoadASNID") Is Nothing Then
                Response.Redirect(MapVirtualPath("Screens/CLDWEIGHT.aspx"))
            End If

            DO1.Value("ASNID") = Session("CreateLoadASNID")
            DO1.Value("CONSIGNEE") = Session("CreateLoadConsignee")
            DO1.Value("SKU") = Session("CreateLoadSKU")
            DO1.Value("RECEIPT") = Session("CreateLoadRCN")
            DO1.Value("RECEIPTLINE") = Session("CreateLoadRCNLine")
            Dim oSku As New Logic.SKU(Session("CreateLoadConsignee"), Session("CreateLoadSKU"))
            DO1.Value("SKUDESC") = oSku.SKUDESC
            DO1.Value("SKUNOTES") = oSku.NOTES
            DO1.Value("UNITS") = Session("CreateLoadUnits")
            DO1.Value("UOM") = Session("CreateLoadUOM")
            DO1.Value("LOCATION") = Session("CreateLoadLocation")
            DO1.Value("WEIGHT") = Session("CreateLoadWeight")
            DO1.setVisibility("SKUNOTES", False)

        End If
    End Sub

    Private Sub doMenu()
        MobileUtils.ClearCreateLoadProcessSession()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub doAbort(ByVal Full As Boolean)
        If Full Then
            APXSERIAL_AbortASNSerialUpdate(Session("CreateLoadASNID"), Session("CreateLoadSKU"))
        End If
        Session.Remove("CreateLoadRCN")
        Session.Remove("CreateLoadRCNLine")
        Session.Remove("CreateLoadConsignee")
        Session.Remove("CreateLoadSKU")
        Session.Remove("CreateLoadLocation")
        Session.Remove("CreateLoadWeight")
        Session.Remove("CreateLoadLocation")
        Session.Remove("CreateLoadUnits")
        Session.Remove("CreateLoadASNID")
        Response.Redirect(MapVirtualPath("Screens/CreateLoadSelectRCNLine.aspx"))
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("ASNID")
        DO1.AddLabelLine("RECEIPT")
        DO1.AddLabelLine("RECEIPTLINE")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("SKUNOTES")
        DO1.AddLabelLine("LOCATION")
        DO1.AddLabelLine("WEIGHT")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "createload"
                SubmitCreate(ExtractAttributeValues(), False)
            Case "abort"
                doAbort(True)
        End Select
    End Sub

    Private Function ExtractAttributeValues() As WMS.Logic.AttributesCollection
        Dim oSku As String = Session("CreateLoadSKU")
        Dim oConsignee As String = Session("CreateLoadConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            Dim req As Boolean = False
            If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Dim val As Object
                Try
                    Select Case oAtt.Type
                        Case Logic.AttributeType.Boolean
                            val = CType(DO1.Value(oAtt.Name), Boolean)
                        Case Logic.AttributeType.DateTime
                            val = CType(DO1.Value(oAtt.Name), DateTime)
                        Case Logic.AttributeType.Decimal
                            val = CType(DO1.Value(oAtt.Name), Decimal)
                        Case Logic.AttributeType.Integer
                            val = CType(DO1.Value(oAtt.Name), Int32)
                        Case Else
                            val = DO1.Value(oAtt.Name)
                    End Select
                    oAttCol.Add(oAtt.Name, val)
                Catch ex As Exception

                End Try
            End If
        Next
        Return oAttCol
    End Function

    Private Sub SubmitCreate(ByVal oAttributes As WMS.Logic.AttributesCollection, Optional ByVal DoPutaway As Boolean = False)
        Dim ResponseMessage As String = ""
        Dim attributesarray As New ArrayList
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        Try
            Dim ldId As String = Session("CreateLoadLoadId")
            If ldId = "" Or ldId Is Nothing Then
                ldId = WMS.Logic.Load.GenerateLoadId()
            End If

            Dim ld() As WMS.Logic.Load
            Dim oRec As New Logic.Receiving
            Dim oReceiptLine As ReceiptDetail
            Dim err As String = APXSERIAL_UpdateASNSerialNumbers(Session("CreateLoadASNID"), Session("CreateLoadSKU"), ldId, CDbl(Session("CreateLoadWeight")), Logic.GetCurrentUser)
            If String.IsNullOrEmpty(err) Then

                If Session("CreateLoadNumLoads") Is Nothing Then Session("CreateLoadNumLoads") = 1

                If Session("CreateLoadRCNMultipleLines") Then
                    ld = oRec.CreateLoadFromMultipleLines(Session("CreateLoadRCN"), Session("CreateLoadSKU"), _
                        Session("CreateLoadUOM"), Session("CreateLoadLocation"), Session("CreateLoadUnits"), Session("CreateLoadStatus"), _
                        Session("CreateLoadHoldRC"), Logic.GetCurrentUser, oAttributes, Session("CreateLoadLabelPrinter"), "", "")
                Else

                    oReceiptLine = New ReceiptDetail(Session("CreateLoadRCN").ToString(), CType(Session("CreateLoadRCNLine"), Integer))

                    ' Check if number of loads is more than one and if we need to create containers
                    ld = oRec.CreateLoad(Session("CreateLoadRCN"), Session("CreateLoadRCNLine"), Session("CreateLoadSKU"), ldId, _
                        Session("CreateLoadUOM"), Session("CreateLoadLocation"), Session("CreateLoadUnits"), "Held", _
                        "", Session("CreateLoadNumLoads"), Logic.GetCurrentUser, oAttributes, "", "", "", "")
                End If
            Else
                Made4Net.Mobile.MessageQue.Enqueue(t.Translate(err))
                Return
            End If

            ' After Load Creation we will put the loads on his container
            If Session("CreateContainer") = "1" Then
                Dim LoadsCreatedCount As Int32
                For LoadsCreatedCount = 0 To ld.Length() - 1
                    Dim cntr As New WMS.Logic.Container()
                    cntr.ContainerId = Made4Net.Shared.getNextCounter("CONTAINER")
                    cntr.HandlingUnitType = Session("HUTYPE")
                    cntr.Location = Session("CreateLoadLocation")
                    cntr.Post(WMS.Logic.Common.GetCurrentUser)

                    cntr.Place(ld(LoadsCreatedCount), WMS.Logic.Common.GetCurrentUser)
                Next
            Else
                If Session("CreateLoadContainerID") <> "" Then
                    Dim cntr As New WMS.Logic.Container(Session("CreateLoadContainerID"), True)
                    Dim LoadsCreatedCount As Int32
                    For LoadsCreatedCount = 0 To ld.Length() - 1
                        cntr.Place(ld(LoadsCreatedCount), WMS.Logic.Common.GetCurrentUser)
                    Next
                End If
            End If


            ' Create Handling unit transaction if needed
            If DO1.Value("HUTRANS") = "YES" Then
                Try
                    Dim strHUTransIns As String
                    If Session("CreateLoadContainerID") <> "" Then
                        strHUTransIns = "INSERT INTO [HANDLINGUNITTRANSACTION]([TRANSACTIONID],[TRANSACTIONTYPE],[TRANSACTIONTYPEID],[TRANSACTIONDATE],[CONSIGNEE],[ORDERID],[DOCUMENTTYPE],[COMPANY],[COMPANYTYPE],[HUTYPE],[HUQTY],[ADDDATE],[ADDUSER],[EDITDATE],[EDITUSER])" & _
                                                      "VALUES('" & Made4Net.Shared.getNextCounter("HUTRANSID") & "','RECEIPT','" & Session("CreateLoadRCN") & "',GETDATE(),'" & Session("CreateLoadConsignee") & "','" & oReceiptLine.ORDERID & "','" & oReceiptLine.DOCUMENTTYPE & "','" & oReceiptLine.COMPANY & "','" & oReceiptLine.COMPANYTYPE & "','" & DO1.Value("HUTYPE") & "','1',GETDATE(),'" & Logic.GetCurrentUser & "',GETDATE(),'" & Logic.GetCurrentUser & "')"
                    Else
                        strHUTransIns = "INSERT INTO [HANDLINGUNITTRANSACTION]([TRANSACTIONID],[TRANSACTIONTYPE],[TRANSACTIONTYPEID],[TRANSACTIONDATE],[CONSIGNEE],[ORDERID],[DOCUMENTTYPE],[COMPANY],[COMPANYTYPE],[HUTYPE],[HUQTY],[ADDDATE],[ADDUSER],[EDITDATE],[EDITUSER])" & _
                                                      "VALUES('" & Made4Net.Shared.getNextCounter("HUTRANSID") & "','RECEIPT','" & Session("CreateLoadRCN") & "',GETDATE(),'" & Session("CreateLoadConsignee") & "','" & oReceiptLine.ORDERID & "','" & oReceiptLine.DOCUMENTTYPE & "','" & oReceiptLine.COMPANY & "','" & oReceiptLine.COMPANYTYPE & "','" & DO1.Value("HUTYPE") & "','" & Session("CreateLoadNumLoads") & "',GETDATE(),'" & Logic.GetCurrentUser & "',GETDATE(),'" & Logic.GetCurrentUser & "')"
                    End If
                    DataInterface.RunSQL(strHUTransIns)
                Catch ex As Exception
                End Try
            End If

            ' If Create and Putaway then request pickup for each load
            If DoPutaway Then
                Dim pw As New Putaway
                Dim i As Int32
                For i = 0 To ld.Length() - 1
                    ld(i).RequestPickUp(Logic.GetCurrentUser)
                    'pw.RequestDestination(ld(i).LOADID)
                Next
                Response.Redirect(MapVirtualPath("Screens/RPK2.aspx?sourcescreen=cld1"))
            End If
            MessageQue.Enqueue(t.Translate("Load Created"))
        Catch ex As Made4Net.Shared.M4NException
            MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Return
        Catch ex As ApplicationException
            MessageQue.Enqueue(ex.Message)
            Return
        End Try
        DO1.Value("LOADID") = ""
        DO1.Value("UNITS") = ""
        DO1.Value("CONTAINERID") = ""
        clearAttributeValues()
        doAbort(False)
    End Sub

    Private Sub clearAttributeValues()
        Dim oSku As String = Session("CreateLoadSKU")
        Dim oConsignee As String = Session("CreateLoadConsignee")
        Dim objSkuClass As WMS.Logic.SkuClass = New WMS.Logic.SKU(oConsignee, oSku).SKUClass
        If objSkuClass Is Nothing Then Return
        Dim oAttCol As New WMS.Logic.AttributesCollection
        For Each oAtt As WMS.Logic.SkuClassLoadAttribute In objSkuClass.LoadAttributes
            If oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Required Or oAtt.CaptureAtReceiving = Logic.SkuClassLoadAttribute.CaptureType.Capture Then
                Try
                    DO1.Value(oAtt.Name) = ""
                Catch ex As Exception
                End Try
            End If
        Next
    End Sub
End Class
