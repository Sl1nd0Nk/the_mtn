Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

<CLSCompliant(False)> Public Class REPL5
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then
            WMS.Logic.GotoLogin()
        End If
        If Not IsPostBack Then
            If Not Request.QueryString.Item("sourcescreen") Is Nothing Then
                Session("REPLSRCSCREEN") = Request.QueryString.Item("sourcescreen")
            End If
            SetScreen()
        End If
    End Sub

    Private Sub SetScreen()
        Dim ReplTask As WMS.Logic.ReplenishmentTask = Session("REPLTSKTaskId")
        Dim ReplTaskDetail As WMS.Logic.Replenishment = Session("REPLTSKTDetail")
        Dim replJob As ReplenishmentJob = Session("REPLJobDetail")

        If replJob.IsHandOff Then
            'Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            'DO1.setVisibility("Note", True)
            'DO1.Value("Note") = trans.Translate("Task Destination Location is a Hand Off Location!")
        Else
            'DO1.setVisibility("Note", False)
        End If
        DO1.Value("TASKTYPE") = replJob.TaskType
        DO1.Value("LOCATION") = replJob.toLocation
        DO1.Value("LOADID") = replJob.fromLoad
        DO1.Value("UNITS") = replJob.Units
        DO1.Value("CONSIGNEE") = replJob.Consignee
        DO1.Value("SKU") = replJob.Sku
        DO1.Value("SKUDESC") = replJob.skuDesc
        DO1.Value("UOMUNITS") = replJob.UOMUnits
        DO1.setVisibility("UNITS", False)
        DO1.setVisibility("UOMUNITS", False)
    End Sub

    'If the location is the right one then replenish the location else just move
    'the load to new location and leave the Job activ

    Private Sub doNext()
        Dim errStr As String = CheckSKU()
        If Not String.IsNullOrEmpty(errStr) Then
            Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            MessageQue.Enqueue(trans.Translate(errStr))
            Return
        Else
            Response.Redirect(MapVirtualPath("Screens/REPL6.aspx"))
        End If
    End Sub


    Private Sub doBack()
        'Response.Redirect(MapVirtualPath("Screens/REPL1.aspx"))
        Dim ReplTask As WMS.Logic.ReplenishmentTask = Session("REPLTSKTaskId")
        If Not ReplTask Is Nothing Then
            If ReplTask.ASSIGNMENTTYPE = WMS.Lib.TASKASSIGNTYPE.MANUAL Then
                If Not Session("REPLSRCSCREEN") Is Nothing Then
                    Response.Redirect(MapVirtualPath("Screens/" & Session("REPLSRCSCREEN") & ".aspx"))
                Else
                    Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
                End If
            Else
                Response.Redirect(MapVirtualPath("Screens/TaskManager.aspx"))
            End If
        Else
            Response.Redirect(MapVirtualPath("Screens/REPL.aspx"))
        End If
    End Sub

    'Private Function CheckSKU() As String
    '    Dim repljob As ReplenishmentJob = Session("REPLJobDetail")

    '    If repljob.Sku <> DO1.Value("DSKU") Then
    '        Return "Incorrect SKU scanned."
    '    Else
    '        Return ""
    '    End If
    'End Function

    Private Function CheckSKU() As String
        Dim repljob As ReplenishmentJob = Session("REPLJobDetail")
        Dim inpSku As String
        Dim SQL As String

        SQL = String.Format("SELECT SKU FROM vSKUCODE WHERE SKUCODE = '{0}'", DO1.Value("DSKU"))

        inpSku = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        If repljob.Sku <> inpSku Then
            Return "Incorrect SKU scanned."
        Else
            Return ""
        End If
    End Function


    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        'DO1.AddLabelLine("Note")
        DO1.AddLabelLine("TASKTYPE")
        DO1.AddLabelLine("LOADID", "From Load")
        DO1.AddLabelLine("CONSIGNEE")
        DO1.AddLabelLine("SKU")
        DO1.AddLabelLine("SKUDESC")
        DO1.AddLabelLine("LOCATION")
        DO1.AddLabelLine("UOMUNITS")
        DO1.AddLabelLine("UNITS")
        DO1.AddSpacer()
        DO1.AddTextboxLine("DSKU", "Scan SKU")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            'Case "override"
            '    doOverride()
            Case "next"
                doNext()
            Case "back"
                doBack()
        End Select
    End Sub
End Class

