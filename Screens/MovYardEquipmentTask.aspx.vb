Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports WMS.Logic

Partial Public Class MovYardEquipmentTask
    Inherits System.Web.UI.Page

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Session.Remove("YardEquipmentMoveTask")
            Session.Remove("YardEquipmentMoveJob")
            If Not CheckAssigned() Then
                NextClicked(False)
            End If
        End If
    End Sub

    Private Sub MenuClick()
        Try
            Dim UserId As String = WMS.Logic.Common.GetCurrentUser
            If WMS.Logic.TaskManager.isAssigned(UserId, WMS.Lib.TASKTYPE.YARDEQUIPMOVE) Then
                Dim tm As New WMS.Logic.TaskManager(UserId, WMS.Lib.TASKTYPE.YARDEQUIPMOVE)
                tm.ExitTask()
            End If
            Session.Remove("YardEquipmentMoveTask")
            Session.Remove("YardEquipmentMoveJob")
        Catch ex As Exception
        End Try
        Made4Net.Mobile.Common.GoToMenu()
    End Sub

    Private Sub NextClicked(ByVal pTaskConfirmed As Boolean)
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        Dim trans As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
        If Session("YardEquipmentMoveJob") Is Nothing Then
            Try
                Dim tm As New WMS.Logic.TaskManager
                tm.RequestTask(UserId, WMS.Lib.TASKTYPE.YARDEQUIPMOVE)
                CheckAssigned()
            Catch ex As Made4Net.Shared.M4NException
                MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
            Catch ex As Exception
                MessageQue.Enqueue(ex.Message)
            End Try
        Else
            ' Confirm the from location scan
            If pTaskConfirmed Then
                Try
                    Dim oTranslator As New Made4Net.Shared.Translation.Translator
                    If DO1.Value("Confirm") = String.Empty Then
                        MessageQue.Enqueue(oTranslator.Translate("Confirmation cannot be blank"))
                        Return
                    End If
                    Dim oMoveJob As WMS.Logic.YardMovementJob = Session("YardEquipmentMoveJob")
                    Dim oMoveTask As WMS.Logic.YardMovementTask = Session("YardEquipmentMoveTask")
                    'oMoveTask.ConfirmStartMove(DO1.Value("Confirm"), oMoveJob)
                    If oMoveTask.TASKTYPE = WMS.Lib.TASKTYPE.YARDVEHICLEMOVE Then
                        If Not oMoveJob.VehicleId.Equals(DO1.Value("Confirm"), StringComparison.OrdinalIgnoreCase) Then
                            MessageQue.Enqueue(oTranslator.Translate("Vehicle confirmation mismatch"))
                            DO1.Value("Confirm") = ""
                            Return
                        End If
                    Else
                        If Not oMoveJob.TrailerId.Equals(DO1.Value("Confirm"), StringComparison.OrdinalIgnoreCase) Then
                            MessageQue.Enqueue(oTranslator.Translate("Trailer confirmation mismatch"))
                            DO1.Value("Confirm") = ""
                            Return
                        End If
                    End If
                    Response.Redirect(MapVirtualPath("Screens/MovYardEquipmentTask2.aspx"))
                Catch ex As Threading.ThreadAbortException
                Catch ex As Made4Net.Shared.M4NException
                    MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
                    Return
                Catch ex As Exception
                    MessageQue.Enqueue(ex.Message)
                    Return
                End Try
            End If
        End If
    End Sub

    Private Function CheckAssigned() As Boolean
        Dim UserId As String = WMS.Logic.Common.GetCurrentUser
        Dim sTaskId As String = WMS.Logic.TaskManager.getUserAssignedTask(UserId, WMS.Lib.TASKTYPE.YARDEQUIPMOVE)
        If sTaskId <> String.Empty Then
            Dim oYardMoveTask As New WMS.Logic.YardMovementTask(sTaskId)
            Session("YardEquipmentMoveTask") = oYardMoveTask
            If Not oYardMoveTask Is Nothing Then
                Dim oMoveJob As WMS.Logic.YardMovementJob = oYardMoveTask.GetYardMovementJob
                Session("YardEquipmentMoveJob") = oMoveJob
                setAssigned(oMoveJob)
                Return True
            Else
                Session("YardEquipmentMoveJob") = Nothing
                setNotAssigned()
                Return False
            End If
        Else
            Session("YardEquipmentMoveTask") = Nothing
            Session("YardEquipmentMoveJob") = Nothing
            setNotAssigned()
            Return False
        End If
    End Function

    Protected Sub setNotAssigned()
        DO1.Value("Assigned") = "Not Assigned"
        DO1.LeftButtonText = "requesttask"

        DO1.setVisibility("TaskId", False)
        DO1.setVisibility("TaskType", False)
        DO1.setVisibility("VehicleId", False)
        DO1.setVisibility("Trailer", False)
        DO1.setVisibility("YardLocation", False)
        DO1.setVisibility("Confirm", False)
    End Sub

    Protected Sub setAssigned(ByVal oMoveJob As WMS.Logic.YardMovementJob)
        DO1.Value("Assigned") = "Assigned"
        DO1.setVisibility("TaskId", True)
        DO1.setVisibility("TaskType", True)
        DO1.setVisibility("VehicleId", True)
        DO1.setVisibility("Trailer", True)
        DO1.setVisibility("YardLocation", True)
        DO1.setVisibility("Confirm", True)
        DO1.Value("Assigned") = "Assigned"
        DO1.Value("TaskId") = oMoveJob.TaskId
        Dim sSql As String = String.Format("select description from CODELISTDETAIL where CODE = '{0}'", oMoveJob.TaskType)
        DO1.Value("TaskType") = Made4Net.DataAccess.DataInterface.ExecuteScalar(sSql)
        DO1.Value("VehicleId") = oMoveJob.VehicleId
        DO1.Value("Trailer") = oMoveJob.TrailerId
        DO1.Value("YardLocation") = oMoveJob.FromYardLocation
        DO1.LeftButtonText = "Next"
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddLabelLine("Assigned")
        DO1.AddLabelLine("TaskId")
        DO1.AddLabelLine("TaskType")
        DO1.AddLabelLine("VehicleId")
        DO1.AddLabelLine("Trailer")
        DO1.AddLabelLine("YardLocation")
        DO1.AddSpacer()
        DO1.AddTextboxLine("Confirm", "Confirm (equipment id)")
        DO1.DefaultButton = "Next"
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "next"
                NextClicked(True)
            Case "requesttask"
                NextClicked(False)
            Case "menu"
                MenuClick()
        End Select
    End Sub

End Class