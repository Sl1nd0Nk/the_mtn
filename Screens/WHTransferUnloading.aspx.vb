Imports Made4Net.Shared.Web
Imports Made4Net.Mobile

<CLSCompliant(False)> Public Class WHTransferUnloading
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen

    Protected WithEvents ddUOM As Made4Net.WebControls.MobileDropDown
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "ViewState"

    Protected Overrides Function LoadPageStateFromPersistenceMedium() As Object
        Return Session("_ViewState")
    End Function

    Protected Overrides Sub SavePageStateToPersistenceMedium(ByVal viewState As Object)
        Session("_ViewState") = viewState
    End Sub
#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If WMS.Logic.GetCurrentUser Is Nothing Then WMS.Logic.GotoLogin()

        If Not IsPostBack Then SetScreen()
    End Sub

    Private Sub SetScreen()

    End Sub



    Private Sub doNext()

        Dim t As New Made4Net.Shared.Translation.Translator()

        Dim location As String = DO1.Value("LOCATION")
        If String.IsNullOrEmpty(location) Then
            MessageQue.Enqueue(t.Translate("Location can not be empty"))
            Return
        End If

        If Not WMS.Logic.Location.Exists(location) Then
            MessageQue.Enqueue(t.Translate("Location does not exist"))
            Return
        End If

        Dim vehicleId As String = DO1.Value("VEHICLEID")
        If String.IsNullOrEmpty(vehicleId) Then
            MessageQue.Enqueue(t.Translate("VehicleID can not be empty"))
            Return
        End If



        Dim sql As String = ""
        sql = $"select s.shipment from shipment s inner join YARDAPPOINTMENT ya on s.YARDAPPOINTMENTID = ya.APPOINTMENTID where s.status<>'CANCELED' and s.TRANSPORTTYPE ='WHTRANSFER' and datediff(day, getdate(), ya.scheduledate) = 0 and s.vehicle='{vehicleId}' and s.status in ('ATDOCK', '{WMS.Lib.Statuses.Shipment.SHIPPED}') "
        Dim dt As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dt)

        Dim s As Shipment
        If dt.Rows.Count = 0 Then
            MessageQue.Enqueue(t.Translate("No shipment was found"))
            Return
        End If

        s = New WMS.Logic.Shipment(dt.Rows(0)("SHIPMENT"))

        Session()("TransferShipment") = s
        Session()("TransferLocation") = location
        Response.Redirect(MapVirtualPath("Screens/WHTransferUnloading1.aspx"))
    End Sub

    Private Sub doMenu()
        Made4Net.Mobile.Common.GoToMenu()
    End Sub




    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddTextboxLine("VEHICLEID")
        DO1.AddTextboxLine("LOCATION")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Select Case e.CommandText.ToLower
            Case "menu"
                doMenu()
            Case "next"
                doNext()
        End Select
    End Sub





End Class
