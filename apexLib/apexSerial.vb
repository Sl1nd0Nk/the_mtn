﻿Imports System.Data.SqlClient 'Import SQL Capabilities
Imports Made4Net.Shared

Public Class apexSerial
    Public Shared Sub APXSERIAL_AddSerialToLoad(ByVal SerialNumber As String, ByVal SKU As String, ByVal LoadId As String, ByVal UOM As String, ByVal User As String, ByVal weight As Double, ByVal serialStatus As String, ByVal Notes As String)
        Try
            Dim sqlComm As New SqlCommand
            sqlComm.Connection = Made4Net.DataAccess.DataInterface.GetConnection()

            sqlComm.CommandText = "apx_serial_insert"
            sqlComm.CommandType = CommandType.StoredProcedure
            sqlComm.Parameters.AddWithValue("serial", SerialNumber)
            sqlComm.Parameters.AddWithValue("loadID", LoadId)
            sqlComm.Parameters.AddWithValue("SKU", SKU)
            sqlComm.Parameters.AddWithValue("ASNID", "")
            sqlComm.Parameters.AddWithValue("UOM", UOM)
            sqlComm.Parameters.AddWithValue("orderID", "")
            sqlComm.Parameters.AddWithValue("received", True)
            sqlComm.Parameters.AddWithValue("userid", User)
            sqlComm.Parameters.AddWithValue("serialweight", weight)
            sqlComm.Parameters.AddWithValue("serialStatus", serialStatus)
            sqlComm.Parameters.AddWithValue("notes", Notes)
            sqlComm.Parameters.AddWithValue("ancestorID", 0)
            sqlComm.Parameters.AddWithValue("insertedID", 0)
            sqlComm.Parameters("insertedID").Direction = ParameterDirection.InputOutput
            'sqlCon.Open()

            sqlComm.ExecuteNonQuery()
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Sub APAXSERIAL_MoveSerialToLoad(ByVal Serial As String, ByVal id As Integer, ByVal SKU As String, ByVal loadID As String, ByVal breakContainer As Boolean, ByVal parentID As Integer)
        Dim SQL As String

        If breakContainer Then
            SQL = String.Format("Update apx_serial set loadID={0}, parentNodeID=0 where SKU={1} and dispatched={2} and received={3} and serial={4} and id={5}", FormatField(loadID), FormatField(SKU), FormatField(False), FormatField(True), FormatField(Serial), FormatField(id))
            Made4Net.DataAccess.DataInterface.RunSQL(SQL)

            SQL = String.Format("Update apx_serial set serialStatus='BROKEN' where id={0} and SKU={1}", FormatField(parentID), FormatField(SKU))
            Made4Net.DataAccess.DataInterface.RunSQL(SQL)
        Else
            SQL = String.Format("Update apx_serial set loadID={0} where SKU={1} and dispatched={2} and received={3} and serial={4} and id={5}", FormatField(loadID), FormatField(SKU), FormatField(False), FormatField(True), FormatField(Serial), FormatField(id))
            Made4Net.DataAccess.DataInterface.RunSQL(SQL)
        End If
    End Sub

    Public Shared Function APXSERIAL_MoveChildSerials(ByVal SKU As String, ByVal loadID As String, ByVal id As Integer) As Integer
        Dim SQL As String = String.Format("Select * apx_serial where SKU={0} and dispatched={1} and received={2} and parentNodeID={3}", FormatField(SKU), FormatField(False), FormatField(True), FormatField(id))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        If dtLines.Rows.Count > 0 Then
            Dim count As Integer = 0
            SQL = String.Format("Update apx_serial set loadID={0} where SKU={1} and dispatched={2} and received={3} and parentNodeID={4}", FormatField(loadID), FormatField(SKU), FormatField(False), FormatField(True), FormatField(id))
            Made4Net.DataAccess.DataInterface.RunSQL(SQL)

            For Each dr As DataRow In dtLines.Rows
                count += APXSERIAL_MoveChildSerials(SKU, loadID, dr("id"))
            Next

            Return count
        Else
            Return 1
        End If
    End Function

    Public Shared Sub APEXSERIAL_GetLoadSerialCount(ByVal loadID As String, ByRef Count As Integer)
        Dim SQL As String = String.Format("Select * from apx_serial where loadID={0} and dispatched={1} and received={2}", FormatField(loadID), FormatField(False), FormatField(True))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        If dtLines.Rows.Count > 0 Then
            For Each dr As DataRow In dtLines.Rows
                AddChildCount(loadID, Count, dr("id"))
            Next
        Else
            Count = 0
        End If
    End Sub

    Private Shared Sub AddChildCount(ByVal loadID As String, ByRef Count As Integer, ByVal id As Integer)
        Dim SQL As String = String.Format("Select * from apx_serial where loadID={0} and dispatched={1} and received={2} and parentNodeID={3}", FormatField(loadID), FormatField(False), FormatField(True), FormatField(id))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        If dtLines.Rows.Count > 0 Then
            For Each dr As DataRow In dtLines.Rows
                AddChildCount(loadID, Count, dr("id"))
            Next
        Else
            Count += 1
        End If
    End Sub

    Public Shared Function APXSERIAL_GetSerialData(ByVal SerialNumber As String) As DataTable
        Try
            Dim FoundAnything As Boolean = True
            Dim sqlComm As New SqlCommand
            sqlComm.Connection = Made4Net.DataAccess.DataInterface.GetConnection()

            sqlComm.CommandText = "apx_serial_get"
            sqlComm.CommandType = CommandType.StoredProcedure
            sqlComm.Parameters.AddWithValue("serial", SerialNumber)

            Dim dt As New DataTable

            Dim reader As SqlDataReader
            reader = sqlComm.ExecuteReader()
            If reader.HasRows Then
                dt.Columns.Add("serial")
                dt.Columns.Add("serialCount")
                dt.Columns.Add("serialStatus")
                dt.Columns.Add("UOM")
                dt.Columns.Add("dispatched")
                dt.Columns.Add("received")
                dt.Columns.Add("SKU")

                While reader.Read()
                    dt.Rows.Add(reader(0), reader(1), reader(2), reader(3), reader(4), reader(5), reader(6))
                End While
            Else
                FoundAnything = False
            End If

            reader.Close()

            If FoundAnything Then
                Return dt
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Shared Sub APXSERIAL_AddSerialToLoad1(ByVal SerialNumber As String, ByVal SKU As String, ByVal LoadId As String, ByVal UOM As String, ByVal User As String, ByVal weight As Double, ByVal ParentId As Integer, ByVal depth As Integer, ByVal serialStatus As String, ByVal Notes As String)
        Try
            Dim sqlComm As New SqlCommand
            sqlComm.Connection = Made4Net.DataAccess.DataInterface.GetConnection()

            sqlComm.CommandText = "apx_serial_insert"
            sqlComm.CommandType = CommandType.StoredProcedure
            sqlComm.Parameters.AddWithValue("serial", SerialNumber)
            sqlComm.Parameters.AddWithValue("loadID", LoadId)
            sqlComm.Parameters.AddWithValue("SKU", SKU)
            sqlComm.Parameters.AddWithValue("ASNID", "")
            sqlComm.Parameters.AddWithValue("UOM", UOM)
            sqlComm.Parameters.AddWithValue("orderID", "")
            sqlComm.Parameters.AddWithValue("received", True)
            sqlComm.Parameters.AddWithValue("userid", User)
            sqlComm.Parameters.AddWithValue("serialweight", weight)
            sqlComm.Parameters.AddWithValue("serialStatus", serialStatus)
            sqlComm.Parameters.AddWithValue("notes", Notes)
            sqlComm.Parameters.AddWithValue("ancestorID", ParentId)
            sqlComm.Parameters.AddWithValue("insertedID", 0)
            sqlComm.Parameters("insertedID").Direction = ParameterDirection.InputOutput


            'sqlComm.Parameters
            'sqlCon.Open()

            sqlComm.ExecuteNonQuery()
            'sqlComm.Parameters.a()
            Dim returnValue As Integer = sqlComm.Parameters("insertedID").Value

            Dim strTransIns As String
            strTransIns = String.Format("Select * From [apx_ReceiveStation] Where ParentSerial={0}", Made4Net.Shared.FormatField(SerialNumber))
            Dim DtLines As New DataTable
            Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines)

            Dim x As Integer = 0
            Dim dp As Integer = depth + 1
            While x < DtLines.Rows.Count
                APXSERIAL_AddSerialToLoad1(DtLines.Rows(x)("SerialNumber"), DtLines.Rows(x)("SKU"), LoadId, DtLines.Rows(x)("UOM"), User, weight, returnValue, 0, serialStatus, Notes)
                x = x + 1
            End While
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Function APXSERIAL_FindSerialNumber(ByVal SerialNumber As String, ByVal SKU As String) As Integer
        Dim strTransIns As String
        strTransIns = String.Format("Select * From [apx_serial] Where serial={0} and SKU={1}", Made4Net.Shared.FormatField(SerialNumber), Made4Net.Shared.FormatField(SKU))
        Dim DtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines)

        Return DtLines.Rows.Count
    End Function

    Public Shared Function APXSERIAL_UpdateASNSerialNumbers(ByVal ASNID As String, ByVal SKU As String, ByRef LoadId As String, ByVal weight As Double, ByVal user As String) As String
        Try
            Dim strTransIns As String
            strTransIns = String.Format("update [apx_serial] Set received=1, loadID={2}, serialweight={3}, userId={4} where ASNID={0} and SKU={1}", Made4Net.Shared.FormatField(ASNID), Made4Net.Shared.FormatField(SKU), Made4Net.Shared.FormatField(LoadId), Made4Net.Shared.FormatField(weight), Made4Net.Shared.FormatField(user))
            Made4Net.DataAccess.DataInterface.RunSQL(strTransIns)

            strTransIns = String.Format("select TOP (1) loadID FROM [apx_serial] where ASNID={0} and SKU={1}", Made4Net.Shared.FormatField(ASNID), Made4Net.Shared.FormatField(SKU))
            Dim DtLines As New DataTable
            Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines)
            If DtLines.Rows.Count > 0 Then
                LoadId = DtLines.Rows(0)("loadID")
            End If

            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Sub APXSERIAL_AbortASNSerialUpdate(ByVal ASNID As String, ByVal SKU As String)
        Try
            Dim strTransIns As String
            strTransIns = String.Format("update [apx_serial] Set received=0 where ASNID={0} and SKU={1}", Made4Net.Shared.FormatField(ASNID), Made4Net.Shared.FormatField(SKU))
            Made4Net.DataAccess.DataInterface.RunSQL(strTransIns)
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Function APXSERIAL_GetSerialDetailBySerial(ByVal SerialNumber As String) As DataTable
        Dim SQL As String = String.Format("SELECT top(1) * FROM apx_serial WHERE serial={0} ORDER BY id DESC", FormatField(SerialNumber))
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtLines)

        Return dtLines
    End Function

    Public Shared Function APXSERIAL_TransferSerial(ByVal SerialNumber As String, ByVal FromLoad As String, ByVal ToLoad As String, ByVal SKU As String, ByRef SerialQty As Integer) As String
        Dim strTransIns As String
        strTransIns = String.Format("Select * From [apx_serial] Where serial={0} and SKU={1} and loadID={2} and received=1 and dispatched=0", Made4Net.Shared.FormatField(SerialNumber), Made4Net.Shared.FormatField(SKU), Made4Net.Shared.FormatField(FromLoad))
        Dim DtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines)

        If DtLines.Rows.Count >= 1 Then
            Dim breakContainer As Boolean = False
            If DtLines.Rows(0)("parentNodeID") > 0 Then
                breakContainer = True
            End If
            APAXSERIAL_MoveSerialToLoad(SerialNumber, DtLines.Rows(0)("id"), SKU, ToLoad, breakContainer, DtLines.Rows(0)("parentNodeID"))
            SerialQty = APXSERIAL_MoveChildSerials(SKU, ToLoad, DtLines.Rows(0)("id"))
            Return ""
            'Dim DtLines1 As New DataTable
            'strTransIns = String.Format("Select * From [SKUUOM] Where SKU={0} and UOM={1}", Made4Net.Shared.FormatField(SKU), Made4Net.Shared.FormatField(DtLines.Rows(0)("UOM")))
            'Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines1)
            'If (DtLines1.Rows.Count >= 1) Then
            '    'SerialQty = DtLines1.Rows(0)("UNITSPERLOWESTUOM")
            '    If IsDBNull(DtLines1.Rows(0)("UNITSPERLOWESTUOM")) Then
            '        SerialQty = CInt(DtLines1.Rows(0)("UNITSPERMEASURE"))
            '    Else
            '        SerialQty = CInt(DtLines1.Rows(0)("UNITSPERLOWESTUOM"))
            '    End If
            'End If
            'Try
            '    Dim sqlComm As New SqlCommand
            '    sqlComm.Connection = Made4Net.DataAccess.DataInterface.GetConnection()

            '    sqlComm.CommandText = "apx_serial_update_load"
            '    sqlComm.CommandType = CommandType.StoredProcedure
            '    sqlComm.Parameters.AddWithValue("currentLoadID", FromLoad)
            '    sqlComm.Parameters.AddWithValue("newLoadID", ToLoad)
            '    'sqlCon.Open()

            '    sqlComm.ExecuteNonQuery()
            '    Return ""
            'Catch ex As Exception
            '    Return ex.Message
            'End Try
        Else
            Return "Scanned Serialnumber is not found on load " + FromLoad
        End If
    End Function

    Public Shared Sub APXSERIAL_UpdateSerialsIntoDB(ByVal LoadId As String, ByVal SKU As String, ByVal Container As String, ByVal Receipt As String, ByVal ReceiptLine As String, ByVal Weight As String, ByVal UOM As String, ByVal user As String, ByVal serialStatus As String, ByVal notes As String)
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_serial_returns] Where containerID={0} and SKU={1} and receipt={2} and receiptline={3}", Made4Net.Shared.FormatField(Container), Made4Net.Shared.FormatField(SKU), Made4Net.Shared.FormatField(Receipt), Made4Net.Shared.FormatField(ReceiptLine))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        If dtLines.Rows.Count > 0 Then
            Dim SerialStr As String = "("
            Dim x As Integer = 0
            For Each dr As DataRow In dtLines.Rows
                SerialStr = SerialStr & "'" & dr("serial") & "'"
                If (x + 1) < dtLines.Rows.Count Then
                    SerialStr = SerialStr & ","
                End If
                x += 1
            Next
            SerialStr = SerialStr & ")"

            strUnverSerTransIns = String.Format("update apx_serial set dispatched = 1 where dispatched = 0 and serial in {0}", SerialStr)
            Made4Net.DataAccess.DataInterface.RunSQL(strUnverSerTransIns)

            For Each dr As DataRow In dtLines.Rows
                Dim serial As String = dr("serial")

                'Dim RetDtLines As New DataTable
                'Dim strTransIns As String = String.Format("Select * From [apx_serial] Where serial={0} and SKU={1}", Made4Net.Shared.FormatField(serial), Made4Net.Shared.FormatField(SKU))
                'Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, RetDtLines)
                'If RetDtLines.Rows.Count > 0 Then
                'Dim updateQuery As String
                'updateQuery = String.Format("update [apx_serial] Set dispatched={0}, loadID={1}, SKU={2}, ASNID="", orderID="", userid={3}, serialweight={4} where serial={5}", Made4Net.Shared.FormatField(False), Made4Net.Shared.FormatField(LoadId), Made4Net.Shared.FormatField(SKU), Made4Net.Shared.FormatField(user), Made4Net.Shared.FormatField(Weight), Made4Net.Shared.FormatField(serial))
                'Made4Net.DataAccess.DataInterface.RunSQL(updateQuery)
                'Else

                'End If
                APXSERIAL_AddSerialToLoad(serial, SKU, LoadId, UOM, user, CDbl(Weight), serialStatus, notes)
            Next
        End If

    End Sub

End Class
