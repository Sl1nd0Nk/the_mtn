﻿Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports System.Collections.Generic

Public Class apexInventory
    Public Shared Function APXINV_CheckActiveReplens(ByVal user As String, ByRef Depositing As Boolean) As Boolean

        Dim sql As String = String.Format("select * from apx_ReplenRdt where UserId={0}", Made4Net.Shared.FormatField(user))

        Dim NdtReplen As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, NdtReplen)

        If NdtReplen.Rows.Count <= 0 Then
            Depositing = False
            Return False
        Else
            Depositing = True
            Return True
        End If

        Return False
    End Function

    Public Shared Sub APXINV_UpdateReplenStatus(ByVal ReplenId As String)
        Dim strUnverSerTransIns As String = String.Format("UPDATE [apx_ReplenRdt] Set Status={0} where ReplenId={1}", Made4Net.Shared.FormatField("DEPOSIT"), Made4Net.Shared.FormatField(WMS.Logic.GetCurrentUser), Made4Net.Shared.FormatField(ReplenId))
        Try
            Made4Net.DataAccess.DataInterface.RunSQL(strUnverSerTransIns)
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Sub APXINV_QueueReplen(ByVal User As String, ByVal Replen As String, ByVal Units As Integer)
        Try
            Dim sql As String = String.Format("select * from apx_ReplenRdt where ReplenId={0}", Made4Net.Shared.FormatField(Replen))
            Dim dtReplen As New DataTable

            Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)

            If dtReplen.Rows.Count > 0 Then
                Dim strUnverSerTransIns As String = String.Format("UPDATE [apx_ReplenRdt] Set UserId={0}, Qty={1} where ReplenId={2}", Made4Net.Shared.FormatField(User), Made4Net.Shared.FormatField(Units), Made4Net.Shared.FormatField(Replen))
                Made4Net.DataAccess.DataInterface.RunSQL(strUnverSerTransIns)
            Else
                Dim strUnverSerTransIns As String = "INSERT INTO [apx_ReplenRdt]([UserId],[ReplenId],[Qty],[Status])" & _
                                                      "VALUES('" & User & "','" & Replen & "', " & Units & ",'NEW')"
                Made4Net.DataAccess.DataInterface.RunSQL(strUnverSerTransIns)
            End If

            'Try
            '    strUnverSerTransIns = String.Format("UPDATE [TASKS] Set ASSIGNED=1, STATUS={0}, USERID={1} where REPLENISHMENT={2}", Made4Net.Shared.FormatField("ASSIGNED"), Made4Net.Shared.FormatField(WMS.Logic.GetCurrentUser), Made4Net.Shared.FormatField(Replen))
            '    Made4Net.DataAccess.DataInterface.RunSQL(strUnverSerTransIns)
            'Catch ex As Exception
            'End Try
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Sub APXINV_DequeueReplen(ByVal ReplenId As String)
        Try
            Dim sql As String = String.Format("delete from apx_ReplenRdt where UserId={0} and ReplenId={1}", Made4Net.Shared.FormatField(WMS.Logic.Common.GetCurrentUser), Made4Net.Shared.FormatField(ReplenId))
            Made4Net.DataAccess.DataInterface.RunSQL(sql)
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Sub APXINV_ClearReplen(ByVal User As String)
        Try
            Dim sql As String = String.Format("delete from apx_ReplenRdt where UserId={0}", Made4Net.Shared.FormatField(User))
            Made4Net.DataAccess.DataInterface.RunSQL(sql)
        Catch ex As Exception
        End Try
    End Sub

    Public Shared Function APXINV_FindReplenByUser(ByVal User As String) As DataTable
        'Dim sql As String = String.Format("select * from apx_ReplenRdt where UserId={0}", Made4Net.Shared.FormatField(User))
        Dim sql As String = String.Format("select * from TASKS where ASSIGNED=1 and USERID={0} and TASKTYPE LIKE '%REPL'", Made4Net.Shared.FormatField(User))

        Dim dtReplen As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)

        Return dtReplen
    End Function

    Public Shared Function APXINV_GetReplenToLoad(ByVal ToLocation As String) As String
        Dim LoadId As String = ""
        Dim sql As String = String.Format("select * from LOADS where LOCATION={0}", Made4Net.Shared.FormatField(ToLocation))

        Dim dtReplen As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtReplen)

        If dtReplen.Rows.Count >= 1 Then
            LoadId = dtReplen.Rows(0)("LOADID")
        End If

        Return LoadId
    End Function

    Public Shared Function APXINV_CheckIfTableExist(ByVal TableName As String) As Boolean
        Dim Result As Boolean = False
        Dim SQL As String = String.Format("Select * from SYS.OBJECTS where TYPE='U' and NAME={0}", Made4Net.Shared.FormatField(TableName))
        Dim dtData As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(SQL, dtData)

        If dtData.Rows.Count > 0 Then
            Result = True
        End If

        Return Result
    End Function

    Public Shared Sub APXINV_CreateTable(ByVal TableName As String, ByVal columns As List(Of String))
        Dim str As String
        Dim ColumnStr As String
        Dim x As Integer = 1
        For Each str In columns
            If x = 1 Then
                ColumnStr = str
            Else
                ColumnStr = ColumnStr & str
            End If

            If x <> columns.Count Then
                ColumnStr = ColumnStr & ","
            End If
            x += 1
        Next

        Dim SQL As String = String.Format("CREATE TABLE {0}({1})", TableName, ColumnStr)
        Made4Net.DataAccess.DataInterface.RunSQL(SQL)
    End Sub

    Public Shared Function APXINV_GetFailRTLocInProgress(UserID As String) As DataTable
        Dim sql As String = String.Format("select * from apx_separation_station where UserID={0}", Made4Net.Shared.FormatField(UserID))
        Dim Dt As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(sql, Dt, False, Nothing)
        Return Dt
    End Function

    Public Shared Function APXINV_GetLoadUsingLocation(Location As String) As String
        Dim sql As String = String.Format("select loadID from INVLOAD where location = {0}", Made4Net.Shared.FormatField(Location))
        Dim dtHeader As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtHeader, False, Nothing)
        Dim flag As Boolean = dtHeader.Rows.Count > 0
        Dim ReturnValue As String
        If flag Then
            ReturnValue = CStr(dtHeader.Rows(0)("LOADID"))
        Else
            ReturnValue = ""
        End If
        Return ReturnValue
    End Function

    Public Shared Function APXINV_GetLoadSTOriginalQty(LoadID As String) As Integer
        Dim count As Integer = 0
        Dim sql As String = String.Format("select * from apx_ST_Header where Status not in ('CANCELED', 'CANCELLED', 'COMPLETE')")
        Dim dtHeader As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtHeader, False, Nothing)
        Dim flag As Boolean = dtHeader.Rows.Count > 0
        If flag Then
            Dim STID As String = CStr(dtHeader.Rows(0)("Id"))
            sql = String.Format("Select PRECOUNTQTY from apx_Load_St_detail where STID={0} and loadID={1}", Made4Net.Shared.FormatField(STID), Made4Net.Shared.FormatField(LoadID))
            Dim dtQty As DataTable = New DataTable()
            Made4Net.DataAccess.DataInterface.FillDataset(sql, dtQty, False, Nothing)
            flag = (dtQty.Rows.Count > 0)
            If flag Then
                count = CInt(dtQty.Rows(0)("PRECOUNTQTY"))
            End If
        End If
        Return count
    End Function

    Public Shared Function APXINV_GetRTNextFailedLoc() As String
        Dim x As Integer = 0
        Dim sql As String = String.Format("select PKEY1 from ATTRIBUTE where HARVEST={0} and PKEYTYPE={1} and PKEY1<>{2}", Made4Net.Shared.FormatField(0), Made4Net.Shared.FormatField("LOAD"), Made4Net.Shared.FormatField(""))
        Dim dtHeader As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtHeader, False, Nothing)
        Dim flag As Boolean = dtHeader.Rows.Count > 0
        ' The following expression was wrapped in a checked-statement
        Dim ReturnValue As String
        If flag Then
            While x < dtHeader.Rows.Count
                Dim dt As DataTable = apexInventory.APXINV_GetSucessFullSerialsForLoad(CStr(dtHeader.Rows(x)("PKEY1")))
                flag = (dt.Rows.Count > 0)
                If flag Then
                    sql = String.Format("select * from apx_separation_station where LoadID={0}", Made4Net.Shared.FormatField(CStr(dtHeader.Rows(x)("PKEY1"))))
                    Dim dt2 As DataTable = New DataTable()
                    Made4Net.DataAccess.DataInterface.FillDataset(sql, dt2, False, Nothing)
                    flag = (dt2.Rows.Count <= 0)
                    If flag Then
                        ReturnValue = CStr(dtHeader.Rows(x)("PKEY1"))
                        Return ReturnValue
                    End If
                End If
                x += 1
            End While
        End If
        ReturnValue = ""
        Return ReturnValue
    End Function

    Public Shared Function APXINV_GetRepRTNextFailedLoc() As String
        Dim x As Integer = 0
        Dim LocFound As Boolean = False
        Dim sql As String = String.Format("select PKEY1 from ATTRIBUTE where HARVEST={0} and PKEYTYPE={1} and PKEY1<>{2}", Made4Net.Shared.FormatField(0), Made4Net.Shared.FormatField("LOAD"), Made4Net.Shared.FormatField(""))
        Dim dtHeader As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtHeader, False, Nothing)
        Dim flag As Boolean = dtHeader.Rows.Count > 0
        ' The following expression was wrapped in a checked-statement
        Dim ReturnValue As String
        If flag Then
            While x < dtHeader.Rows.Count
                Dim dt As DataTable = apexInventory.APXINV_GetSucessFullSerialsForLoad(CStr(dtHeader.Rows(x)("PKEY1")))
                flag = (dt.Rows.Count <= 0)
                If flag Then
                    sql = String.Format("select * from apx_separation_station where LoadID={0}", Made4Net.Shared.FormatField(dtHeader.Rows(x)("PKEY1")))
                    Dim dt2 As DataTable = New DataTable()
                    Made4Net.DataAccess.DataInterface.FillDataset(sql, dt2, False, Nothing)
                    flag = (dt2.Rows.Count <= 0)
                    If flag Then
                        sql = String.Format("select serial from apx_serial where LoadID={0} and ReturnStatus={1}", Made4Net.Shared.FormatField(dtHeader.Rows(x)("PKEY1")), Made4Net.Shared.FormatField(0))
                        Dim dt3 As DataTable = New DataTable()
                        Made4Net.DataAccess.DataInterface.FillDataset(sql, dt3, False, Nothing)
                        flag = (dt3.Rows.Count > 0)
                        Dim flag2 As Boolean
                        If flag Then
                            For y As Integer = 0 To dt3.Rows.Count - 1
                                sql = String.Format("select * from apx_serial_returns where serial={0}", Made4Net.Shared.FormatField(dt3.Rows(y)("serial")))
                                Dim dt4 As DataTable = New DataTable()
                                Made4Net.DataAccess.DataInterface.FillDataset(sql, dt4, False, Nothing)
                                flag = (dt4.Rows.Count > 0)
                                If flag Then
                                    flag2 = IsDBNull(dt4.Rows(0)("reprocess"))
                                    If flag2 Then
                                        LocFound = True
                                        Exit For
                                    End If
                                End If
                            Next
                        End If
                        flag2 = LocFound
                        If flag2 Then
                            ReturnValue = CStr(dtHeader.Rows(x)("PKEY1"))
                            Return ReturnValue
                        End If
                    End If
                End If
                x += 1
            End While
        End If
        ReturnValue = ""
        Return ReturnValue
    End Function

    Public Shared Sub APXINV_ClearLocFromStation(Location As String, loadID As String, UserId As String)
        Dim sql As String = ""
        Try
            Dim flag As Boolean = Not String.IsNullOrEmpty(Location)
            If flag Then
                sql = String.Format("delete from apx_separation_station where Location={0}", Made4Net.Shared.FormatField(Location))
            Else
                flag = Not String.IsNullOrEmpty(loadID)
                If flag Then
                    sql = String.Format("delete from apx_separation_station where loadID={0}", Made4Net.Shared.FormatField(loadID))
                Else
                    flag = Not String.IsNullOrEmpty(UserId)
                    If flag Then
                        sql = String.Format("delete from apx_separation_station where UserId={0}", Made4Net.Shared.FormatField(UserId))
                    End If
                End If
            End If
            flag = Not String.IsNullOrEmpty(sql)
            If flag Then
                Made4Net.DataAccess.DataInterface.RunSQL(sql)
            End If
        Catch expr_90 As Exception
            Throw expr_90
        End Try
    End Sub

    Public Shared Function APXINV_GetSucessFullSerialsForLoad(LoadID As String) As DataTable
        Dim Sql As String = String.Format("select serial from apx_serial where loadID={0} and ReturnStatus={1}", Made4Net.Shared.FormatField(LoadID), Made4Net.Shared.FormatField(1))
        Dim dt As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(Sql, dt, False, Nothing)
        Return dt
    End Function

    Public Shared Sub APXINV_UpdateCustomsStockTake(loadID As String, qty As Integer, User As String)
        Dim sql As String = String.Format("select * from apx_ST_Header where Status not in ('CANCELED', 'CANCELLED', 'COMPLETE')")
        Dim dtHeader As DataTable = New DataTable()
        Made4Net.DataAccess.DataInterface.FillDataset(sql, dtHeader, False, Nothing)
        Dim flag As Boolean = dtHeader.Rows.Count > 0
        If flag Then
            Dim STID As String = CStr(dtHeader.Rows(0)("Id"))
            sql = String.Format("UPDATE [apx_Load_St_detail] Set LOADCOUNTUSER={0}, POSTCOUNTQTY={1}, RECOUNTS= (CASE WHEN (LOADCOUNTED = 1) THEN RECOUNTS + 1 ELSE 0 END), LOADCOUNTED={4}, LOADCOUNTDATE={5} where STID={2} and loadID={3}", Made4Net.Shared.FormatField(User), Made4Net.Shared.FormatField(qty), Made4Net.Shared.FormatField(STID), Made4Net.Shared.FormatField(loadID), Made4Net.Shared.FormatField(True), Made4Net.Shared.FormatField(DateTime.Now))
            Made4Net.DataAccess.DataInterface.RunSQL(sql)
            sql = String.Format("UPDATE [apx_ST_Header] Set [LoadCompleted]=(select count (*) from apx_Load_St_detail where STID = {0} and LOADCOUNTED ={1}), [Status]= {2} where Id={0}", Made4Net.Shared.FormatField(STID), Made4Net.Shared.FormatField(True), Made4Net.Shared.FormatField("INPROGRESS"))
            Made4Net.DataAccess.DataInterface.RunSQL(sql)
        End If
    End Sub

    Public Shared Function APXINV_ScanSerialAtDeposit() As Boolean

        Dim Value As String = "NO"
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'ScanSerialReplen'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        If Value = "YES" Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function APXINV_CheckWHASuitableMultiRePlenProcess(ByVal WH_area As String) As Boolean
        Dim Value As String = ""
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'ReplenPickFaceKey'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)


        Dim whas As String() = Value.Split(New Char() {","c})

        ' Use For Each loop over words and display them

        Dim wha As String
        For Each wha In whas
            If WH_area = wha Then
                Return True
            End If
        Next

        Return False
    End Function

    Public Shared Function APXINV_AddSerialsDuringStockTake() As Boolean
        Dim Sql As String = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'AddSerialsStockTake'"
        Dim Value As String = CStr(Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql))
        If Value = "YES" Then
            Return True
        End If

        Return False
    End Function

    Public Shared Function APXINV_SerialisedStockTake() As Boolean
        Dim Sql As String = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'SerialisedStockTake'"
        Dim Value As String = CStr(Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql))
        If Value = "YES" Then
            Return True
        End If

        Return False
    End Function

    Public Shared Sub APXINV_CreateSeparationStnData(Location As String, loadID As String, UserID As String, Optional Process As String = "")
        Try
            Dim strUnverSerTransIns As String = String.Concat(New String() {"INSERT INTO apx_separation_station(Location,LoadID,UserID, Process)VALUES('", Location, "','", loadID, "','", UserID, "','", Process, "')"})
            Made4Net.DataAccess.DataInterface.RunSQL(strUnverSerTransIns)
        Catch expr_5B As Exception
            Throw expr_5B
        End Try
    End Sub
End Class
