﻿Imports Made4Net.Shared.Web
Imports Made4Net.Mobile
Imports Made4Net.Shared
Imports Made4Net.DataAccess
Imports WMS.Logic
Imports System.Data.SqlClient 'Import SQL Capabilities

Public Class apexInbound

    'Checks that the weight entered is within the lower/upper inbound weight tolerance
    '   arguments: weightEntered (Double), SKUWeight (Double)
    '   Returns: TRUE or FALSE
    'Inbound Weight Upper and Lower Tolerance values are found in TABLE: apex_wh_sys_param
    Public Shared Function APXINBOUND_CheckWeightInTolerance(ByVal weightEntered As Double, ByVal SKUWeight As Double) As Boolean

        Dim UpperWeightTolerance As Double = 0.0
        Dim LowerWeightTolerance As Double = 0.0
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'InboundWeightUpTol'"
        UpperWeightTolerance = CDbl(Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql))

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'InboundWeightLowTol'"
        LowerWeightTolerance = CDbl(Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql))

        Dim lim_plus As Double = (1 + (UpperWeightTolerance / 100)) * SKUWeight
        Dim lim_minus As Double = (1 - (LowerWeightTolerance / 100)) * SKUWeight

        Return ((lim_minus <= weightEntered) And (weightEntered <= lim_plus))
    End Function

    'Adds a serial number to apex_ReceiveStation for verification
    '   Returns: err exception
    Public Shared Function APXINBOUND_AddSerialToStation(ByVal SerialNumber As String, ByVal User As String, ByVal Receipt As String, ByVal ReceiptLine As String, ByVal Consignee As String, ByVal SKU As String, ByVal NoOfUnits As Integer, ByVal Weight As Double, ByVal UOM As String, ByVal Location As String) As String
        If Not String.IsNullOrEmpty(SerialNumber) Then
            Dim AlreadyScanned As String = APXINBOUND_CheckIfSerialAlreadyScanned(SerialNumber)

            If Not String.IsNullOrEmpty(AlreadyScanned) Then
                Return AlreadyScanned
            Else
                Try
                    Dim strUnverSerTransIns As String
                    strUnverSerTransIns = "INSERT INTO [apx_ReceiveStation]([UserId],[Receipt],[Line],[Consignee],[SKU],[SerialNumber],[Verified],[TotalUnits],[Weight],[UOM],[Location])" & _
                                                      "VALUES('" & User & _
                                                              "','" & Receipt & _
                                                              "','" & ReceiptLine & _
                                                              "','" & Consignee & _
                                                              "','" & SKU & _
                                                              "','" & SerialNumber & _
                                                              "','U','" & NoOfUnits & _
                                                              "','" & Weight & _
                                                              "','" & UOM & _
                                                              "','" & Location & "')"
                    Made4Net.DataAccess.DataInterface.RunSQL(strUnverSerTransIns)
                    Return ""
                Catch ex As Exception
                    Return ex.Message
                End Try
            End If
        Else
            Return "No Serial Number Scanned"
        End If
    End Function

    Public Shared Function APXINBOUND_AddSerialToStationBuild(ByVal SerialNumber As String, ByVal User As String, ByVal Receipt As String, ByVal ReceiptLine As String, ByVal Consignee As String, ByVal SKU As String, ByVal Weight As Double, ByVal UOM As String, ByVal Location As String, ByVal QtyOfUOM As Integer, ByVal LowerUOM As String, ByVal ParentSerial As String, ByVal CurrentQty As Integer, ByVal Type As String, ByVal PalletQty As Integer) As String
        If Not String.IsNullOrEmpty(SerialNumber) Then
            Dim AlreadyScanned As String = APXINBOUND_CheckIfSerialAlreadyScanned(SerialNumber)

            If Not String.IsNullOrEmpty(AlreadyScanned) Then
                Return AlreadyScanned
            Else
                Try
                    Dim strUnverSerTransIns As String
                    strUnverSerTransIns = "INSERT INTO [apx_ReceiveStation]([UserId],[Receipt],[Line],[Consignee],[SKU],[SerialNumber],[Verified],[UOM],[Location],[QtyOfUOM],[LowerUOM],[ParentSerial],[CurrentQty],[Type],[Weight],[TotalUnits])" & _
                                                      "VALUES('" & User & _
                                                              "','" & Receipt & _
                                                              "','" & ReceiptLine & _
                                                              "','" & Consignee & _
                                                              "','" & SKU & _
                                                              "','" & SerialNumber & _
                                                              "','V'," & _
                                                              "'" & UOM & _
                                                              "','" & Location & _
                                                              "'," & QtyOfUOM & _
                                                              ",'" & LowerUOM & _
                                                              "','" & ParentSerial & _
                                                              "'," & CurrentQty & _
                                                              ",'" & Type & "','" & Weight & "'," & PalletQty & ")"
                    Made4Net.DataAccess.DataInterface.RunSQL(strUnverSerTransIns)
                    Return ""
                Catch ex As Exception
                    Return ex.Message
                End Try
            End If
        Else
            Return "No Serial Number Scanned"
        End If
    End Function

    Public Shared Function APXINBOUND_FindSerialsOnStationByState(ByVal User As String, ByVal Receipt As String, ByVal ReceiptLine As String, ByVal Consignee As String, ByVal SKU As String, ByVal Location As String, ByVal State As String) As DataTable
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_ReceiveStation] Where UserId={0}", FormatField(User))

        If Not String.IsNullOrEmpty(State) Then
            strUnverSerTransIns = strUnverSerTransIns + String.Format(" and Receipt={0}", FormatField(Receipt))
        End If

        If Not String.IsNullOrEmpty(State) Then
            strUnverSerTransIns = strUnverSerTransIns + String.Format(" and Line={0}", FormatField(ReceiptLine))
        End If

        If Not String.IsNullOrEmpty(State) Then
            strUnverSerTransIns = strUnverSerTransIns + String.Format(" and Consignee={0}", FormatField(Consignee))
        End If

        If Not String.IsNullOrEmpty(State) Then
            strUnverSerTransIns = strUnverSerTransIns + String.Format(" and SKU={0}", FormatField(SKU))
        End If

        If Not String.IsNullOrEmpty(State) Then
            strUnverSerTransIns = strUnverSerTransIns + String.Format(" and Location={0}", FormatField(Location))
        End If

        If Not String.IsNullOrEmpty(State) Then
            strUnverSerTransIns = strUnverSerTransIns + String.Format(" and Verified={0}", FormatField(State))
        End If
        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        Return dtLines
    End Function

    Public Shared Function APXINBOUND_RemoveSerialNumbersFromStation(ByVal User As String, ByVal Receipt As String, ByVal ReceiptLine As String, ByVal Consignee As String, ByVal SKU As String, ByVal Location As String) As String
        Try
            Dim strUnverSerTransIns As String = String.Format("Delete From [apx_ReceiveStation] Where UserId={0} and Receipt={1} and Line={2} and Consignee={3} and SKU={4} and Location={5}", _
                                                                                                                                                    FormatField(User), _
                                                                                                                                                    FormatField(Receipt), _
                                                                                                                                                    FormatField(ReceiptLine), _
                                                                                                                                                    FormatField(Consignee), _
                                                                                                                                                    FormatField(SKU),
                                                                                                                                                    FormatField(Location))
            DataInterface.RunSQL(strUnverSerTransIns)
            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Function APXINBOUND_RemoveSerialNumbersFromStationByState(ByVal User As String, ByVal Receipt As String, ByVal ReceiptLine As String, ByVal Consignee As String, ByVal SKU As String, ByVal Location As String, ByVal State As String) As String
        Try
            Dim strUnverSerTransIns As String = String.Format("Delete From [apx_ReceiveStation] Where UserId={0} and Receipt={1} and Line={2} and Consignee={3} and SKU={4} and Location={5} and Verified={6}", _
                                                                                                                                                    FormatField(User), _
                                                                                                                                                    FormatField(Receipt), _
                                                                                                                                                    FormatField(ReceiptLine), _
                                                                                                                                                    FormatField(Consignee), _
                                                                                                                                                    FormatField(SKU),
                                                                                                                                                    FormatField(Location),
                                                                                                                                                    FormatField(State))
            DataInterface.RunSQL(strUnverSerTransIns)
            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Shared Sub APXINBOUND_UpdateSerialStatusAtStation(ByVal Status As String, ByVal SerialNumber As String, ByVal Reason As String)
        Dim strUnverSerTransIns As String = String.Format("UPDATE [apx_ReceiveStation] Set Verified={0}, FailedReason={1} where SerialNumber={2}", FormatField(Status), FormatField(Reason), FormatField(SerialNumber))
        DataInterface.RunSQL(strUnverSerTransIns)
    End Sub

    Public Shared Function APXINBOUND_OEMVerifySerialsAtStation(ByVal SerialNumber As String, ByVal SKU As String) As Integer
        Dim strOEMTransIns As String
        strOEMTransIns = String.Format("Select * From [apx_oem_serial] Where serial={0} and SKU={1}", FormatField(SerialNumber), FormatField(SKU))
        Dim OEMDtLinees As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strOEMTransIns, OEMDtLinees)

        Return OEMDtLinees.Rows.Count
    End Function

    Private Shared Function ValidateSerialCheckDigit(ByVal SerialNumber As String) As Boolean
        Dim SerLength As Integer = SerialNumber.Length
        Dim DBL As Boolean = True
        Dim Value As Integer = 0
        Dim ValueStr As String = ""
        Dim NewStr As String = ""

        'Validate if all characters are digits
        Dim AllCharactersAreNumbers As Boolean = True
        Dim ndx As Integer = 0
        While ndx < SerLength
            If Char.IsNumber(SerialNumber, ndx) = False Then
                AllCharactersAreNumbers = False
                Exit While
            End If
        End While

        If AllCharactersAreNumbers = False Then
            Return False
        End If

        ' Strip each digit out from right to left (missing out check digit 
        ' and create a new string by double first one and creating a string
        ' then double every other one and concatenate it to the string   
        For i As Integer = (SerLength - 2) To 0 Step -1
            Dim SerChar0 As Char = SerialNumber(i)
            If (DBL) Then
                Value = 2 * CInt(Val(SerChar0))
            Else
                Value = CInt(Val(SerChar0))
            End If

            DBL = Not DBL
            ValueStr = Value.ToString()

            If NewStr = "" Then
                NewStr = ValueStr
            Else
                NewStr = NewStr & ValueStr
            End If
        Next

        ' Now take each digit of the new string and add them together into a result
        SerLength = NewStr.Length
        Dim Total As Integer = 0

        For i As Integer = 0 To SerLength - 1
            Dim SerChar1 As Char = NewStr(i)

            Total = Total + CInt(Val(SerChar1))
        Next

        ' Extract the Least Significant Digit from the value
        While (Total > 9)
            Total = Total Mod 10
        End While

        ' Check Digit = the LSD of the remainder of 10 - Total
        Total = 10 - Total
        Total = Total Mod 10

        ' Compare the result to the actual check digit
        Dim SerChar As Char = SerialNumber(SerialNumber.Length - 1)
        If Total = CInt(Val(SerChar)) Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Shared Function APXINBOUND_GenarateSerialCheckDigit(ByVal SerialNumber As String) As Integer
        Dim SerLength As Integer = SerialNumber.Length
        Dim DBL As Boolean = True
        Dim Value As Integer = 0
        Dim ValueStr As String = ""
        Dim NewStr As String = ""

        'Validate if all characters are digits
        Dim AllCharactersAreNumbers As Boolean = True
        Dim ndx As Integer = 0
        While ndx < SerLength
            If Char.IsNumber(SerialNumber, ndx) = False Then
                AllCharactersAreNumbers = False
                Exit While
            End If
            ndx += 1
        End While

        If AllCharactersAreNumbers = False Then
            Return -1
        End If

        ' Strip each digit out from right to left (missing out check digit 
        ' and create a new string by double first one and creating a string
        ' then double every other one and concatenate it to the string   
        For i As Integer = (SerLength - 1) To 0 Step -1
            Dim SerChar0 As Char = SerialNumber(i)
            If (DBL) Then
                Value = 2 * CInt(Val(SerChar0))
            Else
                Value = CInt(Val(SerChar0))
            End If

            DBL = Not DBL
            ValueStr = Value.ToString()

            If NewStr = "" Then
                NewStr = ValueStr
            Else
                NewStr = NewStr & ValueStr
            End If
        Next

        ' Now take each digit of the new string and add them together into a result
        SerLength = NewStr.Length
        Dim Total As Integer = 0

        For i As Integer = 0 To SerLength - 1
            Dim SerChar1 As Char = NewStr(i)

            Total = Total + CInt(Val(SerChar1))
        Next

        ' Extract the Least Significant Digit from the value
        While (Total > 9)
            Total = Total Mod 10
        End While

        ' Check Digit = the LSD of the remainder of 10 - Total
        Total = 10 - Total
        Total = Total Mod 10

        Return Total

    End Function

    Public Shared Sub APXINBOUND_UpdateParentSerialToStationBuild(ByVal ParentSerialNumber As String, ByVal Qty As Integer)
        Dim strUnverSerTransIns As String = String.Format("UPDATE [apx_ReceiveStation] Set CurrentQty={0}  where SerialNumber={1}", FormatField(Qty), FormatField(ParentSerialNumber))
        DataInterface.RunSQL(strUnverSerTransIns)

        Dim strTransIns As String = String.Format("Select * From [apx_ReceiveStation] Where SerialNumber={0}", FormatField(ParentSerialNumber))
        Dim DtLinees As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLinees)

        If DtLinees.Rows.Count > 0 Then
            Dim Parent As String = ""

            If Not IsDBNull(DtLinees.Rows(0)("ParentSerial")) Then
                Parent = DtLinees.Rows(0)("ParentSerial")
            End If

            If DtLinees.Rows(0)("QtyOfUOM") <> DtLinees.Rows(0)("CurrentQty") Then
                Parent = ""
            End If

            While Parent <> ""
                strTransIns = String.Format("UPDATE [apx_ReceiveStation] Set CurrentQty=ISNULL(CurrentQty, 0) +1 Where SerialNumber={0}", FormatField(Parent))
                DataInterface.RunSQL(strTransIns)

                strTransIns = String.Format("Select * From [apx_ReceiveStation] Where SerialNumber={0}", FormatField(Parent))
                Dim DtLinees1 As New DataTable
                Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLinees1)
                If DtLinees1.Rows.Count > 0 Then
                    Parent = ""
                    If Not IsDBNull(DtLinees1.Rows(0)("ParentSerial")) Then
                        Parent = DtLinees1.Rows(0)("ParentSerial")
                    End If

                    If DtLinees1.Rows(0)("QtyOfUOM") <> DtLinees1.Rows(0)("CurrentQty") Then
                        Parent = ""
                    End If
                End If
            End While
        End If

    End Sub

    Public Shared Function APXINBOUND_VerifySerialsUsingSerialAttrib(ByVal SerialNumber As String, ByVal SKU As String) As String
        Dim Result As String = "Serial Attributes For SKU Not Configured"
        Dim strTransIns As String
        strTransIns = String.Format("Select * From [apx_Sku_Serial_Attributes] Where SKU={0}", FormatField(SKU))
        Dim DtLinees As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLinees)

        If DtLinees.Rows.Count > 0 Then
            Dim SerLength As Integer = DtLinees.Rows(0)("SERNUMLENGTH")
            Dim SerPrefix As String = DtLinees.Rows(0)("SERNUMPREFIX")
            Dim SerCheckDigit As Boolean = DtLinees.Rows(0)("SERNUMCHECKDIGIT")
            Dim SerialPfx As String = SerialNumber.Substring(0, SerPrefix.Length)

            If SerialPfx <> SerPrefix Then
                Result = "Incorrect Serial Number Prefix"
            ElseIf SerialNumber.Length <> SerLength Then
                Result = "Incorrect Serial Number length"
            Else
                Result = ""
            End If

            If Result = "" And SerCheckDigit Then
                If ValidateSerialCheckDigit(SerialNumber) = False Then
                    Result = "Serial Number Failed Check Digit"
                End If
            End If
        End If
        Return Result
    End Function

    Public Shared Function APXINBOUND_CheckIfSerialAlreadyScanned(ByVal SerialNumber As String) As String
        Dim strOEMTransIns As String
        Dim strResponse As String = ""
        strOEMTransIns = String.Format("Select * From [apx_ReceiveStation] Where SerialNumber={0}", FormatField(SerialNumber))
        Dim OEMDtLinees As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strOEMTransIns, OEMDtLinees)

        If (OEMDtLinees.Rows.Count > 0) Then
            Dim Verified As String = OEMDtLinees.Rows(0)("Verified")

            If Verified(0) <> "F" Then
                Dim User As String = OEMDtLinees.Rows(0)("UserId")
                Dim Receipt As String = OEMDtLinees.Rows(0)("Receipt")
                strResponse = "Serial Number already scanned for receiving by user " + User + " on Receipt " + Receipt

                If Verified(0) = "U" Then
                    strResponse = strResponse + " but not yet verified"
                ElseIf Verified(0) = "V" Then
                    strResponse = strResponse + " and passed verification"
                End If
            Else
                strOEMTransIns = String.Format("Delete From [apx_ReceiveStation] Where SerialNumber={0}", FormatField(SerialNumber))
                DataInterface.RunSQL(strOEMTransIns)
            End If
        End If

        Return strResponse
    End Function

    Public Shared Function APXINBOUND_ValidateScannedASNID(ByVal AsnId As String, ByVal Receipt As String, ByVal ReceiptLine As Integer, ByVal NoOfUnits As Integer, ByVal SKU As String, ByVal UOM As String) As String
        Dim strTransIns As String
        strTransIns = String.Format("Select * From [ASNDETAIL] Where ASNID={0} and RECEIPT={1} and RECEIPTLINE={2}", FormatField(AsnId), FormatField(Receipt), FormatField(ReceiptLine))
        Dim DtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines)

        If DtLines.Rows.Count <= 0 Then
            Return "No ASN found for supplied Id"
        Else
            Dim AsnDtLines As New DataTable
            Dim strTransInsAsn As String = String.Format("Select * From [apx_serial] Where ASNID={0} and SKU={1} and UOM={2} and received=0", FormatField(AsnId), FormatField(SKU), FormatField(UOM))
            Made4Net.DataAccess.DataInterface.FillDataset(strTransInsAsn, AsnDtLines)
            If AsnDtLines.Rows.Count <> NoOfUnits Then
                Return "ASN quatity does not match supplied quantity"
            Else
                Return ""
            End If

        End If
    End Function

    Public Shared Function APXINBOUND_FindASNUsingIDAndSku(ByVal AsnId As String, ByVal SKU As String, ByRef Receipt As String, ByRef ReceiptLine As Integer) As String
        Dim strTransIns As String
        strTransIns = String.Format("Select * From [ASNDETAIL] Where ASNID={0}", FormatField(AsnId))
        Dim DtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines)

        Receipt = ""
        ReceiptLine = -1

        If DtLines.Rows.Count <= 0 Then
            Return "No ASN found for supplied Id"
        Else
            Dim AsnDtLines As New DataTable
            Dim strTransInsAsn As String = String.Format("Select * From [apx_serial] Where ASNID={0} and SKU={1} and received=0", FormatField(AsnId), FormatField(SKU))
            Made4Net.DataAccess.DataInterface.FillDataset(strTransInsAsn, AsnDtLines)
            If AsnDtLines.Rows.Count > 0 Then
                Receipt = DtLines.Rows(0)("RECEIPT")
                ReceiptLine = DtLines.Rows(0)("RECEIPTLINE")
                Return ""
            Else
                Return "No ASN Serial Numbers found for scanned ASN ID"
            End If

        End If
    End Function

    Public Shared Function APXINBOUND_SkuGroupSuitableForASN(ByVal SkuGroup As String) As Boolean
        Dim Value As String = ""
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'SkuGroupForASN'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)


        Dim SkuGroups As String() = Value.Split(New Char() {","c})

        ' Use For Each loop over words and display them

        Dim skugrp As String
        For Each skugrp In SkuGroups
            If SkuGroup = skugrp Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Shared Function APXINBOUND_MaxSerialScansBeforeValidation() As Integer

        Dim Value As String = ""
        Dim Sql As String = ""

        Sql = "select wh_param_value from apx_wh_sys_param where wh_param_code = 'SerialScansPriorValidation'"
        Value = Made4Net.DataAccess.DataInterface.ExecuteScalar(Sql)

        If IsDBNull(Value) Then
            Return 0
        Else
            Return Convert.ToInt32(Value)
        End If
    End Function

    Public Shared Function APXINBOUND_FindUOMsForSku(ByVal sku As String) As DataTable
        Dim DtLines As New DataTable

        Dim strTransIns As String
        strTransIns = String.Format("Select * From [SKUUOM] Where SKU={0}", FormatField(sku))
        Made4Net.DataAccess.DataInterface.FillDataset(strTransIns, DtLines)

        Return DtLines

    End Function

    Public Shared Sub APXINBOUND_FindOnStationUncompletedUOM(ByVal sku As String, ByVal user As String, ByVal UOM As String, ByVal QtyOfUOM As Integer, ByVal receipt As String, ByVal receiptline As String, ByRef SerialNumber As String, ByRef Qtyscanned As Integer, ByRef LowerUOM As String)
        Dim strOEMTransIns As String
        Dim strResponse As String = ""
        strOEMTransIns = String.Format("Select * From [apx_ReceiveStation] Where UOM={0} and sku={1} and qtyOfUOM={2} and userId={3} and receipt={4} and line={5} and CurrentQty<>{6}", FormatField(UOM), _
                                                                                                                                                        FormatField(sku), _
                                                                                                                                                        FormatField(QtyOfUOM), _
                                                                                                                                                        FormatField(user), _
                                                                                                                                                        FormatField(receipt), _
                                                                                                                                                        FormatField(receiptline), _
                                                                                                                                                        FormatField(QtyOfUOM))
        Dim OEMDtLinees As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strOEMTransIns, OEMDtLinees)

        If OEMDtLinees.Rows.Count >= 1 Then
            SerialNumber = OEMDtLinees.Rows(0)("SerialNumber")
            Qtyscanned = OEMDtLinees.Rows(0)("CurrentQty")
            LowerUOM = OEMDtLinees.Rows(0)("LowerUOM")
        Else
            SerialNumber = ""
            Qtyscanned = 0
            LowerUOM = ""
        End If
    End Sub

    Public Shared Function APXINBOUND_GetLowestUOMCount(ByVal sku As String, ByVal receipt As String, ByVal receiptline As String, ByVal user As String) As Integer
        Dim strOEMTransIns As String
        strOEMTransIns = String.Format("Select * From [apx_ReceiveStation] Where LowerUOM={0} and sku={1} and receipt={2} and line={3} and userId={4}", FormatField(""), _
                                                                                                                                                        FormatField(sku), _
                                                                                                                                                        FormatField(receipt), _
                                                                                                                                                        FormatField(receiptline), _
                                                                                                                                                        FormatField(user))
        Dim OEMDtLinees As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strOEMTransIns, OEMDtLinees)

        Return OEMDtLinees.Rows.Count
    End Function

    Public Shared Function APXINBOUND_FindParentSerialsOnStation(ByVal User As String, ByVal Receipt As String, ByVal ReceiptLine As String, ByVal Consignee As String, ByVal SKU As String, ByVal Location As String) As DataTable
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_ReceiveStation] Where UserId={0} and Receipt={1} and Line={2} and Consignee={3} and SKU={4} and Location={5} and ParentSerial={6}", FormatField(User), FormatField(Receipt), FormatField(ReceiptLine), FormatField(Consignee), FormatField(SKU), FormatField(Location), FormatField(""))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        Return dtLines
    End Function

    Public Shared Function APXINBOUND_FindReturnByContainer(ByVal ContainerID As String) As DataTable
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_serial_returns] Where containerID={0} and complete=1 and loadID IS NULL", FormatField(ContainerID))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        Return dtLines
    End Function

    Public Shared Function APXINBOUND_FindReturnByContainerSKU(ByVal SKU As String, ByVal ContainerID As String) As DataTable
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_serial_returns] Where containerID={0} and SKU={1} and complete=1 and loadID IS NULL", FormatField(ContainerID), FormatField(SKU))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        Return dtLines
    End Function

    Public Shared Function APXINBOUND_FindReturnByContainerSKUSerial(ByVal Serial As String, ByVal SKU As String, ByVal ContainerID As String) As DataTable
        Dim strUnverSerTransIns As String
        strUnverSerTransIns = String.Format("Select * From [apx_serial_returns] Where containerID={0} and serial={1} and complete=1 and loadID IS NULL", FormatField(ContainerID), FormatField(Serial))

        Dim dtLines As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(strUnverSerTransIns, dtLines)

        Return dtLines
    End Function

End Class
