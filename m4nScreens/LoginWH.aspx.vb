Imports WMS.Logic

<CLSCompliant(False)> Public Class LoginWH
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Methods"

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not IsPostBack Then
            Dim SQL As String = "SELECT * FROM USERWAREHOUSE WHERE USERID='" & Logic.GetCurrentUser & "'"
            Dim dt As New DataTable
            Made4Net.DataAccess.DataInterface.FillDataset(SQL, dt, False, "Made4NetSchema")
            'Dim dr As DataRow
            '--added by oded 190313--start 
            'Try
            '    If Not ShiftInstance.checkUserClockinShift(Logic.GetCurrentUser) And WMS.Logic.GetSysParam("AutoClockinonLogin") = "1" Then
            '        ShiftInstance.ClockUser(Logic.GetCurrentUser, WMS.Lib.Shift.ClockStatus.IN, "")
            '    End If
            'Catch ex As Made4Net.Shared.M4NException

            '    ' Screen1.Warn(ex.Message)
            '    Made4Net.Mobile.MessageQue.Enqueue(ex.GetErrMessage())
            '    ' Return
            '    If dt.Rows.Count = 1 Then
            '        Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/Login.aspx?ExWasTrown=1"))
            '    Else
            '        ContinueToScreenByDtRowsCount(dt)
            '    End If
            'End Try
          
            ContinueToScreenByDtRowsCount(dt)
           
        End If
    End Sub
    Private Sub ContinueToScreenByDtRowsCount(ByRef dt As DataTable)
        If dt.Rows.Count = 0 Then
            Session()("NoWarehouseAssigned") = "1"
            Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/Login.aspx"))
        ElseIf dt.Rows.Count = 1 Then
            doNext(dt.Rows(0)("warehouse"))
            'Warehouse.setCurrentWarehouse(dt.Rows(0)("warehouse"))
            'Made4Net.DataAccess.DataInterface.ConnectionName = Warehouse.WarehouseConnection
            'If Session("MHType") = "" Then
            '    Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/LoginHQT.aspx"))
            'Else
            '    Dim oWHActivity As New WMS.Logic.WHActivity
            '    oWHActivity.ACTIVITY = "Login"
            '    oWHActivity.LOCATION = Session("LoginLocation")
            '    oWHActivity.USERID = Logic.GetCurrentUser
            '    oWHActivity.HETYPE = Session("MHType")
            '    oWHActivity.ACTIVITYTIME = DateTime.Now
            '    oWHActivity.ADDDATE = DateTime.Now
            '    oWHActivity.EDITDATE = DateTime.Now
            '    oWHActivity.ADDUSER = Logic.GetCurrentUser
            '    oWHActivity.EDITUSER = Logic.GetCurrentUser
            '    oWHActivity.HANDLINGEQUIPMENTID = Session("MHEID")
            '    oWHActivity.TERMINALTYPE = Session("TERMINALTYPE")
            '    Session("ActivityID") = oWHActivity.Post()
            '    Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("Screens/Main.aspx"))
            'End If
        Else
            Dim dd As Made4Net.WebControls.MobileDropDown
            dd = DO1.Ctrl("WHLIST")
            dd.ConnectionName = "Made4NetSchema"
            dd.AllOption = False
            dd.TableName = "vUSERWAREHOUSE"
            dd.ValueField = "WAREHOUSE"
            dd.TextField = "WAREHOUSENAME"
            dd.Where = "USERID = '" & Logic.GetCurrentUser & "'"
            dd.DataBind()
        End If
        dt.Dispose()
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddDropDown("WHLIST")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        If e.CommandText.ToLower = "warehouseselect" Then
            doNext(DO1.Value("WHLIST"))
            'Warehouse.setCurrentWarehouse(DO1.Value("WHLIST"))
            'Made4Net.DataAccess.DataInterface.ConnectionName = Warehouse.WarehouseConnection
            'If Session("MHType") = "" Then
            '    Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/LoginHQT.aspx"))
            'Else
            '    Dim oWHActivity As New WMS.Logic.WHActivity
            '    oWHActivity.ACTIVITY = "Login"
            '    oWHActivity.LOCATION = Session("LoginLocation")
            '    oWHActivity.USERID = Logic.GetCurrentUser
            '    oWHActivity.HETYPE = Session("MHType")
            '    oWHActivity.ACTIVITYTIME = DateTime.Now
            '    oWHActivity.ADDDATE = DateTime.Now
            '    oWHActivity.EDITDATE = DateTime.Now
            '    oWHActivity.ADDUSER = Logic.GetCurrentUser
            '    oWHActivity.EDITUSER = Logic.GetCurrentUser
            '    oWHActivity.HANDLINGEQUIPMENTID = Session("MHEID")
            '    oWHActivity.TERMINALTYPE = Session("TERMINALTYPE")
            '    Session("ActivityID") = oWHActivity.Post()
            '    Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("Screens/Main.aspx"))
            'End If
            'Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/LoginHQT.aspx"))
        End If
    End Sub

    Private Sub doNext(ByVal pWarehouse As String)
        Warehouse.setCurrentWarehouse(pWarehouse)
        Made4Net.DataAccess.DataInterface.ConnectionName = Warehouse.WarehouseConnection

        Try
            If Not ShiftInstance.checkUserClockinShift(Logic.GetCurrentUser) And WMS.Logic.GetSysParam("AutoClockinonLogin") = "1" Then
                ShiftInstance.ClockUser(Logic.GetCurrentUser, WMS.Lib.Shift.ClockStatus.IN, "")
            End If
        Catch ex As Made4Net.Shared.M4NException
            Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate(ex.GetErrMessage()))
            Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/Login.aspx?ExWasTrown=1"))
        Catch ex As Exception
            Made4Net.Mobile.MessageQue.Enqueue(ex.ToString())
            Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/Login.aspx?ExWasTrown=1"))
        End Try




        If Session("MHType") = "" Then
            Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/LoginHQT.aspx"))
        Else
            Dim oWHActivity As New WMS.Logic.WHActivity
            oWHActivity.ACTIVITY = "Login"
            oWHActivity.LOCATION = Session("LoginLocation")
            oWHActivity.USERID = Logic.GetCurrentUser
            oWHActivity.HETYPE = Session("MHType")
            oWHActivity.ACTIVITYTIME = DateTime.Now
            oWHActivity.ADDDATE = DateTime.Now
            oWHActivity.EDITDATE = DateTime.Now
            oWHActivity.ADDUSER = Logic.GetCurrentUser
            oWHActivity.EDITUSER = Logic.GetCurrentUser
            oWHActivity.HANDLINGEQUIPMENTID = Session("MHEID")
            oWHActivity.TERMINALTYPE = Session("TERMINALTYPE")
            Session("ActivityID") = oWHActivity.Post()
            Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("Screens/Main.aspx"))
        End If
    End Sub

#End Region

End Class
