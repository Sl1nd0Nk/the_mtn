'Imports Made4Net.WebControls
'Imports WMS.Logic
'Imports Made4Net.DataAccess

'<CLSCompliant(False)> Public Class Login
'    Inherits System.Web.UI.Page

'#Region " Web Form Designer Generated Code "

'    'This call is required by the Web Form Designer.
'    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

'    End Sub
'    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
'    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm
'    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

'    'NOTE: The following placeholder declaration is required by the Web Form Designer.
'    'Do not delete or move it.
'    Private designerPlaceholderDeclaration As System.Object

'    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
'        'CODEGEN: This method call is required by the Web Form Designer
'        'Do not modify it using the code editor.
'        InitializeComponent()
'    End Sub

'#End Region

'    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
'        If Not IsPostBack Then
'            If String.IsNullOrEmpty(Request.QueryString("ExWasTrown")) Then
'                Made4Net.Mobile.MessageQue.Clear()
'            End If

'            Dim lang As Int32 = WMS.Logic.GetSysParam("DEFAULTLANGUAGE")
'            Made4Net.Shared.Translation.Translator.CurrentLanguageID = lang
'            ClientScript.RegisterStartupScript(Page.GetType, "", DO1.SetFocusElement("DO1:UserIdval:tb"))
'        End If
'        If Not String.IsNullOrEmpty(Session()("NoWarehouseAssigned")) Then
'            Session()("NoWarehouseAssigned") = Nothing
'            Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
'            Made4Net.Mobile.MessageQue.Enqueue(t.Translate("User is not assigned to any warehouse"))
'        End If
'        If Request.QueryString("Skin") <> "" Then
'            Dim skinStr As String = Request.QueryString("Skin")
'            Dim skinCookie As New System.Web.HttpCookie("UserSkin", skinStr)
'            skinCookie.Expires = DateTime.MaxValue
'            Response.Cookies.Add(skinCookie)
'            Made4Net.Mobile.MobileSkinManager.ChangeSkin(Request.QueryString("Skin"))
'            Session("Skin") = Request.QueryString("Skin")
'        ElseIf Session("Skin") <> "" Then
'            Made4Net.Mobile.MobileSkinManager.ChangeSkin(Session("Skin"))
'        Else
'            Try
'                Dim skinCookie As System.Web.HttpCookie = Request.Cookies("UserSkin")
'                If Not skinCookie Is Nothing AndAlso Not String.IsNullOrEmpty(skinCookie.Value) Then
'                    Made4Net.Mobile.MobileSkinManager.ChangeSkin(skinCookie.Value)
'                    Session("Skin") = skinCookie.Value
'                Else
'                    Dim skin As String = WMS.Logic.GetSysParam("DEFAULTSKIN")
'                    Made4Net.Mobile.MobileSkinManager.ChangeSkin(skin)
'                End If
'            Catch ex As Exception
'            End Try
'        End If
'    End Sub

'    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
'        DO1.AddTextboxLine("UserId")
'        DO1.AddTextboxLine("Password", "Password", "", True)
'        DO1.AddTextboxLine("Location")
'    End Sub

'    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
'        Dim MoveToMain As Boolean = False
'        Dim MoveToWH As Boolean = False
'        Dim MoveToMHType As Boolean = False
'        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

'        If e.CommandText.ToLower = "login" Then
'            Try
'                Made4Net.Shared.Authentication.User.Login(DO1.Value("UserId"), DO1.Value("Password"))
'                Dim hasLicense As Boolean = False
'                hasLicense = DataInterface.IsLicensedUser(Made4Net.DataAccess.LicenseUtils.ConnectionContext.HttpSession)

'                If Not hasLicense Then
'                    Made4Net.Mobile.MessageQue.Enqueue(t.Translate("No License Was Found. Please Contact Made4Net Global Support Team!"))
'                    Return
'                End If
'            Catch ex As Made4Net.Shared.M4NException
'                Made4Net.Mobile.MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
'                'MoveToMHType = True
'                Return
'            Catch ex As Exception
'                Made4Net.Mobile.MessageQue.Enqueue(t.Translate(ex.Message))
'                Return
'            End Try
'            Try
'                Session("LoginLocation") = DO1.Value("Location")
'                Dim whSQL As String = "SELECT * FROM USERWAREHOUSE WHERE USERID='" & Logic.GetCurrentUser & "' AND WAREHOUSE='" & Request.QueryString("WH") & "'"
'                Dim dt As New DataTable
'                Made4Net.DataAccess.DataInterface.FillDataset(whSQL, dt, False, "Made4NetSchema")
'                'Dim dr As DataRow
'                If Session("WH") = "" Then
'                    If dt.Rows.Count = 0 Then
'                        Session("WH") = ""
'                        MoveToWH = True
'                    Else
'                        Session("WH") = Request.QueryString("WH")
'                        MoveToWH = False
'                    End If
'                Else
'                    MoveToWH = False
'                End If
'                MoveToMHType = SetMHEID()
'                If MoveToMHType Or MoveToWH Then
'                    MoveToMain = False
'                Else
'                    MoveToMain = True
'                End If
'            Catch ex As Made4Net.Shared.M4NException
'                Made4Net.Mobile.MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
'                MoveToMHType = True
'            Catch ex As ApplicationException
'                Made4Net.Mobile.MessageQue.Enqueue(t.Translate(ex.Message))
'                MoveToMHType = True
'            Catch ex As Exception
'                Made4Net.Mobile.MessageQue.Enqueue(t.Translate(ex.Message))
'                MoveToMHType = True
'            End Try
'            Dim _user As New WMS.Logic.User(DO1.Value("UserId"))
'            Try
'                Session.Timeout = Convert.ToInt32(WMS.Logic.GetSysParam("RDTLOGOFFTIME"))
'            Catch ex As Exception
'            End Try
'            Dim lang As Int32
'            Try
'                lang = Int32.Parse(_user.LANGUAGE)
'            Catch ex As Exception
'            End Try
'            Made4Net.Shared.Translation.Translator.CurrentLanguageID = lang
'            If MoveToWH Then
'                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/LoginWH.aspx"))
'            End If
'            If MoveToMHType Then
'                Warehouse.setCurrentWarehouse(Session("WH"))
'                Made4Net.DataAccess.DataInterface.ConnectionName = Warehouse.WarehouseConnection
'                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/LoginHQT.aspx"))
'            End If

'            If MoveToMain Then
'                Warehouse.setCurrentWarehouse(Session("WH"))
'                Made4Net.DataAccess.DataInterface.ConnectionName = Warehouse.WarehouseConnection
'                'Add the Login as an activity of the user
'                Dim oWHActivity As New WMS.Logic.WHActivity
'                oWHActivity.ACTIVITY = "Login"
'                oWHActivity.LOCATION = Session("LoginLocation")
'                oWHActivity.USERID = DO1.Value("UserId")
'                oWHActivity.HETYPE = Session("MHType")
'                oWHActivity.ACTIVITYTIME = DateTime.Now
'                oWHActivity.ADDDATE = DateTime.Now
'                oWHActivity.EDITDATE = DateTime.Now
'                oWHActivity.ADDUSER = DO1.Value("UserId")
'                oWHActivity.EDITUSER = DO1.Value("UserId")
'                oWHActivity.HANDLINGEQUIPMENTID = Session("MHEID")
'                oWHActivity.TERMINALTYPE = Session("TERMINALTYPE")
'                Session("ActivityID") = oWHActivity.Post()
'                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("Screens/Main.aspx"))
'            Else
'                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/LoginWH.aspx"))
'            End If
'        End If
'    End Sub

'    Private Function SetMHEID() As Boolean
'        If Session("TERMINALTYPE") = "" Then
'            If Request.QueryString("TERMINALTYPE") = "" Then
'                Session("TERMINALTYPE") = ""
'            Else
'                Session("TERMINALTYPE") = Request.QueryString("TERMINALTYPE")
'            End If
'        End If
'        Dim MoveToMHType As Boolean
'        If Session("MHEID") = "" Then
'            If Request.QueryString("MHEID") = "" Then
'                Session("MHEID") = ""
'                MoveToMHType = True
'            Else
'                Session("MHEID") = Request.QueryString("MHEID")
'                MoveToMHType = False
'                SetMHType(Session("MHEID"))
'                Return MoveToMHType
'            End If
'        Else
'            MoveToMHType = False
'            SetMHType(Session("MHEID"))
'            Return MoveToMHType
'        End If
'        If Session("MHType") = "" Then
'            If Request.QueryString("MHTYPE") = "" Then
'                Session("MHType") = ""
'                MoveToMHType = True
'            Else
'                Session("MHType") = Request.QueryString("MHTYPE")
'                MoveToMHType = False
'            End If
'        Else
'            MoveToMHType = False
'        End If
'        Return MoveToMHType
'    End Function

'    Private Sub SetMHType(ByVal MHEID As String)
'        Dim mhSQL As String = "SELECT isnull(LABELPRINTER,'') as LABELPRINTER, isnull(MHETYPE,'') as MHETYPE  FROM MHE WHERE MHEID='" & MHEID & "'"
'        Dim dtMH As New DataTable
'        Made4Net.DataAccess.DataInterface.FillDataset(mhSQL, dtMH)
'        If dtMH.Rows.Count = 1 Then
'            Session("MHType") = dtMH.Rows(0)("MHETYPE")
'            Session("MHEIDLabelPrinter") = dtMH.Rows(0)("LABELPRINTER")
'        Else
'            Throw New Made4Net.Shared.M4NException(New Exception, "MHE ID not found", "MHE ID not found")
'        End If
'    End Sub

'End Class

Imports Made4Net.WebControls
Imports WMS.Logic
Imports Made4Net.DataAccess

<CLSCompliant(False)> Public Class Login
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    <CLSCompliant(False)> Protected WithEvents Screen1 As WMS.MobileWebApp.WebCtrls.Screen
    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm
    Protected WithEvents DO1 As Made4Net.Mobile.WebCtrls.DataObject

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            If String.IsNullOrEmpty(Request.QueryString("ExWasTrown")) Then
                Made4Net.Mobile.MessageQue.Clear()
            End If

            Dim lang As Int32 = WMS.Logic.GetSysParam("DEFAULTLANGUAGE")
            Made4Net.Shared.Translation.Translator.CurrentLanguageID = lang
            ClientScript.RegisterStartupScript(Page.GetType, "", DO1.SetFocusElement("DO1:UserIdval:tb"))
        End If
        If Not String.IsNullOrEmpty(Session()("NoWarehouseAssigned")) Then
            Session()("NoWarehouseAssigned") = Nothing
            Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)
            Made4Net.Mobile.MessageQue.Enqueue(t.Translate("User is not assigned to any warehouse"))
        End If
        If Request.QueryString("Skin") <> "" Then
            Dim skinStr As String = Request.QueryString("Skin")
            Dim skinCookie As New System.Web.HttpCookie("UserSkin", skinStr)
            skinCookie.Expires = DateTime.MaxValue
            Response.Cookies.Add(skinCookie)
            Made4Net.Mobile.MobileSkinManager.ChangeSkin(Request.QueryString("Skin"))
            Session("Skin") = Request.QueryString("Skin")
        ElseIf Session("Skin") <> "" Then
            Made4Net.Mobile.MobileSkinManager.ChangeSkin(Session("Skin"))
        Else
            Try
                Dim skinCookie As System.Web.HttpCookie = Request.Cookies("UserSkin")
                If Not skinCookie Is Nothing AndAlso Not String.IsNullOrEmpty(skinCookie.Value) Then
                    Made4Net.Mobile.MobileSkinManager.ChangeSkin(skinCookie.Value)
                    Session("Skin") = skinCookie.Value
                Else
                    Dim skin As String = WMS.Logic.GetSysParam("DEFAULTSKIN")
                    Made4Net.Mobile.MobileSkinManager.ChangeSkin(skin)
                End If
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub DO1_CreatedChildControls(ByVal sender As Object, ByVal e As System.EventArgs) Handles DO1.CreatedChildControls
        DO1.AddTextboxLine("UserId")
        DO1.AddTextboxLine("Password", "Password", "", True)
        DO1.AddTextboxLine("Location")
    End Sub

    Private Sub DO1_ButtonClick(ByVal sender As Object, ByVal e As Made4Net.Mobile.WebCtrls.ButtonClickEventArgs) Handles DO1.ButtonClick
        Dim MoveToMain As Boolean = False
        Dim MoveToWH As Boolean = False
        Dim MoveToMHType As Boolean = False
        Dim t As New Made4Net.Shared.Translation.Translator(Made4Net.Shared.Translation.Translator.CurrentLanguageID)

        If e.CommandText.ToLower = "login" Then
            Try
                Made4Net.Shared.Authentication.User.Login(DO1.Value("UserId"), DO1.Value("Password"))
                Dim hasLicense As Boolean = False
                hasLicense = DataInterface.IsLicensedUser(Made4Net.DataAccess.LicenseUtils.ConnectionContext.HttpSession)

                If Not hasLicense Then
                    Made4Net.Mobile.MessageQue.Enqueue(t.Translate("No License Was Found. Please Contact Made4Net Global Support Team!"))
                    Return
                End If
            Catch ex As Made4Net.Shared.M4NException
                Made4Net.Mobile.MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
                'MoveToMHType = True
                Return
            Catch ex As Exception
                Made4Net.Mobile.MessageQue.Enqueue(t.Translate(ex.Message))
                Return
            End Try
            Try
                Session("LoginLocation") = DO1.Value("Location")
                Dim whSQL As String = "SELECT * FROM USERWAREHOUSE WHERE USERID='" & Logic.GetCurrentUser & "' AND WAREHOUSE='" & Request.QueryString("WH") & "'"
                Dim dt As New DataTable
                Made4Net.DataAccess.DataInterface.FillDataset(whSQL, dt, False, "Made4NetSchema")
                'Dim dr As DataRow
                If Session("WH") = "" Then
                    If dt.Rows.Count = 0 Then
                        Session("WH") = ""
                        MoveToWH = True
                    Else
                        Session("WH") = Request.QueryString("WH")
                        MoveToWH = False
                    End If
                Else
                    MoveToWH = False
                End If
                MoveToMHType = SetMHEID()
                If MoveToMHType Or MoveToWH Then
                    MoveToMain = False
                Else
                    MoveToMain = True
                End If
            Catch ex As Made4Net.Shared.M4NException
                Made4Net.Mobile.MessageQue.Enqueue(ex.GetErrMessage(Made4Net.Shared.Translation.Translator.CurrentLanguageID))
                MoveToMHType = True
            Catch ex As ApplicationException
                Made4Net.Mobile.MessageQue.Enqueue(t.Translate(ex.Message))
                MoveToMHType = True
            Catch ex As Exception
                Made4Net.Mobile.MessageQue.Enqueue(t.Translate(ex.Message))
                MoveToMHType = True
            End Try
            Dim _user As New WMS.Logic.User(DO1.Value("UserId"))
            Try
                Session.Timeout = Convert.ToInt32(WMS.Logic.GetSysParam("RDTLOGOFFTIME"))
            Catch ex As Exception
            End Try
            Dim lang As Int32
            Try
                lang = Int32.Parse(_user.LANGUAGE)
            Catch ex As Exception
            End Try
            Made4Net.Shared.Translation.Translator.CurrentLanguageID = lang
            If MoveToWH Then
                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/LoginWH.aspx"))
            End If
            If MoveToMHType Then
                Warehouse.setCurrentWarehouse(Session("WH"))
                Made4Net.DataAccess.DataInterface.ConnectionName = Warehouse.WarehouseConnection
                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/LoginHQT.aspx"))
            End If

            If MoveToMain Then
                Warehouse.setCurrentWarehouse(Session("WH"))
                Made4Net.DataAccess.DataInterface.ConnectionName = Warehouse.WarehouseConnection
                'Add the Login as an activity of the user
                Dim oWHActivity As New WMS.Logic.WHActivity
                oWHActivity.ACTIVITY = "Login"
                oWHActivity.LOCATION = Session("LoginLocation")
                oWHActivity.USERID = DO1.Value("UserId")
                oWHActivity.HETYPE = Session("MHType")
                oWHActivity.ACTIVITYTIME = DateTime.Now
                oWHActivity.ADDDATE = DateTime.Now
                oWHActivity.EDITDATE = DateTime.Now
                oWHActivity.ADDUSER = DO1.Value("UserId")
                oWHActivity.EDITUSER = DO1.Value("UserId")
                oWHActivity.HANDLINGEQUIPMENTID = Session("MHEID")
                oWHActivity.TERMINALTYPE = Session("TERMINALTYPE")
                Session("ActivityID") = oWHActivity.Post()
                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("Screens/Main.aspx"))
            Else
                Response.Redirect(Made4Net.Shared.Web.MapVirtualPath("m4nScreens/LoginWH.aspx"))
            End If
        ElseIf e.CommandText.ToLower() = "changemht" Then
            Response.Redirect(RemoveSessionValues())
        End If
    End Sub

    Private Function RemoveSessionValues() As String
        Dim ret, urlparams As String
        Dim skin, wh As String ', mheid, mhtype, terminaltype As String
        skin = Session("Skin")
        wh = Session("WH")
        'mheid = Session("MHEID")
        'mhtype = Session("MHTYPE")
        'terminaltype = Session("TERMINALTYPE")
        Session.Remove("MHEID")
        Session.Remove("MHTYPE")
        Session.Remove("TERMINALTYPE")
        urlparams = "?"
        If skin <> String.Empty Then
            urlparams = urlparams & String.Format("Skin={0}&", skin)
        End If
        If wh <> String.Empty Then
            urlparams = urlparams & String.Format("WH={0}&", wh)
        End If
        'If mheid <> String.Empty Then
        '    urlparams = urlparams & String.Format("MHEID={0}&", mheid)
        'End If
        'If mhtype <> String.Empty Then
        '    urlparams = urlparams & String.Format("MHTYPE={0}&", mhtype)
        'End If
        'If terminaltype <> String.Empty Then
        '    urlparams = urlparams & String.Format("TERMINALTYPE={0}&", terminaltype)
        'End If
        ret = Made4Net.Shared.Web.MapVirtualPath("m4nScreens/Login.aspx") & urlparams.TrimEnd("&".ToCharArray())
        Return ret
    End Function
    Private Function SetMHEID() As Boolean
        If Session("TERMINALTYPE") = "" Then
            If Request.QueryString("TERMINALTYPE") = "" Then
                Session("TERMINALTYPE") = ""
            Else
                Session("TERMINALTYPE") = Request.QueryString("TERMINALTYPE")
            End If
        End If
        Dim MoveToMHType As Boolean
        If Session("MHEID") = "" Then
            If Request.QueryString("MHEID") = "" Then
                Session("MHEID") = ""
                MoveToMHType = True
            Else
                Session("MHEID") = Request.QueryString("MHEID")
                MoveToMHType = False
                SetMHType(Session("MHEID"))
                Return MoveToMHType
            End If
        Else
            MoveToMHType = False
            SetMHType(Session("MHEID"))
            Return MoveToMHType
        End If
        If Session("MHType") = "" Then
            If Request.QueryString("MHTYPE") = "" Then
                Session("MHType") = ""
                MoveToMHType = True
            Else
                Session("MHType") = Request.QueryString("MHTYPE")
                MoveToMHType = False
            End If
        Else
            MoveToMHType = False
        End If
        Return MoveToMHType
    End Function

    Private Sub SetMHType(ByVal MHEID As String)
        Dim mhSQL As String = "SELECT isnull(LABELPRINTER,'') as LABELPRINTER, isnull(MHETYPE,'') as MHETYPE  FROM MHE WHERE MHEID='" & MHEID & "'"
        Dim dtMH As New DataTable
        Made4Net.DataAccess.DataInterface.FillDataset(mhSQL, dtMH)
        If dtMH.Rows.Count = 1 Then
            Session("MHType") = dtMH.Rows(0)("MHETYPE")
            Session("MHEIDLabelPrinter") = dtMH.Rows(0)("LABELPRINTER")
        Else
            Throw New Made4Net.Shared.M4NException(New Exception, "MHE ID not found", "MHE ID not found")
        End If
    End Sub

End Class
