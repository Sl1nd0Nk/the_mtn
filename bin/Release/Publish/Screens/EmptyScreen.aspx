﻿<%@ Register TagPrefix="cc1" Namespace="WMS.MobileWebApp.WebCtrls" Assembly="WMS.MobileWebApp" %>
<%@ Register TagPrefix="cc2" Namespace="Made4Net.Mobile.WebCtrls" Assembly="Made4Net.Mobile" %>
<%@ Register TagPrefix="cc3" Namespace="Made4Net.WebControls" Assembly="Made4Net.WebControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="EmptyScreen.aspx.vb" Inherits="WMS.MobileWebApp.EmptyScreen" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CLD1</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<div align="center" ><cc1:screen id="Screen1" runat="server" title="Empty Screen" ScreenID="emptyscreen" Visible="True" EnableTheming="False"></cc1:screen></div>
            <div align="center" ><br />
                <asp:Table ID="Table1" runat="server" Width="100%" style="margin-bottom: 0px">
                    <asp:TableRow>
                        <asp:TableCell>
                            <div align="left" ><asp:Label ID="lblUser" runat="server" Text=""></asp:Label></div>
                        </asp:TableCell>
                        <asp:TableCell>
                            <div align="left" ><asp:Label ID="Label1" runat="server" Text=""></asp:Label></div>
                        </asp:TableCell>
                        <asp:TableCell>
                            <div align="right" ><asp:Button ID="Logout" runat="server" OnClick="Logout_Click" Text="logout" Font-Underline="True" Font-Italic="True" BorderStyle="None" CausesValidation="False" /></div>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                
                <asp:panel runat="server">
                    <hr />
                    <asp:Label ID="PackStn" runat="server" Text="Pack Station 1" Font-Size="X-Large" ForeColor="#FF3300"></asp:Label>
                    <br /><br />
                    <asp:panel runat="server" ID="ScannedTotePanel" Visible="False">
                        <asp:Label ID="lblScannedTote" runat="server" Text="Scanned ToteID:" Font-Names="Calibri" Font-Size="Medium" ForeColor="#006699" Visible="true"></asp:Label>
                        &nbsp;&nbsp;
                        <asp:Label ID="ScannedTote" runat="server" Text="0000101" Font-Names="Calibri" Font-Size="Medium" ForeColor="#006699"></asp:Label>
                        <br /><br />
                    </asp:panel>
                    <asp:panel runat="server" ID="AttribPanel" Visible="False">
                    </asp:panel>
                    <asp:panel runat="server" ID="InputPanel" Visible="False">
                        <asp:TextBox ID="Input" runat="server" Visible="True"></asp:TextBox>
                        <br />
                        <asp:Label ID="lblInput" runat="server" Visible="True" Font-Names="Calibri" Font-Size="Medium"></asp:Label>
                    </asp:panel>
                </asp:panel>
                <br /><br />
                <asp:panel runat="server">
                    <asp:panel runat="server" ID="CurrentOrderPanel" Visible="False">
                        <asp:Label ID="lblCurrentOrder" runat="server" Text="OrderID:" Font-Names="Calibri" Font-Size="Medium" ForeColor="#006699" Visible="True"></asp:Label>
                        &nbsp;&nbsp;
                        <asp:Label ID="CurrentOrder" runat="server" Text="Ord123" Font-Names="Calibri" Font-Size="Medium" ForeColor="#006699"></asp:Label>
                    </asp:panel>
                    <br />
                    <div align="center" ><asp:Table ID="InfoTable" runat="server" Width="100%" style="margin-bottom: 0px" Visible="False"></asp:Table></div>                    
                </asp:panel>
            </div>
		</form>
	</body>
</HTML>

