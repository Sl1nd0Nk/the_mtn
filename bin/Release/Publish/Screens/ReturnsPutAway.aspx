﻿<%@ Register TagPrefix="cc1" Namespace="WMS.MobileWebApp.WebCtrls" Assembly="WMS.MobileWebApp" %>
<%@ Register TagPrefix="cc2" Namespace="Made4Net.Mobile.WebCtrls" Assembly="Made4Net.Mobile" %>
<%@ Register TagPrefix="cc3" Namespace="Made4Net.WebControls" Assembly="Made4Net.WebControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Codebehind="ReturnsPutAway.aspx.vb" Inherits="WMS.MobileWebApp.ReturnsPutAway" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CLD1</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
	</HEAD>
	<body bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<form id="Form1" method="post" runat="server">
			<div align="center" ><cc1:screen id="Screen1" runat="server" title="RETURNS PUTAWAY" ScreenID="ReturnsPutAway" Visible="True" EnableTheming="True"></cc1:screen></div>
			<DIV align="center" width="100%"><cc2:dataobject id="DO1" runat="server" LeftButtonText="Next" CenterButtonText="Menu" RightButtonText="Deposit" title="Returns PutAway" CenterLeftButtonText="Problem"></cc2:dataobject>
                <asp:Label id="Timeout" runat="server"></asp:Label>
			</DIV>
		</form>
	</body>
    <script type="text/javascript">
        function PageReload() {
            var StrTimeOut = document.getElementById('<%= Timeout.ClientID %>').innerText;
            document.getElementById('<%= Timeout.ClientID %>').style.visibility = "hidden";

            if (StrTimeOut) {
                var TimeOut = parseInt(StrTimeOut);

                setTimeout(function () {
                    location.reload();
                }, TimeOut);

                console.log(TimeOut);
            }
        }

        PageReload();
    </script>
</HTML>
